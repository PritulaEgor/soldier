﻿using System;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.DAL.DAL.Models;

namespace Soldier.Core.Managers
{
    public class PersonalAccessTokenManager
    {
        protected readonly IGenericRepository<PersonalAccessToken> personalAccessTokenRepository;

        public PersonalAccessTokenManager(IGenericRepository<PersonalAccessToken> personalAccessTokenRepository)
        {
            this.personalAccessTokenRepository = personalAccessTokenRepository;
        }

        public virtual void AddToken(BaseAppUser user, string token)
        {
            if (IsTokenExistForUser(user.Id))
            {
                RevokeTokenForUser(user);
            }
            CreatePersonalAccessToken(user, token);
        }

        public virtual bool IsTokenValid(Guid userId, string token)
        {
            return personalAccessTokenRepository.Any(pat => pat.UserId == userId && pat.Token == token);
        }
        public virtual bool IsTokenExistForUser(Guid userId)
        {
            return personalAccessTokenRepository.Any(pat => pat.UserId == userId);
        }

        public virtual void RevokeTokenForUser(BaseAppUser user)
        {
            RevokeTokenForUser(user.Id);
        }

        public virtual void RevokeTokenForUser(Guid userId)
        {
            if (personalAccessTokenRepository.Any(pat => pat.UserId == userId))
            {
                personalAccessTokenRepository.Remove(pat => pat.UserId == userId);
            }
        }

        private void CreatePersonalAccessToken(BaseAppUser user, string token)
        {
            personalAccessTokenRepository.Create(new PersonalAccessToken
            {
                User = user,
                UserId = user.Id,
                Token = token
            });
        }
    }
}
