﻿using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.DAL.DAL.Models;
using System;

namespace Soldier.Core.Services
{
    public class UserTypeService : IUserTypeService
    {
        protected readonly IGenericRepository<UserType> _typeRepository;
        protected readonly IUnitOfWorkFactory _uofFactory;

        public UserTypeService(IGenericRepository<UserType> typeRepository, IUnitOfWorkFactory uofFactory)
        {
            _typeRepository = typeRepository;
            _uofFactory = uofFactory;
        }

        public void CreateRelation(UserTypes userType, string passportNumber)
        {
            var type = new UserType
            {
                Id = Guid.NewGuid(),
                Type = userType,
                PassportNumber = passportNumber
            };

            using (_uofFactory.Create())
            {
                _typeRepository.Create(type);
            }
        }
    }
}
