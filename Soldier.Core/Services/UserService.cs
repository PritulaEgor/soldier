﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.DAL.DAL.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Soldier.Core.Services
{
    public class UserService : IUserService
    {
        protected readonly IGenericRepository<Employee> _employeeRepository;
        protected readonly IGenericRepository<Civilian> _civillianRepository;
        protected readonly IGenericRepository<Identity> _identityRepository;
        protected readonly IGenericRepository<UserType> _typeRepository;
        protected readonly IUnitOfWorkFactory _uofFactory;
        protected readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(
            IGenericRepository<Employee> employeeRepository,
            IGenericRepository<Civilian> civillianRepository,
            IGenericRepository<Identity> identityRepository,
            IGenericRepository<UserType> typeRepository,
            IUnitOfWorkFactory uofFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            _employeeRepository = employeeRepository;
            _civillianRepository = civillianRepository;
            _identityRepository = identityRepository;
            _typeRepository = typeRepository;
            _uofFactory = uofFactory;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Identity> GetUserIdentityByCredentials(string passportNumber, string password)
        {
            if (string.IsNullOrEmpty(passportNumber) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = await _identityRepository.GetBy(employee => employee.PassportNumber == passportNumber && employee.PasswordSalt == password)
                                          .FirstOrDefaultAsync();

            return user;
        }

        public async Task<Identity> GetUserIdentityByPassport(string passportNumber)
        {
            if (string.IsNullOrEmpty(passportNumber))
            {
                return null;
            }

            var user = await _identityRepository.GetBy(employee => employee.PassportNumber == passportNumber)
                                          .FirstOrDefaultAsync();

            return user;
        }

        public void Update(Identity identity)
        {
            if (identity is null)
            {
                throw new NotImplementedException();
            }

            using (_uofFactory.Create())
            {
                _identityRepository.Update(identity);
            }
        }

        public Identity Create(Identity identity)
        {
            if (identity is null)
            {
                throw new NotImplementedException();
            }

            using (_uofFactory.Create())
            {
              return _identityRepository.Create(identity);
            }
        }

        public async Task<UserTypes> GetUserType(string userPassportValue)
        {
            try
            {
                var userType = await _typeRepository.GetBy(type => type.PassportNumber == userPassportValue).FirstOrDefaultAsync();

                return userType is null ? UserTypes.Guest : userType.Type;
            }
            catch(Exception ex)
            {
                return UserTypes.Guest;
            }
        }

        public async Task<bool> ChangePasswordAsync(string oldPass, string newPass)
        {
            var passportNumber = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name).Value;

            var identity = await _identityRepository.GetBy(employee => employee.PassportNumber == passportNumber)
                                          .FirstOrDefaultAsync();

            if(identity.PasswordSalt.Equals(oldPass))
            {
                identity.PasswordSalt = newPass;

                using (_uofFactory.Create())
                {
                    _identityRepository.Update(identity);
                }

                return true;
            }

            return false;
        }
    }
}
