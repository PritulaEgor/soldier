﻿using Soldier.Common.BaseIdentity.Services.Contracts;
using Soldier.Common.BaseIdentity.ViewModels;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.Repositories.Soldier;

namespace Soldier.Core.Services
{
    public class UserProfileService : IUserProfileService
    {
        protected readonly IGenericRepository<BaseAppUser> _userRepository;

        public UserProfileService(IGenericRepository<BaseAppUser> userRepository)
        {
            _userRepository = userRepository;
        }

        public void Update(UpdateUserProfileModel model)
        {

        }
    }
}
