﻿using Microsoft.AspNetCore.Identity;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Core.Managers;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Soldier.Core.Services
{
    public class PersonalAccessTokenService : IPersonalAccessTokenService
    {
        protected readonly UserManager<BaseAppUser> _userManager;
        protected readonly PersonalAccessTokenManager _patManager;
        protected readonly ICurrentUserService<BaseAppUser> _currentUserService;

        public PersonalAccessTokenService(
            UserManager<BaseAppUser> userManager,
            PersonalAccessTokenManager patManager,
            ICurrentUserService<BaseAppUser> currentUserService)
        {
            this._userManager = userManager;
            this._patManager = patManager;
            this._currentUserService = currentUserService;
        }

        public async Task<ClaimsPrincipal> ValidateToken(string userEmail, string personalAccessToken)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);

            if (_patManager.IsTokenValid(user.Id, personalAccessToken))
            {
                var claims = await GetClaimsForUser(user);
                return new ClaimsPrincipal(new ClaimsIdentity(claims, "Basic"));
            }
            //throw new UserPersonalTokenNotValidException();
            throw new NotImplementedException();
        }

        public string CreateToken()
        {
            var user = _currentUserService.GetActualUser("");
            var token = _userManager.GenerateNewAuthenticatorKey();
            _patManager.AddToken(user, token);
            return token;
        }

        public async Task RevokeToken(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            _patManager.RevokeTokenForUser(user);
        }

        private async Task<Claim[]> GetClaimsForUser(BaseAppUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            return new[]
            {
                new Claim(
                    ClaimTypes.NameIdentifier,
                    user.Id.ToString(),
                    ClaimValueTypes.String),
                new Claim(
                    ClaimTypes.Name,
                    user.Id.ToString(),
                    ClaimValueTypes.String),
                new Claim(
                    ClaimTypes.Role,
                    roles.FirstOrDefault(),
                    ClaimValueTypes.String)
            };
        }
    }
}
