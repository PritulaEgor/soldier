﻿using Soldier.Common.Core.DataProviders;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Web.DTO;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Enums;
using Soldier.DAL.DAL.Models;
using Soldier.Shared.Helpers.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Core.DataProviders
{
    public class EnlistmentDataProvider : BaseDataProvider<Enlistment, EnlistmentViewModel>, IEnlistmentDataProvider
    {
        private readonly ICivilianDataProvider _civilianDataProvider;
        private readonly ISubpoenaDataProvider _subpoenaDataProvider;

        public EnlistmentDataProvider(IGenericRepository<Enlistment> repository,
            IUnitOfWorkFactory uowFactory,
            ICivilianDataProvider civilianDataProvider,
            ISubpoenaDataProvider subpoenaDataProvider) : base(repository, uowFactory)
        {
            _civilianDataProvider = civilianDataProvider;
            _subpoenaDataProvider = subpoenaDataProvider;
        }

        public override List<EnlistmentViewModel> GetByFilter(string value)
        {
            return repository.GetBy(GetFilterExpression(value))
                .Select(c => repository.Mapper.Map<EnlistmentViewModel>(c))
                .ToList();
        }

        public override EnlistmentViewModel GetById(Guid id)
        {
            var entity = repository.GetBy(e => e.Id == id).FirstOrDefault();
            return repository.Mapper.Map<EnlistmentViewModel>(entity);
        }

        protected override Expression<Func<Enlistment, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<Enlistment, bool>> GetFilterExpression(string filterBy)
        {
            if (string.IsNullOrEmpty(filterBy))
            {
                return c => true;
            }
            return c => true;
        }

        public int GetCount()
        {
            return repository.Count();
        }

        public EnlistmentStatisticsDTO GetStatistics(Guid id)
        {
            var allSubps = _subpoenaDataProvider.GetByEnlistmentId(id);

            var deviators = allSubps.Where(subpoena => subpoena.Status == (int)SubpoenaStates.Overdue).ToList();

            var awaiting = allSubps.Where(subpoena => subpoena.Status == (int)SubpoenaStates.Received || subpoena.Status == (int)SubpoenaStates.Sended).ToList();

            var arrived = allSubps.Where(subpoena => subpoena.Status == (int)SubpoenaStates.Realized).ToList();

            var rejected = allSubps.Where(subpoena => subpoena.Status == (int)SubpoenaStates.Cancelled).ToList();

            var allCount = _civilianDataProvider.GetByEnlistmentId(allSubps).Count;
            var arrivedEnt = _civilianDataProvider.GetByEnlistmentId(arrived);
            var arrivingAwaitingEnt = _civilianDataProvider.GetByEnlistmentId(awaiting);
            var notValidEnt = _civilianDataProvider.GetByEnlistmentId(rejected);
            var deviatorsEnt = _civilianDataProvider.GetByEnlistmentId(deviators);

            var result = new EnlistmentStatisticsDTO
            {
                Arrived = arrivedEnt is null ? 0 : arrivedEnt.Count,
                ArrivingAwaiting = arrivingAwaitingEnt is null ? 0 : arrivingAwaitingEnt.Count,
                NotValid = notValidEnt is null ? 0 : notValidEnt.Count,
                ConscriptsCount = allCount,
                DeviantsPercentage = allCount != 0 && deviatorsEnt != null ? deviatorsEnt.Count / allCount * 100 : 0,
            };

            return result;
        }

        protected override Func<IQueryable<Enlistment>, IQueryable<Enlistment>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {
            var sortPair = GetSortOption(orderBy);
            if (sortPair == null)
            {
                return null;
            }

            var order = sortPair.Value.Value;
            var columnName = sortPair.Value.Key.ToLowerInvariant();

            return EnlistmentSortingHelper.EnlistmentSortingMapping.ContainsKey(columnName)
                ? SortExpression(EnlistmentSortingHelper.EnlistmentSortingMapping[columnName], order)
                : null;
        }
    }
}
