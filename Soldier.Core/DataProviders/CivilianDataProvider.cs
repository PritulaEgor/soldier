﻿using Soldier.Common.Core.DataProviders;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Shared.Helpers.Sorting;
using Soldier.Common.Core.ReportModels;
using AutoMapper;

namespace Soldier.Core.DataProviders
{
    public class CivilianDataProvider : BaseDataProvider<Civilian, UserViewModel>, ICivilianDataProvider
    {
        private readonly IMedicalCardDataProvider _medicalCardDataProvider;
        private readonly IUserService _userIdentityService;
        private readonly IUserTypeService _userTypeService;
        private readonly IMilitaryIdentificatorDataProvider _militaryIdDataProvider;
        private readonly IMapper _mapper;

        public CivilianDataProvider(IGenericRepository<Civilian> repository,
            IUnitOfWorkFactory uowFactory,
            IMedicalCardDataProvider mCardProvider,
            IUserService userIdentityService,
            IUserTypeService userTypeService,
            IMilitaryIdentificatorDataProvider militaryIdDataProvider,
            IMapper mapper) : base(repository, uowFactory)
        {
            _medicalCardDataProvider = mCardProvider;
            _userIdentityService = userIdentityService;
            _userTypeService = userTypeService;
            _militaryIdDataProvider = militaryIdDataProvider;
            _mapper = mapper;
        }

        public List<UserReportModel> GetForReport()
        {
            var civilians = repository.Get().ToList();

            List<UserReportModel> forReport = new List<UserReportModel>();

            foreach (var civilian in civilians)
            {
                forReport.Add(_mapper.Map<UserReportModel>(civilian));
            }

            return forReport;
        }

        public List<CivilianReportModel> GetForUniqueReport()
        {
            var civilians = repository.Get().ToList();

            List<CivilianReportModel> forReport = new List<CivilianReportModel>();

            foreach (var civilian in civilians)
            {
                forReport.Add(_mapper.Map<CivilianReportModel>(civilian));
            }

            return forReport;
        }

        public MilitaryIdentificatorViewModel GetMilitaryIdentificator(Guid id)
        {
            if (id == Guid.Empty)
            {
                return null;
            }

            var militaryIdentificator = _militaryIdDataProvider.GetById(id);

            return militaryIdentificator;
        }

        public int UpdateValidityGroup(Guid civilianId, int validityGroup)
        {
            try
            {
                var medCardId = GetById(civilianId).MedicalCardID;

                var medicalCard = _medicalCardDataProvider.GetById(medCardId);

                medicalCard.ValidityGroup = validityGroup;

                _medicalCardDataProvider.Update(medicalCard);

                return validityGroup;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public List<UserViewModel> GetByEnlistmentId(List<SubpoenaViewModel> subpoenas)
        {
            if (subpoenas == null || subpoenas.Count == 0)
            {
                return null;
            }

            var conscripts = new List<UserViewModel>();

            foreach (var subpoena in subpoenas)
            {
                var conscript = GetById(subpoena.RecipientID);

                conscripts.Add(conscript);
            }

            return conscripts;
        }

        //public CompleteCivilianInfoViewModel GetForProfile(Guid id)
        //{

        //}

        public override UserViewModel Create(UserViewModel model)
        {
            var medicalCard = new MedicalCardViewModel()
            {
                Id = Guid.NewGuid(),
                ValidityGroup = 0,
                Issues = new List<IssueViewModel>()
                {
                    new IssueViewModel()
                    {
                         Description = "t",
                         DoctorData = "d",
                         Name = "n"
                    }
                },
                MedicalExaminations = new List<MedicalExaminationViewModel>()
                {
                    new MedicalExaminationViewModel()
                    {
                         ExaminationResult = "res"
                    }
                }
            };

            var newMedicalCard = _medicalCardDataProvider.Create(medicalCard);

            if (newMedicalCard is null)
            {
                return null;
            }

            model.MedicalCardID = newMedicalCard.Id;

            var identity = new Identity
            {
                Id = Guid.NewGuid(),
                PassportNumber = model.PassportNumber,
                PasswordSalt = model.TemporaryPassword,
                Role = model.Role
            };

            var newIdentity = _userIdentityService.Create(identity);

            if (newIdentity is null)
            {
                return null;
            }

            model.IdentityId = newIdentity.Id;

            var militaryId = new MilitaryIdentificatorViewModel
            {
                Id = Guid.NewGuid()
            };

            var newMilitaryId = _militaryIdDataProvider.Create(militaryId);

            if (newMilitaryId is null)
            {
                return null;
            }

            model.MilitaryID = newMilitaryId.Id;

            _userTypeService.CreateRelation(model.UserType, model.PassportNumber);

            model.Id = Guid.NewGuid();

            return base.Create(model);
        }

        public override List<UserViewModel> GetByFilter(string value)
        {
            return repository.GetBy(GetFilterExpression(value))
                .Select(c => repository.Mapper.Map<UserViewModel>(c))
                .ToList();
        }

        public int GetCount()
        {
            return repository.Count();
        }

        protected override Expression<Func<Civilian, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<Civilian, bool>> GetFilterExpression(string filterBy)
        {
            if (string.IsNullOrEmpty(filterBy))
            {
                return c => true;
            }
            return c =>
                c.Surname.Contains(filterBy) ||
                c.Name.Contains(filterBy) ||
                c.Patronymic.Contains(filterBy);
        }

        protected override Func<IQueryable<Civilian>, IQueryable<Civilian>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {
            var sortPair = GetSortOption(orderBy);
            if (sortPair == null)
            {
                return null;
            }

            var order = sortPair.Value.Value;
            var columnName = sortPair.Value.Key.ToLowerInvariant();

            return CivilianSortingHelper.CivilianSortingMapping.ContainsKey(columnName)
                ? SortExpression(CivilianSortingHelper.CivilianSortingMapping[columnName], order)
                : null;
        }

        protected override void ValidateModel(UserViewModel model)
        {
            if (string.IsNullOrEmpty(model.PassportIdentificationNumber)
                || model.MedicalCardID == Guid.Empty
                || model.Id == Guid.Empty
                || string.IsNullOrEmpty(model.PassportNumber)
                || string.IsNullOrEmpty(model.Name)
                || string.IsNullOrEmpty(model.Surname)
                || string.IsNullOrEmpty(model.Patronymic)
                || string.IsNullOrEmpty(model.PlaceOfResidence)
                || model.CreatorId == Guid.Empty)
            {
                throw new Exception("Civilian model is not valid");
            }
        }
    }
}
