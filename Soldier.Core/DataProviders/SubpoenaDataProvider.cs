﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Soldier.Common.Core.DataProviders;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.DataProviders.Models;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Web.DTO;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Enums;
using Soldier.DAL.DAL.Models;
using Soldier.Shared.Helpers.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Core.DataProviders
{
    public class SubpoenaDataProvider : BaseDataProvider<Subpoena, SubpoenaViewModel>, ISubpoenaDataProvider
    {
        private readonly IEmployeeDataProvider _employeeDataProvider;
        private readonly ICivilianDataProvider _civilianDataProvider;
        private readonly IMapper _mapper;

        public SubpoenaDataProvider(IGenericRepository<Subpoena> repository,
            IUnitOfWorkFactory uowFactory,
            IEmployeeDataProvider employeeDataProvider,
            ICivilianDataProvider civilianDataProvider,
            IMapper mapper) : base(repository, uowFactory)
        {
            _employeeDataProvider = employeeDataProvider;
            _mapper = mapper;
            _civilianDataProvider = civilianDataProvider;
        }

        public override List<SubpoenaViewModel> GetByFilter(string value)
        {
            UpdateOverueSubpoenas();

            return repository.GetBy(GetFilterExpression(value))
                .Select(c => repository.Mapper.Map<SubpoenaViewModel>(c))
                .ToList();
        }

        public List<SubpoenaViewModel> SaveSubpoenaBatch(DistributionDTO distribution)
        {
            UpdateOverueSubpoenas();

            var conscriptsList = new List<Civilian>();

            foreach (var cons in distribution.Conscripts)
            {
                conscriptsList.Add(_mapper.Map<Civilian>(cons));
            }

            var signaturer = _mapper.Map<Employee>(distribution.Signaturer);
            var enlistment = _mapper.Map<Enlistment>(distribution.Enlistment);

            var subpoenasList = new List<SubpoenaViewModel>();

            var daysActive = 0;

            if (enlistment.StartDate < DateTime.Now)
            {
                daysActive = (int)((enlistment.EndDate - DateTime.Now).TotalDays);
            }
            else
            {
                daysActive = (int)((enlistment.EndDate - enlistment.StartDate).TotalDays);
            }

            foreach (var conscript in conscriptsList)
            {
                var newSubpoena = new SubpoenaViewModel
                {
                    RecipientID = conscript.Id,
                    Sended = DateTime.Now.ToString(),
                    SignaturerID = signaturer.Id,
                    SoldierDateOfArriving = enlistment.StartDate < DateTime.Now ? DateTime.Now.AddDays(2).ToString() : enlistment.StartDate.ToString(),
                    Status = 0,
                    DaysActive = daysActive,
                    SignaturerName = signaturer.Surname,
                    EnlistmentID = enlistment.Id
                };

                var createdEntity = base.Create(newSubpoena);

                subpoenasList.Add(createdEntity);
            }

            return subpoenasList;
        }

        public bool IsArrived(Guid userId, Guid enlistmentId)
        {
            UpdateOverueSubpoenas();

            var subpoenaEntity = repository.Get()
                    .FirstOrDefault(subpoena => subpoena.EnlistmentId == enlistmentId && subpoena.RecipientID == userId);

            var subpoena = _mapper.Map<SubpoenaViewModel>(subpoenaEntity);

            return subpoena.Status == (int)SubpoenaStates.Realized;
        }

        public int UpdateSubpoenaStatus(Guid userId, int newStatus, Guid enlistmentId)
        {
            try
            {
                var subpoenaEntity = repository.Get()
                    .FirstOrDefault(subpoena => subpoena.EnlistmentId == enlistmentId && subpoena.RecipientID == userId);

                var subpoena = _mapper.Map<SubpoenaViewModel>(subpoenaEntity);

                subpoena.Status = newStatus;

                Update(subpoena);

                UpdateOverueSubpoenas();

                return newStatus;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public void SetReceivedStatus(SubpoenaViewModel subpoena)
        {
            var subpoenaEntity = GetById(subpoena.Id);

            subpoenaEntity.Status = (int)SubpoenaStates.Received;

            base.Update(subpoenaEntity);
        }

        public List<SubpoenaViewModel> GetByEnlistmentId(Guid enlistmentId)
        {
            UpdateOverueSubpoenas();

            if (enlistmentId == Guid.Empty)
            {
                return null;
            }

            return repository.Get()
                .Where(subpoena => subpoena.EnlistmentId == enlistmentId)
                .Select(c => repository.Mapper.Map<SubpoenaViewModel>(c))
                .ToList();
        }

        public Page<SubpoenaViewModel> GetByRecipientId(
            PageInfo pageInfo,
            Guid conscriptId)
        {
            UpdateOverueSubpoenas();

            var filter = GetFilterExpression(pageInfo.FilterBy);
            var entities = repository.GetPage(pageInfo.PageNumber,
                pageInfo.PageSize,
                e => e.RecipientID == conscriptId,
                null,
                GetSortExpression(pageInfo.OrderBy))
                .Select(e => repository.Mapper.Map<SubpoenaViewModel>(e))
                .ToList();

            for (int i = 0; i < entities.Count; i++)
            {
                var signaturer = _employeeDataProvider.GetById(entities[i].SignaturerID);

                entities[i].SignaturerName = signaturer.Surname + ' ' + signaturer.Name;
            }

            var page = new Page<SubpoenaViewModel>
            {
                Number = pageInfo.PageNumber,
                Size = pageInfo.PageSize,
                TotalCount = repository.Count(filter),
                Items = entities
            };

            return page;
        }

        public int GetCountByRecipientId(Guid conscriptId)
        {
            return repository.Count(e => e.RecipientID == conscriptId);
        }

        public override SubpoenaViewModel GetById(Guid id)
        {
            UpdateOverueSubpoenas();

            var entity = repository.GetBy(e => e.Id == id, subpoena => subpoena.Include(sub => sub.Recipient)).FirstOrDefault();
            return repository.Mapper.Map<SubpoenaViewModel>(entity);
        }

        protected override Expression<Func<Subpoena, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<Subpoena, bool>> GetFilterExpression(string filterBy)
        {
            if (string.IsNullOrEmpty(filterBy))
            {
                return c => true;
            }
            return c => true;
        }

        public int GetCount()
        {
            return repository.Count();
        }

        public List<DeviatorsReportModel> GetForDeviatorsReport()
        {
            UpdateOverueSubpoenas();

            var overueSubpoenas = repository
                .Get()
                .Where(subpoena => subpoena.Status == SubpoenaStates.Overdue)
                .Include(subpoena => subpoena.Enlistment)
                .Select(c => repository.Mapper.Map<SubpoenaViewModel>(c))
                .ToList();

            var reportEntities = new List<DeviatorsReportModel>();

            foreach (var sub in overueSubpoenas)
            {
                var deviator = _civilianDataProvider.GetById(sub.RecipientID);

                var entity = new DeviatorsReportModel
                {
                    CurrentAddress = deviator.CurrentAddress,
                    DaysActive = sub.DaysActive,
                    Email = deviator.Email,
                    EnlistmentEndDate = Convert.ToDateTime(sub.Enlistment.EndDate),
                    EnlistmentStartDate = Convert.ToDateTime(sub.Enlistment.StartDate),
                    Name = deviator.Name,
                    PassportNumber = deviator.PassportNumber,
                    Patronymic = deviator.Patronymic,
                    PhoneNumber = deviator.PhoneNumber,
                    PlaceOfResidence = deviator.PlaceOfResidence,
                    Sended = Convert.ToDateTime(sub.Sended),
                    SignaturerName = sub.SignaturerName,
                    SoldierDateOfArriving = Convert.ToDateTime(sub.SoldierDateOfArriving),
                    Surname = deviator.Surname
                };

                reportEntities.Add(entity);
            }

            return reportEntities;
        }

        private void UpdateOverueSubpoenas()
        {
            var overdueSubpoenas = repository
                .Get()
                .Where(subpoena => subpoena.Enlistment.EndDate < DateTime.Now)
                .Include(subpoena => subpoena.Enlistment)
                .Select(c => repository.Mapper.Map<SubpoenaViewModel>(c))
                .ToList();

            foreach (var sub in overdueSubpoenas)
            {
                sub.Status = (int)SubpoenaStates.Overdue;

                this.Update(sub);
            }

            SetWrongOverdue();
        }

        private void SetWrongOverdue()
        {
            var overdueSubpoenas = repository
                .Get()
                .Where(subpoena => subpoena.Enlistment.EndDate > DateTime.Now && subpoena.Status == SubpoenaStates.Overdue)
                .Include(subpoena => subpoena.Enlistment)
                .Select(c => repository.Mapper.Map<SubpoenaViewModel>(c))
                .ToList();

            foreach (var sub in overdueSubpoenas)
            {
                sub.Status = (int)SubpoenaStates.Received;

                this.Update(sub);
            }
        }

        protected override Func<IQueryable<Subpoena>, IQueryable<Subpoena>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {
            var sortPair = GetSortOption(orderBy);
            if (sortPair == null)
            {
                return null;
            }

            var order = sortPair.Value.Value;
            var columnName = sortPair.Value.Key.ToLowerInvariant();

            return SubpoenaSortingHelper.SubpoenaSortingMapping.ContainsKey(columnName)
                ? SortExpression(SubpoenaSortingHelper.SubpoenaSortingMapping[columnName], order)
                : null;
        }
    }
}
