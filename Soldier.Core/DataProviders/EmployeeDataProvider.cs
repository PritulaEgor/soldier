﻿using AutoMapper;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DataProviders;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using Soldier.Shared.Helpers.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Core.DataProviders
{
    public class EmployeeDataProvider : BaseDataProvider<Employee, UserViewModel>, IEmployeeDataProvider
    {
        private readonly IUserService _userIdentityService;
        private readonly IUserTypeService _userTypeService;
        private readonly IMapper _mapper;

        public EmployeeDataProvider(IGenericRepository<Employee> repository,
            IUnitOfWorkFactory uowFactory,
            IUserService userIdentityService,
            IUserTypeService userTypeService,
            IMapper mapper) : base(repository, uowFactory)
        {
            _userIdentityService = userIdentityService;
            _userTypeService = userTypeService;
            _mapper = mapper;       
        }

        public List<UserReportModel> GetForReport()
        {
            var employees = repository.Get().ToList();

            List<UserReportModel> forReport = new List<UserReportModel>();

            foreach (var employee in employees)
            {
                forReport.Add(_mapper.Map<UserReportModel>(employee));
            }

            return forReport;
        }

        public List<EmployeeReportModel> GetForUniqueReport()
        {
            var employees = repository.Get().ToList();

            List<EmployeeReportModel> forReport = new List<EmployeeReportModel>();

            foreach (var employee in employees)
            {
                forReport.Add(_mapper.Map<EmployeeReportModel>(employee));
            }

            return forReport;
        }
        public override UserViewModel Create(UserViewModel model)
        {
            var identity = new Identity
            {
                Id = Guid.NewGuid(),
                PassportNumber = model.PassportNumber,
                PasswordSalt = model.TemporaryPassword,
                Role = model.Role
            };

            var newIdentity = _userIdentityService.Create(identity);

            if (newIdentity is null)
            {
                return null;
            }

            model.IdentityId = newIdentity.Id;

            _userTypeService.CreateRelation(model.UserType, model.PassportNumber);

            model.Id = Guid.NewGuid();

            return base.Create(model);
        }

        public override List<UserViewModel> GetByFilter(string value)
        {
            return repository.GetBy(GetFilterExpression(value))
                .Select(c => repository.Mapper.Map<UserViewModel>(c))
                .ToList();
        }

        public int GetCount()
        {
            return repository.Count();
        }

        protected override Expression<Func<Employee, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<Employee, bool>> GetFilterExpression(string filterBy)
        {
            if (string.IsNullOrEmpty(filterBy))
            {
                return c => true;
            }
            return c =>
                c.Surname.Contains(filterBy) ||
                c.Name.Contains(filterBy) ||
                c.Patronymic.Contains(filterBy);
        }

        protected override Func<IQueryable<Employee>, IQueryable<Employee>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {
            var sortPair = GetSortOption(orderBy);
            if (sortPair == null)
            {
                return null;
            }

            var order = sortPair.Value.Value;
            var columnName = sortPair.Value.Key.ToLowerInvariant();

            return EmployeeSortingHelper.EmployeeSortingMapping.ContainsKey(columnName)
                ? SortExpression(EmployeeSortingHelper.EmployeeSortingMapping[columnName], order)
                : null;
        }

        protected override void ValidateModel(UserViewModel model)
        {
            if (string.IsNullOrEmpty(model.WorkStartDate)
                || string.IsNullOrEmpty(model.ServiceStartDate)
                || string.IsNullOrEmpty(model.Rank)
                || model.Id == Guid.Empty
                || string.IsNullOrEmpty(model.PassportNumber)
                || string.IsNullOrEmpty(model.Name)
                || string.IsNullOrEmpty(model.Surname)
                || string.IsNullOrEmpty(model.Patronymic)
                || string.IsNullOrEmpty(model.PlaceOfResidence)
                || model.CreatorId == Guid.Empty)
            {
                throw new Exception("Employee model is not valid");
            }
        }
    }
}
