﻿using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Core.DataProviders.Interfaces
{
    public interface IMedicalCardDataProvider : IDataProvider<MedicalCard, MedicalCardViewModel>
    {
        IssueViewModel AddIssue(IssueViewModel issue);

        MedicalExaminationViewModel AddExamination(MedicalExaminationViewModel exam);
    }
}
