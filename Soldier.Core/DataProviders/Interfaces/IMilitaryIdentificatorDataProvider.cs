﻿using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;

namespace Soldier.Core.DataProviders.Interfaces
{
    public interface IMilitaryIdentificatorDataProvider : IDataProvider<MilitaryIdentificator, MilitaryIdentificatorViewModel>
    {
    }
}
