﻿using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Web.DTO;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Core.DataProviders.Interfaces
{
    public interface IEnlistmentDataProvider : IDataProvider<Enlistment, EnlistmentViewModel>
    {
        int GetCount();

        EnlistmentStatisticsDTO GetStatistics(Guid id);
    }
}
