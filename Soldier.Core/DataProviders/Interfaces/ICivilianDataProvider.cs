﻿using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;

namespace Soldier.Core.DataProviders.Interfaces
{
    public interface ICivilianDataProvider : IDataProvider<Civilian, UserViewModel>
    {
        int GetCount();
        List<UserReportModel> GetForReport();
        List<CivilianReportModel> GetForUniqueReport();
        MilitaryIdentificatorViewModel GetMilitaryIdentificator(Guid id);
        List<UserViewModel> GetByEnlistmentId(List<SubpoenaViewModel> subpoenas);
        int UpdateValidityGroup(Guid civilianId, int validityGroup);
        //CompleteCivilianInfoViewModel GetForProfile(Guid id);
    }
}
