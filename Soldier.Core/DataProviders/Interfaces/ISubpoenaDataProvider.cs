﻿using Soldier.Common.Core.DataProviders.Models;
using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Web.DTO;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Core.DataProviders.Interfaces
{
    public interface ISubpoenaDataProvider : IDataProvider<Subpoena, SubpoenaViewModel>
    {
        int GetCount();

        Page<SubpoenaViewModel> GetByRecipientId(
            PageInfo pageInfo, 
            Guid conscriptId);

        int GetCountByRecipientId(Guid conscriptId);

        List<SubpoenaViewModel> SaveSubpoenaBatch(DistributionDTO distribution);

        void SetReceivedStatus(SubpoenaViewModel subpoena);

        List<SubpoenaViewModel> GetByEnlistmentId(Guid enlistmentId);

        List<DeviatorsReportModel> GetForDeviatorsReport();

        int UpdateSubpoenaStatus(Guid userId, int newStatus, Guid enlistmentId);

        bool IsArrived(Guid userI, Guid enlistmentId);
    }
}
