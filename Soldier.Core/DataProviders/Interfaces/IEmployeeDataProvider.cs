﻿using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System.Collections.Generic;

namespace Soldier.Core.DataProviders.Interfaces
{
    public interface IEmployeeDataProvider : IDataProvider<Employee, UserViewModel>
    {
        int GetCount();
        List<UserReportModel> GetForReport();
        List<EmployeeReportModel> GetForUniqueReport();
    }
}
