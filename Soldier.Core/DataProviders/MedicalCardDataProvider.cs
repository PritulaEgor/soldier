﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Soldier.Common.Core.DataProviders;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using Soldier.Shared.Helpers.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Core.DataProviders
{
    public class MedicalCardDataProvider : BaseDataProvider<MedicalCard, MedicalCardViewModel>, IMedicalCardDataProvider
    {
        private readonly IGenericRepository<Issue> _issueRepository;
        private readonly IGenericRepository<MedicalExamination> _examinationRepository;
        private readonly IMapper _mapper;

        public MedicalCardDataProvider(IGenericRepository<MedicalCard> repository,
            IUnitOfWorkFactory uowFactory,
            IGenericRepository<Issue> issueRepository,
            IGenericRepository<MedicalExamination> examinationRepository,
            IMapper mapper) : base(repository, uowFactory)
        {
            _issueRepository = issueRepository;
            _examinationRepository = examinationRepository;
            _mapper = mapper;
        }

        public IssueViewModel AddIssue(IssueViewModel issue)
        {
            try
            {
                using (uowFactory.Create())
                {
                    var created = _issueRepository.Create(_mapper.Map<Issue>(issue));

                    return _mapper.Map<IssueViewModel>(created);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public MedicalExaminationViewModel AddExamination(MedicalExaminationViewModel exam)
        {
            try
            {
                using (uowFactory.Create())
                {
                    var created = _examinationRepository.Create(_mapper.Map<MedicalExamination>(exam));

                    return _mapper.Map<MedicalExaminationViewModel>(created);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public override List<MedicalCardViewModel> GetByFilter(string value)
        {
            return repository.GetBy(GetFilterExpression(value))
                .Select(c => repository.Mapper.Map<MedicalCardViewModel>(c))
                .ToList();
        }

        public override MedicalCardViewModel GetById(Guid id)
        {
            var entity = repository.GetBy(e => e.Id == id, query => query.Include(card => card.Issues).Include(card => card.MedicalExaminations)).FirstOrDefault();
            return repository.Mapper.Map<MedicalCardViewModel>(entity);
        }

        protected override Expression<Func<MedicalCard, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<MedicalCard, bool>> GetFilterExpression(string filterBy)
        {
            if (string.IsNullOrEmpty(filterBy))
            {
                return c => true;
            }
            return c => c.ValidityGroup.ToString() == filterBy;
        }

        protected override Func<IQueryable<MedicalCard>, IQueryable<MedicalCard>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {

            var sortPair = GetSortOption(orderBy);
            if (sortPair == null)
            {
                return null;
            }

            var order = sortPair.Value.Value;
            var columnName = sortPair.Value.Key.ToLowerInvariant();

            return MedicalCardSortingHelper.MedicalCardSortingMapping.ContainsKey(columnName)
                ? SortExpression(MedicalCardSortingHelper.MedicalCardSortingMapping[columnName], order)
                : null;
        }
    }
}
