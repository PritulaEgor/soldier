﻿using Soldier.Common.Core.DataProviders;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Core.DataProviders
{
    public class MilitaryIdentificatorDataProvider : BaseDataProvider<MilitaryIdentificator, MilitaryIdentificatorViewModel>, IMilitaryIdentificatorDataProvider
    {
        public MilitaryIdentificatorDataProvider(IGenericRepository<MilitaryIdentificator> repository,
            IUnitOfWorkFactory uowFactory) : base(repository, uowFactory)
        {

        }

        public override List<MilitaryIdentificatorViewModel> GetByFilter(string value)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<MilitaryIdentificator, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<MilitaryIdentificator, bool>> GetFilterExpression(string filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Func<IQueryable<MilitaryIdentificator>, IQueryable<MilitaryIdentificator>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {
            throw new NotImplementedException();
        }
    }
}
