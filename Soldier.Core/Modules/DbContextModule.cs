﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.IoC.Soldier;
using Soldier.DAL.DAL;
using System;

namespace Soldier.Core.Modules
{
    public class DbContextModule : IModule
    {
        protected readonly IConfiguration configuration;

        public DbContextModule(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Load(IServiceCollection services)
        {
            services.AddDbContext<SoldierContext>(options =>
                options
                    //.UseSqlServer(configuration.GetConnectionString("DbConnectionString"))
                    .UseSqlServer("Data Source=./;Initial Catalog=Soldier;Trusted_Connection=true;Integrated security = true;MultipleActiveResultSets=true;", builder =>
                    {
                        builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null);
                    })
                );
            services.AddTransient<IDbContextProvider, SoldierContextProvider>();
        }
    }
}
