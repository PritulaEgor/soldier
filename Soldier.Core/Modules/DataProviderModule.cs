﻿using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.IoC.Soldier;
using Soldier.Core.DataProviders;
using Soldier.Core.DataProviders.Interfaces;

namespace Soldier.Core.Modules
{
    public class DataProviderModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<ICivilianDataProvider, CivilianDataProvider>();
            services.AddTransient<IEmployeeDataProvider, EmployeeDataProvider>();
            services.AddTransient<IMedicalCardDataProvider, MedicalCardDataProvider>();
            services.AddTransient<IMilitaryIdentificatorDataProvider, MilitaryIdentificatorDataProvider>();
            services.AddTransient<IEnlistmentDataProvider, EnlistmentDataProvider>();
            services.AddTransient<ISubpoenaDataProvider, SubpoenaDataProvider>();
        }
    }
}
