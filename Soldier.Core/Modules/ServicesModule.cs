﻿using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Core.Services;

namespace Soldier.Core.Modules
{
    public class ServicesModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserTypeService, UserTypeService>();
        }
    }
}
