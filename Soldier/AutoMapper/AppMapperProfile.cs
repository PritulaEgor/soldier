﻿using AutoMapper;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;

namespace Soldier.AutoMapper
{
    public class AppMapperProfile : Profile
    {
        public AppMapperProfile()
        {
            CreateMap<UserViewModel, Employee>()
                .ForMember("ServiceStartDate", map=>map.MapFrom(employee=> DateTime.Parse(employee.ServiceStartDate)))
                .ForMember("WorkStartDate", map => map.MapFrom(employee => DateTime.Parse(employee.WorkStartDate)))
                .ReverseMap();
            CreateMap<UserViewModel, Civilian>()
                .ReverseMap();
            CreateMap<MedicalCardViewModel, MedicalCard>()
                .ForMember("Issues", map=>map.MapFrom(card=>new List<Issue>()))
                .ForMember("MedicalExaminations", map=>map.MapFrom(card=>new List<MedicalExamination>()))
                .ReverseMap();
            CreateMap<MedicalExaminationViewModel, MedicalExamination>()
                .ReverseMap();
            CreateMap<IssueViewModel, Issue>()
                .ReverseMap();
            CreateMap<MilitaryIdentificatorViewModel, MilitaryIdentificator>()
                .ReverseMap();

            CreateMap<UserReportModel, Employee>()
                .ForMember("ServiceStartDate", map => map.MapFrom(employee => DateTime.Parse(employee.ServiceStartDate)))
                .ForMember("WorkStartDate", map => map.MapFrom(employee => DateTime.Parse(employee.WorkStartDate)))
                .ReverseMap();
            CreateMap<UserReportModel, Civilian>()
                .ReverseMap();

            CreateMap<EmployeeReportModel, Employee>()
                .ForMember("ServiceStartDate", map => map.MapFrom(employee => DateTime.Parse(employee.ServiceStartDate)))
                .ForMember("WorkStartDate", map => map.MapFrom(employee => DateTime.Parse(employee.WorkStartDate)))
                .ReverseMap();
            CreateMap<CivilianReportModel, Civilian>()
                .ReverseMap();

            CreateMap<EnlistmentViewModel, Enlistment>()
               .ForMember("StartDate", map => map.MapFrom(enlistment => DateTime.Parse(enlistment.StartDate)))
               .ForMember("EndDate", map => map.MapFrom(enlistment => DateTime.Parse(enlistment.EndDate)))
               .ReverseMap();

            CreateMap<SubpoenaViewModel, Subpoena>()
               .ForMember("Sended", map => map.MapFrom(subpoena => DateTime.Parse(subpoena.Sended)));

            CreateMap<Subpoena, SubpoenaViewModel>()
               .ForMember("SignaturerName", map => map.MapFrom(subpoena => subpoena.Signaturer.Surname + ' ' + subpoena.Signaturer.Name));
        }
    }
}
