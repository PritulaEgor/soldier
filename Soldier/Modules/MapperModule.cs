﻿using Microsoft.Extensions.DependencyInjection;
using Soldier.AutoMapper;
using Soldier.Common.Core.IoC.Soldier;
using AutoMapper;
namespace Soldier.Modules
{
    public class MapperModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AppMapperProfile));
        }
    }
}
