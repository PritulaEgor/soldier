using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Soldier.Common.BaseIdentity;
using Soldier.Common.BaseIdentity.Modules;
using Soldier.Common.Core.IoC;
using Soldier.Common.Core.Modules;
using Soldier.Common.Web;
using Soldier.Core.Modules;
using Soldier.DAL.DAL.Models;
using Soldier.Modules;

namespace Soldier
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            new ServiceCollectionBuilder(services)
                .WithCoreModules(Configuration)
                .RegisterModule(new DbContextModule(Configuration))
                .RegisterModule(new RepositoryModule())
                .RegisterModule(new CoreServicesModule())
                .RegisterModule(new AuthModule())
                .RegisterModule(new ServicesModule())
                .RegisterModule(new TokenHandlingModule())
                .RegisterModule(new UserManagementModule<Employee>())
                .RegisterModule(new UserManagementModule<Civilian>())
                .RegisterModule(new BaseWebModule())
                .RegisterModule(new MapperModule())
                .RegisterModule(new DataProviderModule())
                .RegisterModule(new ReportModule())
                .Build();

            services.AddControllersWithViews();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");

                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
