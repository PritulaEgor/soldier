﻿using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Core.Constants;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Core.DataProviders.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soldier.Controllers.Reports
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubpoenaReportController : ControllerBase
    {
        private readonly ISubpoenaDataProvider _subpoenaDataProvider;
        private readonly IReportService<DeviatorsReportModel> _deviatorsReportService;

        public SubpoenaReportController(ISubpoenaDataProvider subpoenaDataProvider,
             IReportService<DeviatorsReportModel> deviatorsReportService)
        {
            _subpoenaDataProvider = subpoenaDataProvider;
            _deviatorsReportService = deviatorsReportService;
        }

        [Route("deviators")]
        [HttpGet]
        public IActionResult DeviatorsReport()
        {
            var subpoenas = _subpoenaDataProvider.GetForDeviatorsReport();

            string fileName = $"Deviators_Report_{DateTime.Now:dd.MM}.xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            var file = _deviatorsReportService.GetReport(
                subpoenas,
                fileName,
                ReportFieldsNames.Deviators);

            return File(
                file,
                fileType,
                fileName);
        }
    }
}
