﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Core.Constants;
using Soldier.Common.Core.ReportModels;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Core.DataProviders.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soldier.Controllers.Reports
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserReportController : ControllerBase
    {
        private readonly ICivilianDataProvider _civilianDataProvider;
        private readonly IEmployeeDataProvider _employeeDataProvider;
        private readonly IReportService<UserReportModel> _allUsersReportService;
        private readonly IReportService<CivilianReportModel> _civiliansReportService;
        private readonly IReportService<EmployeeReportModel> _employeesReportService;

        public UserReportController(
            ICivilianDataProvider civilianDataProvider,
            IEmployeeDataProvider employeeDataProvider,
            IReportService<UserReportModel> allUsersReportService,
            IReportService<CivilianReportModel> civilianReportService,
            IReportService<EmployeeReportModel> employeesReportService)
        {
            _civilianDataProvider = civilianDataProvider;
            _employeeDataProvider = employeeDataProvider;
            _allUsersReportService = allUsersReportService;
            _civiliansReportService = civilianReportService;
            _employeesReportService = employeesReportService;
        }

        [Route("complete")]
        [HttpGet]
        public IActionResult CompleteReport()
        {
            var civilians = _civilianDataProvider.GetForReport();

            var employees = _employeeDataProvider.GetForReport();

            employees.AddRange(civilians);

            string fileName = $"User_Complete_Report_{DateTime.Now:dd.MM}.xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            var file = _allUsersReportService.GetReport(
                employees,
                fileName,
                ReportFieldsNames.CompleteUser);

            return File(
                file,
                fileType,
                fileName);
        }

        [Route("civilians")]
        [HttpGet]
        public IActionResult CiviliansReport()
        {
            var civilians = _civilianDataProvider.GetForUniqueReport();

            string fileName = $"Civilians_Report_{DateTime.Now:dd.MM}.xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            var file = _civiliansReportService.GetReport(
                civilians,
                fileName,
                ReportFieldsNames.Civilians);

            return File(
                file,
                fileType,
                fileName);
        }

        [Route("employees")]
        [HttpGet]
        public IActionResult EmployeesReport()
        {
            var employee = _employeeDataProvider.GetForUniqueReport();

            string fileName = $"Employees_Report_{DateTime.Now:dd.MM}.xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            var file = _employeesReportService.GetReport(
                employee,
                fileName,
                ReportFieldsNames.Employees);

            return File(
                file,
                fileType,
                fileName);
        }
    }
}
