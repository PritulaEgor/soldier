﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.BaseIdentity.Services.Interfaces;
using Soldier.Common.BaseIdentity.ViewModels;
using Soldier.Common.Core.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Soldier.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IUserService _userIdentityService;
        private readonly ITokenService _tokenService;

        public TokenController(IUserService userIdentityContext, ITokenService tokenService)
        {
            _userIdentityService = userIdentityContext;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("refresh")]
        public async Task<IActionResult> Refresh(TokenApiModel tokenApiModel)
        {
            if (tokenApiModel is null)
            {
                return BadRequest("Invalid client request");
            }

            string accessToken = tokenApiModel.AccessToken;
            string refreshToken = tokenApiModel.RefreshToken;

            var principal = _tokenService.GetPrincipalFromExpiredToken(accessToken);

            var passportNumber = principal.Identity.Name; //this is mapped to the Name claim by default

            var user = await _userIdentityService.GetUserIdentityByPassport(passportNumber);

            if (user is null
                || user.RefreshToken != refreshToken
                || user.RefreshTokenExpiryTime <= DateTime.Now)
            {
                return BadRequest("Invalid client request");
            }

            var newAccessToken = _tokenService.GenerateAccessToken(principal.Claims);
            var newRefreshToken = _tokenService.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;

            _userIdentityService.Update(user);

            return Ok(new AuthenticationResponse()
            {
                Token = newAccessToken,
                RefreshToken = newRefreshToken
            });
        }

        [HttpPost, Authorize]
        [Route("revoke")]
        public async Task<IActionResult> Revoke()
        {
            var passportNumber = User.Identity.Name;

            var user = await _userIdentityService.GetUserIdentityByPassport(passportNumber);

            if (user == null)
            { 
                return BadRequest(); 
            }

            user.RefreshToken = null;

            _userIdentityService.Update(user);

            return NoContent();
        }
    }
}
