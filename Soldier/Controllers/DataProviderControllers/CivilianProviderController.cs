﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Web.Controllers;
using Soldier.Common.Web.DTO;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using System;

namespace Soldier.Controllers.DataProviderControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CivilianProviderController : BaseDataProviderController<Civilian, UserViewModel>
    {
        private readonly ISubpoenaDataProvider _subpoenaDataProvider;

        public CivilianProviderController(ICivilianDataProvider civilianDataProvider,
            ISubpoenaDataProvider subpoenaDataProvider) : base(civilianDataProvider)
        {
            _subpoenaDataProvider = subpoenaDataProvider;
        }

        [HttpGet]
        public IActionResult Count()
        {
            return Ok(((ICivilianDataProvider)dataProvider).GetCount());
        }

        [HttpGet]
        public IActionResult GetMilitaryIdentificator([FromQuery] Guid id)
        {
            var result = ((ICivilianDataProvider)dataProvider).GetMilitaryIdentificator(id);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        //[HttpGet]
        //public IActionResult GetForProfile([FromQuery] Guid id)
        //{

        //}

        [HttpGet]
        public IActionResult GetByEnlistmentId([FromQuery] Guid enlistmentId)
        {
            var subpoenas = _subpoenaDataProvider.GetByEnlistmentId(enlistmentId);

            var result = ((ICivilianDataProvider)dataProvider).GetByEnlistmentId(subpoenas);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        [HttpPost]
        public IActionResult UpdateValidityGroup([FromBody] UpdateValidityGroupDTO dto)
        {
            var result = ((ICivilianDataProvider)dataProvider).UpdateValidityGroup(dto.CivilianId, dto.ValidityGroup);

            if (result < 0)
            {
                return BadRequest();
            }

            return Ok(result);
        }
    }
}
