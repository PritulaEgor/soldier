﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Web.Controllers;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;

namespace Soldier.Controllers.DataProviderControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class EmployeeProviderController : BaseDataProviderController<Employee, UserViewModel>
    {
        public EmployeeProviderController(IEmployeeDataProvider employeeDataProvider) : base(employeeDataProvider)
        {
        }

        [HttpGet]
        public IActionResult Count()
        {
            return Ok(((IEmployeeDataProvider)dataProvider).GetCount());
        }
    }
}
