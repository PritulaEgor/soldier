﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Core.DataProviders.Models;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Web.Controllers;
using Soldier.Common.Web.DTO;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soldier.Controllers.DataProviderControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class SubpoenaProviderController : BaseDataProviderController<Subpoena, SubpoenaViewModel>
    {
        private readonly ICurrentUserService<Civilian> _civilianService;
        private readonly ICurrentUserService<Employee> _employeeService;

        public SubpoenaProviderController(
            ISubpoenaDataProvider subpoenaDataProvider,
            ICurrentUserService<Civilian> civilianService,
            ICurrentUserService<Employee> employeeService) : base(subpoenaDataProvider)
        {
            _civilianService = civilianService;
            _employeeService = employeeService;
        }

        [HttpGet]
        public IActionResult Count()
        {
            return Ok(((ISubpoenaDataProvider)dataProvider).GetCount());
        }

        [HttpGet]
        public IActionResult ParticularCount() 
        {
            var current = _civilianService.GetCurrentUser(Request);

            return Ok(((ISubpoenaDataProvider)dataProvider).GetCountByRecipientId(current.Id));
        }

        [HttpGet]
        public IActionResult ParticularGet([FromQuery] PageInfo pageInfo)
        {
            var current = _civilianService.GetCurrentUser(Request);

            var page = ((ISubpoenaDataProvider)dataProvider).GetByRecipientId(pageInfo, current.Id);
            return Ok(page);
        }

        [HttpPut]
        public IActionResult SetReceived([FromBody] SubpoenaViewModel subpoena)
        {
            ((ISubpoenaDataProvider)dataProvider).SetReceivedStatus(subpoena);

            return Ok(true);
        }

        [HttpPut]
        public IActionResult SendSubpoenasBatch([FromBody] DistributionDTO distribution)
        {
            var result = ((ISubpoenaDataProvider)dataProvider).SaveSubpoenaBatch(distribution);

            //Сделать рассылку повесток

            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetArriving([FromQuery]GetArrivingDTO dto)
        {
            var result = ((ISubpoenaDataProvider)dataProvider).IsArrived(
                dto.UserId,
                dto.EnlistmentId);

            return Ok(result);
        }

        [HttpPost]
        public IActionResult UpdateSubpoenaStatus([FromBody] UpdateSubpoenaStatusDTO dto)
        {
            var result = ((ISubpoenaDataProvider)dataProvider).UpdateSubpoenaStatus(
                dto.UserId,
                dto.NewStatus,
                dto.EnlistmentId);

            if (result < 0)
            {
                return BadRequest();
            }

            return Ok(result);
        }
    }
}
