﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Web.Controllers;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;

namespace Soldier.Controllers.DataProviderControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MedicalCardProviderController : BaseDataProviderController<MedicalCard, MedicalCardViewModel>
    {
        public MedicalCardProviderController(IMedicalCardDataProvider medicalCardDataProvider) : base(medicalCardDataProvider)
        {

        }

        [HttpPost]
        public IActionResult AddIssue([FromBody] IssueViewModel issue)
        {
            var result = ((IMedicalCardDataProvider)dataProvider).AddIssue(issue);

            if (result == null)
            {
                return BadRequest();
            }

            return Created("medicalCard/AddIssue", result);
        }

        [HttpPost]
        public IActionResult AddExam([FromBody] MedicalExaminationViewModel exam)
        {
            var result = ((IMedicalCardDataProvider)dataProvider).AddExamination(exam);

            if (result == null)
            {
                return BadRequest();
            }

            return Created("medicalCard/AddExam", result);
        }
    }
}
