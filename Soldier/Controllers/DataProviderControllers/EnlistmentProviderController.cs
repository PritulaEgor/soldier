﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Web.Controllers;
using Soldier.Common.Web.ViewModels;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.DAL.DAL.Models;
using System;

namespace Soldier.Controllers.DataProviderControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class EnlistmentProviderController : BaseDataProviderController<Enlistment, EnlistmentViewModel>
    {
        public EnlistmentProviderController(IEnlistmentDataProvider enlistmentDataProvider) : base(enlistmentDataProvider)
        {

        }


        [HttpGet]
        public IActionResult Count()
        {
            return Ok(((IEnlistmentDataProvider)dataProvider).GetCount());
        }

        [HttpGet]
        public IActionResult GetStaticstics([FromQuery] Guid id)
        {
            var result = ((IEnlistmentDataProvider)dataProvider).GetStatistics(id);

            return Ok(result);
        }
    }
}
