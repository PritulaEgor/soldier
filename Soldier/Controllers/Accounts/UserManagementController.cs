﻿using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Web.ViewModels;
using Soldier.DAL.DAL.Models;
using System.Threading.Tasks;
using Soldier.Common.Core.DAL.Soldier;
using AutoMapper;
using Soldier.Core.DataProviders.Interfaces;
using Soldier.Common.BaseIdentity.ViewModels;
using Microsoft.Extensions.Primitives;
using Soldier.Common.BaseIdentity.Services.Interfaces;

namespace Soldier.Controllers.Accounts
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userIdentityService;
        private readonly ICurrentUserService<Civilian> _civilianService;
        private readonly ICurrentUserService<Employee> _employeeService;
        private readonly ICivilianDataProvider _civilianDataProvider;
        private readonly IEmployeeDataProvider _employeeDataProvider;
        private readonly ITokenService _tokenService;

        public UserManagementController(IMapper mapper,
            IUserService userIdentityService,
            ICurrentUserService<Civilian> civilianService,
            ICurrentUserService<Employee> employeeService,
            ICivilianDataProvider civilianDataProvider,
            IEmployeeDataProvider employeeDataProvider,
            ITokenService tokenService)
        {
            _mapper = mapper;
            _userIdentityService = userIdentityService;
            _civilianService = civilianService;
            _employeeService = employeeService;
            _civilianDataProvider = civilianDataProvider;
            _employeeDataProvider = employeeDataProvider;
            _tokenService = tokenService;
        }

        [HttpGet]
        [Route("current")]
        public async Task<IActionResult> GetCurrentUser()
        {
            StringValues token;

            if (!Request.Headers.TryGetValue("Authorization", out token))
            {
                return StatusCode(401);
            }

            var finalToken = token.ToString().Split(' ');

            if(finalToken.Length < 2)
            {
                return StatusCode(401);
            }

            var passportValue = _tokenService.GetPasssportNumberFromToken(finalToken[1]);

            var userType = await _userIdentityService.GetUserType(passportValue);

            if (userType.ToString() == nameof(Employee))
            {
                var user = _employeeService.GetCurrentUser(Request);

                var resultModel = _mapper.Map<UserViewModel>(user);

                resultModel.UserType = userType;

                return Ok(resultModel);
            }
            else if (userType.ToString() == nameof(Civilian))
            {
                var user = _civilianService.GetCurrentUser(Request);

                var resultModel = _mapper.Map<UserViewModel>(user);

                resultModel.UserType = userType;

                return Ok(resultModel);
            }
            else
            {
                return Ok(null);
            }
        }

        [HttpPost]
        [Route("createUser")]
        public async Task<IActionResult> CreateUser([FromBody] UserViewModel userViewModel)
        {
            if (userViewModel is null)
            {
                return BadRequest("Wrong user info");
            }

            var tempPass = userViewModel.TemporaryPassword;

            if (userViewModel.UserType == UserTypes.Employee)
            {
                var result = _employeeDataProvider.Create(userViewModel);

                result.TemporaryPassword = tempPass;

                return Ok(result);
            }
            else if (userViewModel.UserType == UserTypes.Civilian)
            {
                var result = _civilianDataProvider.Create(userViewModel);

                result.TemporaryPassword = tempPass;

                return Ok(result);
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("updateUser")]
        public async Task<IActionResult> UpdateUser([FromBody] UserViewModel userViewModel)
        {
            if (userViewModel is null)
            {
                return BadRequest("Wrong user info");
            }

            var tempPass = userViewModel.TemporaryPassword;

            if (userViewModel.UserType == UserTypes.Employee)
            {
                _employeeDataProvider.Update(userViewModel);

                return Ok();
            }
            else if (userViewModel.UserType == UserTypes.Civilian)
            {
                _civilianDataProvider.Update(userViewModel);

                return Ok();
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] UpdatePasswordModel updatePasswordModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid new password");
            }

            var result = await _userIdentityService.ChangePasswordAsync(updatePasswordModel.OldPassword, updatePasswordModel.NewPassword);

            return Ok(result);
        }
    }
}
