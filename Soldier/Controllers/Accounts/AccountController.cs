﻿using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using Soldier.Common.BaseIdentity.ViewModels;
using Soldier.Common.BaseIdentity.Services.Interfaces;
using System.Threading.Tasks;

namespace Soldier.Controllers.Accounts
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userIdentityService;
        private readonly ITokenService _tokenService;

        public AccountController(IUserService userIdentityService, ITokenService tokenService)
        {
            _userIdentityService = userIdentityService;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody]LoginModel loginData)
        {
            if (loginData is null)
            {
                return BadRequest("Invalid client request");
            }

            var user = await _userIdentityService.GetUserIdentityByCredentials(loginData.PassportNumber, loginData.Password);

            if (user is null)
            {
                return null;
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.PassportNumber),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString()),
            };

            if (claims is null)
            {
                return Unauthorized();
            }

            var encJwt = _tokenService.GenerateAccessToken(claims);

            var refJwt = _tokenService.GenerateRefreshToken();

            user.RefreshToken = refJwt;
            user.RefreshTokenExpiryTime = DateTime.Now.AddDays(7);

            _userIdentityService.Update(user);

            var response = new AuthenticationResponse
            {
                Token = encJwt,
                RefreshToken = refJwt
            };

            return Ok(response);
        }
    }
}
