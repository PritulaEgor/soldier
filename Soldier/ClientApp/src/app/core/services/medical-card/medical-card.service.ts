import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, isEmpty } from 'rxjs';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';
import { MedicalCard } from '../../models/MedicalCard';
import { Issue } from '../../models/Issue';
import { MedicalExamination } from '../../models/MedicalExamination';

@Injectable({
  providedIn: 'root',
})
export class MedicalCardService {
  readonly currentContext: string = 'api/medicalCardProvider/';

  constructor(private http: HttpClient) {}

  get(id: string) {
    return this.http.get<MedicalCard>(this.currentContext + 'getById', {
      params: { id },
    });
  }

  getIssuesCount(mCardId: string): Observable<number> {
    return this.http.get<number>(this.currentContext + 'issues-count', {
      params: { mCardId },
    });
  }

  getExaminationsCount(mCardId: string): Observable<number> {
    return this.http.get<number>(this.currentContext + 'examinations-count', {
      params: { mCardId },
    });
  }

  addIssue(issue: Issue) {
    return this.http.post<Issue>(this.currentContext + 'AddIssue', {
      Name: issue.name,
      Description: issue.description,
      DateOfDiagnosis: issue.dateOfDiagnosis,
      DoctorData: issue.doctorData,
      MedicalCardId: issue.medicalCardID
    });
  }

  addExamination(exam: MedicalExamination){
    return this.http.post<MedicalExamination>(this.currentContext + 'addExam', {
      MedicalCardId: exam.medicalCardID,
      ExaminationResult: exam.examinationResult,
      ExaminationDate: exam.examinationDate });
  }

  getIssuesPage(pageInfo: PageInfo, mCardId: string) {
    let pageNumber = pageInfo.pageNumber;
    let pageSize = pageInfo.pageSize;
    let filterBy = pageInfo.filterBy;
    let userType = pageInfo.userType;
    let OrderBy = 'null';

    return this.http.get<Page>(this.currentContext + 'get', {
      params: { pageNumber, pageSize, filterBy, userType, mCardId },
    });
  }

  getExaminationsPage(pageInfo: PageInfo, mCardId: string) {
    let pageNumber = pageInfo.pageNumber;
    let pageSize = pageInfo.pageSize;
    let filterBy = pageInfo.filterBy;
    let userType = pageInfo.userType;
    let OrderBy = 'null';

    return this.http.get<Page>(this.currentContext + 'get', {
      params: { pageNumber, pageSize, filterBy, userType, mCardId },
    });
  }
}
