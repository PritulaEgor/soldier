import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { UserRoles } from 'src/app/common/models/enums/userRoles';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';
import { UpdatePasswordModel } from 'src/app/common/models/UpdatePasswordModel';
import { AuthService } from 'src/app/common/services/auth/auth.service';
import { UserType } from '../models/enum/UserType';
import { UserViewModel } from '../models/User';
import { TokenStorageService } from 'src/app/common/services/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public static isCurrentUserFetched: boolean = false;

  private static user: UserViewModel = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    identityId: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    role: UserRoles.Guest,
    placeOfResidence: '',
    creatorId: '',
    passportIdentificationNumber: '',
    militaryId: '',
    medicalCardId: '',
    // militaryId: '562ffd6d-c6da-4acf-8e56-606c3a23b118',
    // medicalCardId: '562ffd6d-c6da-4acf-8e56-606c3a23b118',
    rank: '',
    serviceStartDate: '',
    workStartDate: '',
    temporaryPassword: '',
    userType: UserType.Guest
  };

  readonly currentContext: string = "api/userManagement/";

  constructor(private http: HttpClient,
    private authService: AuthService,
    private tokenService: TokenStorageService) { }

  private static isUserFetched(): boolean {
    return this.isCurrentUserFetched;
  }

  private static getCurrent(): UserViewModel {
    return this.user;
  }

  private static setCurrent(user: UserViewModel) {
    this.user = user;

    this.isCurrentUserFetched = true;
  }

  static unsetUser() {
    this.user = {
      id: '',
      passportNumber: '',
      name: '',
      surname: '',
      patronymic: '',
      identityId: '',
      currentAddress: '',
      email: '',
      phoneNumber: '',
      role: UserRoles.Guest,
      placeOfResidence: '',
      creatorId: '',
      passportIdentificationNumber: '',
      militaryId: '',
      medicalCardId: '',
      rank: '',
      serviceStartDate: '',
      workStartDate: '',
      temporaryPassword: '',
      userType: UserType.Guest
    };

    this.isCurrentUserFetched = false;
  }

  GetCurrentUserTest(): Observable<UserViewModel> {

    return this.http.get<UserViewModel>(this.currentContext + 'current');
  }

  getCurrent(): Observable<UserViewModel> {

    return this.http.get<UserViewModel>(this.currentContext + 'current');
  }

  create(user: UserViewModel) {

    return this.http.post<UserViewModel>(this.currentContext + 'createUser', {
      UserType: user.userType,
      TemporaryPassword: user.temporaryPassword,
      PassportNumber: user.passportNumber,
      Name: user.name,
      Surname: user.surname,
      Patronymic: user.patronymic,
      CurrentAddress: user.currentAddress,
      Email: user.email,
      PhoneNumber: user.phoneNumber,
      Role: user.role,
      PlaceOfResidence: user.placeOfResidence,
      CreatorId: user.creatorId,
      PassportIdentificationNumber: user.passportIdentificationNumber,
      Rank: user.rank,
      ServiceStartDate: user.serviceStartDate,
      WorkStartDate: user.workStartDate
    });
  }

  update(user: UserViewModel) {

    return this.http.put<UserViewModel>(this.currentContext + 'updateUser', user);
  }

  changePassword(data: UpdatePasswordModel) {

    return this.http.put<boolean>(this.currentContext + 'change-password', data);
  }

  getPage(pageInfo: PageInfo) {
    return this.http.post<Page>(this.currentContext + 'get-all', pageInfo);
  }
}
