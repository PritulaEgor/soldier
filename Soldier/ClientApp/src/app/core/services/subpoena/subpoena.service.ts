import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';
import { Distribution } from '../../../common/models/Distribution'
import { Subpoena } from '../../models/Subpoena';
import { SubpoenaStates } from '../../models/enum/SubpoenaStates';

@Injectable({
  providedIn: 'root'
})
export class SubpoenaService {

  readonly currentContext: string = "api/SubpoenaProvider/";

  constructor(private http: HttpClient) { }

  getCount(): Observable<number> {
    return this.http.get<number>(this.currentContext + 'ParticularCount');
  }

  getPage(pageInfo: PageInfo) {

    let pageNumber = pageInfo.pageNumber;
    let pageSize = pageInfo.pageSize;
    let filterBy = pageInfo.filterBy;
    let userType = pageInfo.userType;
    let OrderBy = 'null';

    return this.http.get<Page>(this.currentContext + 'ParticularGet', {
      params: { pageNumber, pageSize, filterBy, userType }
    });
  }

  sendSubpoenasBatch(model: Distribution) {
    return this.http.put<Subpoena[]>(this.currentContext + 'SendSubpoenasBatch', model)
  }

  updateSubpoenaStatus(status: SubpoenaStates, userId: string, enlistmentId: string){
    return this.http.post<SubpoenaStates>(this.currentContext + 'updateSubpoenaStatus', { UserId: userId, NewStatus: status, EnlistmentId: enlistmentId })
  }

  getArriving(userId: string, enlistmentId: string){
    return this.http.get<boolean>(this.currentContext + 'GetArriving', {
      params: { userId, enlistmentId }
    });
  }

  setReceived(subpoena: Subpoena) {

    return this.http.put<boolean>(this.currentContext + 'SetReceived', subpoena )
  }
}
