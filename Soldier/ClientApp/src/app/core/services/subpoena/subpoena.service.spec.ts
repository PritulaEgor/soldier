import { TestBed } from '@angular/core/testing';

import { SubpoenaService } from './subpoena.service';

describe('SubpoenaService', () => {
  let service: SubpoenaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubpoenaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
