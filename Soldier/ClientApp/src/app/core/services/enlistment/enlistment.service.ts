import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';
import { Enlistment } from '../../models/Enlistment';
import { EnlistmentStatistics } from '../../models/EnlistmentStatistics';

@Injectable({
  providedIn: 'root'
})
export class EnlistmentService {

  readonly currentContext: string = "api/enlistmentProvider/";

  constructor(private http: HttpClient) { }

  get(id: string) {
    return this.http.get<Enlistment>(this.currentContext + 'getById', {
      params: { id },
    });
  }

  getCount(): Observable<number> {
    return this.http.get<number>(this.currentContext + 'count');
  }

  getPage(pageInfo: PageInfo){

    let pageNumber = pageInfo.pageNumber;
    let pageSize = pageInfo.pageSize;
    let filterBy = pageInfo.filterBy;
    let userType = pageInfo.userType;
    let OrderBy = 'null';

    return this.http.get<Page>(this.currentContext + 'get', {
      params: {pageNumber, pageSize, filterBy, userType}
    });
  }

  getStats(id: string){
    return this.http.get<EnlistmentStatistics>(this.currentContext + 'getStaticstics', {
      params: {id}
    });
  }

  create(enlistment: Enlistment){
    return this.http.post<Enlistment>(this.currentContext + 'save', {
      StartDate: enlistment.startDate,
      EndDate: enlistment.endDate,
    })
  }
}
