import { TestBed } from '@angular/core/testing';

import { EnlistmentService } from './enlistment.service';

describe('EnlistmentService', () => {
  let service: EnlistmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnlistmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
