import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';
import { MilitaryIdentificator } from '../../models/MilitaryIdentificator';
import { ValidityGroups } from '../../models/enum/ValidityGroups';
import { Civilian } from '../../models/Civilian';

@Injectable({
  providedIn: 'root',
})
export class CivilianService {
  readonly currentContext: string = 'api/civilianProvider/';

  constructor(private http: HttpClient) { }

  getPage(pageInfo: PageInfo) {

    let pageNumber = pageInfo.pageNumber;
    let pageSize = pageInfo.pageSize;
    let filterBy = pageInfo.filterBy;
    let userType = pageInfo.userType;
    let OrderBy = 'null';

    return this.http.get<Page>(this.currentContext + 'get', {
      params: { pageNumber, pageSize, filterBy, userType }
    });
  }

  getCount(): Observable<number> {
    return this.http.get<number>(this.currentContext + 'count');
  }

  getMilitaryIdentificator(id: string) {
    return this.http.get<MilitaryIdentificator>(this.currentContext + 'getMilitaryIdentificator', {
      params: { id }
    })
  }

  updateValidityGroup(id: string, vGroup: ValidityGroups) {
    return this.http.post<ValidityGroups>(this.currentContext + 'updateValidityGroup', { CivilianId: id, ValidityGroup: vGroup })
  }

  getByEnlistmentId(enlistmentId: string){
    return this.http.get<Civilian[]>(this.currentContext + 'getByEnlistmentId', {
      params: { enlistmentId }
    })
  }
}
