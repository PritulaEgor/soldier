import { TestBed } from '@angular/core/testing';

import { CivilianService } from './civilian.service';

describe('CivilianService', () => {
  let service: CivilianService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CivilianService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
