import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  readonly currentContext: string = "api/employeeProvider/";

  constructor(private http: HttpClient) { }

  getCount(): Observable<number> {
    return this.http.get<number>(this.currentContext + 'count');
  }

  getPage(pageInfo: PageInfo){

    let pageNumber = pageInfo.pageNumber;
    let pageSize = pageInfo.pageSize;
    let filterBy = pageInfo.filterBy;
    let userType = pageInfo.userType;
    let OrderBy = 'null';

    return this.http.get<Page>(this.currentContext + 'get', {
      params: {pageNumber, pageSize, filterBy, userType}
    });
  }
}
