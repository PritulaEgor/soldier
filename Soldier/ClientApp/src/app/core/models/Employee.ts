import IUser from "src/app/common/models/IUser";

export interface Employee extends IUser{
  rank:  string,
  serviceStartDate: Date,
  workStartDate: Date,
  reportsToID: string
}
