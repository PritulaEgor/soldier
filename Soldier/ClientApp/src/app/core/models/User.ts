import IUser from "src/app/common/models/IUser";
import { UserType } from "./enum/UserType";

export interface UserViewModel extends IUser {
    userType: UserType,
    passportIdentificationNumber: string,
    militaryId: string,
    identityId: string,
    medicalCardId: string,
    rank: string,
    serviceStartDate: string,
    workStartDate: string,
    // reportsToId: string,
    temporaryPassword: string
}
