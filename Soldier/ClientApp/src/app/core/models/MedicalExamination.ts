import IEntity from '../../common/models/IEntity';
import { MedicalCard } from './MedicalCard';

export interface MedicalExamination extends IEntity {
  medicalCardID: string;
  medicalCard: MedicalCard;
  examinationResult: string;
  examinationDate: Date;
}
