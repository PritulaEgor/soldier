import IUser from "src/app/common/models/IUser";
import { MedicalCard } from "./MedicalCard";
import { MilitaryIdentificator } from "./MilitaryIdentificator";
import { Postponement } from "./Postponement";

export interface Civilian extends IUser{
  passportIdentificationNumber: string ,
  militaryID: string,
  militaryIdentificator: MilitaryIdentificator,
  postponements: Postponement[],
  medicalCardID: string,
  medicalCard: MedicalCard
}
