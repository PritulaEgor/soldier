import IEntity from "src/app/common/models/IEntity";
import { SubpoenaStates } from "./enum/SubpoenaStates";

export interface SubpoenaViewModel extends IEntity{
    recipientId: string,
    status: SubpoenaStates,
    sended: Date,
    daysActive: number,
    soldierDateOfArriving: Date,
    signaturerId: string,
    signaturerName: string,
    enlistmentId: string
}
