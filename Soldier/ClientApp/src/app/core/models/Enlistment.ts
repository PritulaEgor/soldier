import IEntity from "src/app/common/models/IEntity";

export interface Enlistment extends IEntity{
    startDate: Date 
    endDate: Date 
}