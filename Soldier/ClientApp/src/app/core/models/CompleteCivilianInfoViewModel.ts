import IUser from "src/app/common/models/IUser";
import { UserType } from "./enum/UserType";
import { UserRoles } from "src/app/common/models/enums/userRoles";
import { MilitaryIdentificator } from "./MilitaryIdentificator";
import { MedicalCard } from "./MedicalCard";
import { Subpoena } from "./Subpoena";
import IEntity from "src/app/common/models/IEntity";

export interface CompleteCivilianInfoVewModel extends IEntity{
    UserType: UserType,
    TemporaryPassword: string,
    PassportNumber: string,
    Name: string,
    Surname: string,
    Patronymic: string,
    CurrentAddress: string,
    Email: string,
    PhoneNumber: string,
    Role: UserRoles,
    PlaceOfResidence: string,
    CreatorId: string,
    PassportIdentificationNumber: string,
    MilitaryID: MilitaryIdentificator,
    MedicalCardID: MedicalCard,
    Subpoenas: Subpoena[]
}