export interface EnlistmentStatistics{
  deviantsPercentage: number,
  conscriptsCount: number,
  arrivingAwaiting: number,
  arrived: number,
  notValid: number
}
