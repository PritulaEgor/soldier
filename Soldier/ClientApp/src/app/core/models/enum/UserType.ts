export enum UserType{
    Employee = 0,
    Civilian = 1,
    Guest = 2
}