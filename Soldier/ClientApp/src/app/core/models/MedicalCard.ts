import { Civilian } from "./Civilian";
import { ValidityGroups } from "./enum/ValidityGroups";
import IEntity from "../../common/models/IEntity";
import { Issue } from "./Issue";
import { MedicalExamination } from "./MedicalExamination";

export interface MedicalCard extends IEntity{
  validityGroup: ValidityGroups ,
  issues: Issue[],
  medicalExaminations: MedicalExamination []
}
