import IEntity from '../../common/models/IEntity';
import { MedicalCard } from './MedicalCard';

export interface Issue extends IEntity {
  name: string;
  description: string;
  dateOfDiagnosis: Date;
  doctorData: string;
  medicalCardID: string;
  medicalCard: MedicalCard;
}
