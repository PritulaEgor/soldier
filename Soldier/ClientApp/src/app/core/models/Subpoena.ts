import IEntity from "src/app/common/models/IEntity";
import { SubpoenaStates } from "./enum/SubpoenaStates";

export interface Subpoena extends IEntity{
    recipientId: string,
    status: SubpoenaStates,
    sended: Date,
    daysActive: number,
    soldierDateOfArriving: Date,
    signaturerId: string,
    enlistmentId: string
}
