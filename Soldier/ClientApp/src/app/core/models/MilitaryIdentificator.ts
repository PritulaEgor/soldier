import IEntity from "../../common/models/IEntity";

export interface MilitaryIdentificator extends IEntity{
    isMilitaryServicePassed: boolean,
    militaryID: string,
    militarySpeciality: string,
    serviceStartDate: Date,
    serviceEndDate: Date
}
