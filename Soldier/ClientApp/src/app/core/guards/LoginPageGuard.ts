import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationResponse } from 'src/app/common/models/AuthenticationResponse';
import { AuthService } from 'src/app/common/services/auth/auth.service';
import { TokenStorageService } from 'src/app/common/services/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class LoginPageGuard   {

  constructor(private router:Router,
     private jwtHelper: JwtHelperService,
     private tokenService: TokenStorageService){}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let token = this.tokenService.getToken();

    if (token && !this.jwtHelper.isTokenExpired(token)){
      this.router.navigate(['/'])
    }

    return true;
  }

}
