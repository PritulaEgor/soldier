import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationResponse } from 'src/app/common/models/AuthenticationResponse';
import { AuthService } from 'src/app/common/services/auth/auth.service';
import { TokenStorageService } from 'src/app/common/services/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard   {

  constructor(private router:Router,
     private jwtHelper: JwtHelperService,
     private authService: AuthService,
     private tokenService: TokenStorageService){}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let token = this.tokenService.getToken();

    // if (token && !this.jwtHelper.isTokenExpired(token)){
      if (token){
      return true;
    }

    const isRefreshSuccess = await this.tryRefreshingTokens(<string>token);
    if (!isRefreshSuccess) {
      this.router.navigate(["login"]);
    }
    return isRefreshSuccess;
  }

  private async tryRefreshingTokens(token: string): Promise<boolean> {
    const refreshToken = this.tokenService.getRefreshToken();

    if (!token || !refreshToken) {
      return false;
    }

    const credentials = JSON.stringify({ accessToken: token, refreshToken: refreshToken });

    let isRefreshSuccess: boolean;

    const refreshRes = await new Promise<AuthenticationResponse>((resolve, reject) => {
    this.authService.refresh(credentials).subscribe({
        next: (res: AuthenticationResponse) => resolve(res),
        error: (_) => {
           reject; isRefreshSuccess = false;
          }
      });
    });

    this.tokenService.saveToken( refreshRes.token)
    this.tokenService.saveRefreshToken( refreshRes.refreshToken)

    isRefreshSuccess = true;
    return isRefreshSuccess;
  }
}
