import IUser from "./IUser";
import { Enlistment } from "../../core/models/Enlistment"

export interface Distribution {
    conscripts: IUser[]
    signaturer: IUser
    enlistment: Enlistment
}