import IEntity from "./IEntity";

export interface Page {
    number: number,
    size: number,
    totalCount: number,
    items: IEntity[]
}