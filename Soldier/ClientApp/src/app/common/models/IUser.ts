import { UserRoles } from "src/app/common/models/enums/userRoles";
import IEntity from "./IEntity";

export default interface IUser extends IEntity{
    passportNumber: string,
    name: string,
    surname: string,
    patronymic: string,
    currentAddress: string,
    email: string,
    phoneNumber: string,
    placeOfResidence: string,
    creatorId: string,
    role: UserRoles
}
