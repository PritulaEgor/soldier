import { UserType } from "src/app/core/models/enum/UserType";

export interface PageInfo {
    pageNumber: number,
    pageSize: number,
    //  orderBy:
    filterBy: string,
    userType: UserType    
}