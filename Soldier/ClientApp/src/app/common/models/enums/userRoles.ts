export enum UserRoles{
  PotentialConscript = 0, //Призывник
  Volunteer = 1,          //Доброволец
  Conscript = 2,          //Срочник
  Commissar = 3,          //Коммисар
  MedicalStaff = 4,
  AccountantStaff = 5,
  ITStaff = 6,
  ITAdmin = 7,
  CommissariatStaff = 8,
  Guest = 9
}
