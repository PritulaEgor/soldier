import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UserViewModel } from 'src/app/core/models/User';

@Injectable({
  providedIn: 'root',
})
export class SharedUserUpdateService {
  constructor() {}

  declare observedUser: UserViewModel;

  // Observable string sources
  private emitChangeSource = new Subject<any>();
  private userUpdateSource = new Subject<any>();

  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  userUpdated$ = this.userUpdateSource.asObservable();

  // Service message commands
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  updateUser(user: UserViewModel) {
    if (this.observedUser != user) {
      this.observedUser = user;

      this.userUpdateSource.next(user);
    }
  }
}
