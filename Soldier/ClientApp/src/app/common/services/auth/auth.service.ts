import { Injectable } from '@angular/core';
import { AuthenticationResponse } from '../../models/AuthenticationResponse';
import { LoginModel } from '../../models/LoginModel';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenStorageService } from '../token/token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  credentials: LoginModel = { passportNumber: '', password: '' };

  readonly currentContext: string = 'api/';

  private static isRefreshed: boolean = false;

  constructor(
    private router: Router,
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private tokenService: TokenStorageService
  ) { }

  login(data: LoginModel) {
    return this.http.post<AuthenticationResponse>(
      this.currentContext + 'account/login',
      data
    );
  }

  refresh(credentials: string) {
    return this.http.post<AuthenticationResponse>(
      this.currentContext + 'token/refresh',
      credentials,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
      }
    );
  }

  isUserAuthenticated = (): boolean => {
    const token = this.tokenService.getToken();

    if (token) {
      return true
    }

    return false;
  };
}
