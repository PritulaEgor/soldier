import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver-es';

const ExcelType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const ExcelExtension = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  readonly currentContextUser: string = "api/userReport/";
  readonly currentContextSubpoena: string = "api/subpoenareport/"

  constructor(private http: HttpClient) { }

  getCompleteUserReport(){
    let date = new Date();

    this.http.get(this.currentContextUser + 'complete', {responseType: 'blob'})
      .subscribe(buffer=>{
        const data: Blob = new Blob([buffer], {type: ExcelType});
        FileSaver.saveAs(data, `User_Complete_Report_${date.getDate()}.${date.getMonth()}`+ ExcelExtension)
      })
  }

  getCiviliansReport(){
    let date = new Date();

    this.http.get(this.currentContextUser + 'civilians', {responseType: 'blob'})
      .subscribe(buffer=>{
        const data: Blob = new Blob([buffer], {type: ExcelType});
        FileSaver.saveAs(data, `Civilians_Report_${date.getDate()}.${date.getMonth()}`+ ExcelExtension)
      })
  }

  getEmployeesReport(){
    let date = new Date();

    this.http.get(this.currentContextUser + 'employees', {responseType: 'blob'})
      .subscribe(buffer=>{
        const data: Blob = new Blob([buffer], {type: ExcelType});
        FileSaver.saveAs(data, `Employees_Report_${date.getDate()}.${date.getMonth()}`+ ExcelExtension)
      })
  }

  getDeviatorsReport(){
    let date = new Date();

    this.http.get(this.currentContextSubpoena + 'deviators', {responseType: 'blob'})
      .subscribe(buffer=>{
        const data: Blob = new Blob([buffer], {type: ExcelType});
        FileSaver.saveAs(data, `Deviators_Report_${date.getDate()}.${date.getMonth()}`+ ExcelExtension)
      })
  }
}
