import { TestBed } from '@angular/core/testing';

import { SharedUserUpdateService } from './shared-user-update.service';

describe('SharedUserUpdateService', () => {
  let service: SharedUserUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SharedUserUpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
