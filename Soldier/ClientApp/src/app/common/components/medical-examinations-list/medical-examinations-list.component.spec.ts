import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalExaminationsListComponent } from './medical-examinations-list.component';

describe('MedicalExaminationsListComponent', () => {
  let component: MedicalExaminationsListComponent;
  let fixture: ComponentFixture<MedicalExaminationsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalExaminationsListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MedicalExaminationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
