import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationResponse } from '../../models/AuthenticationResponse';
import { LoginModel } from '../../models/LoginModel';
import { AuthService } from '../../services/auth/auth.service';
import { TokenStorageService } from '../../services/token/token.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  invalidLogin: boolean = false;

  data: LoginModel = {
    passportNumber: '',
    password: ''
  }

  constructor(private authService: AuthService,
    private router: Router,
    private tokenService: TokenStorageService) { }

  ngOnInit(): void {
  }

  logIn() {
    this.authService.login(this.data).subscribe({
      next: (response: AuthenticationResponse) =>{
        const token = response.token;
        const refreshToken = response.refreshToken;

        this.tokenService.saveToken(token);
        this.tokenService.saveRefreshToken(refreshToken);

        this.invalidLogin = false;
        this.router.navigate(["/"]);
      },
      error: (err:  HttpErrorResponse) => this.invalidLogin = true
    })
  }
}
