import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Enlistment } from 'src/app/core/models/Enlistment';
import { EnlistmentService } from 'src/app/core/services/enlistment/enlistment.service';

@Component({
  selector: 'app-add-enlistment',
  templateUrl: './add-enlistment.component.html',
  styleUrls: ['./add-enlistment.component.scss']
})
export class AddEnlistmentComponent {

  @Output() updateEnlistments = new EventEmitter<Enlistment>();

  successMessage = 'Призыв успешно создан и помещен в список призывов.'
  failMessage = 'Не удалось создать призыв. Обновите страницу и попробуйте еще раз.'

  startDate = new Date('');
  endDate = new Date('');

  constructor(
    private enlistmentService: EnlistmentService,
    private snackBar: MatSnackBar) {

  }

  save() {
    let enlistment: Enlistment = {
      id: '',
      startDate: this.startDate,
      endDate: this.endDate
    }

    this.enlistmentService.create(enlistment).subscribe(result => {

      if (result) {
        this.updateEnlistments.emit(result)

        this.openSnackBar(this.successMessage)
      }
      else {
        this.openSnackBar(this.failMessage);
      }
    })
  }

  isDataValid() {
    let res = false;

    if (this.startDate != null
      && this.endDate != null
      && `${this.startDate}` != 'Invalid Date'
      && `${this.endDate}` != 'Invalid Date') {
      res = true;
    }

    return res
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Понятно')
  }
}

