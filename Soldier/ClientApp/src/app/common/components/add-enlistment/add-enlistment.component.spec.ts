import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEnlistmentComponent } from './add-enlistment.component';

describe('AddEnlistmentComponent', () => {
  let component: AddEnlistmentComponent;
  let fixture: ComponentFixture<AddEnlistmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEnlistmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEnlistmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
