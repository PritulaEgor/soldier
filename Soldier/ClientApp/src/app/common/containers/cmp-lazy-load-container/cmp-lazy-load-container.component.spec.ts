import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyLoadContainerComponent } from './cmp-lazy-load-container.component';

describe('CmpLazyLoadContainerComponent', () => {
  let component: LazyLoadContainerComponent;
  let fixture: ComponentFixture<LazyLoadContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LazyLoadContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LazyLoadContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
