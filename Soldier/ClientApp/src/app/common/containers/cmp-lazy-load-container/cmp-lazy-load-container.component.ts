import { AfterContentChecked, Component, ContentChild, ElementRef, TemplateRef } from '@angular/core';

@Component({
  selector: 'cmp-lazy-load-container',
  templateUrl: './cmp-lazy-load-container.component.html',
  styleUrls: ['./cmp-lazy-load-container.component.scss'],
})
export class LazyLoadContainerComponent implements AfterContentChecked {
  constructor(private elRef: ElementRef) {}

  @ContentChild('body', { static: true })

  declare contentTemplate: TemplateRef<ElementRef>;

  loadContent: boolean = false; 
  ngAfterContentChecked() {
    if (this.elRef.nativeElement.offsetParent) this.loadContent = true;
  }
}
