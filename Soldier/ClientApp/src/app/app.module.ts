import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './common/components/auth/auth.component';
import { AuthGuard } from './core/guards/AuthGuard';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UserDialogComponent } from './modals/user-dialog/user-dialog/user-dialog.component';
import { ProfileComponent } from './profile/profile.component';
import { GuideComponent } from './guide/guide.component';
import { UserGuideComponent } from './guide/user-guide/user-guide.component';
import { EmployeeGuideComponent } from './guide/employee-guide/employee-guide.component';
import { AdminGuideComponent } from './guide/admin-guide/admin-guide.component';
import { UserListDialogComponent } from './modals/user-list-dialog/user-list-dialog.component';
import { MedicalCardComponent } from './medical-card/medical-card.component';
import { IssuesListComponent } from './common/components/issues-list/issues-list.component';
import { MedicalExaminationsListComponent } from './common/components/medical-examinations-list/medical-examinations-list.component';
import { LazyLoadContainerComponent } from './common/containers/cmp-lazy-load-container/cmp-lazy-load-container.component';
import { EnlistmentsComponent } from './enlistments/enlistments.component';
import { AddEnlistmentComponent } from './common/components/add-enlistment/add-enlistment.component';
import { ValidityChooseComponent } from './modals/bottom-sheets/validity-choose-component/validity-choose.component';
import { IssueAddComponent } from './modals/bottom-sheets/issue-add/issue-add.component';
import { ExamAddComponent } from './modals/bottom-sheets/exam-add/exam-add.component';
import { GuestGuideComponent } from './guide/guest-guide/guest-guide.component';
import { EnlistmentComponent } from './enlistment/enlistment.component';
import { AuthInterceptor } from './common/interceptors/auth.interceptor';
import { LoginPageGuard } from './core/guards/LoginPageGuard';
import { Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { JwtModule } from '@auth0/angular-jwt';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'enlistment/:id', component: EnlistmentComponent, canActivate: [AuthGuard] },
  { path: 'login', component: AuthComponent, canActivate: [LoginPageGuard] }];

export function tokenGetter() {
  // return localStorage.getItem("jwt");
  return window.sessionStorage.getItem('auth-token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    AuthComponent,
    UserDialogComponent,
    ProfileComponent,
    GuideComponent,
    UserGuideComponent,
    EmployeeGuideComponent,
    AdminGuideComponent,
    UserListDialogComponent,
    MedicalCardComponent,
    IssuesListComponent,
    MedicalExaminationsListComponent,
    LazyLoadContainerComponent,
    EnlistmentsComponent,
    AddEnlistmentComponent,
    ValidityChooseComponent,
    IssueAddComponent,
    ExamAddComponent,
    GuestGuideComponent,
    EnlistmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:5001"],
        disallowedRoutes: [],
      }
    })
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
     }],
  bootstrap: [AppComponent],
})
export class AppModule { }
