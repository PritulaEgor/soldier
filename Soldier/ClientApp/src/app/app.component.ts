import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationResponse } from './common/models/AuthenticationResponse';
import { UserRoles } from './common/models/enums/userRoles';
import IUser from './common/models/IUser';
import { AuthService } from './common/services/auth/auth.service';
import { UserService } from './core/services/user.service';
import { UserViewModel } from './core/models/User';
import { UserType } from './core/models/enum/UserType';
import { SharedUserUpdateService } from './common/services/shared-user-update.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
// export class AppComponent implements OnInit, DoCheck {
export class AppComponent implements OnInit {
  title = 'Soldier';

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private _sharedService: SharedUserUpdateService
  ) {
    _sharedService.changeEmitted$.subscribe(() => {
      this.getUserActual();
    });
  }

  user: UserViewModel = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    identityId: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    role: UserRoles.Guest,
    placeOfResidence: '',
    creatorId: '',
    passportIdentificationNumber: '',
    militaryId: '',
    medicalCardId: '',
    rank: '',
    serviceStartDate: '',
    workStartDate: '',
    temporaryPassword: '',
    userType: UserType.Guest,
  };

  ngOnInit() {}

  getUserActual() {
    this.userService.getCurrent().subscribe({
      next: (user: UserViewModel) => {
        this.user = user;

        this._sharedService.updateUser(this.user);
      },
    });
  }
}
