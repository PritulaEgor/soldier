import { UserRoles } from "src/app/common/models/enums/userRoles";
import { UserType } from "src/app/core/models/enum/UserType";
import { UserViewModel } from "src/app/core/models/User";

export interface IUserState{
  user: UserViewModel
}

export const initialUserState: IUserState = {
  user: {
      id: '',
      passportNumber: '',
      name: '',
      surname: '',
      patronymic: '',
      identityId: '',
      currentAddress: '',
      email: '',
      phoneNumber: '',
      role: UserRoles.Guest,
      placeOfResidence: '',
      creatorId: '',
      passportIdentificationNumber: '',
      militaryId: '',
      medicalCardId: '',
      rank: '',
      serviceStartDate: '',
      workStartDate: '',
      temporaryPassword: '',
      userType: UserType.Guest,
    }
}
