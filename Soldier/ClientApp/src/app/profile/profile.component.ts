import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserRoles } from '../common/models/enums/userRoles';
import { UpdatePasswordModel } from '../common/models/UpdatePasswordModel';
import { UserType } from '../core/models/enum/UserType';
import { UserViewModel } from '../core/models/User';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  updatePass: UpdatePasswordModel = {
    oldPassword: '',
    newPassword: '',
  };

  invalidPass: boolean = false;

  declare user: UserViewModel;

  // user: UserViewModel = {
  //   id: '',
  //   passportNumber: '',
  //   name: '',
  //   surname: '',
  //   patronymic: '',
  //   identityId: '',
  //   currentAddress: '',
  //   email: '',
  //   phoneNumber: '',
  //   role: UserRoles.Guest,
  //   placeOfResidence: '',
  //   creatorId: '',
  //   passportIdentificationNumber: '',
  //   militaryId: '',
  //   medicalCardId: '',
  //   rank: '',
  //   serviceStartDate: '',
  //   workStartDate: '',
  //   temporaryPassword: '',
  //   userType: UserType.Guest,
  // };

  isInEditing: boolean = false;

  isInEditingPass: boolean = false;

  constructor(private userService: UserService) {
      this.setCurrent();
    }

  ngOnInit(): void {
  }

  setCurrent() {
    this.userService.getCurrent().subscribe({
      next: (user: UserViewModel) => {
        this.user = user
      }
    });
  }

  startEditing() {
this.setCurrent()

    this.isInEditing = true;
  }

  startEditingPass() {
    this.isInEditingPass = true;
  }

  saveChanges() {
    this.userService.update(this.user).subscribe(result=>{
    });

    this.isInEditing = false;
  }

  updatePassword() {
    this.userService.changePassword(this.updatePass).subscribe({
      next: (response: boolean) =>{
        this.invalidPass = !response
      },
      error: (err:  HttpErrorResponse) => this.invalidPass = true
    })

    this.isInEditingPass = false;
  }
}
