import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './common/components/auth/auth.component';
import { AuthGuard } from './core/guards/AuthGuard';
import { LoginPageGuard } from './core/guards/LoginPageGuard';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { EnlistmentComponent } from './enlistment/enlistment.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{ path: '', component: HomeComponent, pathMatch: 'full' },
{ path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
{ path: 'enlistment/:id', component: EnlistmentComponent, canActivate: [AuthGuard] },
{ path: 'login', component: AuthComponent, canActivate: [LoginPageGuard] }];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    bindToComponentInputs: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
