import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { UserRoles } from '../common/models/enums/userRoles';
import IUser from '../common/models/IUser';
import { Page } from '../common/models/Page';
import { PageInfo } from '../common/models/PageInfo';
import { Civilian } from '../core/models/Civilian';
import { Enlistment } from '../core/models/Enlistment';
import { SubpoenaStates } from '../core/models/enum/SubpoenaStates';
import { UserType } from '../core/models/enum/UserType';
import { Subpoena } from '../core/models/Subpoena';
import { SubpoenaViewModel } from '../core/models/SubpoenaViewModel';
import { EnlistmentService } from '../core/services/enlistment/enlistment.service';
import { SubpoenaService } from '../core/services/subpoena/subpoena.service';
import { EmployeeService } from '../core/services/employee/employee.service';
import { CivilianService } from '../core/services/civilian/civilian.service';
import { Distribution } from '../common/models/Distribution';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enlistments',
  templateUrl: './enlistments.component.html',
  styleUrls: ['./enlistments.component.scss'],
})
export class EnlistmentsComponent implements OnInit, DoCheck {
  reFetched = false;
  step = '';

  sendSubpoenaFailed = 'Произошла ошибка при отправке повесток. Обновите страницу и попробуйте снова.'
  subpoenaOpenedByConscript = 'Вы просмотрели отправленную вам повестку. С этого момента она переходит в статус полученной.'
  subpoenaReceivingFailure = 'Произошла ошибка обновления статуса повестки. Обновите страницу и откройте повестку заново.'

  @Input() user: IUser = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    placeOfResidence: '',
    creatorId: '',
    role: UserRoles.Guest,
  };

  isCheckedEnlFinished: boolean = false;
  enlistmentCount = 0;
  subpoenaCount = 0;

  enlistmentStep = '';
  subpoenaStep = '';

  choosedConscripts: IUser[] = [];
  choosedEnlistment: Enlistment = {
    startDate: new Date(),
    endDate: new Date(),
    id: ''
  };

  enlistments: Enlistment[] = [];
  subpoenas: SubpoenaViewModel[] = [];

  enlistmentsPage: Enlistment[] = [];
  subpoenasPage: SubpoenaViewModel[] = [];

  choosedSubpoena: SubpoenaViewModel = {
    id: '',
    recipientId: '',
    status: SubpoenaStates.Отменена,
    sended: new Date(),
    daysActive: 0,
    soldierDateOfArriving: new Date(),
    signaturerId: '',
    signaturerName: '',
    enlistmentId: ''
  };

  enlistmentPageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 3,
    filterBy: '',
    userType: UserType.Civilian,
  };

  subpoenaPageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 3,
    filterBy: '',
    userType: UserType.Civilian,
  };

  constructor(
    private enlistmentService: EnlistmentService,
    private subpoenaService: SubpoenaService,
    private employeeService: EmployeeService,
    private civilianService: CivilianService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  users: IUser[] = [];

  pageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 10,
    filterBy: '',
    userType: UserType.Civilian,
  };

  usersCount = 10;

  ngDoCheck() {
    if (
      this.subpoenas &&
      this.subpoenas.length == 0 &&
      !this.reFetched &&
      (<Civilian>this.user).id != ''
    ) {
      this.reFetched = true;

      this.updateSubpoenasList();
    }
  }

  ngOnInit(): void {
    this.updateEnlistmentsList();
    this.updateSubpoenasList();
    this.updateUsersList();
  }

  isChecked(civ: IUser): boolean {
    let isChecked = this.choosedConscripts.find((civilian) => { return civilian.id === civ.id })

    return isChecked != undefined;
  }

  updateUsersList(){
    this.civilianService.getCount().subscribe({
      next: (result: number) => {
        this.usersCount = result;

      },
      error: () => {
        this.usersCount = 0;
      },
    });

    this.civilianService.getPage(this.pageInfo).subscribe({
      next: (result: Page) => {
        this.users = <IUser[]>result.items;
      },
    });
  }

  isEnlistmentChecked(enl: Enlistment) {
    return this.enlistmentStep == enl.id && !this.isCheckedEnlFinished;
  }

  choosedConsCount(): number {
    return this.choosedConscripts.length
  }

  isPropperDate(enl: Enlistment) {
    let today = new Date()

    let endDate = this.normalizeDate(enl.endDate.toString());

    this.isCheckedEnlFinished = today > endDate;
  }

  updateCheckedList(civ: IUser) {
    if (this.choosedConscripts.find((civilian) => { return civilian.id === civ.id }) != undefined) {
      let index = this.choosedConscripts.indexOf(civ, 0);

      this.choosedConscripts.splice(index, 1);
    } else {
      this.choosedConscripts.push(civ);
    }
  }

  routeToEnlistment(enlistmentId: string) {
    this.router.navigate(['enlistment/'+ enlistmentId]);
  }

  send() {
    let distribution: Distribution = {
      conscripts: this.choosedConscripts,
      signaturer: this.user,
      enlistment: this.choosedEnlistment
    }

    this.subpoenaService.sendSubpoenasBatch(distribution).subscribe(result => {
      if (result) {
        this.openSnackBar(`Успешно отправлено ${result.length} повесток`)
      }
      else {
        this.openSnackBar(this.sendSubpoenaFailed)
      }
    })
  }

  normalizeDate(date: string): Date {
    date = date.replaceAll('.', '/')

    let dates = date.split('/')

    date = `${dates[1]}/${dates[0]}/${dates[2]}`

    return new Date(date);
  }

  setEnlistmentStep(number: string) {
    this.enlistmentStep = number;
  }

  setEnlistmentByEmployee(enl: Enlistment) {
    this.enlistmentStep = enl.id;

    this.choosedEnlistment = enl;
  }

  isCivilian(): boolean {
    return (
      this.user.role == UserRoles.PotentialConscript ||
      this.user.role == UserRoles.Conscript
    );
  }

  setSubpoenaStep(number: string, subpoena: SubpoenaViewModel) {
    this.subpoenaStep = number;

    this.choosedSubpoena = subpoena;

    if (subpoena.status == SubpoenaStates.Отправлена) {
      this.subpoenaReceived(subpoena)
    }
  }

  handleEnlistmentPageChange(e: PageEvent) {
    let pageStart: number = e.pageIndex * 3;

    this.enlistmentsPage = this.enlistments.slice(pageStart, pageStart + 3);

    this.enlistmentPageInfo.pageNumber = e.pageIndex + 1;

    this.updateEnlistmentsList();
  }

  subpoenaReceived(subpoena: SubpoenaViewModel) {
    this.subpoenaService.setReceived(subpoena).subscribe(result => {
      if (result) {
        this.openSnackBar(this.subpoenaOpenedByConscript)

        this.updateSubpoenasList()
      }
      else {
        this.openSnackBar(this.subpoenaReceivingFailure)
      }
    })
  }

  handleSubpoenaPageChange(e: PageEvent) {
    let pageStart: number = e.pageIndex * 3;

    this.subpoenasPage = this.subpoenas.slice(pageStart, pageStart + 3);
  }

  updateEnlistmentsList() {
    this.enlistmentService.getCount().subscribe({
      next: (result: number) => {
        this.enlistmentCount = result;
      },
      error: () => {
        this.enlistmentCount = 0;
      },
    });

    this.enlistmentService.getPage(this.enlistmentPageInfo).subscribe({
      next: (result: Page) => {
        this.enlistments = <Enlistment[]>result.items;
      },
    });
  }

  updateSubpoenasList() {

    if (this.isCivilian()) {
      this.subpoenaService.getCount().subscribe({
        next: (result: number) => {
          this.subpoenaCount = result;
        },
        error: () => {
          this.subpoenaCount = 0;
        },
      });

      this.subpoenaService.getPage(this.subpoenaPageInfo).subscribe({
        next: (result: Page) => {
          this.subpoenas = <SubpoenaViewModel[]>result.items;
        },
      });
    }
  }

  GetChoosenSubpoenaState() {
    if (this.choosedSubpoena.id != '') {
      return SubpoenaStates[this.choosedSubpoena.status];
    }
    return 'Здесь будет отображаться статус повестки';
  }

  getChoosenSubpoenaSended() {
    if (this.choosedSubpoena.id != '') {
      return this.choosedSubpoena.sended;
    }
    return 'Здесь будет отображена дата отправки';
  }

  getChoosenSubpoenaDaysActive() {
    if (this.choosedSubpoena.id != '') {
      return this.choosedSubpoena.daysActive;
    }
    return 'Здесь будет отображено то, сколько дней будет у призывника чтобы явиться';
  }

  getChoosenSubpoenaSoldierDateOfArriving() {
    if (
      this.choosedSubpoena.id != '' &&
      this.choosedSubpoena.soldierDateOfArriving != null
    ) {
      return this.choosedSubpoena.soldierDateOfArriving;
    }
    return 'Здесь будет отображена дата, в которую явился призывник';
  }

  getChoosenSubpoenaSignaturerData() {
    if (this.choosedSubpoena.id != '') {
      return this.choosedSubpoena.signaturerName;
    }
    return 'Здесь будут имя и фамилия уполномоченного сотрудника, который подписал повестку';
  }

  GetSubpoenaState(sub: SubpoenaViewModel) {
    return SubpoenaStates[sub.status];
  }

  setStep(number: string) {
    this.step = number;
  }

  handlePageChange(e: PageEvent) {
    this.pageInfo.pageNumber = e.pageIndex + 1;
    this.pageInfo.userType = this.getUserType();
    this.step = '';

    this.updateList();
  }

  getUserType(): UserType {
    return UserType.Civilian;
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Понятно')
  }

  updateList() {
    this.civilianService.getCount().subscribe({
      next: (result: number) => {
        this.usersCount = result;
      },
      error: () => {
        this.usersCount = 0;
      },
    });

    this.civilianService.getPage(this.pageInfo).subscribe({
      next: (result: Page) => {
        this.users = <IUser[]>result.items;
      },
    });
  }
}
