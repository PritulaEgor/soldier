import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnlistmentsComponent } from './enlistments.component';

describe('EnlistmentsComponent', () => {
  let component: EnlistmentsComponent;
  let fixture: ComponentFixture<EnlistmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnlistmentsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EnlistmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
