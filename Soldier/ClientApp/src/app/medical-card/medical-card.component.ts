import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { UserRoles } from '../common/models/enums/userRoles';
import IUser from '../common/models/IUser';
import { Page } from '../common/models/Page';
import { PageInfo } from '../common/models/PageInfo';
import { Civilian } from '../core/models/Civilian';
import { UserType } from '../core/models/enum/UserType';
import {
  MatBottomSheet,
  MatBottomSheetModule,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { ValidityGroups } from '../core/models/enum/ValidityGroups';
import { Issue } from '../core/models/Issue';
import { MedicalCard } from '../core/models/MedicalCard';
import { MedicalExamination } from '../core/models/MedicalExamination';
import { UserViewModel } from '../core/models/User';
import { MedicalCardService } from '../core/services/medical-card/medical-card.service';
import { UserService } from '../core/services/user.service';
import { CivilianService } from '../core/services/civilian/civilian.service';
import { ValidityChooseComponent } from '../modals/bottom-sheets/validity-choose-component/validity-choose.component';
import { ExamAddComponent } from '../modals/bottom-sheets/exam-add/exam-add.component';
import { IssueAddComponent } from '../modals/bottom-sheets/issue-add/issue-add.component';

@Component({
  selector: 'app-medical-card',
  templateUrl: './medical-card.component.html',
  styleUrls: ['./medical-card.component.scss']
})
export class MedicalCardComponent implements OnInit, DoCheck {

  issuesCount = 0;
  examinationsCount = 0;
  usersCount = 10;

  step = ''
  issueStep = ''
  examinationStep = ''

  @Input() user: IUser = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    placeOfResidence: '',
    creatorId: '',
    role: UserRoles.Guest
  };

  medicalCard: MedicalCard = {
    id: '',
    validityGroup: ValidityGroups.А,
    issues: [],
    medicalExaminations: []
  }

  issues: Issue[] = [];
  examinations: MedicalExamination[] = [];
  users: IUser[] = [];

  issuesPage: Issue[] = [];
  examinationsPage: MedicalExamination[] = [];

  pageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 10,
    filterBy: '',
    userType: UserType.Civilian,
  };

  issuesPageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 3,
    filterBy: '',
    userType: UserType.Civilian,
  };

  examinationsPageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 3,
    filterBy: '',
    userType: UserType.Civilian,
  };

  choosedUser: Civilian = {
    id: '',
    passportNumber: 'Призывник не выбран',
    name: 'Призывник не выбран',
    surname: '',
    patronymic: '',
    currentAddress: 'Призывник не выбран',
    email: '',
    phoneNumber: '',
    placeOfResidence: 'Призывник не выбран',
    creatorId: '',
    role: UserRoles.Guest,
    passportIdentificationNumber: 'Призывник не выбран',
    militaryID: '',
    militaryIdentificator: {
      id: 'Призывник не выбран',
      isMilitaryServicePassed: false,
      militaryID: 'Призывник не выбран',
      militarySpeciality: 'Призывник не выбран',
      serviceEndDate: new Date(),
      serviceStartDate: new Date()
    },
    postponements: [],
    medicalCardID: '',
    medicalCard: this.medicalCard
  }

  constructor(
    private medicalCardService: MedicalCardService,
    private userService: UserService,
    private civilianService: CivilianService,
    private _bottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {

    if (this.isCivilian()) {
      this.refreshMedCard();
    }
    this.updateUsersList();
  }

  refreshMedCard() {
    this.medicalCardService.get((<Civilian>this.user).medicalCardID).subscribe({
      next: (result: MedicalCard) => {
        this.medicalCard = result;
        this.issues = result.issues;
        this.issuesCount = this.issues.length
        this.examinations = result.medicalExaminations;
        this.examinationsCount = result.medicalExaminations.length;

        this.issuesPage = this.issues.slice(0, 3)
        this.examinationsPage = this.examinations.slice(0, 3)
      },
      error: () => {
        console.log('Error while fetching medical card')
      },
    });
  }

  getChoosedIDNUmber() {
    return (<Civilian>this.choosedUser).passportIdentificationNumber;
  }

  ngDoCheck() {
    if (this.user.role != UserRoles.MedicalStaff
      && (<Civilian>this.user).medicalCardID
      && (<Civilian>this.user).medicalCardID != ''
      && this.medicalCard.id != (<Civilian>this.user).medicalCardID) {
      this.refreshMedCard()
    }
    else if (this.user.role == UserRoles.MedicalStaff
      && (<Civilian>this.choosedUser).medicalCardID
      && (<Civilian>this.choosedUser).medicalCardID != ''
      && this.medicalCard.id != (<Civilian>this.choosedUser).medicalCardID
      && this.choosedUser.id != '') {
      this.refreshChoosedMedCard(this.choosedUser)
    }
  }

  isCivilian() {
    return this.user.role != UserRoles.MedicalStaff
  }

  refreshChoosedMedCard(user: IUser) {
    this.medicalCardService.get((<Civilian>user).medicalCardID).subscribe({
      next: (result: MedicalCard) => {
        this.medicalCard = result;
        this.issues = result.issues;
        this.issuesCount = this.issues.length
        this.examinations = result.medicalExaminations;
        this.examinationsCount = result.medicalExaminations.length;

        this.issuesPage = this.issues.slice(0, 3)
        this.examinationsPage = this.examinations.slice(0, 3)
      },
      error: () => {
        console.log('Error while fetching medical card')
      },
    });

    if (!this.choosedUser.militaryIdentificator) {
      this.updateChoosedMilitaryId()
    }
  }

  updateChoosedMilitaryId() {
    this.civilianService.getMilitaryIdentificator(this.choosedUser.militaryID).subscribe(result => {
      if (result) {
        this.choosedUser.militaryIdentificator = result;
      }
      else {
        console.log(result)
      }
    })
  }

  updateUsersList() {
    this.civilianService.getCount().subscribe({
      next: (result: number) => {
        this.usersCount = result;

      },
      error: () => {
        this.usersCount = 0;
      },
    });

    this.civilianService.getPage(this.pageInfo).subscribe({
      next: (result: Page) => {
        this.users = <IUser[]>result.items;
      },
    });
  }

  isChecked(user: IUser) {
    return this.choosedUser.id == user.id
  }

  openBottomSheet() {
    let validity = this._bottomSheet.open(ValidityChooseComponent, {
      data: this.choosedUser.id
    });

    validity.afterDismissed().subscribe(result => {
      if (result) {
        this.medicalCard.validityGroup = result;
      }
    })
  }

  openIssueBottomSheet(){
    let validity = this._bottomSheet.open(IssueAddComponent, {
      data: this.medicalCard.id
    });

    validity.afterDismissed().subscribe(result => {
      if (result) {
        this.medicalCard.issues.push(result);
      }
    })
  }

  openExamBottomSheet(){
    let validity = this._bottomSheet.open(ExamAddComponent, {
      data: this.medicalCard.id
    });

    validity.afterDismissed().subscribe(result => {
      if (result) {
        this.medicalCard.medicalExaminations.push(result);
      }
    })
  }

  handlePageChange(e: PageEvent) {
    this.pageInfo.pageNumber = e.pageIndex + 1;
    this.pageInfo.userType = this.getUserType();
    this.step = '';

    this.updateList();
  }

  isUserChoosed() {
    return this.choosedUser.id != ''
  }

  checkUser(user: IUser) {
    this.choosedUser = <Civilian>user;
  }

  updateList() {
    this.civilianService.getCount().subscribe({
      next: (result: number) => {
        this.usersCount = result;
      },
      error: () => {
        this.usersCount = 0;
      },
    });

    this.civilianService.getPage(this.pageInfo).subscribe({
      next: (result: Page) => {
        this.users = <IUser[]>result.items;
      },
    });
  }

  getUserType(): UserType {
    return UserType.Civilian;
  }

  // getUserActual() {
  //   this.userService.getCurrent().subscribe({
  //     next: (user: UserViewModel) => {
  //       this.user = user
  //     }
  //   });
  // }

  getValidityGroup(): string {
    return ValidityGroups[this.medicalCard.validityGroup]
  }

  getMilitaryId(): string {
    return "Номер военного билета: нет"
  }

  getChoosedMilitaryId(): string {
    return 'Номер военного билета: ' + this.choosedUser.militaryIdentificator.militaryID
  }

  handleIssuePageChange(e: PageEvent) {
    let pageStart: number = e.pageIndex * 3;

    this.issuesPage = this.issues.slice(pageStart, pageStart + 3)
  }

  handleExaminationPageChange(e: PageEvent) {
    let pageStart: number = e.pageIndex * 3;

    this.examinationsPage = this.examinations.slice(pageStart, pageStart + 3)
  }

  choosedIsServicePassed() {

    if (!this.choosedUser.militaryIdentificator) {
      this.updateChoosedMilitaryId()
    }

    if (this.choosedUser.militaryIdentificator && this.choosedUser.militaryIdentificator.isMilitaryServicePassed) {
      return 'Срочную службу прошел'
    }
    else {
      return 'Срочную службу не проходил'
    }
  }

  setIssueStep(number: string) {
    this.issueStep = number;
  }
  setExaminationStep(number: string) {
    this.examinationStep = number;
  }
}
