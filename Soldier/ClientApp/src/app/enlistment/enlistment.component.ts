import { Component } from '@angular/core';
import { UserService } from '../core/services/user.service';
import { UserViewModel } from '../core/models/User';
import { ActivatedRoute } from '@angular/router';
import { EnlistmentService } from '../core/services/enlistment/enlistment.service';
import { Enlistment } from '../core/models/Enlistment';
import { Civilian } from '../core/models/Civilian';
import { PageInfo } from '../common/models/PageInfo';
import { UserType } from '../core/models/enum/UserType';
import { CivilianService } from '../core/services/civilian/civilian.service';
import { Page } from '../common/models/Page';
import { PageEvent } from '@angular/material/paginator';
import IUser from '../common/models/IUser';
import { SubpoenaStates } from '../core/models/enum/SubpoenaStates';
import { SubpoenaService } from '../core/services/subpoena/subpoena.service';
import { Observable } from 'rxjs';
import { EnlistmentStatistics } from '../core/models/EnlistmentStatistics';

@Component({
  selector: 'app-enlistment',
  templateUrl: './enlistment.component.html',
  styleUrls: ['./enlistment.component.scss'],
})
export class EnlistmentComponent {
  declare user: UserViewModel;

  arrivedFetched: boolean = false;

  arrivedList: { userId: string; isArrived: boolean }[] = [];

  conscripts: Civilian[] = [];

  pageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 10,
    filterBy: '',
    userType: UserType.Civilian,
  };

  usersCount = 10;

  enlistment: Enlistment = {
    id: '',
    startDate: new Date(''),
    endDate: new Date(''),
  };

  declare enlistmentStat: EnlistmentStatistics;

  step: string = '';

  enlistmentId: string = '';
  // user: UserViewModel = {
  //   id: '',
  //   passportNumber: '',
  //   name: '',
  //   surname: '',
  //   patronymic: '',
  //   identityId: '',
  //   currentAddress: '',
  //   email: '',
  //   phoneNumber: '',
  //   role: UserRoles.Guest,
  //   placeOfResidence: '',
  //   creatorId: '',
  //   passportIdentificationNumber: '',
  //   militaryId: '',
  //   medicalCardId: '',
  //   rank: '',
  //   serviceStartDate: '',
  //   workStartDate: '',
  //   temporaryPassword: '',
  //   userType: UserType.Guest,
  // };
  constructor(
    private userService: UserService,
    private activateRoute: ActivatedRoute,
    private enlistmentService: EnlistmentService,
    private civilianService: CivilianService,
    private subpoenaService: SubpoenaService
  ) {
    this.userService.getCurrent().subscribe({
      next: (user: UserViewModel) => {
        this.user = user
      }
    });

    this.enlistmentId = activateRoute.snapshot.params['id'];

    enlistmentService.get(this.enlistmentId).subscribe((result) => {
      if (result) {
        this.enlistment = result;

        enlistmentService.getStats(this.enlistmentId).subscribe((result) => {
          if (result) {
            this.enlistmentStat = result;
          }
        });
      } else {
      }
    });

    civilianService.getByEnlistmentId(this.enlistmentId).subscribe((result) => {
      if (result) {
        this.conscripts = result;
      } else {
      }
    });
  }

  ngOnInit(): void {
  }

  updateSubpoenaStatus(type: number, userId: string) {
    this.arrivedFetched = false;

    let subType: SubpoenaStates = <SubpoenaStates>type;

    this.subpoenaService
      .updateSubpoenaStatus(subType, userId, this.enlistmentId)
      .subscribe((result) => {
        if (result) {
          let civ = this.conscripts.find((civilian) => {
            return civilian.id === userId;
          });

          let index = this.conscripts.indexOf(<Civilian>civ, 0);

          this.fetchArrived();
        }
      });
  }

  checkArrived(civ: IUser) {
    if (!this.arrivedFetched) {
      this.fetchArrived();

      this.arrivedFetched = true;
    }

    let arrive = this.arrivedList.find((arrived) => {
      return arrived.userId == civ.id;
    });

    return arrive?.isArrived;
  }

  isArrived(civ: IUser): Observable<boolean> {
    return this.subpoenaService.getArriving(civ.id, this.enlistmentId);
  }

  updateStats() {
    this.enlistmentService.getStats(this.enlistmentId).subscribe((result) => {
      if (result) {
        this.enlistmentStat = result;
      }
    });
  }

  updateConscript(civ: IUser) {
    if (
      this.conscripts.find((civilian) => {
        return civilian.id === civ.id;
      }) != undefined
    ) {
      let index = this.conscripts.indexOf(<Civilian>civ, 0);

      let newStatus = this.checkArrived(civ)
        ? SubpoenaStates.Просрочена
        : SubpoenaStates.Реализована;

      this.updateSubpoenaStatus(newStatus, civ.id);
      this.updateStats()
      // this.conscripts.splice(index, 1);
    } else {
      // this.conscripts.push(<Civilian>civ);
    }
  }

  updateUsersList() {
    this.civilianService.getCount().subscribe({
      next: (result: number) => {
        this.usersCount = result;
      },
      error: () => {
        this.usersCount = 0;
      },
    });

    this.civilianService.getPage(this.pageInfo).subscribe({
      next: (result: Page) => {
        this.conscripts = <Civilian[]>result.items;

        if (!this.arrivedFetched) {
          this.fetchArrived();
        }
      },
    });
  }

  fetchArrived() {
    this.arrivedList = [];

    for (let x = 0; x < this.conscripts.length; x += 1) {
      let isArrived = false;

      this.isArrived(this.conscripts[x]).subscribe((result) => {
        isArrived = result;

        let arrive = {
          userId: this.conscripts[x].id,
          isArrived: isArrived,
        };

        this.arrivedList.push(arrive);
      });
    }

    this.arrivedFetched = true;
  }

  handlePageChange(e: PageEvent) {
    this.pageInfo.pageNumber = e.pageIndex + 1;
    this.pageInfo.userType = this.getUserType();
    this.step = '';
    this.arrivedFetched = false;

    this.updateUsersList();
  }

  getUserType(): UserType {
    return UserType.Civilian;
  }
}
