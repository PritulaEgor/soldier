import { Component, DoCheck, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserRoles } from '../common/models/enums/userRoles';
import IUser from '../common/models/IUser';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../core/services/user.service';
import { UserViewModel } from '../core/models/User';
import { BehaviorSubject, Observable, throwError, finalize } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { TokenStorageService } from '../common/services/token/token.service';
import { SharedUserUpdateService } from '../common/services/shared-user-update.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent{
  constructor(
    private jwtHelper: JwtHelperService,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private tokenService: TokenStorageService,
    private _sharedService: SharedUserUpdateService
  ) {
    _sharedService.userUpdated$.subscribe((user: UserViewModel) => {
      this.user = user;
    });

    this.spinner.show()

    //WIP: make spinner work again
    this.spinner.hide();

    this.configRoles();

    this.requestUserChange();
  }

  isCurrentUserAdmin: Boolean = false;

  declare user: IUser;

  getUserActual(): Observable<UserViewModel> {

    return this.userService.getCurrent();
  }

  configRoles() {

    const token = this.tokenService.getToken();

    if (token != null && (!this.user || this.user.id != '')) {
      this.requestUserChange();
    }

    this.isCurrentUserAdmin = this.user?.role == UserRoles.ITAdmin
      || this.user?.role == UserRoles.Commissar
  }

  tabChange() {
  }

  requestUserChange(){
    this._sharedService.emitChange('update');
  }

  isUserAuthenticated = (): boolean => {
    const token = this.tokenService.getToken();

    if (token) {
      return true;
    }

    return false;
  };

  validateMCardRoles() {
    if (this.isUserAuthenticated()) {
      return this.user.role == UserRoles.Conscript
        || this.user.role == UserRoles.PotentialConscript
        || this.user.role == UserRoles.Volunteer
        || this.user.role == UserRoles.MedicalStaff
    }

    return false;
  }

  validateEnlRoles() {
    if (this.isUserAuthenticated()) {
      return this.user.role == UserRoles.AccountantStaff
        || this.user.role == UserRoles.Commissar
        || this.user.role == UserRoles.CommissariatStaff
        || this.user.role == UserRoles.Conscript
        || this.user.role == UserRoles.PotentialConscript
        || this.user.role == UserRoles.Volunteer
    }

    return false;
  }
}
