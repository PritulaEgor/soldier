import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserRoles } from 'src/app/common/models/enums/userRoles';
import { Civilian } from 'src/app/core/models/Civilian';
import { Employee } from 'src/app/core/models/Employee';
import { UserType } from 'src/app/core/models/enum/UserType';
import { ValidityGroups } from 'src/app/core/models/enum/ValidityGroups';
import { UserViewModel } from 'src/app/core/models/User';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss'],
})
export class UserDialogComponent implements OnInit {
  userType: string = '';
  userRole: string = '';

  isUserAdmin: boolean = false;

  user: UserViewModel = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    identityId: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    role: UserRoles.Guest,
    placeOfResidence: '',
    creatorId: '',
    passportIdentificationNumber: '',
    militaryId: '',
    medicalCardId: '',
    rank: '',
    serviceStartDate: '',
    workStartDate: '',
    temporaryPassword: this.GenerateTemporaryPassword(),
    userType: UserType.Guest
  };

  constructor(private userService: UserService,
    public dialogRef: MatDialogRef<UserDialogComponent>,
    private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.IsUserAdmin()
  }

  GenerateTemporaryPassword(): string {
    var text = '';
    var possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  setUserType() {
    if (this.userType == '1') {
      this.user.userType = UserType.Civilian
    }
    else if (this.userType == '0') {
      this.user.userType = UserType.Employee
    }
  }

  setUserRole() {
    if (this.userRole == '0') {
      this.user.role = UserRoles.PotentialConscript
    }
    else if (this.userRole == '1') {
      this.user.role = UserRoles.Volunteer
    }
    else if (this.userRole == '2') {
      this.user.role = UserRoles.Conscript
    }
    else if (this.userRole == '3') {
      this.user.role = UserRoles.Commissar
    }
    else if (this.userRole == '4') {
      this.user.role = UserRoles.MedicalStaff
    }
    else if (this.userRole == '5') {
      this.user.role = UserRoles.AccountantStaff
    }
    else if (this.userRole == '6') {
      this.user.role = UserRoles.ITStaff
    }
    else if (this.userRole == '7') {
      this.user.role = UserRoles.ITAdmin
    }
    else if (this.userRole == '8') {
      this.user.role = UserRoles.CommissariatStaff
    }
  }

  IsUserAdmin() {
    let creator = this.user;

    this.userService.getCurrent().subscribe({
      next: (user: UserViewModel) => {
        creator = user

        this.isUserAdmin =  creator.role == UserRoles.ITAdmin
            || creator.role == UserRoles.Commissar
      }
    });
  }

  submit() {

    this.setUserType();

    this.setUserRole();

    this.user.temporaryPassword = this.GenerateTemporaryPassword();

    this.userService.getCurrent().subscribe({
      next: (user: UserViewModel) => {
        this.user.creatorId = user.id;
      }
    });

    this.userService.create(this.user).subscribe(result => {
      if (result) {
        this.openSnackBar(result.passportNumber, result.temporaryPassword)


        this.dialogRef.close(result)
      }
      else {
        console.log('wrong')
      }
    })
  }

  clearGenerickFields() {

    this.userRole = '';
    this.user.rank = '';
    this.user.serviceStartDate = '';
    this.user.workStartDate = '';
    this.user.passportIdentificationNumber = '';
  }

  clearAllFields() {
    this.userType = '';
    this.userRole = '';

    this.user = {
      id: '',
      passportNumber: '',
      name: '',
      surname: '',
      patronymic: '',
      identityId: '',
      currentAddress: '',
      email: '',
      phoneNumber: '',
      role: UserRoles.Guest,
      placeOfResidence: '',
      creatorId: '',
      passportIdentificationNumber: '',
      militaryId: '',
      medicalCardId: '',
      rank: '',
      serviceStartDate: '',
      workStartDate: '',
      temporaryPassword: this.GenerateTemporaryPassword(),
      userType: UserType.Guest
    };
  }

  openSnackBar(passportNumber: string,
    temporaryPassword: string) {
    this.snackBar.open(`Пользователь с идентификационным номеров: ${passportNumber}, и временным паролем: ${temporaryPassword} успешно создан`, 'Закрыть')
  }
}
