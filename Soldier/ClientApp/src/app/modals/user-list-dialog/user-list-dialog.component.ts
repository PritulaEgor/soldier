import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import IUser from 'src/app/common/models/IUser';
import { Page } from 'src/app/common/models/Page';
import { PageInfo } from 'src/app/common/models/PageInfo';
import { UserType } from 'src/app/core/models/enum/UserType';
import { CivilianService } from 'src/app/core/services/civilian/civilian.service';
import { EmployeeService } from 'src/app/core/services/employee/employee.service';

@Component({
  selector: 'app-user-list-dialog',
  templateUrl: './user-list-dialog.component.html',
  styleUrls: ['./user-list-dialog.component.scss'],
})
export class UserListDialogComponent implements OnInit {
  step = '';

  constructor(
    private employeeService: EmployeeService,
    private civilianService: CivilianService
  ) {}

  userType = 'Civilian';

  users: IUser[] = [];

  pageInfo: PageInfo = {
    pageNumber: 1,
    pageSize: 10,
    filterBy: '',
    userType: UserType.Civilian,
  };

  usersCount = 10;

  ngOnInit() {
    this.civilianService.getCount().subscribe({
      next: (result: number) => {
        this.usersCount = result;
      },
      error: () => {
        this.usersCount = 0;
      },
    });

    this.civilianService.getPage(this.pageInfo).subscribe({
      next: (result: Page) => {
        this.users = <IUser[]>result.items;
      },
    });
  }

  setStep(number: string) {
    this.step = number;
  }

  handlePageChange(e: PageEvent) {
    this.pageInfo.pageNumber = e.pageIndex + 1;
    this.pageInfo.userType = this.getUserType();
    this.step = '';

    this.updateList();
  }

  getUserType(): UserType {
    var uType: string = UserType[1];

    if (uType === this.userType) {
      return UserType.Civilian;
    } else {
      return UserType.Employee;
    }
  }

  isUserAdmin(): boolean {
    return true;
  }

  resetPage() {
    this.pageInfo.pageNumber = 1;
  }

  updateList() {
    if (this.userType === 'Civilian') {
      this.civilianService.getCount().subscribe({
        next: (result: number) => {
          this.usersCount = result;
        },
        error: () => {
          this.usersCount = 0;
        },
      });

      this.civilianService.getPage(this.pageInfo).subscribe({
        next: (result: Page) => {
          this.users = <IUser[]>result.items;
        },
      });
    } else if (this.userType === 'Employee') {
      this.employeeService.getCount().subscribe({
        next: (result: number) => {
          this.usersCount = result;
        },
        error: () => {
          this.usersCount = 0;
        },
      });

      this.employeeService.getPage(this.pageInfo).subscribe({
        next: (result: Page) => {
          this.users = <IUser[]>result.items;
        },
      });
    }
  }
}
