import { Component, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Civilian } from 'src/app/core/models/Civilian';
import { ValidityGroups } from 'src/app/core/models/enum/ValidityGroups';
import { CivilianService } from 'src/app/core/services/civilian/civilian.service';

@Component({
  selector: 'app-validity-choose',
  templateUrl: './validity-choose.component.html',
  styleUrls: ['./validity-choose.component.scss']
})
export class ValidityChooseComponent {
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private data: string,
    private civilianService: CivilianService,
    private _bottomSheetRef: MatBottomSheetRef<ValidityChooseComponent>
    ) {}


    update(dGroup: number){

      let group: ValidityGroups = <ValidityGroups>dGroup

      this.civilianService.updateValidityGroup(this.data, group).subscribe(result=>{
        if(result){
          this._bottomSheetRef.dismiss(result);
        }
      })
    }
}
