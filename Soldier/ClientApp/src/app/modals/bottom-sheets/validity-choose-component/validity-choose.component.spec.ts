import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidityChooseComponent } from './validity-choose.component';

describe('ValidityChooseComponent', () => {
  let component: ValidityChooseComponent;
  let fixture: ComponentFixture<ValidityChooseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidityChooseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ValidityChooseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
