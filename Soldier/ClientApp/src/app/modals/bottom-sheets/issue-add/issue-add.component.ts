import { Component, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Issue } from 'src/app/core/models/Issue';
import { MedicalCard } from 'src/app/core/models/MedicalCard';
import { ValidityGroups } from 'src/app/core/models/enum/ValidityGroups';
import { MedicalCardService } from 'src/app/core/services/medical-card/medical-card.service';

@Component({
  selector: 'app-issue-add',
  templateUrl: './issue-add.component.html',
  styleUrls: ['./issue-add.component.scss']
})
export class IssueAddComponent {
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private data: string,
    private _bottomSheetRef: MatBottomSheetRef<IssueAddComponent>,
    private _medicalCardService: MedicalCardService
  ) { }

  issue: Issue = {
    id: '',
    name: '',
    description: '',
    dateOfDiagnosis: new Date(''),
    doctorData: '',
    medicalCardID: this.data,
    medicalCard: {
      id: '',
      validityGroup: ValidityGroups.А,
      issues: [],
      medicalExaminations: []
    }
  }

  add() {
    if(this.isFilled()){
      this._medicalCardService.addIssue(this.issue).subscribe(result=>{
        if(result){

          this._bottomSheetRef.dismiss(result);
        }
      })
    }
  }

  isFilled() {
    return this.issue.name != ''
      && this.issue.description != ''
      && this.issue.dateOfDiagnosis != new Date('')
      && this.issue.doctorData != ''
      && this.issue.medicalCardID != ''

  }
}
