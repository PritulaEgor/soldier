import { Component, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MedicalExamination } from 'src/app/core/models/MedicalExamination';
import { ValidityGroups } from 'src/app/core/models/enum/ValidityGroups';
import { MedicalCardService } from 'src/app/core/services/medical-card/medical-card.service';

@Component({
  selector: 'app-exam-add',
  templateUrl: './exam-add.component.html',
  styleUrls: ['./exam-add.component.scss']
})
export class ExamAddComponent {
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private data: string,
    private _bottomSheetRef: MatBottomSheetRef<ExamAddComponent>,
    private _medicalCardService: MedicalCardService
  ) { }

  medicalExamination: MedicalExamination = {
    id: '',
    examinationDate: new Date(''),
    examinationResult: '',
    medicalCardID: this.data,
    medicalCard: {
      id: '',
      validityGroup: ValidityGroups.А,
      issues: [],
      medicalExaminations: []
    }
  }

  add() {
    if(this.isFilled()){
      this._medicalCardService.addExamination(this.medicalExamination).subscribe(result=>{
        if(result){

          this._bottomSheetRef.dismiss(result);
        }
      })
    }
  }

  isFilled() {
    return this.medicalExamination.examinationResult != ''
      && this.medicalExamination.examinationDate != new Date('')
      && this.medicalExamination.medicalCardID != ''

  }
}
