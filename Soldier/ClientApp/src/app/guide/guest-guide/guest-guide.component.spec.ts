import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestGuideComponent } from './guest-guide.component';

describe('GuestGuideComponent', () => {
  let component: GuestGuideComponent;
  let fixture: ComponentFixture<GuestGuideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuestGuideComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GuestGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
