import { Component, Input, OnInit } from '@angular/core';
import { UserRoles } from '../common/models/enums/userRoles';
import IUser from '../common/models/IUser';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss'],
})
export class GuideComponent implements OnInit {
  opened = true;

  currentGuide = 'User';

  @Input() user: IUser = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    placeOfResidence: '',
    creatorId: '',
    role: UserRoles.Guest,
  };

  ngOnInit(): void {
    if (this.user && this.user.id != '') {
      switch (this.user.role) {
        case UserRoles.Guest:
          this.currentGuide = 'User';
          break;
        case UserRoles.Conscript ||
          UserRoles.PotentialConscript ||
          UserRoles.Volunteer:
          this.currentGuide = 'Civilian';
          break;
        case UserRoles.ITAdmin || UserRoles.Commissar:
          this.currentGuide = 'Admin';
          break;
        case UserRoles.AccountantStaff ||
          UserRoles.CommissariatStaff ||
          UserRoles.ITStaff ||
          UserRoles.MedicalStaff:
          this.currentGuide = 'Employee';
          break;
        default:
          this.currentGuide = 'User';
      }
    } else {
      this.currentGuide = 'User';
    }
  }

  isGuest() {
    return !this.user || this.user.role == UserRoles.Guest;
  }

  isGuestVisible() {
    return (
      !this.user ||
      this.user.role == UserRoles.Guest ||
      this.user.role == UserRoles.ITAdmin
    );
  }

  isVisible(type: string) {
    switch (type) {
      case 'User':
        return (
          !this.user ||
          this.user.role == UserRoles.ITAdmin ||
          this.user.role == UserRoles.Guest
        );
      case 'Civilian':
        return (
          this.user &&
          (this.user.role == UserRoles.ITAdmin ||
            this.user.role == UserRoles.Conscript ||
            this.user.role == UserRoles.PotentialConscript ||
            this.user.role == UserRoles.Volunteer)
        );
      case 'Admin':
        return (
          this.user &&
          (this.user.role == UserRoles.ITAdmin ||
            this.user.role == UserRoles.Commissar)
        );
      case 'Employee':
        return (
          this.user &&
          (this.user.role == UserRoles.AccountantStaff ||
            this.user.role == UserRoles.CommissariatStaff ||
            this.user.role == UserRoles.ITStaff ||
            this.user.role == UserRoles.MedicalStaff ||
            this.user.role == UserRoles.ITAdmin)
        );
      default:
        return false;
    }
  }

  toggle(val: boolean) {
    this.opened = val;
  }
}
