import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeGuideComponent } from './employee-guide.component';

describe('EmployeeGuideComponent', () => {
  let component: EmployeeGuideComponent;
  let fixture: ComponentFixture<EmployeeGuideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeGuideComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
