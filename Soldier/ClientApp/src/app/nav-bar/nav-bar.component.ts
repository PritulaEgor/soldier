import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserRoles } from '../common/models/enums/userRoles';
import { AuthService } from '../common/services/auth/auth.service';
import { ReportService } from '../common/services/report/report.service';
import { UserType } from '../core/models/enum/UserType';
import { UserViewModel } from '../core/models/User';
import { UserService } from '../core/services/user.service';
import { UserDialogComponent } from '../modals/user-dialog/user-dialog/user-dialog.component';
import { UserListDialogComponent } from '../modals/user-list-dialog/user-list-dialog.component';
import { TokenStorageService } from '../common/services/token/token.service';
import { Router } from '@angular/router';
import { SharedUserUpdateService } from '../common/services/shared-user-update.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  @Input() user: UserViewModel = {
    id: '',
    passportNumber: '',
    name: '',
    surname: '',
    patronymic: '',
    identityId: '',
    currentAddress: '',
    email: '',
    phoneNumber: '',
    role: UserRoles.Guest,
    placeOfResidence: '',
    creatorId: '',
    passportIdentificationNumber: '',
    militaryId: '',
    medicalCardId: '',
    rank: '',
    serviceStartDate: '',
    workStartDate: '',
    temporaryPassword: '',
    userType: UserType.Guest,
  };

  isExpanded = false;
  isCurrentUserAdmin = false;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private userService: UserService,
    private reportService: ReportService,
    private tokenService: TokenStorageService,
    private _sharedService: SharedUserUpdateService
  ) {
  }

  ngOnInit(): void {
    console.log('nav');
  }

  update() {
    this._sharedService.emitChange('update');
  }

  isUserAuthenticated(): boolean {
    return this.authService.isUserAuthenticated();
  }

  getUserReport() {
    this.reportService.getCompleteUserReport();
  }

  getCiviliansReport() {
    this.reportService.getCiviliansReport();
  }

  getEmployeesReport() {
    this.reportService.getEmployeesReport();
  }

  getDeviatorsReport() {
    this.reportService.getDeviatorsReport();
  }

  isUserAdmin(): boolean {
    this.isCurrentUserAdmin =
      this.user.role == UserRoles.ITAdmin ||
      this.user.role == UserRoles.Commissar;

    return this.isCurrentUserAdmin;
  }

  collapse() {
    this.isExpanded = false;
  }

  openUserDialog() {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: '750px',
      height: '900px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log(result);
      }
    });
  }

  routeToLogin() {
    this.update();

    this.router.navigate(['login']);
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  routeHome() {
    this.router.navigate(['/']);
  }

  openUserListDialog() {
    const dialogRef = this.dialog.open(UserListDialogComponent, {
      width: '750px',
      height: '750px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log(result);
      }
    });
  }

  goToProfile() {
    this.router.navigateByUrl('/profile');
  }

  goToHome() {
    this.router.navigateByUrl('');
  }

  logOut = () => {
    this.tokenService.removetokens();

    this.unsetUser();

    this.routeToLogin();
  };

  unsetUser() {
    UserService.unsetUser();

    this.user = {
      id: '',
      passportNumber: '',
      name: '',
      surname: '',
      patronymic: '',
      identityId: '',
      currentAddress: '',
      email: '',
      phoneNumber: '',
      role: UserRoles.Guest,
      placeOfResidence: '',
      creatorId: '',
      passportIdentificationNumber: '',
      militaryId: '',
      medicalCardId: '',
      rank: '',
      serviceStartDate: '',
      workStartDate: '',
      temporaryPassword: '',
      userType: UserType.Guest,
    };
  }
}
