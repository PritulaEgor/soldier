﻿namespace Soldier.Shared.Constants
{
    public static class CivilianKeys
    {
        public const string PassportNumber = "passportNumber";

        public const string Name = "name";

        public const string Surname = "surname";
    }
}
