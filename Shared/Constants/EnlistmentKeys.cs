﻿namespace Soldier.Shared.Constants
{
    public static class EnlistmentKeys
    {
        public const string StartDate = "startDate";

        public const string EndDate = "endDate";
    }
}
