﻿using Soldier.DAL.DAL.Models;
using Soldier.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq.Expressions;

namespace Soldier.Shared.Helpers.Sorting
{
    public class EmployeeSortingHelper
    {
        public static readonly ImmutableDictionary<string, Expression<Func<Employee, object>>> EmployeeSortingMapping =
               new Dictionary<string, Expression<Func<Employee, object>>>
               {
                {EmployeeKeys.Name, c => c.Name},
                {EmployeeKeys.PassportNumber, c => c.PassportNumber},
                {EmployeeKeys.Surname, c => c.Surname},
               }.ToImmutableDictionary();
    }
}
