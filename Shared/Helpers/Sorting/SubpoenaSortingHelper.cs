﻿using Soldier.DAL.DAL.Models;
using Soldier.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Shared.Helpers.Sorting
{
    public class SubpoenaSortingHelper
    {
        public static readonly ImmutableDictionary<string, Expression<Func<Subpoena, object>>> SubpoenaSortingMapping =
               new Dictionary<string, Expression<Func<Subpoena, object>>>
               {
                {SubpoenaKeys.Status, c => c.Status},
                {SubpoenaKeys.Sended, c => c.Sended},
               }.ToImmutableDictionary();
    }
}
