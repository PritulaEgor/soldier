﻿using Soldier.DAL.DAL.Models;
using Soldier.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Shared.Helpers.Sorting
{
    public class EnlistmentSortingHelper
    {
        public static readonly ImmutableDictionary<string, Expression<Func<Enlistment, object>>> EnlistmentSortingMapping =
               new Dictionary<string, Expression<Func<Enlistment, object>>>
               {
                {EnlistmentKeys.StartDate, c => c.StartDate},
                {EnlistmentKeys.EndDate, c => c.EndDate},
               }.ToImmutableDictionary();
    }
}
