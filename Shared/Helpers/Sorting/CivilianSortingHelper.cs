﻿using Soldier.DAL.DAL.Models;
using Soldier.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq.Expressions;

namespace Soldier.Shared.Helpers.Sorting
{
    public class CivilianSortingHelper
    {
        public static readonly ImmutableDictionary<string, Expression<Func<Civilian, object>>> CivilianSortingMapping =
            new Dictionary<string, Expression<Func<Civilian, object>>>
            {
                {CivilianKeys.Name, c => c.Name},
                {CivilianKeys.PassportNumber, c => c.PassportNumber},
                {CivilianKeys.Surname, c => c.Surname},
            }.ToImmutableDictionary();
    }
}
