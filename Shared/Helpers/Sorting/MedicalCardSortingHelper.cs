﻿using Soldier.DAL.DAL.Models;
using Soldier.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq.Expressions;

namespace Soldier.Shared.Helpers.Sorting
{
    public class MedicalCardSortingHelper
    {
        public static readonly ImmutableDictionary<string, Expression<Func<MedicalCard, object>>> MedicalCardSortingMapping =
                 new Dictionary<string, Expression<Func<MedicalCard, object>>>
                 {
                {MedicalCardKeys.ValidityGroup, c => c.ValidityGroup}
                 }.ToImmutableDictionary();
    }
}
