﻿using Soldier.Common.BaseIdentity.Emails.Models;
using Soldier.Common.Emails.Templates;
using System.Text;

namespace Soldier.Common.BaseIdentity.Emails.Templates
{
    public class AccountCreatedEmailTemplate : ImmidiateDeliverEmailTemplate
    {
        public override string TemplateName => "AccountCreatedEmailTemplate";

        public override string Subject => $"Your account successfuly created";

        public override string GetPlainView()
        {
            if (!(Model is AccountCreatedEmailModel model))
            {
                return string.Empty;
            }
            var message = new StringBuilder();

            message.AppendLine("Your account has been created succesfuly");

            message.AppendLine();
            message.AppendLine(
                $"Passport Id that has been used for registration: {model.PassportId}, Temporary password: {model.TemporaryPassword}");
            message.AppendLine("Dont forget to change password using the Personal Page");

            return message.ToString();
        }
    }
}
