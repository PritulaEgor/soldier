﻿using Soldier.Common.Emails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.BaseIdentity.Emails.Models
{
    public class AccountCreatedEmailModel : BaseEmailModel
    {
        public Guid Id { get; set; }

        public string PassportId { get; set; }

        public string TemporaryPassword { get; set; }
    }
}
