﻿using Soldier.Common.BaseIdentity.Emails.Models;
using Soldier.Common.BaseIdentity.Emails.Templates;
using Soldier.Common.Emails.Services.Soldier;
using Soldier.Common.Emails.Templates;
using System;

namespace Soldier.Common.BaseIdentity.Emails.Builders
{
    public class AccountCreatedEmailTemplateBuilder : IEmailTemplateBuilder
    {
        protected readonly Guid UserId;
        protected readonly string PassportId;
        protected readonly string TemporaryPassword;

        public AccountCreatedEmailTemplateBuilder(
            Guid userId,
            string passportId,
            string temporaryPassword)
        {
            UserId = userId;
            PassportId = passportId;
            TemporaryPassword = temporaryPassword;
        }

        public BaseEmailTemplate GetEmail()
        {
            return new AccountCreatedEmailTemplate
            {
                Model = new AccountCreatedEmailModel
                {
                    EventDate = DateTime.UtcNow,
                    Id = UserId,
                    TemporaryPassword = TemporaryPassword
                }
            };
        }
    }
}
