﻿using System.ComponentModel.DataAnnotations;

namespace Soldier.Common.BaseIdentity.ViewModels
{
    public class ResetPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
