﻿using System.ComponentModel.DataAnnotations;

namespace Soldier.Common.BaseIdentity.ViewModels
{
    public class BaseChangePasswordModel
    {
        [Required]
        [StringLength(32, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 8)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).*")]
        public string Password { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 8)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).*")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
