﻿using System.ComponentModel.DataAnnotations;

namespace Soldier.Common.BaseIdentity.ViewModels
{
    public class GeneralEnquiryModel
    {
        [Required]
        public string Message { get; set; }
    }
}
