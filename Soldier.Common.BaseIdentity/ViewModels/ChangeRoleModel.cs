﻿using Soldier.Common.Core.ViewModels;

namespace Soldier.Common.BaseIdentity.ViewModels
{
    public class ChangeRoleModel : BaseViewModel
    {
        public string Email { get; set; }

        public string TargetRole { get; set; }
    }
}
