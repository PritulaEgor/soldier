﻿using System;
using Soldier.Common.Core.DAL;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.IoC.Soldier;

namespace Soldier.Common.BaseIdentity
{
    public class IdentityModule<TUser, TUserRole, TRole> : IModule
        where TUser : BaseAppUser
        where TRole : IdentityRole<Guid>
        where TUserRole : IdentityUserRole<Guid>
    {
        protected readonly IConfiguration configuration;

        public IdentityModule(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Load(IServiceCollection services)
        {
            services.AddIdentity<TUser, TRole>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = true;
                    config.User.RequireUniqueEmail = true;
                    config.Lockout.AllowedForNewUsers = false;
                    config.Password.RequireDigit = true;
                    config.Password.RequiredLength = 8;
                    config.Password.RequireNonAlphanumeric = true;
                    config.Password.RequireLowercase = false;
                    config.Password.RequireUppercase = false;
                    config.Password.RequiredUniqueChars = 2;
                })
                //.AddEntityFrameworkStores<AppDbContext<TUser, TUserRole, TRole>>()
                .AddDefaultTokenProviders();
        }
    }
}
