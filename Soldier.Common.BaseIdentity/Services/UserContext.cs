﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Http;

namespace Soldier.Common.BaseIdentity.Services
{
    /// <summary>
    /// Encapsulate works with http context to provide User Id
    /// </summary>
    public class UserContext
    {
        protected readonly IHttpContextAccessor httpContextAccessor;

        public UserContext(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public virtual void ExecuteInUserContext(Guid userId)
        {
            httpContextAccessor.HttpContext.User.AddIdentity(
                new ClaimsIdentity(new List<Claim> { new Claim(ClaimTypes.NameIdentifier, userId.ToString()) })
            );
        }

        public virtual string GetActualUserPassport()
        {
            var userPassportValue = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name).Value;

            return string.IsNullOrEmpty(userPassportValue) ? null : userPassportValue;
        }

        //public virtual Guid? GetAdoptedUserId()
        //{
        //    if (IsUserAdopted())
        //    {
        //        //var adoptedIdentity = httpContextAccessor.HttpContext.Request.Headers[RequestKeys.AdoptedIdentityKey];
        //        //return Guid.Parse(adoptedIdentity[0]);
        //    }
        //    return GetActualUserPassport();
        //}

        public virtual Guid GetUserId()
        {
            return Guid.Parse(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        public bool IsUserAdopted()
        {
            if (httpContextAccessor.HttpContext.Request != null)
            {
                //var adoptedIdentity = httpContextAccessor.HttpContext.Request.Headers[RequestKeys.AdoptedIdentityKey];
                //return HasAdoptedIdentityCount(adoptedIdentity);
            }
            return false;
        }

        public virtual string GetUserIPAddress()
        {
            return httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        private bool HasAdoptedIdentityCount(StringValues adoptedIdentity)
        {
            return adoptedIdentity.Count != 0 && adoptedIdentity[0] != null;
        }
    }
}
