﻿using System;
using System.Threading.Tasks;
using Soldier.Common.BaseIdentity.Services.Contracts;
using Soldier.Common.BaseIdentity.ViewModels;
using Soldier.Common.Core.DAL;
using Microsoft.AspNetCore.Identity;
using Soldier.Common.Core.Services.Interfaces;

namespace Soldier.Common.BaseIdentity.Services
{
    public class AccountSecurityService<TUser> : IAccountSecurityService
        where TUser : BaseAppUser
    {
        protected readonly UserManager<TUser> userManager;
        protected readonly ICurrentUserService<TUser> currentUserService;

        public AccountSecurityService(UserManager<TUser> userManager,
            ICurrentUserService<TUser> currentUserService)
        {
            this.userManager = userManager;
            this.currentUserService = currentUserService;
        }

        public virtual async Task SendResetPasswordEmail(ResetPasswordModel model)
        {
            var user = await userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                //throw new UserDoesNotExistException(model.Email);
            }
            await userManager.UpdateAsync(user);
            var code = await userManager.GeneratePasswordResetTokenAsync(user);
            //await postmanService.Send(new[] {user.Email}, new ResetPasswordEmailTemplateBuilder(user.Id, code));
        }

        public virtual async Task ChangePassword(ChangePasswordModel model)
        {
            var user = await userManager.FindByIdAsync(model.UserId.ToString());
            await userManager.ResetPasswordAsync(user, model.Code, model.Password);
            user = await userManager.FindByIdAsync(model.UserId.ToString());
            await userManager.UpdateAsync(user);
        }

        public virtual async Task UpdatePassword(UpdatePasswordModel model)
        {
            var user = currentUserService.GetActualUser("");
            var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                //throw new PasswordNotChangedException();
            }
        }

        public virtual async Task MarkPasswordAsCompromised(Guid userId)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            await userManager.UpdateAsync(user);
            await SendResetPasswordEmail(new ResetPasswordModel {Email = user.Email});
        }
    }
}
