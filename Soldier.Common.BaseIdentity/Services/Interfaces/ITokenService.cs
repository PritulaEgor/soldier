﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Soldier.Common.BaseIdentity.Services.Interfaces
{
    public interface ITokenService
    {
        string GenerateAccessToken(IEnumerable<Claim> claims);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);

        string GetPasssportNumberFromToken(string token);
    }
}
