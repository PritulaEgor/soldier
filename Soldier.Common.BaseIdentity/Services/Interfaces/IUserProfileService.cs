﻿using Soldier.Common.BaseIdentity.ViewModels;

namespace Soldier.Common.BaseIdentity.Services.Contracts
{
    public interface IUserProfileService
    {
        void Update(UpdateUserProfileModel model);
    }
}
