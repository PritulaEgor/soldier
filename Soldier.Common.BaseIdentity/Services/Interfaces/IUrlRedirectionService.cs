﻿using System;

namespace Soldier.Common.BaseIdentity.Services.Contracts
{
    public interface IUrlRedirectionService
    {
        string RedirectToLoginPage();

        string RedirectToVerificationExpired();

        string RedirectToSetupPassword(Guid userId, string code);
    }
}
