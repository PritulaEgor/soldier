﻿using System;
using System.Threading.Tasks;
using Soldier.Common.BaseIdentity.ViewModels;

namespace Soldier.Common.BaseIdentity.Services.Contracts
{
    public interface IAccountSecurityService
    {
        /// <summary>
        /// Sends the reset password email.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task SendResetPasswordEmail(ResetPasswordModel model);

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task ChangePassword(ChangePasswordModel model);

        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task UpdatePassword(UpdatePasswordModel model);

        /// <summary>
        /// Marks the password as compromised.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task MarkPasswordAsCompromised(Guid userId);
    }
}
