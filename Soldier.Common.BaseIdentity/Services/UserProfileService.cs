﻿using Soldier.Common.BaseIdentity.Services.Contracts;
using Soldier.Common.BaseIdentity.ViewModels;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Core.UnitOfWork.Contracts;

namespace Soldier.Common.BaseIdentity.Services
{
    public class UserProfileService<TUser> : IUserProfileService
        where TUser : BaseAppUser
    {
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly ICurrentUserService<TUser> currentUserService;
        protected readonly IUnitOfWorkFactory uowFactory;

        public UserProfileService(IGenericRepository<TUser> userRepository,
            ICurrentUserService<TUser> currentUserService,
            IUnitOfWorkFactory uowFactory)
        {
            this.userRepository = userRepository;
            this.currentUserService = currentUserService;
            this.uowFactory = uowFactory;
        }

        public void Update(UpdateUserProfileModel model)
        {
            var user = currentUserService.GetActualUser("");
            
            //(Soldier)add updating

            using (uowFactory.Create())
            {
                userRepository.Update(user);
            }
        }
    }
}
