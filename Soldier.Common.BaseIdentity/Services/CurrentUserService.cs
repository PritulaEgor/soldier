﻿using System;
using System.Linq;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.DAL.Soldier;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Http;
using Soldier.Common.BaseIdentity.Services.Interfaces;

namespace Soldier.Common.BaseIdentity.Services
{
    /// <summary>
    /// Service provide ability to get necessary info about current user
    /// </summary>
    public class CurrentUserService<TUser> : ICurrentUserService<TUser>
        where TUser : BaseAppUser
    {
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly UserContext userContext;
        protected readonly ITokenService _tokenService;

        public CurrentUserService(
            IGenericRepository<TUser> userRepository,
            UserContext userContext,
            ITokenService tokenService
            )
        {
            this.userRepository = userRepository;
            this.userContext = userContext;
            _tokenService = tokenService;
        }

        public virtual TUser GetCurrentUser(HttpRequest request)
        {
            StringValues token;

            request.Headers.TryGetValue("Authorization", out token);

            var finalToken = token.ToString().Split(' ');

            var passportValue = _tokenService.GetPasssportNumberFromToken(finalToken[1]);

            var actualUser = GetActualUser(passportValue);

            return actualUser;
        }

        public virtual TUser GetActualUser(string userPassportId)
        {
            return string.IsNullOrEmpty(userPassportId)
                ? null
                : userRepository.GetBy(u => u.PassportNumber.Equals(userPassportId)).FirstOrDefault();
        }

        public virtual string GetUserIPAddress()
        {
            return userContext.GetUserIPAddress();
        }
    }
}
