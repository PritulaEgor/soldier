﻿using Microsoft.IdentityModel.Tokens;
using Soldier.Common.BaseIdentity.Services.Interfaces;
using Soldier.Common.Core.Constants;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Soldier.Common.BaseIdentity.Services
{
    public class TokenService : ITokenService
    {
        public string GenerateAccessToken(IEnumerable<Claim> claims)
        {
            var actualTime = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: AuthConfigs.ISSUER,
                audience: AuthConfigs.AUDIENCE,
                notBefore: actualTime,
                claims: claims,
                expires: actualTime.Add(TimeSpan.FromMinutes(AuthConfigs.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthConfigs.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encJwt;
        }

        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);

                return Convert.ToBase64String(randomNumber);
            }
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = AuthConfigs.GetSymmetricSecurityKey(),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken securityToken;

            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);

            var jwtSecurityToken = securityToken as JwtSecurityToken;

            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token");
            }
            return principal;
        }

        public string GetPasssportNumberFromToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();

            var jwtSecurityToken = handler.ReadJwtToken(token);

            var passport = jwtSecurityToken.Claims.First(claim => claim.Type == ClaimsIdentity.DefaultNameClaimType).Value;

            return passport;
        }
    }
}
