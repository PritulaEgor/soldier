﻿using Soldier.Common.BaseIdentity.Services;
using Soldier.Common.BaseIdentity.Services.Contracts;
using Soldier.Common.Core.DAL;
using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Core.Services.Interfaces;

namespace Soldier.Common.BaseIdentity
{
    public class UserManagementModule<TUser> : IModule
        where TUser : BaseAppUser
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<ICurrentUserService<TUser>, CurrentUserService<TUser>>();
            services.AddSingleton<UserContext>();
        }
    }
}
