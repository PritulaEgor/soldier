﻿using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.BaseIdentity.Services;
using Soldier.Common.BaseIdentity.Services.Interfaces;
using Soldier.Common.Core.IoC.Soldier;

namespace Soldier.Common.BaseIdentity.Modules
{
    public class TokenHandlingModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<ITokenService, TokenService>();
        }
    }
}
