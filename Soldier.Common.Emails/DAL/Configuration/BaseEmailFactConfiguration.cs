﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Soldier.Common.Emails.DAL.Configuration
{
    public class BaseEmailFactConfiguration<TEmailFact> : IEntityTypeConfiguration<TEmailFact>
        where TEmailFact : BaseEmailFact
    {
        public void Configure(EntityTypeBuilder<TEmailFact> builder)
        {
            builder.HasIndex(ef => ef.DeliverTime);
        }
    }
}
