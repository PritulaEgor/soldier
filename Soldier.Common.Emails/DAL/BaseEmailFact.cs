﻿using System;
using System.ComponentModel.DataAnnotations;
using Soldier.Common.Core.DAL;

namespace Soldier.Common.Emails.DAL
{
    public abstract class BaseEmailFact : Entity
    {
        public Guid UserId { get; set; }

        [Required]
        public DateTime DeliverTime { get; set; }

        [Required]
        [MaxLength(10000)]
        public string Model { get; set; }
    }
}
