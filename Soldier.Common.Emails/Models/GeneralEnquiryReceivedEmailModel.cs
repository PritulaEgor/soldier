﻿namespace Soldier.Common.Emails.Models
{
    public class GeneralEnquiryReceivedEmailModel : BaseEmailModel
    {
        public string Message { get; set; }

        public string UserEmail { get; set; }
    }
}
