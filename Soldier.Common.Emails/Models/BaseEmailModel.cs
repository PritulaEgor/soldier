﻿using System;
using Soldier.Common.Emails.Models.Configs;

namespace Soldier.Common.Emails.Models
{
    public abstract class BaseEmailModel
    {
        public ConfigProperties Properties { get; set; }

        public DateTime EventDate { get; set; }
    }
}
