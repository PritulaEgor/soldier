﻿namespace Soldier.Common.Emails.Models.Configs
{
    public class ConfigProperties
    {
        public string AppUrl { get; set; }

        public string Header { get; set; }

        public string AppName { get; set; }
    }
}
