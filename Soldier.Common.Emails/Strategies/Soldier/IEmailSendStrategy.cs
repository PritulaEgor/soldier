﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Soldier.Common.Emails.Templates;

namespace Soldier.Common.Emails.Strategies.Soldier
{
    /// <summary>
    /// Interface for strategy, which encapsulate logic about sending emails
    /// </summary>
    public interface IEmailSendStrategy
    {
        Task ProcessEmail(IEnumerable<string> to, BaseEmail emailTemplate);
    }
}
