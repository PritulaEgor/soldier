﻿using System;
using System.Threading.Tasks;
using LR.Common.Core.Exceptions;
using LR.Common.Documents.DAL;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LR.Common.Web.Documents.Controllers
{
    public class BaseDocumentController : Controller
    {
        protected readonly IDocumentService documentService;

        public BaseDocumentController(IDocumentService documentService)
        {
            this.documentService = documentService;
        }

        /// <summary>
        /// Uploads a form file to database and file storage under a document group
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        /// <returns>
        /// DocumentModel
        /// </returns>
        [HttpDelete]
        [Authorize]
        public virtual async Task<IActionResult> RemoveDocument(Guid documentId)
        {
            await documentService.RemoveDocument(documentId);
            return Ok();
        }

        protected virtual IActionResult GetDocumentsForReport<TDocument>(Guid reportId) where TDocument: BaseDocument
        {
            return new JsonResult(documentService.GetDocumentsForReport<TDocument>(reportId));
        }

        protected virtual async Task<DocumentModel> SaveDocument(BaseDocumentUploadModel uploadModel)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }
            using (uploadModel.Stream)
            {
                return await documentService.SaveNewDocument(uploadModel);
            }
        }
    }
}
