﻿using System;
using System.Net;
using System.Threading.Tasks;
using Soldier.Common.Core;
using Soldier.Common.Core.Exceptions;
using Soldier.Common.Core.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Soldier.Common.Web.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        protected readonly RequestDelegate next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext, ILogService logService)
        {
            try
            {
                await next(httpContext);
            }
            catch (MrvApplicationException ex)
            {
                await HandleApplicationExceptionAsync(httpContext, logService, ex);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, logService, ex);
            }
        }

        private Task HandleApplicationExceptionAsync(HttpContext context, ILogService logService, MrvApplicationException exception)
        {
            var result = JsonConvert.SerializeObject(new { exception.ErrorCode, Data = exception.ErrorData });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)exception.StatusCode;
            if (exception.InnerException != null)
            {
                logService.LogError(exception.InnerException);
            }
            return context.Response.WriteAsync(result);
        }

        private Task HandleExceptionAsync(HttpContext context, ILogService logService, Exception exception)
        {
            logService.LogError(exception);
            var result = JsonConvert.SerializeObject(new { ErrorCode = ErrorCodes.UnknownServerError, Data = logService.CorrelationId.ToString() });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return context.Response.WriteAsync(result);
        }
    }
}
