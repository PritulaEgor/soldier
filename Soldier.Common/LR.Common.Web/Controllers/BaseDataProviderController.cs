﻿using System;
using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Core.DataProviders.Models;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Exceptions;
using Soldier.Common.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Soldier.Common.Web.Controllers
{
    public abstract class BaseDataProviderController<TEntity, TModel> : Controller where TEntity : class, IEntity where TModel : BaseViewModel
    {
        protected IDataProvider<TEntity, TModel> dataProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseDataProviderController{TEntity, TModel}"/> class.
        /// </summary>
        /// <param name="dataProvider">The data provider.</param>
        protected BaseDataProviderController(IDataProvider<TEntity, TModel> dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        [HttpGet]
        public virtual IActionResult Get([FromQuery] PageInfo pageInfo)
        {
            var page = dataProvider.Get(pageInfo);
            return new JsonResult(page);
        }

        [HttpGet]
        public virtual IActionResult GetById([FromQuery] Guid id)
        {
            var data = dataProvider.GetById(id);
            return new JsonResult(data);
        }

        [HttpGet]
        public virtual IActionResult Filter([FromQuery] string filter)
        {
            var page = dataProvider.GetByFilter(filter);
            return new JsonResult(page);
        }

        [HttpPost]
        public virtual IActionResult Save([FromBody] TModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            var createdModel = dataProvider.Create(model);
            return new JsonResult(createdModel);
        }

        [HttpPut]
        public virtual IActionResult Update([FromBody] TModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            dataProvider.Update(model);
            return Ok();
        }

        [HttpPost]
        public virtual IActionResult Delete([FromBody] TModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            dataProvider.Delete(model);
            return Ok();
        }
    }
}
