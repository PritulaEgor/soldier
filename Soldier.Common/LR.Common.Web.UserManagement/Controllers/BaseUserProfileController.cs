﻿using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.Exceptions;
using LR.Common.Web.Filters;
using Microsoft.AspNetCore.Mvc;

namespace LR.Common.Web.UserManagement.Controllers
{
    [ServiceFilter(typeof(LogActionFilterAttribute))]
    public class BaseUserProfileController : Controller
    {
        protected readonly IUserProfileService userProfileService;

        public BaseUserProfileController(IUserProfileService userProfileService)
        {
            this.userProfileService = userProfileService;
        }

        [HttpPost]
        public virtual IActionResult UpdateUser([FromBody] UpdateUserProfileModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            userProfileService.Update(model);
            return Ok();
        }
    }
}
