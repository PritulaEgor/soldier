﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.Exceptions;
using LR.Common.Notifications.Services.Contracts;
using LR.Common.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LR.Common.Web.UserManagement.Controllers
{
    public class BaseAccountManagementController : Controller
    {
        protected readonly IAccountManagementService accountService;
        protected readonly INotificationService notificationService;
        protected readonly IAccountAccessibilityService accountAccessibilityService;
        protected readonly IAccountSecurityService accountSecurityService;
        protected readonly IAccountEmailService accountEmailService;

        protected BaseAccountManagementController(IAccountManagementService accountService,
            INotificationService notificationService, IAccountAccessibilityService accountAccessibilityService,
            IAccountSecurityService accountSecurityService, IAccountEmailService accountEmailService)
        {
            this.accountService = accountService;
            this.notificationService = notificationService;
            this.accountAccessibilityService = accountAccessibilityService;
            this.accountSecurityService = accountSecurityService;
            this.accountEmailService = accountEmailService;
        }

        [HttpPost]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> CreateUser([FromBody] CreateUserModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            model.CreatorId = Guid.Parse(currentUserId);
            var user = await accountService.CreateUser(model);
            await accountEmailService.SendEmailConfirm(user.Email);
            return user;
        }

        [HttpPost]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> DeleteUser([FromBody] DeleteUserModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            await accountService.DeleteUser(model.Id);
            return Ok();
        }

        [HttpGet]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> DisableUser(Guid id)
        {
            await accountAccessibilityService.BlockUser(id);
            return Ok();
        }

        [HttpGet]
        [Authorize]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual object GetCurrentUser()
        {
            return accountService.GetCurrentUser();
        }

        [HttpGet]
        [Authorize(Roles = "SuperUser, ClientManager, TechnicalSpecialist, ShipManager")]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> EnableUser(Guid id)
        {
            await accountAccessibilityService.UnlockUser(id);
            return Ok();
        }

        [HttpGet]
        [Authorize]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> SendVerificationEmail(Guid id)
        {
            await accountEmailService.SendVerificationEmail(id);
            return Ok();
        }

        [HttpPost]
        [Authorize]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> SendGeneralEnquiryEmail([FromBody] GeneralEnquiryModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }
            await accountEmailService.SendGeneralEnquiry(model.Message);
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> ResendVerificationEmail(string email)
        {
            await accountEmailService.SendVerificationEmail(email);
            return Ok();
        }

        [HttpGet]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> MarkPasswordAsCompromised(Guid id)
        {
            await accountSecurityService.MarkPasswordAsCompromised(id);
            return Ok();
        }

        [HttpPost]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> UpdateUser([FromBody] UserProfileModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }
            var shouldSendVerificationEmail = await accountService.Update(model);
            if (shouldSendVerificationEmail)
            {
                await accountEmailService.SendEmailConfirm(model.Email);
            }
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<object> ResetPassword([FromBody] ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            await accountSecurityService.SendResetPasswordEmail(model);
            return Ok();
        }

        [HttpPost]
        public virtual async Task<object> UpdatePassword([FromBody] UpdatePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }
            await accountSecurityService.UpdatePassword(model);
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual async Task<object> ChangePassword([FromBody] ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            await accountSecurityService.ChangePassword(model);
            return Ok();
        }

        [HttpGet]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual async Task<IActionResult> VerifyEmail(string code, Guid userId)
        {
            var result = await accountAccessibilityService.VerifyEmail(userId, code);
            return Redirect(result);
        }

        [HttpPost]
        [AllowAnonymous]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public virtual IActionResult AcceptAgreement([FromBody] AcceptAgreementModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            notificationService.SubscribeUserForAllNotifications(model.UserId);
            return Ok();
        }
    }
}
