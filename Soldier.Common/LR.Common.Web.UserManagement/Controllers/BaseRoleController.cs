﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace LR.Common.Web.UserManagement.Controllers
{
    public class BaseRoleController<TUser> : Controller
        where TUser : BaseAppUser
    {
        protected readonly IRoleService<TUser> roleService;

        public BaseRoleController(IRoleService<TUser> roleService)
        {
            this.roleService = roleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetRoles(string targetUserEmail)
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var availableRoles = await roleService.GetPossibleAssignRole(Guid.Parse(currentUserId), targetUserEmail);
            return new JsonResult(availableRoles);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserRole(string email)
        {
            var role = await roleService.GetUserRole(email);
            return new JsonResult(role);
        }

        [HttpPost]
        public async Task<object> SetRole([FromBody] ChangeRoleModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            var currentUserEmail = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            await roleService.AssignToRole(currentUserEmail, model);
            return Ok();
        }
    }
}
