﻿using System;
using LR.Common.AuditEvents.DAL;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LR.Common.Web.UserManagement.Filters
{
    public abstract class BaseAcceptAgreementFilterAttribute : ActionFilterAttribute
    {
        protected readonly IUnitOfWorkFactory uowFactory;
        protected readonly IGenericRepository<BaseAuditEvent> auditLogRepository;

        protected AcceptAgreementModel AgreementModel { get; set; }

        protected BaseAcceptAgreementFilterAttribute(IUnitOfWorkFactory uowFactory,
            IGenericRepository<BaseAuditEvent> auditLogRepository)
        {
            this.uowFactory = uowFactory;
            this.auditLogRepository = auditLogRepository;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            AgreementModel = context.ActionArguments["model"] as AcceptAgreementModel;
            base.OnActionExecuting(context);
        }

        public override void OnResultExecuted(ResultExecutedContext context)
        {
            if (AgreementModel != null && AgreementModel.UserId != Guid.Empty)
            {
                var user = GetUser(AgreementModel);
                var agreementAccepted = new AgreementAcceptedAuditEvent
                {
                    EventDate = DateTime.UtcNow,
                    UserId = user.Id
                };
                using (uowFactory.Create())
                {
                    auditLogRepository.Create(agreementAccepted);
                }
            }

            base.OnResultExecuted(context);
        }

        protected abstract BaseAppUser GetUser(AcceptAgreementModel model);
    }
}
