﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Repositories.Soldier;
using Microsoft.EntityFrameworkCore;

namespace Soldier.Common.Core.Repositories
{
    public class GenericViewRepository<TViewRow> : IGenericViewRepository<TViewRow> where TViewRow: class
    {
        protected readonly IDbContext dbContext;

        public GenericViewRepository(IDbContextProvider contextProvider)
        {
            dbContext = contextProvider.GetContext();
        }

        protected DbQuery<TViewRow> GetQuerySet()
        {
            return dbContext.Query<TViewRow>();
        }

        public IQueryable<TViewRow> GetPage(int pageNumber, int pageSize, Expression<Func<TViewRow, bool>> predicate,
            Func<IQueryable<TViewRow>, IQueryable<TViewRow>> sortExpression)
        {
            return GetPage(pageNumber, pageSize, predicate, null, sortExpression);
        }

        public IQueryable<TViewRow> GetPage(int pageNumber, int pageSize, Expression<Func<TViewRow, bool>> predicate)
        {
            return GetPage(pageNumber, pageSize, predicate, null, null);
        }

        public IQueryable<TViewRow> GetPage(int pageNumber, int pageSize, Func<IQueryable<TViewRow>, IQueryable<TViewRow>> sortExpression)
        {
            return GetPage(pageNumber, pageSize, null, null, sortExpression);
        }

        public IQueryable<TViewRow> GetPage(int pageNumber, int pageSize, Expression<Func<TViewRow, bool>> predicate,
            Func<IQueryable<TViewRow>, IQueryable<TViewRow>> includes,
            Func<IQueryable<TViewRow>, IQueryable<TViewRow>> sortExpression)
        {
            var query = GetQuerySet().AsQueryable();
            query = sortExpression == null ? query : sortExpression(query);
            query = includes == null
                ? query
                : includes(query);
            query = predicate == null
                ? query
                : query.Where(predicate).AsQueryable();
            return query.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public int Count()
        {
            return GetQuerySet().Count();
        }

        public int Count(Expression<Func<TViewRow, bool>> predicate, Func<IQueryable<TViewRow>, IQueryable<TViewRow>> includes = null)
        {
            var query = includes == null ? GetQuerySet() : includes(GetQuerySet());
            return query.Count(predicate);
        }

        public IQueryable<TViewRow> GetBy(Expression<Func<TViewRow, bool>> predicate)
        {
            return GetQuerySet().Where(predicate).AsQueryable();
        }
    }
}
