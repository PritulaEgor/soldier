﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Common.Core.Repositories.Soldier
{
    public interface IPageableRepository<TItem>
    {
        IQueryable<TItem> GetPage(int pageNumber, int pageSize, Expression<Func<TItem, bool>> predicate,
            Func<IQueryable<TItem>, IQueryable<TItem>> sortExpression);

        IQueryable<TItem> GetPage(int pageNumber, int pageSize, Expression<Func<TItem, bool>> predicate);

        IQueryable<TItem> GetPage(int pageNumber, int pageSize,
            Func<IQueryable<TItem>, IQueryable<TItem>> sortExpression);

        IQueryable<TItem> GetPage(int pageNumber, int pageSize, Expression<Func<TItem, bool>> predicate,
            Func<IQueryable<TItem>, IQueryable<TItem>> includes,
            Func<IQueryable<TItem>, IQueryable<TItem>> sortExpression);
    }
}
