﻿using System;
using System.Net;

namespace Soldier.Common.Core.Exceptions
{
    [Serializable]
    public class MailSendingFailedException : SoldierApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.NotAcceptable;
        public override int ErrorCode => ErrorCodes.EmailSendingFailed;
        public override string ErrorData => string.Empty;

        public MailSendingFailedException(string message, Exception ex) : base(message, ex)
        {

        }
    }
}
