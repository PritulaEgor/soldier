﻿using System;
using System.Net;

namespace Soldier.Common.Core.Exceptions
{
    [Serializable]
    public class ModelNotValidException : SoldierApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.ValidationError;

        public override string ErrorData => String.Empty;
    }
}
