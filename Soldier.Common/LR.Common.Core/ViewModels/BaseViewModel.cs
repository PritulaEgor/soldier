﻿using System;

namespace Soldier.Common.Core.ViewModels
{
    public class BaseViewModel
    {
        public Guid Id { get; set; }
    }
}
