﻿using System;

namespace Soldier.Common.Core.UnitOfWork.Contracts
{
    /// <summary>
    /// Unit of work interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Commits all made changes.
        /// </summary>
        void Commit();
    }
}
