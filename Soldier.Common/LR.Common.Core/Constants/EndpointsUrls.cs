﻿namespace Soldier.Common.Core.Constants
{
    public static class EndpointsUrls
    {
        public static readonly string LoginUrl = "/login";

        public static readonly string VerificationExpiredUrl = "/verificationExpired";

        public static readonly string SetupPasswordUrl = "/setupPassword";
    }
}