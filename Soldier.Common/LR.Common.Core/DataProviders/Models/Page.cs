﻿using System.Collections.Generic;

namespace Soldier.Common.Core.DataProviders.Models
{
    public class Page<TModel>
    {
        public int Number { get; set; }

        public int Size { get; set; }

        public int TotalCount { get; set; }

        public List<TModel> Items { get; set; }
    }
}
