﻿using System;
using System.Collections.Generic;
using Soldier.Common.Core.DataProviders.Models;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.ViewModels;

namespace Soldier.Common.Core.DataProviders.Soldier
{
    public interface IDataProvider<TEntity, TModel> where TEntity : class, IEntity
                                                    where TModel: BaseViewModel
    {
        Page<TModel> Get(PageInfo pageInfo);

        TModel Create(TModel model);

        void Delete(TModel model);

        void Update(TModel model);

        TModel GetById(Guid id);
        List<TModel> GetByFilter(string value);
    }
}
