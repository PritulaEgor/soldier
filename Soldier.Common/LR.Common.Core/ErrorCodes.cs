﻿namespace Soldier.Common.Core
{
    public static class ErrorCodes
    {
        public static readonly int UnknownServerError = 505;
        public static readonly int ValidationError = 1001;
        public static readonly int EmailSendingFailed = 1016;
        public static readonly int UserNotAuthorized = 1013;
        public static readonly int FileZeroLength = 4001;
        public static readonly int FileDocumentNotSaved = 4003;
        public static readonly int FileNameTooLong = 4009;
    }
}
