﻿using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Core.Services;
using Soldier.Common.Core.Services.Soldier;
using Soldier.Common.Core.UnitOfWork;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Soldier.Common.Core.Modules
{
    public class CoreServicesModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IMailSenderService, SmtpSenderService>();
            services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddTransient<ILogService, LogService>();
        }
    }
}
