﻿using Soldier.Common.Core.DAL;

namespace Soldier.Common.Core.Services.Soldier
{
    public interface ICurrentUserService<out TUser> where TUser : BaseAppUser
    {
        TUser GetCurrentUser();

        TUser GetActualUser();

        string GetUserIPAddress();
    }
}
