﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Soldier.Common.Core.Services.Soldier
{
    public interface IAuditLogService
    {
        void LogAuditEntitiesChangeEvents(IEnumerable<EntityEntry> entries);
    }
}