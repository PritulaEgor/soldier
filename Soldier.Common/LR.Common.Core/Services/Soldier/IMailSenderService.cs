﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Services.Soldier
{
    public interface IMailSenderService
    {
        Task SendEmail(string to, MailMessage mailMessage);

        Task SendEmail(IEnumerable<string> toList, MailMessage mailMessage);
    }
}
