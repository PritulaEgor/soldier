﻿using System;
using System.Collections.Generic;
using Soldier.Common.Core.Constants;
using Soldier.Common.Core.Services.Soldier;
using Newtonsoft.Json;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Core.Enrichers;

namespace Soldier.Common.Core.Services
{
    public class LogService : ILogService
    {
        protected readonly Guid correlationId = Guid.NewGuid();

        public Guid CorrelationId => correlationId;

        public void LogRequest(string displayName, IDictionary<string, object> actionArguments)
        {
            LogInfo($"Method: {displayName}. Parameters: {JsonConvert.SerializeObject(actionArguments)}");
        }

        public void LogInfo(string message)
        {
            using (LogContext.Push(GetProperties()))
            {
                Log.Information(message);
            }
        }

        public void LogError(Exception e)
        {
            LogError(e.Message, e);
        }

        public void LogError(string message, Exception e)
        {
            using (LogContext.Push(GetProperties()))
            {
                Log.Error(e, message);
            }
        }

        protected virtual ILogEventEnricher[] GetProperties()
        {
            var properties = new List<ILogEventEnricher>
            {
                new PropertyEnricher(LogAttributeKeys.IdName, CorrelationId)
            };
            return properties.ToArray();
        }
    }
}
