﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Soldier.Common.Core.Configurations;
using Soldier.Common.Core.Exceptions;
using Soldier.Common.Core.Services.Soldier;
using Microsoft.Extensions.Options;

namespace Soldier.Common.Core.Services
{
    public class SmtpSenderService : IMailSenderService
    {
        protected readonly SmtpConfig smtpConfig;
        protected readonly ILogService logService;

        public SmtpSenderService(IOptions<SmtpConfig> smtpConfigOptions, ILogService logService)
        {
            smtpConfig = smtpConfigOptions.Value;
            this.logService = logService;
        }

        public async Task SendEmail(string to, MailMessage mailMessage)
        {
            if (string.IsNullOrWhiteSpace(to))
            {
                return;
            }

            mailMessage.From = new MailAddress(smtpConfig.NoReply);

            mailMessage.To.Clear();
            mailMessage.To.Add(to);
            try
            {
                using (var smtpClient = GetSmtpClient())
                {
                    await smtpClient.SendMailAsync(mailMessage);
                }
            }
            catch (SmtpException ex)
            {
                logService.LogError(ex);
                throw new MailSendingFailedException(ex.Message, ex);
            }
        }

        public async Task SendEmail(IEnumerable<string> toList, MailMessage mailMessage)
        {
            mailMessage.From = new MailAddress(smtpConfig.NoReply);
            var tasks = toList.Where(t => !string.IsNullOrWhiteSpace(t)).Distinct()
                .Select(to => SendEmail(to, mailMessage));
            await Task.WhenAll(tasks);
        }

        protected SmtpClient GetSmtpClient()
        {
            return new SmtpClient(smtpConfig.SmtpHost, smtpConfig.SmtpPort)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(smtpConfig.Username, smtpConfig.Password),
                EnableSsl = smtpConfig.EnableSsl
            };
        }
    }
}
