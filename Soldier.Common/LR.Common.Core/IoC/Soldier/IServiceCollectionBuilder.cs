﻿using Microsoft.Extensions.DependencyInjection;

namespace Soldier.Common.Core.IoC.Soldier
{
    public interface IServiceCollectionBuilder
    {
        IServiceCollectionBuilder RegisterModule(IModule module);

        IServiceCollection Build();
    }
}
