﻿using System;
using Soldier.Common.Core.DAL.Soldier;
using Microsoft.AspNetCore.Identity;

namespace Soldier.Common.Core.DAL
{
    public abstract class BaseAppUser : IdentityUser<Guid>, IEntity
    {
        public Guid? CreatedById { get; set; }

        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateLastLogin { get; set; }

        public DateTime? LastFailedLoginDate { get; set; }

        public string DisplayName { get; set; }

        public bool IsPasswordCompromised { get; set; }
    }
}
