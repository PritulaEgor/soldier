﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Soldier.Common.Core.DAL.Soldier
{
    /// <summary>
    /// Interface for DB context
    /// </summary>
    public interface IDbContext : IDisposable
    {
        int SaveChanges();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        DbQuery<TQuery> Query<TQuery>() where TQuery : class;

        IEnumerable<EntityEntry> GetEntries();
    }
}
