﻿namespace LR.Common.Configurations.Services.Contracts
{
    /// <summary>
    /// Interface for provide possibility to work with global app configuration
    /// </summary>
    public interface IConfigurationService
    {
        void Set<TConfigurationValue>(string key, TConfigurationValue configValue) where TConfigurationValue : class, new();

        TConfigurationValue Get<TConfigurationValue>(string key) where TConfigurationValue : class, new();
    }
}
