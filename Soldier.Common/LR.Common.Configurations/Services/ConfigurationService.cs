﻿using System.Linq;
using LR.Common.Configurations.DAL;
using LR.Common.Configurations.Services.Contracts;
using LR.Common.Core.Helpers;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;

namespace LR.Common.Configurations.Services
{
    /// <summary>
    /// Implements service which, set or get configuration
    /// </summary>
    /// <seealso cref="IConfigurationService"/>
    public class ConfigurationService<TConfiguration> : IConfigurationService
        where TConfiguration : BaseConfiguration, new()
    {
        protected readonly IUnitOfWorkFactory uowFactory;
        protected readonly IGenericRepository<TConfiguration> configurationRepository;

        public ConfigurationService(IUnitOfWorkFactory uowFactory,
            IGenericRepository<TConfiguration> configurationRepository)
        {
            this.uowFactory = uowFactory;
            this.configurationRepository = configurationRepository;
        }

        public void Set<TConfigurationValue>(string key, TConfigurationValue configValue)
            where TConfigurationValue : class, new()
        {
            using (uowFactory.Create())
            {
                if (configurationRepository.Any(c => c.Key == key))
                {
                    var config = configurationRepository.GetBy(c => c.Key == key).First();
                    config.Value = JsonHelper.SerializeObjectToJson(configValue);
                    configurationRepository.Update(config);
                }
                else
                {
                    configurationRepository.Create(new TConfiguration
                    {
                        Key = key,
                        Value = JsonHelper.SerializeObjectToJson(configValue)
                    });
                }
            }
        }

        public TConfigurationValue Get<TConfigurationValue>(string key)
            where TConfigurationValue : class, new()
        {
            if (configurationRepository.Any(c => c.Key == key))
            {
                var config = configurationRepository.GetBy(c => c.Key == key).First();
                return JsonHelper.DeserializeObjectToJson<TConfigurationValue>(config.Value);
            }
            return new TConfigurationValue();
        }
    }
}
