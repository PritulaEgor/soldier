﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Configurations.DAL.Configuration
{
    public class BaseConfigurationConfiguration<TConfiguration> : IEntityTypeConfiguration<TConfiguration>
        where TConfiguration : BaseConfiguration
    {
        public void Configure(EntityTypeBuilder<TConfiguration> builder)
        {
            builder.HasIndex(config => new {config.Key});
        }
    }
}
