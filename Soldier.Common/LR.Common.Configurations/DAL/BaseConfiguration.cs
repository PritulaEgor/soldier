﻿using LR.Common.Core.DAL;

namespace LR.Common.Configurations.DAL
{
    public abstract class BaseConfiguration : Entity
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
