﻿using LR.Common.Configurations.DAL;
using LR.Common.Configurations.Services;
using LR.Common.Configurations.Services.Contracts;
using LR.Common.Core.IoC.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Configurations
{
    public class BaseConfigurationModule<TConfiguration> : IModule
        where TConfiguration : BaseConfiguration, new()
    {
        public void Load(IServiceCollection services)
        {
            services.AddScoped<IConfigurationService, ConfigurationService<TConfiguration>>();
        }
    }
}
