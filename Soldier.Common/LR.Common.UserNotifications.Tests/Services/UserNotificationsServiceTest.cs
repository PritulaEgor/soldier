﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Notifications.Services;
using LR.Common.UserNotifications.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.UserNotifications.Tests.Services
{
    [TestClass]
    public class UserNotificationsServiceTest
    {
        [TestMethod]
        public void IsUserSubscribedForNotification_UserNotFound_ReturnFalse()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>()));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);

            Assert.IsFalse(sut.IsUserSubscribedForNotification("notExist@user.com", "DocumentComments"));
        }

        [TestMethod]
        public void IsUserSubscribedForNotification_UserNotSubscribed_ReturnFalse()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User {Email = "notSubscribed@user.com", UserNotifications = new List<UserNotification>()}
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);

            Assert.IsFalse(sut.IsUserSubscribedForNotification("notSubscribed@user.com", "DocumentComments"));
        }

        [TestMethod]
        public void IsUserSubscribedForNotification_UserSubscribedForAnother_ReturnFalse()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribedAnother@user.com",
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification {Notification = new Notification {Key = "DocumentUploaded"}}
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);

            Assert.IsFalse(sut.IsUserSubscribedForNotification("subscribedAnother@user.com", "DocumentComments"));
        }

        [TestMethod]
        public void IsUserSubscribedForNotification_UserSubscribed_ReturnTrue()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribed@user.com",
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification {Notification = new Notification {Key = "DocumentComments"}}
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);

            Assert.IsTrue(sut.IsUserSubscribedForNotification("subscribed@user.com", "DocumentComments"));
        }

        [TestMethod]
        public void GetUsersForGivenNotification_NoUsersSubscribed_ReturnsEmptyList()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "notSubscribed@user.com",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>()
                    },
                    new User
                    {
                        Email = "notSubscribed@user.org",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>()
                    },
                    new User
                    {
                        Email = "notSubscribed@user.ru",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>()
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string> {"notSubscribed@user.com", "notSubscribed@user.org", "notSubscribed@user.ru"},
                "DocumentComments");

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetUsersForGivenNotification_UsersNotFound_ReturnsEmptyList()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>()));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string> {"notSubscribed@user.com", "notSubscribed@user.org", "notSubscribed@user.ru"},
                "DocumentComments");

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetUsersForGivenNotification_UsersSubscribedForAnother_ReturnsEmptyList()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribedForAnother@user.com",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribedForAnother@user.org",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribedForAnother@user.ru",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string>
                {
                    "subscribedForAnother@user.com",
                    "subscribedForAnother@user.org",
                    "subscribedForAnother@user.ru"
                },
                "DocumentComments");

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetUsersForGivenNotification_OnlyOneSubscribed_ReturnsOneUser()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribedForAnother@user.com",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribed@user.org",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentComments"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribedForAnother@user.ru",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string>
                {
                    "subscribedForAnother@user.com",
                    "subscribed@user.org",
                    "subscribedForAnother@user.ru"
                },
                "DocumentComments");

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void GetUsersForGivenNotification_OnlyOneSubscribedButEmailNotConfirmed_ReturnsEmpty()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribedForAnother@user.com",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribed@user.org",
                        EmailConfirmed = false,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentComments"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribedForAnother@user.ru",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string>
                {
                    "subscribedForAnother@user.com",
                    "subscribed@user.org",
                    "subscribedForAnother@user.ru"
                },
                "DocumentComments");

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetUsersForGivenNotification_OnlyOneSubscribedButLockoutEnabled_ReturnsEmpty()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribedForAnother@user.com",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribed@user.org",
                        EmailConfirmed = true,
                        LockoutEnabled = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentComments"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribedForAnother@user.ru",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentUploaded"
                                }
                            }
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string>
                {
                    "subscribedForAnother@user.com",
                    "subscribed@user.org",
                    "subscribedForAnother@user.ru"
                },
                "DocumentComments");

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetUsersForGivenNotification_AllSubscribed_ReturnsAll()
        {
            var mock = new Mock<IGenericRepository<User>>();
            mock.Setup(ur => ur.GetBy(It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new EnumerableQuery<User>(new List<User>
                {
                    new User
                    {
                        Email = "subscribed@user.com",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentComments"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribed@user.org",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentComments"
                                }
                            }
                        }
                    },
                    new User
                    {
                        Email = "subscribed@user.ru",
                        EmailConfirmed = true,
                        UserNotifications = new List<UserNotification>
                        {
                            new UserNotification
                            {
                                Notification = new Notification
                                {
                                    Key = "DocumentComments"
                                }
                            }
                        }
                    }
                }));

            var sut = new UserNotificationsService<User, UserNotification>(mock.Object);
            var result = sut.GetUsersForGivenNotification(
                new List<string> {"subscribed@user.com", "subscribed@user.org", "subscribed@user.ru"},
                "DocumentComments");

            Assert.AreEqual(3, result.Count());
        }
    }
}