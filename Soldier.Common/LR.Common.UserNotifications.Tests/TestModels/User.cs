﻿using System.Collections.Generic;
using LR.Common.Core.DAL;
using LR.Common.Notifications.DAL.Contracts;

namespace LR.Common.UserNotifications.Tests.TestModels
{
    public class User : BaseAppUser, IUserNotifications<UserNotification>
    {
        public List<UserNotification> UserNotifications { get; set; }
    }
}
