﻿using System.Collections.Generic;
using LR.Common.Notifications.Extensions;
using LR.Common.UserNotifications.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.UserNotifications.Tests.Extensions
{
    [TestClass]
    public class UserExtensionsTests
    {
        [TestMethod]
        public void IsUserSubscribedForNotification_UserNotSubscribeForAny_ReturnFalse()
        {
            var sut = new User
            {
                UserNotifications = new List<UserNotification>()
            };
            var result = sut.IsUserSubscribedForNotification<User, UserNotification>("DocumentComments");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsUserSubscribedForNotification_UserSubscribed_ReturnTrue()
        {
            var sut = new User
            {
                UserNotifications = new List<UserNotification>
                {
                    new UserNotification
                    {
                        Notification = new Notification
                        {
                            Key = "DocumentComments"
                        }
                    }
                }
            };
            var result = sut.IsUserSubscribedForNotification<User, UserNotification>("DocumentComments");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsUserSubscribedForNotification_UserSubscribedForAnother_ReturnFalse()
        {
            var sut = new User
            {
                UserNotifications = new List<UserNotification>
                {
                    new UserNotification
                    {
                        Notification = new Notification
                        {
                            Key = "DocumentUploaded"
                        }
                    }
                }
            };
            var result = sut.IsUserSubscribedForNotification<User, UserNotification>("DocumentComments");
            Assert.IsFalse(result);
        }
    }
}
