﻿using System;
using System.Collections.Generic;
using System.Linq;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.Mocks;
using LR.Common.UserSettings.Constants;
using LR.Common.UserSettings.Services;
using LR.Common.UserSettings.Tests.TestModels;
using LR.Common.UserSettings.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.UserSettings.Tests.Services
{
    [TestClass]
    public class UserSettingServiceTests
    {
        protected Mock<IGenericRepository<User>> GetUserRepositoryMock()
        {
            var userMock = new Mock<IGenericRepository<User>>();
            userMock.Setup(um => um.GetById(It.IsAny<Guid>(),
                It.IsAny<Func<IQueryable<User>, IQueryable<User>>>())).Returns(
                new User
                {
                    Email = "super_admin@lr.org",
                    Id = new Guid("55555555-5555-5555-5555-555555555555"),
                    Settings = new List<UserSetting>
                    {
                        new UserSetting {Key = BaseUserSettingsKeys.NotificationPeriod, Value = "0 */15 * * * *"}
                    }
                });
            return userMock;
        }

        [TestMethod]
        public void Get_ShouldReturnValue()
        {
            var uowMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var userMock = GetUserRepositoryMock();
            var sut = new UserSettingService<User, UserSetting>(userMock.Object, uowMock.Object);
            var setting = sut.Get(new Guid("55555555-5555-5555-5555-555555555555"), BaseUserSettingsKeys.NotificationPeriod);
            Assert.IsNotNull(setting);
        }

        [TestMethod]
        public void Set_ShouldSetValueCorrect()
        {
            var uowMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var userMock = GetUserRepositoryMock();
            var sut = new UserSettingService<User, UserSetting>(userMock.Object, uowMock.Object);
            sut.Set(new UserSettingModel
            {
                SettingKey = BaseUserSettingsKeys.NotificationPeriod,
                UserId = Guid.Parse("55555555-5555-5555-5555-555555555555"),
                Value = "0 */30 * * * *"
            });
            var settings = sut.Get(new Guid("55555555-5555-5555-5555-555555555555"),
                BaseUserSettingsKeys.NotificationPeriod);
            Assert.AreEqual(settings.Value, "0 */30 * * * *");
        }
    }
}
