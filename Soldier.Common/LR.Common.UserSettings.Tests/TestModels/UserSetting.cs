﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.UserSettings.DAL;

namespace LR.Common.UserSettings.Tests.TestModels
{
    public class UserSetting : BaseUserSetting
    {
        [Required]
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
