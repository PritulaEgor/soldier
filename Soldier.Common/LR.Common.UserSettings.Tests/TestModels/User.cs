﻿using System.Collections.Generic;
using LR.Common.Core.DAL;
using LR.Common.UserSettings.DAL.Contracts;

namespace LR.Common.UserSettings.Tests.TestModels
{
    public class User : BaseAppUser, IUserSettings<UserSetting>
    {
        public virtual List<UserSetting> Settings { get; set; }
    }
}
