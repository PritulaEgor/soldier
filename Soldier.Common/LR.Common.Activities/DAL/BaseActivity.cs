﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.BaseIdentity;
using LR.Common.Core.DAL;

namespace LR.Common.Activities.DAL
{
    public abstract class BaseActivity : Entity
    {
        public string Model { get; set; }

        public Guid ActorId { get; set; }

        public DateTime EventDate { get; set; }

        [ForeignKey("ActorId")]
        public BaseAppUser Actor { get; set; }
    }
}
