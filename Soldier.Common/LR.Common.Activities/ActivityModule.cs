﻿using LR.Common.Activities.Services.Contracts;
using LR.Common.Core.IoC.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Activities
{
    public class ActivityModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IActivityService, IActivityService>();
        }
    }
}
