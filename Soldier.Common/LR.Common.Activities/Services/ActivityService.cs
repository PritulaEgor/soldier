﻿using System.Collections.Generic;
using LR.Common.Activities.Services.Contracts;

namespace LR.Common.Activities.Services
{
    public class ActivityService : IActivityService
    {
        protected readonly List<IActivityProcessStrategy> processStrategies;

        public ActivityService(List<IActivityProcessStrategy> processStrategies)
        {
            this.processStrategies = processStrategies;
        }


    }
}
