﻿using System.Linq;
using LR.Common.Core.DAL;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;

namespace LR.Common.Notifications.Extensions
{
    public static class UserExtension
    {
        public static bool IsUserSubscribedForNotification<TUser, TUserNotification>(this TUser user, string notificationKey)
            where TUserNotification : BaseUserNotification, IUserNotification
            where TUser : BaseAppUser, IUserNotifications<TUserNotification>
        {
            return user.UserNotifications.Select(un => un.GetNotification().Key)
                .Contains(notificationKey);
        }
    }
}
