﻿using LR.Common.Notifications.Audit.DAL.Configurations;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Notifications.Extensions
{
    public static class ModelBuilderNotificationAuditEventsExtensions
    {
        public static void ApplyNotificationsAuditEventsConfiguration(this ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BaseNotificationSubscriptionAuditEventConfiguration());
            builder.ApplyConfiguration(new AddedNotificationSubscriptionAuditEventConfiguration());
            builder.ApplyConfiguration(new DeletedNotificationSubscriptionAuditEventConfiguration());
        }
    }
}
