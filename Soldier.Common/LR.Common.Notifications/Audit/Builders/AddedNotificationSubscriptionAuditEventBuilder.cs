﻿using System;
using LR.Common.AuditEvents.Builders.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Notifications.Audit.DAL;
using LR.Common.Notifications.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.Notifications.Audit.Builders
{
    public class AddedNotificationSubscriptionAuditEventBuilder : IAuditLogEntryBuilder
    {
        public bool CanBuild(EntityEntry entityEntry)
        {
            return entityEntry.Entity is BaseUserNotification && entityEntry.State == EntityState.Added;
        }

        public BaseAuditEvent Build(EntityEntry entityEntry)
        {
            var entity = entityEntry.Entity as BaseUserNotification;
            var subscribedEntityEvent = new AddedNotificationSubscriptionAuditEvent
            {
                UserId = entity.LeftId,
                EventDate = DateTime.Now,
                NotificationId = entity.RightId
            };
            return subscribedEntityEvent;
        }
    }
}
