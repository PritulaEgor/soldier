﻿using System;
using LR.Common.Core.DAL;

namespace LR.Common.Notifications.Audit.DAL
{
    public class BaseNotificationSubscriptionAuditEvent : BaseAuditEvent
    {
        public Guid? NotificationId { get; set; }
    }
}
