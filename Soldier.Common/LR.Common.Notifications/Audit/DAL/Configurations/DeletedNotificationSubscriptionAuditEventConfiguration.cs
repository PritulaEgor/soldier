﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Notifications.Audit.DAL.Configurations
{
    public class DeletedNotificationSubscriptionAuditEventConfiguration : IEntityTypeConfiguration<DeletedNotificationSubscriptionAuditEvent>
    {
        public void Configure(EntityTypeBuilder<DeletedNotificationSubscriptionAuditEvent> builder)
        {
            builder.HasBaseType<BaseNotificationSubscriptionAuditEvent>();
        }
    }
}
