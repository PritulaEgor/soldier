﻿using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Notifications.Audit.DAL.Configurations
{
    public class BaseNotificationSubscriptionAuditEventConfiguration : IEntityTypeConfiguration<BaseNotificationSubscriptionAuditEvent>
    {
        public void Configure(EntityTypeBuilder<BaseNotificationSubscriptionAuditEvent> builder)
        {
            builder.HasBaseType<BaseAuditEvent>();
        }
    }
}
