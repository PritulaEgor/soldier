﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Notifications.Audit.DAL.Configurations
{
    public class AddedNotificationSubscriptionAuditEventConfiguration : IEntityTypeConfiguration<AddedNotificationSubscriptionAuditEvent>
    {
        public void Configure(EntityTypeBuilder<AddedNotificationSubscriptionAuditEvent> builder)
        {
            builder.HasBaseType<BaseNotificationSubscriptionAuditEvent>();
        }
    }
}
