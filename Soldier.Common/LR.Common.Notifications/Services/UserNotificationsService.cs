﻿using System;
using System.Collections.Generic;
using System.Linq;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;
using LR.Common.Notifications.Extensions;
using LR.Common.Notifications.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Notifications.Services
{
    public class UserNotificationsService<TUser, TUserNotification> : IUserNotificationsService<TUser, TUserNotification>
        where TUserNotification : BaseUserNotification, IUserNotification
        where TUser : BaseAppUser, IUserNotifications<TUserNotification>
    {
        protected readonly IGenericRepository<TUser> userRepository;

        public UserNotificationsService(IGenericRepository<TUser> userRepository)
        {
            this.userRepository = userRepository;
        }

        public IEnumerable<TUser> GetUsersForGivenNotification(IEnumerable<string> emails, string notificationKey)
        {
            var users = userRepository.GetBy(u => emails.Contains(u.Email), Includes()).ToList();
            return users.Where(user =>
                user.IsUserSubscribedForNotification<TUser, TUserNotification>(notificationKey) && user.EmailConfirmed && !user.LockoutEnabled);
        }

        public bool IsUserSubscribedForNotification(string email, string notificationKey)
        {
            var user = GetUserByEmail(email);
            return user?.IsUserSubscribedForNotification<TUser, TUserNotification>(notificationKey) ?? false;
        }

        protected TUser GetUserByEmail(string email)
        {
            return userRepository.GetBy(item => email == item.Email, Includes()).FirstOrDefault();
        }

        protected Func<IQueryable<TUser>, IQueryable<TUser>> Includes()
        {
            return q => q.Include(u => u.UserNotifications)
                .ThenInclude(un => un.Notification);
        }
    }
}
