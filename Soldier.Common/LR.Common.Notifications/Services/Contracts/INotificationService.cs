﻿using System;
using System.Collections.Generic;

namespace LR.Common.Notifications.Services.Contracts
{
    public interface INotificationService
    {
        List<string> GetAllNotificationTypes();

        List<string> GetUserSubscriptions(Guid userId);

        void UpdateUserNotifications(List<string> types);

        void SubscribeUserForAllNotifications(Guid id);
    }
}
