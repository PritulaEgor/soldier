﻿using System.Collections.Generic;
using LR.Common.Core.DAL;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;

namespace LR.Common.Notifications.Services.Contracts
{
    /// <summary>
    /// Interface which provide information about UserNotifications
    /// </summary>
    public interface IUserNotificationsService<out TUser, TUserNotification>
        where TUserNotification : BaseUserNotification
        where TUser : BaseAppUser, IUserNotifications<TUserNotification>
    {
        /// <summary>
        /// Get users for given notification template by emails
        /// </summary>
        /// <param name="emails">user emails</param>
        /// <param name="notificationKey">notification template key</param>
        /// <returns>users that are subscribed for given notification</returns>
        IEnumerable<TUser> GetUsersForGivenNotification(IEnumerable<string> emails, string notificationKey);

        /// <summary>
        /// Check if user subscribed for given notification template by email
        /// </summary>
        /// <param name="email">user email</param>
        /// <param name="notificationKey">notification template key</param>
        /// <returns>bool</returns>
        bool IsUserSubscribedForNotification(string email, string notificationKey);
    }
}
