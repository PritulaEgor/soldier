﻿using System;
using System.Collections.Generic;
using System.Linq;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;
using LR.Common.Notifications.Services.Contracts;
using LR.Common.Notifications.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Notifications.Services
{
    public class NotificationService<TUser, TUserNotification, TNotification> : INotificationService
        where TUserNotification : BaseUserNotification, new()
        where TUser : BaseAppUser, IUserNotifications<TUserNotification>
        where TNotification : BaseNotification
    {
        protected readonly IGenericRepository<TNotification> notificationRepository;
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly ILinkRepository<TUserNotification> linkRepository;
        protected readonly ICurrentUserService<TUser> currentUserService;
        protected readonly IUnitOfWorkFactory uowFactory;

        public NotificationService(IGenericRepository<TNotification> notificationRepository,
            ILinkRepository<TUserNotification> linkRepository,
            ICurrentUserService<TUser> currentUserService,
            IUnitOfWorkFactory uowFactory,
            IGenericRepository<TUser> userRepository)
        {
            this.notificationRepository = notificationRepository;
            this.linkRepository = linkRepository;
            this.currentUserService = currentUserService;
            this.uowFactory = uowFactory;
            this.userRepository = userRepository;
        }

        public List<string> GetAllNotificationTypes()
        {
            using (uowFactory.Create())
            {
                return notificationRepository.Get().Select(n => n.Key).ToList();
            }
        }

        public List<string> GetUserSubscriptions(Guid userId)
        {
            var user = userRepository.GetById(userId,
                q => q.Include(u => u.UserNotifications).ThenInclude(un => un.Notification));
            return userRepository.Mapper.Map<List<NotificationModel>>(user.UserNotifications)
                .Select(model => model.Key).ToList();
        }

        public void UpdateUserNotifications(List<string> types)
        {
            var currentUser = currentUserService.GetCurrentUser();

            using (uowFactory.Create())
            {
                SubscribeUser(currentUser, types);
                userRepository.Update(currentUser);
            }
        }

        public void SubscribeUserForAllNotifications(Guid id)
        {
            var user = userRepository.GetById(id);
            if (user != null)
            {
                SubscribeUserForAllNotifications(user);
                userRepository.Update(user);
            }
        }

        private void SubscribeUserForAllNotifications(TUser user)
        {
            var notifications = notificationRepository.Get().ToList();
            using (uowFactory.Create())
            {
                SubscribeUserForNotifications(user, notifications);
                userRepository.Update(user);
            }
        }

        private void SubscribeUser(TUser entity, List<string> types)
        {
            var notifications = notificationRepository.GetBy(n => types.Contains(n.Key)).ToList();
            SubscribeUserForNotifications(entity, notifications);
        }

        private void SubscribeUserForNotifications(TUser entity, List<TNotification> notifications)
        {
            var linkedEntities =
                linkRepository.ProcessRelations(entity.Id, notifications.Select(v => v.Id).ToList());

            entity.UserNotifications = linkedEntities;
        }
    }
}
