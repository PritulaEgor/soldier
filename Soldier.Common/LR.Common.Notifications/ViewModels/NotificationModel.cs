﻿using LR.Common.Core;
using LR.Common.Core.ViewModels;

namespace LR.Common.Notifications.ViewModels
{
    public class NotificationModel: BaseViewModel
    {
        public string Key { get; set; }
    }
}
