﻿using System.Collections.Generic;

namespace LR.Common.Notifications.DAL.Contracts
{
    public interface IUserNotifications<TUserNotification>
        where TUserNotification : BaseUserNotification
    {
        List<TUserNotification> UserNotifications { get; set; }
    }
}
