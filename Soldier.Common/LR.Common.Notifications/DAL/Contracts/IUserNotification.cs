﻿using LR.Common.Core.DAL;

namespace LR.Common.Notifications.DAL.Contracts
{
    public interface IUserNotification
    {
        BaseNotification GetNotification();

        BaseAppUser GetUser();
    }
}
