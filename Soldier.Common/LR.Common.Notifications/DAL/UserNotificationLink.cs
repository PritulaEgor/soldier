﻿using LR.Common.Core.DAL;
using LR.Common.Notifications.DAL.Contracts;

namespace LR.Common.Notifications.DAL
{
    public abstract class UserNotificationLink<TUser, TNotification> : BaseUserNotification, IUserNotification
        where TUser : BaseAppUser
        where TNotification : BaseNotification
    {
        public new TUser User { get; set; }

        public new TNotification Notification { get; set; }

        public BaseNotification GetNotification()
        {
            return Notification;
        }

        public BaseAppUser GetUser()
        {
            return User;
        }
    }
}
