﻿using System.ComponentModel.DataAnnotations;
using LR.Common.Core.DAL;

namespace LR.Common.Notifications.DAL
{
    public abstract class BaseNotification : Entity
    {
        [Required]
        [MaxLength(100)]
        public string Key { get; set; }
    }
}
