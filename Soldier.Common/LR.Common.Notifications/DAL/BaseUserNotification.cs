﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.Core.DAL;
using LR.Common.Core.DAL.Contracts;

namespace LR.Common.Notifications.DAL
{
    public abstract class BaseUserNotification : ILinkEntity
    {
        public virtual BaseAppUser User { get; set; }

        public virtual BaseNotification Notification { get; set; }

        public Guid UserId { get; set; }

        public Guid NotificationId { get; set; }

        [NotMapped]
        public Guid LeftId
        {
            get => UserId;
            set => UserId = value;
        }

        [NotMapped]
        public Guid RightId
        {
            get => NotificationId;
            set => NotificationId = value;
        }
    }
}