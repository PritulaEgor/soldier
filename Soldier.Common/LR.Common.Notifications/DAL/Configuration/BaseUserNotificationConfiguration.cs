﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Notifications.DAL.Configuration
{
    public class BaseUserNotificationConfiguration<TUserNotification> : IEntityTypeConfiguration<TUserNotification>
        where TUserNotification : BaseUserNotification
    {
        public void Configure(EntityTypeBuilder<TUserNotification> builder)
        {
            builder.HasKey(un => new {un.UserId, un.NotificationId});
        }
    }
}
