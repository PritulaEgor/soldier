﻿using AutoMapper;
using LR.Common.Core.DAL;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.ViewModels;

namespace LR.Common.Notifications.Mappings
{
    public abstract class BaseNotificationModelProfile<TNotification, TUserNotification, TUser> : Profile
        where TNotification : BaseNotification
        where TUserNotification : UserNotificationLink<TUser, TNotification>
        where TUser : BaseAppUser
    {
        protected BaseNotificationModelProfile()
        {
            CreateMap<TNotification, NotificationModel>()
                .ForMember(nm => nm.Id, config => config.MapFrom(n => n.Id))
                .ForMember(nm => nm.Key, config => config.MapFrom(n => n.Key))
                .ReverseMap();
            CreateMap<TUserNotification, NotificationModel>()
                .ForMember(nm => nm.Key, config => config.MapFrom(un => un.Notification.Key))
                .ReverseMap();
        }
    }
}
