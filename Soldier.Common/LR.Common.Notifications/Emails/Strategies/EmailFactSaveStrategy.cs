﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Core.DAL;
using LR.Common.Core.Helpers;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Emails.Attributes;
using LR.Common.Emails.DAL;
using LR.Common.Emails.Exceptions;
using LR.Common.Emails.Strategies.Contracts;
using LR.Common.Emails.Templates;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;
using LR.Common.Notifications.Services.Contracts;
using LR.Common.UserSettings.Constants;
using LR.Common.UserSettings.Services.Contracts;

namespace LR.Common.Notifications.Emails.Strategies
{
    [EmailProcessedByStrategy(typeof(BaseSummaryEmail))]
    public class EmailFactSaveStrategy<TUser, TUserNotification> : IEmailSendStrategy
        where TUserNotification : BaseUserNotification, IUserNotification
        where TUser : BaseAppUser, IUserNotifications<TUserNotification>
    {
        protected readonly IUserNotificationsService<TUser, TUserNotification> userNotificationService;
        protected readonly IUserSettingService userSettingService;
        protected readonly IUnitOfWorkFactory uowFactory;
        protected readonly IGenericRepository<BaseEmailFact> emailFactsRepository;

        public EmailFactSaveStrategy(IUserNotificationsService<TUser, TUserNotification> userNotificationService,
            IUnitOfWorkFactory uowFactory,
            IGenericRepository<BaseEmailFact> emailFactsRepository, IUserSettingService userSettingService)
        {
            this.userNotificationService = userNotificationService;
            this.uowFactory = uowFactory;
            this.emailFactsRepository = emailFactsRepository;
            this.userSettingService = userSettingService;
        }

        public Task ProcessEmail(IEnumerable<string> to, BaseEmail emailTemplate)
        {
            if (emailTemplate is BaseSummaryEmail summaryEmailTeamplate)
            {
                var addresses =
                    userNotificationService.GetUsersForGivenNotification(to, summaryEmailTeamplate.NotificationKey);
                var facts = addresses.Select(addressee => GetFactForAddressee(summaryEmailTeamplate, addressee))
                    .Where(f => f != null).ToList();
                using (uowFactory.Create())
                {
                    facts.ForEach(fact => emailFactsRepository.Create(fact));
                }
            }
            return Task.CompletedTask;
        }

        private BaseEmailFact GetFactForAddressee(BaseSummaryEmail baseSummaryEmail, TUser addressee)
        {
            try
            {
                var fact = baseSummaryEmail.ConvertToFact();
                fact.UserId = addressee.Id;
                var deliverTime = GetDeliverTime(addressee);
                fact.DeliverTime = deliverTime;
                return fact;
            }
            catch (EmailFactAttributeMissedException)
            {
                return null;
            }
        }

        private DateTime GetDeliverTime(TUser addressee)
        {
            var schedule = userSettingService.Get(addressee.Id, BaseUserSettingsKeys.NotificationPeriod).Value;
            var deliverTime = schedule == null
                ? DateTime.UtcNow
                : CronExpressionHelper.GetNextOccurrenceDate(schedule);
            return deliverTime;
        }
    }
}