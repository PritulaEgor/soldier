﻿using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Emails.Strategies.Contracts;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;
using LR.Common.Notifications.Emails.Strategies;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Notifications.Modules
{
    public class EmailFactsSendingModule<TUser, TUserNotification> : IModule
        where TUserNotification : BaseUserNotification, IUserNotification, new()
        where TUser : BaseAppUser, IUserNotifications<TUserNotification>
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IEmailSendStrategy, EmailFactSaveStrategy<TUser, TUserNotification>>();
        }
    }
}
