﻿using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;
using LR.Common.Notifications.Services;
using LR.Common.Notifications.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Notifications.Modules
{
    public class NotificationModule<TUser, TUserNotification, TNotification> : IModule
        where TUserNotification : BaseUserNotification, IUserNotification, new()
        where TUser : BaseAppUser, IUserNotifications<TUserNotification>
        where TNotification : BaseNotification
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<INotificationService, NotificationService<TUser, TUserNotification, TNotification>>();
            services.AddTransient<IUserNotificationsService<TUser, TUserNotification>, UserNotificationsService<TUser, TUserNotification>>();
        }
    }
}
