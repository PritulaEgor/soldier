﻿using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Notifications.DAL;
using LR.Common.Notifications.DAL.Contracts;

namespace LR.Common.Notifications.Modules
{
    public static class ServiceCollectionBuilderExtensions
    {
        public static IServiceCollectionBuilder WithNotificationModule<TUser, TUserNotification, TNotification>(
            this IServiceCollectionBuilder serviceCollectionBuilder)
            where TUserNotification : BaseUserNotification, IUserNotification, new()
            where TUser : BaseAppUser, IUserNotifications<TUserNotification>
            where TNotification : BaseNotification
        {
            serviceCollectionBuilder.RegisterModule(new NotificationModule<TUser, TUserNotification, TNotification>());
            serviceCollectionBuilder.RegisterModule(new NotificationsAuditLogModule());
            return serviceCollectionBuilder;
        }

        public static IServiceCollectionBuilder WithNotificationAndEmailFactsModule<TUser, TUserNotification, TNotification>(
            this IServiceCollectionBuilder serviceCollectionBuilder)
            where TUserNotification : BaseUserNotification, IUserNotification, new()
            where TUser : BaseAppUser, IUserNotifications<TUserNotification>
            where TNotification : BaseNotification
        {
            serviceCollectionBuilder.RegisterModule(new NotificationModule<TUser, TUserNotification, TNotification>());
            serviceCollectionBuilder.RegisterModule(new EmailFactsSendingModule<TUser, TUserNotification>());
            serviceCollectionBuilder.RegisterModule(new NotificationsAuditLogModule());
            return serviceCollectionBuilder;
        }
    }
}
