﻿using LR.Common.AuditEvents.Builders.Contracts;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Notifications.Audit.Builders;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Notifications.Modules
{
    public class NotificationsAuditLogModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IAuditLogEntryBuilder, AddedNotificationSubscriptionAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, DeletedNotificationSubscriptionAuditEventBuilder>();
        }
    }
}
