﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Soldier.Common.Core.Services.Soldier;
using Soldier.Common.Emails.Attributes;
using Soldier.Common.Emails.Services;
using Soldier.Common.Emails.Strategies.Soldier;
using Soldier.Common.Emails.Templates;

namespace Soldier.Common.Emails.Strategies
{
    /// <summary>
    /// Generate email and deliver for user
    /// </summary>
    /// <seealso cref="IEmailSendStrategy" />
    [EmailProcessedByStrategy(typeof(ImmidiateDeliverEmailTemplate))]
    public class DeliverEmailStrategy : IEmailSendStrategy
    {
        protected readonly EmailTemplateProcessor emailTemplateProcessor;
        protected readonly IMailSenderService mailSender;

        public DeliverEmailStrategy(EmailTemplateProcessor emailTemplateProcessor, IMailSenderService mailSender)
        {
            this.emailTemplateProcessor = emailTemplateProcessor;
            this.mailSender = mailSender;
        }

        public async Task ProcessEmail(IEnumerable<string> to, BaseEmail emailTemplate)
        {
            if (emailTemplate is ImmidiateDeliverEmailTemplate immediateDeliverEmailTemplate)
            {
                var mailMessage = await emailTemplateProcessor.Generate(immediateDeliverEmailTemplate);
                await mailSender.SendEmail(to, mailMessage);
            }
        }
    }
}