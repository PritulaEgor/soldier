﻿using Soldier.Common.Emails.Templates;

namespace Soldier.Common.Emails.Services.Soldier
{
    /// <summary>
    /// Interface for base email builders
    /// </summary>
    public interface IEmailTemplateBuilder
    {
        BaseEmailTemplate GetEmail();
    }
}
