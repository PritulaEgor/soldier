﻿using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Soldier.Common.Core.Configurations;
using Soldier.Common.Emails.Models;
using Soldier.Common.Emails.Models.Configs;
using Soldier.Common.Emails.Templates;
using Microsoft.Extensions.Options;
using RazorLight;

namespace Soldier.Common.Emails.Services
{
    /// <summary>
    /// Class which encapsulate logic to create Mail message and generate markup for email.
    /// </summary>
    public class EmailTemplateProcessor
    {
        protected readonly IRazorLightEngine razorEngine;
        protected readonly ConfigProperties properties;

        public EmailTemplateProcessor(IRazorLightEngine razorEngine,
            ConfigProperties properties)
        {
            this.razorEngine = razorEngine;
            this.properties = properties;
            this.properties.AppUrl = GetAppUrlConfigSettings();
        }

        public virtual async Task<MailMessage> Generate(BaseEmailTemplate emailTemplate)
        {
            emailTemplate.Model.Properties = properties;
            var emailHtml =
                await razorEngine.CompileRenderAsync(emailTemplate.TemplateName, emailTemplate.Model, GetViewBag());
            return CreateMailMessage(emailTemplate, emailHtml);
        }

        protected dynamic GetViewBag()
        {
            dynamic viewBag = new ExpandoObject();
            viewBag.AppUrl = properties.AppUrl;
            viewBag.AppName = properties.AppName;
            viewBag.Header = properties.Header;
            return viewBag;
        }

        private MailMessage CreateMailMessage(BaseEmailTemplate emailTemplate, string htmlText)
        {
            MailMessage mail = new MailMessage
            {
                Subject = emailTemplate.Subject,
                IsBodyHtml = true
            };
            AttachFiles(mail, emailTemplate.Attachments);
            mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(htmlText, null, "text/html"));
            mail.AlternateViews.Add(
                AlternateView.CreateAlternateViewFromString(emailTemplate.GetPlainView(), null, "text/plain"));
            return mail;
        }

        private void AttachFiles(MailMessage mail, Dictionary<string, Stream> emailTemplateAttachments)
        {
            if (emailTemplateAttachments != null && emailTemplateAttachments.Any())
            {
                emailTemplateAttachments
                    .Select(pair => new Attachment(pair.Value, pair.Key)).ToList()
                    .ForEach(mail.Attachments.Add);
            }
        }

        protected BaseEmailModel GetModel(BaseEmailTemplate emailTemplate)
        {
            var model = emailTemplate.Model;
            model.Properties = properties;
            return model;
        }

        protected string GetAppUrlConfigSettings()
        {
            //return configuration.Value.AppUrl;
            return "http://localhost:52472/";
        }
    }
}
