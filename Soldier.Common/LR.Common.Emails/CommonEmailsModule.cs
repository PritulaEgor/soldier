﻿using System.IO;
using System.Reflection;
using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Emails.Models.Configs;
using Soldier.Common.Emails.Services;
using Soldier.Common.Emails.Services.Soldier;
using Soldier.Common.Emails.Strategies;
using Soldier.Common.Emails.Strategies.Soldier;
using Microsoft.Extensions.DependencyInjection;
using RazorLight;

namespace Soldier.Common.Emails
{
    public class CommonEmailsModule : IModule
    {
        protected readonly string appName;
        protected readonly string header;

        public CommonEmailsModule(string appName, string header)
        {
            this.appName = appName;
            this.header = header;
        }

        public void Load(IServiceCollection services)
        {
            services.AddTransient<IRazorLightEngine>(provider =>
            {
                var executeDirPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                var engine = new RazorLightEngineBuilder()
                    .UseFileSystemProject(Path.Combine(executeDirPath, "Views"))
                    .UseMemoryCachingProvider()
                    .Build();
                return engine;
            });
            services.AddSingleton(new ConfigProperties {AppName = appName, Header = header});
            services.AddTransient<EmailTemplateProcessor>();
            services.AddTransient<IEmailSendStrategy, DeliverEmailStrategy>();
        }
    }
}
