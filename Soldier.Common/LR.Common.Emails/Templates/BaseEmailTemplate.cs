﻿using System.Collections.Generic;
using System.IO;

namespace Soldier.Common.Emails.Templates
{
    public abstract class BaseEmailTemplate : BaseEmail
    {
        public abstract string TemplateName { get; }

        public abstract string Subject { get; }

        public Dictionary<string, Stream> Attachments { get; set; }
    }
}
