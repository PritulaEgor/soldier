﻿using BaseEmailModel = Soldier.Common.Emails.Models.BaseEmailModel;

namespace Soldier.Common.Emails.Templates
{
    public abstract class BaseEmail
    {
        public virtual BaseEmailModel Model { get; set; }

        public virtual string GetPlainView()
        {
            return string.Empty;
        }
    }
}
