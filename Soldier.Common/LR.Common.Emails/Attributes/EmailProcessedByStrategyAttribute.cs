﻿using System;

namespace Soldier.Common.Emails.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EmailProcessedByStrategyAttribute : Attribute
    {
        public Type EmailType { get; set; }

        public EmailProcessedByStrategyAttribute(Type emailType)
        {
            EmailType = emailType;
        }
    }
}
