﻿using System;

namespace Soldier.Common.Emails.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class EmailFactConvertAttribute : Attribute
    {
        public Type EmailFactType { get; set; }

        public EmailFactConvertAttribute(Type emailFactType)
        {
            EmailFactType = emailFactType;
        }
    }
}
