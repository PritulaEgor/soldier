﻿using System.Threading.Tasks;
using LR.Common.Auth.Constants;
using LR.Common.Auth.Services;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Auth.Tests.Authenticators
{
    [TestClass]
    public class AuthorizatorTests
    {
        [TestMethod]
        public async Task Authorizator_CallGetStrategy()
        {
            var authStrategyFactoryMock = AuthorizeStrategyFactoryMocks.Get();
            var authStrategy = new Mock<IAuthorizeStrategy>();
            authStrategyFactoryMock.Setup(factory => factory.GetStrategy(GrantTypeKeys.Password))
                .Returns(authStrategy.Object);

            var sut = new Authorizator(authStrategyFactoryMock.Object);

            await sut.Authorize(new LoginModel {GrantType = GrantTypeKeys.Password});

            authStrategyFactoryMock.Verify(factory => factory.GetStrategy(GrantTypeKeys.Password), Times.Once);
        }

        [TestMethod]
        public async Task Authorizator_CallAuthorize()
        {
            var authStrategyFactoryMock = AuthorizeStrategyFactoryMocks.Get();
            var authStrategy = new Mock<IAuthorizeStrategy>();
            authStrategyFactoryMock.Setup(factory => factory.GetStrategy(GrantTypeKeys.Password))
                .Returns(authStrategy.Object);

            var sut = new Authorizator(authStrategyFactoryMock.Object);

            await sut.Authorize(new LoginModel {GrantType = GrantTypeKeys.Password});

            authStrategy.Verify(strategy => strategy.Authorize(It.IsAny<LoginModel>()), Times.Once);
        }
    }
}
