﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.Auth.Services;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.Configurations;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Auth.Tests.Authenticators
{
    [TestClass]
    public class PasswordAuthorizeStrategyTests
    {
        [TestMethod]
        [ExpectedException(typeof(UserDoesNotExistException))]
        public async Task Authorize_UserNotExist_ThrowException()
        {
            var tokenGenerate = new Mock<ITokenGenerateService<User>>().Object;
            var signInManager = SignInManagerMocks.Get().Object;
            var userManagerMock = UserManagerMocks.GetUserManager(new Dictionary<User, string>());
            var options = new Mock<IOptions<ApplicationOptions>>().Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            userManagerMock.Setup(userManager => userManager.FindByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult((User)null));

            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { tokenGenerate }), signInManager, userManagerMock.Object,
                options, accountAccessibilityService);

            await sut.Authorize(new LoginModel { Email = "not@exist.email" });
        }
    }
}
