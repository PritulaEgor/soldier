﻿using System;
using LR.Common.Auth.Constants;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Auth.Tests.Authenticators
{
    [TestClass]
    public class AuthorizeStrategiesFactoryTests
    {
        [DataTestMethod]
        [DataRow(GrantTypeKeys.Password, typeof(PasswordAuthorizeStrategy<User>))]
        [DataRow(GrantTypeKeys.RefreshToken, typeof(RefreshTokenAuthorizeStrategy<User>))]
        public void GetAuthorizeStrategy_Success(string grantType, Type strategyType)
        {
            var sut = AuthorizeStrategiesFactoryCreator.GetFactory();

            var strategy = sut.GetStrategy(grantType);

            Assert.IsNotNull(strategy);
            Assert.IsInstanceOfType(strategy, strategyType);
        }

        [TestMethod]
        [ExpectedException(typeof(GrantTypeNotSupportedException))]
        public void GetUnknownTypeOfAuthStrategy_ThrowException()
        {
            var sut = AuthorizeStrategiesFactoryCreator.GetFactory();
            sut.GetStrategy("unsupported");
        }
    }
}
