﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Services;
using LR.Common.BaseIdentity.Constants;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Auth.Tests.Services
{
    [TestClass]
    public class JwtTokenGenerateServiceTests
    {
        protected IGenericRepository<User> GetRepository()
        {
            var userMock = new Mock<IGenericRepository<User>>();
            userMock.Setup(r => r.GetById(It.IsAny<Guid>(), It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new User { UserCompanies = new List<UserCompany>() });
            return userMock.Object;
        }

        [TestMethod]
        public void GenerateToken_Success()
        {
            var rt = new RefreshToken {Id = Guid.NewGuid()};
               var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var roles = new List<string> { BaseUserRoles.SuperUser};
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org", Id = Guid.NewGuid()};

            var result = sut.GenerateToken(user, roles);

            Assert.IsNotNull(result);
            Assert.AreEqual(rt.Id, result.RefreshToken);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GenerateToken_UserNull()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var roles = new List<string> { BaseUserRoles.SuperUser };

            sut.GenerateToken(null, roles);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GenerateToken_RolesNull()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org", Id = Guid.NewGuid() };
            sut.GenerateToken(user, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GenerateToken_RefreshTokenNull()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org", Id = Guid.NewGuid() };
            sut.GenerateToken(user, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GenerateToken_RolesEmpty()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var roles = new List<string>();
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org", Id = Guid.NewGuid() };

            sut.GenerateToken(user, roles);
        }

        [TestMethod]
        public void GenerateToken_ContainsRightProperties()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var roles = new List<string> { BaseUserRoles.SuperUser };
            var email = "super_user@lr.org";
            var user = new User { Email = email, DisplayName = email, Id = Guid.NewGuid() };

            var result = sut.GenerateToken(user, roles);

            Assert.AreEqual(email, result.Email);
            Assert.AreEqual(email, result.DisplayName);
            Assert.AreEqual(rt.Id, result.RefreshToken);
            Assert.AreNotEqual(DateTime.MinValue, result.Expires);
            Assert.IsFalse(string.IsNullOrEmpty(result.AccessToken));
        }


        [TestMethod]
        public void GenerateToken_AccessToken_ContainsThreeParts()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var roles = new List<string> { BaseUserRoles.SuperUser };
            var email = "super_user@lr.org";
            var user = new User { Email = email, DisplayName = email, Id = Guid.NewGuid() };

            var result = sut.GenerateToken(user, roles);

            Assert.AreEqual(3, result.AccessToken.Split(".", StringSplitOptions.RemoveEmptyEntries).Length);
        }

        [TestMethod]
        public void GenerateToken_AccessToken_ContainsClaims()
        {
            var config = ConfigurationMocks.GetConfigurationForJwtService().Object;
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var sut = new JwtTokenGenerateService<User, UserCompany>(config, GetRepository(), rtmMock.Object);
            var roles = new List<string> { BaseUserRoles.SuperUser };
            var email = "super_user@lr.org";
            var user = new User { Email = email, DisplayName = email, Id = Guid.NewGuid() };

            var result = sut.GenerateToken(user, roles);

            var jwtHandler = new JwtSecurityTokenHandler();

            var token = jwtHandler.ReadJwtToken(result.AccessToken);
            Assert.AreEqual(user.Id, Guid.Parse(token.Payload.Sub));
            var role = token.Payload.Claims.First(c => c.Type == ClaimTypes.Role).Value;
            Assert.AreEqual(BaseUserRoles.SuperUser, role);
            var exp = token.Payload.Claims.First(c => c.Type == ClaimTypes.Expired).Value;
            Assert.AreEqual(result.Expires.ToLongDateString(), exp);
        }

        [TestMethod]
        public void GenerateToken_AccessToken_CanBeReadable()
        {
            var rt = new RefreshToken { Id = Guid.NewGuid() };
            var rtmMock = RefreshTokenManagerMocks.Get();
            rtmMock
                .Setup(rtm => rtm.CreateRefreshToken(It.IsAny<User>()))
                .Returns(rt);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var sut = new JwtTokenGenerateService<User, UserCompany>(config.Object, GetRepository(), rtmMock.Object);
            var roles = new List<string> { BaseUserRoles.SuperUser };
            var email = "super_user@lr.org";
            var user = new User { Email = email, DisplayName = email, Id = Guid.NewGuid() };

            var result = sut.GenerateToken(user, roles);

            var jwtHandler = new JwtSecurityTokenHandler();
            Assert.IsTrue(jwtHandler.CanReadToken(result.AccessToken));
        }
    }
}
