﻿using LR.Common.Auth.DAL;
using LR.Common.Auth.Managers;
using LR.Common.BaseIdentity.Services;
using LR.Common.Core.Configurations;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.TestModels;
using LR.Common.Core.UnitOfWork.Contracts;
using Microsoft.Extensions.Options;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class RefreshTokenManagerMocks
    {
        public static Mock<RefreshTokenManager<User>> Get(
            IMock<IGenericRepository<RefreshToken>> refreshTokenRepositoryMock = null,
            IMock<IUnitOfWorkFactory> unitOfWorkFactoryMock = null,
            IMock<IOptions<ApplicationOptions>> options = null,
            IMock<CurrentUserService<User, UserCompany>> currentUserService = null
        )
        {
            refreshTokenRepositoryMock = refreshTokenRepositoryMock ?? new Mock<IGenericRepository<RefreshToken>>();
            unitOfWorkFactoryMock = unitOfWorkFactoryMock ?? new Mock<IUnitOfWorkFactory>();
            currentUserService = currentUserService ?? CurrentUserServiceMocks.Get<User, UserCompany, Company>();
            options = options ?? new Mock<IOptions<ApplicationOptions>>();
            var refreshTokenManagerMock = new Mock<RefreshTokenManager<User>>(refreshTokenRepositoryMock.Object,
                options.Object,
                unitOfWorkFactoryMock.Object, currentUserService.Object);
            return refreshTokenManagerMock;
        }
    }
}