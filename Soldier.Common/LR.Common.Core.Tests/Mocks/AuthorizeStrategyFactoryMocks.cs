﻿using System.Collections.Generic;
using LR.Common.Auth.Services;
using LR.Common.Auth.Services.Contracts;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.Configurations;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.TestModels;
using Microsoft.Extensions.Options;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class AuthorizeStrategyFactoryMocks
    {
        public static Mock<AuthorizeStrategiesFactory> Get()
        {
            var tokenGenerate = new Mock<ITokenGenerateService<User>>().Object;
            var signInManager = SignInManagerMocks.Get().Object;
            var userManager = UserManagerMocks.GetUserManager(new Dictionary<User, string>()).Object;
            var refreshTokenManager = RefreshTokenManagerMocks.Get().Object;
            var options = new Mock<IOptions<ApplicationOptions>>().Object;
            var accountAccessibilityServiceMock = new Mock<IAccountAccessibilityService>().Object;
            var passAuthStrategy = AuthorizeStrategiesFactoryCreator.CreatePasswordAuthorizeStrategy(tokenGenerate, signInManager, userManager,
                options, accountAccessibilityServiceMock);
            var refreshTokenAuthStrategy =
                AuthorizeStrategiesFactoryCreator.CreateRefreshTokenAuthorizeStrategy(tokenGenerate, userManager,
                    refreshTokenManager);
            var authorizeStrategyFactory =
                new Mock<AuthorizeStrategiesFactory>(new List<IAuthorizeStrategy> { passAuthStrategy, refreshTokenAuthStrategy });
            return authorizeStrategyFactory;
        }
    }
}
