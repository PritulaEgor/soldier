﻿using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.BaseIdentity.Services;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using Microsoft.AspNetCore.Http;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class CurrentUserServiceMocks
    {
        public static Mock<CurrentUserService<TUser, TUserCompany>> Get<TUser, TUserCompany, TCompany>(
            IMock<IGenericRepository<TUser>> userRepositoryMock = null,
            IMock<IHttpContextAccessor> httpContextAccessorMock = null
            )
            where TUser : BaseAppUser, IUserAppRole, IUserCompanies<TUserCompany>
            where TUserCompany : UserCompanyLink<TUser, TCompany>
            where TCompany: BaseCompany
        {
            userRepositoryMock = userRepositoryMock ?? new Mock<IGenericRepository<TUser>>();
            httpContextAccessorMock = httpContextAccessorMock ?? HttpContextAccessorMocks.GetHttpContextAccessor();
            var httpContextWrapperMock = new Mock<CurrentUserService<TUser, TUserCompany>>(userRepositoryMock.Object,
                new UserContext(httpContextAccessorMock.Object));
            return httpContextWrapperMock;
        }
    }
}
