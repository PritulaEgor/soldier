﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.Emails.Services.Contracts;
using LR.Common.Emails.Templates;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public class PostmanServiceMocks
    {
        public IMock<IPostmanService> GetPostmanServiceMock()
        {
            var postmanServiceMock = new Mock<IPostmanService>();
            postmanServiceMock.Setup(s => s.Send(It.IsAny<IEnumerable<string>>(), It.IsAny<BaseEmailTemplate>())).Returns(Task.FromResult(false));
            return postmanServiceMock;
        }
    }
}
