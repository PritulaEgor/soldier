﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class SignInManagerMocks
    {
        public static IMock<SignInManager<User>> Get(
            IMock<UserManager<User>> userManagerMock = null,
            IMock<IHttpContextAccessor> httpContextAccessor = null
        )
        {
            userManagerMock = userManagerMock ?? UserManagerMocks.GetUserManager(new Dictionary<User, string>());
            httpContextAccessor = httpContextAccessor ?? HttpContextAccessorMocks.GetHttpContextAccessor();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            return new Mock<SignInManager<User>>(userManagerMock.Object, httpContextAccessor.Object,
                claimsFactory.Object, null, null, null);
        }

        public static IMock<SignInManager<User>> GetSuccessSignInManager(User user, string role, string password = "qwaszx@1")
        {
            var userManager = UserManagerMocks.GetUserManager(new Dictionary<User, string> { { user, role } });

            var httpContextAccessor = HttpContextAccessorMocks.GetHttpContextAccessor(Guid.NewGuid(), user.Email, "127.0.0.1");
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            var mock = new Mock<SignInManager<User>>(userManager.Object, httpContextAccessor.Object, claimsFactory.Object, null, null, null);
            
            mock.Setup(sim => sim.PasswordSignInAsync(user.Email, "qwaszx@1", false, false))
                .Returns(Task.FromResult(SignInResult.Success));

            return mock;
        }

        public static IMock<SignInManager<User>> GetFailedSignInManager(User user, string role, string password = "qwaszx@1")
        {
            var userManager = UserManagerMocks.GetUserManager(new Dictionary<User, string> { { user, role } });

            var httpContextAccessor = HttpContextAccessorMocks.GetHttpContextAccessor(Guid.NewGuid(), user.Email, "127.0.0.1");
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            var mock = new Mock<SignInManager<User>>(userManager.Object, httpContextAccessor.Object, claimsFactory.Object, null, null, null);

            if (!user.EmailConfirmed)
            {
                mock.Setup(sim => sim.PasswordSignInAsync(user.Email, "qwaszx@1", false, false))
                    .Returns(Task.FromResult(SignInResult.NotAllowed));
                return mock;
            }
            if (user.LockoutEnabled)
            {
                mock.Setup(sim => sim.PasswordSignInAsync(user.Email, "qwaszx@1", false, false))
                    .Returns(Task.FromResult(SignInResult.LockedOut));
                return mock;
            }
            mock.Setup(sim => sim.PasswordSignInAsync(user.Email, "qwaszx@1", false, false))
                .Returns(Task.FromResult(SignInResult.Failed));

            return mock;
        }
    }
}
