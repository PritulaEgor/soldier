﻿using System;
using System.Net;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class HttpContextAccessorMocks
    {
        public static IMock<IHttpContextAccessor> GetHttpContextAccessor()
        {
            var mock = new Mock<IHttpContextAccessor>();
            var httpContextMock = new Mock<HttpContext>();
            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "user@lr.org")
            }));
            httpContextMock.Setup(ctx => ctx.User).Returns(claimsPrincipal);
            httpContextMock.Setup(ctx => ctx.Connection.RemoteIpAddress).Returns(IPAddress.Parse("127.0.0.1"));
            mock.Setup(ctxAccessor => ctxAccessor.HttpContext).Returns(httpContextMock.Object);
            return mock;
        }

        public static IMock<IHttpContextAccessor> GetHttpContextAccessor(Guid userId, string userName, string ip)
        {
            var mock = new Mock<IHttpContextAccessor>();
            var httpContextMock = new Mock<HttpContext>();
            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userId.ToString())
            }));
            httpContextMock.Setup(ctx => ctx.User).Returns(claimsPrincipal);
            httpContextMock.Setup(ctx => ctx.Connection.RemoteIpAddress).Returns(IPAddress.Parse(ip));
            mock.Setup(ctxAccessor => ctxAccessor.HttpContext).Returns(httpContextMock.Object);

            return mock;
        }
    }
}
