﻿using LR.Common.Core.DAL.Contracts;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class DbContextProviderMocks
    {
        public static Mock<IDbContextProvider> GetProvider(IDbContext ctx)
        {
            var mock = new Mock<IDbContextProvider>();
            mock.Setup(dcp => dcp.GetContext()).Returns(ctx);
            return mock;
        }
    }
}
