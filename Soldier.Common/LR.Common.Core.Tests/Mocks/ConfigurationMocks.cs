﻿using LR.Common.Core.Configurations;
using Microsoft.Extensions.Options;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class ConfigurationMocks
    {
        public static IMock<IOptions<ApplicationOptions>> GetConfigurationForJwtService()
        {
            var appSettings = new ApplicationOptions
            {
                AppUrl = "http://appurl",
                RefreshTokenExpiresDays = 1,
                ExpiresMinutes = 15,
                IssuerSigningKey = "lr.mrv.issuer_signing_key",
                JwtIssuer = "JwtIssuer"
            };

            var mock = new Mock<IOptions<ApplicationOptions>>();
            mock.Setup(x => x.Value).Returns(appSettings);
            return mock;
        }
        public static IMock<IOptions<ApplicationOptions>> GetConfigurationForAuthorisationStrategies()
        {
            var appSettings = new ApplicationOptions
            {
                MaxInactivityDays = 30,
                MaxAccessFails = 3
            };

            var mock = new Mock<IOptions<ApplicationOptions>>();
            mock.Setup(x => x.Value).Returns(appSettings);
            return mock;
        }
    }
}