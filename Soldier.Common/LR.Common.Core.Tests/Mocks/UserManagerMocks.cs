﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.DAL;
using Microsoft.AspNetCore.Identity;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class UserManagerMocks
    {
        public static Mock<UserManager<TUser>> GetUserManager<TUser>(Dictionary<TUser, string> users)
            where TUser : BaseAppUser, IUserAppRole
        {
            var store = new Mock<IUserStore<TUser>>(MockBehavior.Strict);
            var mock = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            users.ToList().ForEach(pair =>
            {
                pair.Key.UserRole = pair.Value;
                mock.Setup(um => um.FindByEmailAsync(pair.Key.Email))
                    .Returns(Task.FromResult(pair.Key));
                mock.Setup(um => um.FindByIdAsync(pair.Key.Id.ToString()))
                    .Returns(Task.FromResult(pair.Key));
                mock.Setup(um => um.GetRolesAsync(pair.Key))
                    .Returns(Task.FromResult((IList<string>) new List<string> {pair.Value}));
            });

            mock.Setup(um => um.FindByEmailAsync("not@exist.user"))
                .Returns(Task.FromResult((TUser) null));
            return mock;
        }
    }
}
