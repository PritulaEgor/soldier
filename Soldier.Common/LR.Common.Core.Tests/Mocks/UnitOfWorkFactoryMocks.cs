﻿using LR.Common.Core.DAL.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using Moq;

namespace LR.Common.Core.Tests.Mocks
{
    public static class UnitOfWorkFactoryMocks
    {
        public static Mock<IUnitOfWorkFactory> CreateGetUnitOfWorkFactory()
        {
            var uofFactory = new Mock<IUnitOfWorkFactory>();
            var dbContext = new Mock<IDbContext>();
            var uow = new UnitOfWork.UnitOfWork(dbContext.Object, AuditLogServiceMocks.GetAuditLogService().Object);
            uofFactory.Setup(uowf => uowf.Create(true)).Returns(uow);
            return uofFactory;
        }
    }
}
