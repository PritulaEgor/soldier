﻿using System;
using LR.Common.Core.DAL.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.Core.Tests.TestModels
{
    public class Role : IdentityRole<Guid>, IEntity
    {
    }
}
