﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.DAL;

namespace LR.Common.Core.Tests.TestModels
{
    public class User : BaseAppUser, IUserAppRole, IUserCompanies<UserCompany>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public virtual string UserRole { get; set; }

        public virtual List<UserCompany> UserCompanies { get; set; }

        public virtual List<UserRole> UserRoles { get; set; }
    }
}
