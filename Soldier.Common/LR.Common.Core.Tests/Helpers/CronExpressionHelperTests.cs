﻿using System;
using LR.Common.Core.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.Tests.Helpers
{
    [TestClass]
    public class CronExpressionHelperTests
    {
        [TestMethod]
        public void IsValid_NotValidCronExpression()
        {
            Assert.IsFalse(CronExpressionHelper.IsValid("not valid"));
        }

        [TestMethod]
        public void IsValid_ValidCronExpression()
        {
            Assert.IsTrue(CronExpressionHelper.IsValid("0 */5 * * * *"));
            Assert.IsTrue(CronExpressionHelper.IsValid("0 0 1 * * *"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GetNextOccurrenceDate_NotValidCronExpression()
        {
            CronExpressionHelper.GetNextOccurrenceDate("not valid");
        }

        [TestMethod]
        public void GetNextOccurrenceDate_ValidCronExpression()
        {
            Assert.IsInstanceOfType(CronExpressionHelper.GetNextOccurrenceDate("0 */5 * * * *"), typeof(DateTime));
        }
    }
}
