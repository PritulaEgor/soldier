﻿using LR.Common.Core.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.Tests.Helpers
{
    [TestClass]
    public class UrlGeneratorsHelperTests
    {
        [DataTestMethod]
        [DataRow(null, null, "/setupPassword?code=&userId=")]
        [DataRow("", null, "/setupPassword?code=&userId=")]
        [DataRow("", "", "/setupPassword?code=&userId=")]
        [DataRow(null, "", "/setupPassword?code=&userId=")]
        [DataRow("1234567", "1111-1111-111", "/setupPassword?code=1234567&userId=1111-1111-111")]
        [DataRow(null, "1111-1111-111", "/setupPassword?code=&userId=1111-1111-111")]
        [DataRow("1234567", null, "/setupPassword?code=1234567&userId=")]
        [DataRow("&&%$#@?!||//12,.m", null, "/setupPassword?code=%26%26%25%24%23%40%3f!%7c%7c%2f%2f12%2c.m&userId=")]
        [DataRow("&&%$#@?!||//12,.m", "11111-11111-11111", "/setupPassword?code=%26%26%25%24%23%40%3f!%7c%7c%2f%2f12%2c.m&userId=11111-11111-11111")]
        [DataRow("11111-11111-11111", "&&%$#@?!||//12,.m", "/setupPassword?code=11111-11111-11111&userId=%26%26%25%24%23%40%3f!%7c%7c%2f%2f12%2c.m")]
        public void TestGenerateSetupPasswordUrl(string code, string userId, string result)
        {
            Assert.AreEqual(result, UrlGeneratorsHelper.GenerateSetupPasswordUrl(code, userId));
        }

        [DataTestMethod]
        [DataRow(null, null, "?userId=")]
        [DataRow("/root", null, "/root?userId=")]
        [DataRow(null, "11111-11111-11111", "?userId=11111-11111-11111")]
        [DataRow("/root", "11111-11111-11111", "/root?userId=11111-11111-11111")]
        [DataRow("/root", "&&%$#@?!||//12,.m", "/root?userId=%26%26%25%24%23%40%3f!%7c%7c%2f%2f12%2c.m")]
        public void TestGenerateUrlWithUserIdParam(string root, string userId, string expected)
        {
            Assert.AreEqual(expected, UrlGeneratorsHelper.GenerateUrlWithUserIdParam(root, userId));
        }
    }
}
