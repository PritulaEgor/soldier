﻿using System.Collections.Generic;
using LR.Common.Auth.Managers;
using LR.Common.Auth.Services;
using LR.Common.Auth.Services.Contracts;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.Configurations;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Moq;

namespace LR.Common.Core.Tests.Creators
{
    public static class AuthorizeStrategiesFactoryCreator
    {
        public static AuthorizeStrategiesFactory GetFactory(
            IMock<ITokenGenerateService<User>> tokenGenerateMock = null,
            IMock<SignInManager<User>> signInManagerMock = null,
            IMock<UserManager<User>> userManagerMock = null,
            IMock<RefreshTokenManager<User>> refreshTokenManagerMock = null,
            IMock<IOptions<ApplicationOptions>> optionsMock = null,
            IMock<IAccountAccessibilityService> accountAccessibilityServiceMock = null
            )
        {
            tokenGenerateMock = tokenGenerateMock ?? new Mock<ITokenGenerateService<User>>();
            signInManagerMock = signInManagerMock ?? SignInManagerMocks.Get();
            userManagerMock = userManagerMock ?? UserManagerMocks.GetUserManager(new Dictionary<User, string>());
            refreshTokenManagerMock = refreshTokenManagerMock ?? RefreshTokenManagerMocks.Get();
            optionsMock = optionsMock ?? new Mock<IOptions<ApplicationOptions>>();
            accountAccessibilityServiceMock = accountAccessibilityServiceMock ?? new Mock<IAccountAccessibilityService>();
            var passwordAuthorizeStrategy = CreatePasswordAuthorizeStrategy(
                tokenGenerateMock.Object, signInManagerMock.Object, userManagerMock.Object, optionsMock.Object, accountAccessibilityServiceMock.Object);
            var refreshTokenAuthorizeStrategy =
                CreateRefreshTokenAuthorizeStrategy(tokenGenerateMock.Object, userManagerMock.Object, refreshTokenManagerMock.Object);
            var authorizeStrategyFactory =
                new AuthorizeStrategiesFactory(new List<IAuthorizeStrategy> { passwordAuthorizeStrategy, refreshTokenAuthorizeStrategy });
            return authorizeStrategyFactory;
        }

        public static AuthorizeStrategiesFactory GetFactory(List<IAuthorizeStrategy> strategies)
        {
            return new AuthorizeStrategiesFactory(strategies);
        }

        public static AuthorizeStrategiesFactory GetFactory(
            ITokenGenerateService<User> tokenGenerate,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IOptions<ApplicationOptions> options,
            IAccountAccessibilityService accountAccessibilityService,
            RefreshTokenManager<User> refreshTokenManager
            )
        {
            return new AuthorizeStrategiesFactory(
                new List<IAuthorizeStrategy>
                {
                    CreateRefreshTokenAuthorizeStrategy(tokenGenerate, userManager, refreshTokenManager),
                    CreatePasswordAuthorizeStrategy(tokenGenerate, signInManager, userManager, options, accountAccessibilityService)
                });
        }

        public static RefreshTokenAuthorizeStrategy<User> CreateRefreshTokenAuthorizeStrategy(ITokenGenerateService<User> tokenGenerate, UserManager<User> userManager, RefreshTokenManager<User> refreshTokenManager)
        {
            return new RefreshTokenAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>>{ tokenGenerate }), userManager, refreshTokenManager);
        }

        public static PasswordAuthorizeStrategy<User> CreatePasswordAuthorizeStrategy(
            ITokenGenerateService<User> tokenGenerate,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IOptions<ApplicationOptions> options,
            IAccountAccessibilityService accountAccessibilityService)
        {
            return new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { tokenGenerate }), signInManager, userManager, options, accountAccessibilityService);
        }
    }
}
