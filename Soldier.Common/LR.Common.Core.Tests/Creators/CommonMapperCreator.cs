﻿using System;
using AutoMapper;
using LR.Common.Tasks.Mappings;

namespace LR.Common.Core.Tests.Creators
{
    public class CommonMapperCreator
    {
        public static void Create()
        {
            try
            {
                var unused = Mapper.Instance;
            }
            catch (InvalidOperationException)
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.AddProfile<TaskModelProfile>();
                });
            }
        }
    }
}
