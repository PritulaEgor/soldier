﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LR.Common.Tasks.Attributes;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.Exceptions;
using LR.Common.Tasks.Services;
using LR.Common.Tasks.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Tasks.Tests.Tasks
{
    [TestClass]
    public class TaskWorkerFactoryTests
    {
        private readonly ScheduledTaskModel taskworker = new ScheduledTaskModel { WorkerKey = "taskWorker" };
        private readonly ScheduledTaskModel anothertaskworker = new ScheduledTaskModel { WorkerKey = "anotherTaskWorker" };

        protected IEnumerable<ITaskWorker> GetWorkers(List<string> workerKeys)
        {
            return workerKeys.Select(key =>
            {
                var worker = new Mock<ITaskWorker>();
                var attribute = new TaskWorkerKeyAttribute(key);
                TypeDescriptor.AddAttributes(worker.Object, attribute);
                return worker.Object;
            });
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidTaskWorkersException))]
        public void GetTaskWorker_NullWorkers_ThrowException()
        {
            var sut = new TaskWorkerFactory(null);
            sut.GetTaskWorker(taskworker);
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidTaskWorkersException))]
        public void GetTaskWorker_EmptyWorkers_ThrowException()
        {
            var sut = new TaskWorkerFactory(new List<ITaskWorker>());
            sut.GetTaskWorker(taskworker);
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidTaskWorkersException))]
        public void GetTaskWorker_NotContainWorker_ThrowException()
        {
            var workerKey = taskworker;
            var worker = new Mock<ITaskWorker>();
            var attribute = new TaskWorkerKeyAttribute(workerKey.WorkerKey);
            TypeDescriptor.AddAttributes(worker.Object, attribute);
            var sut = new TaskWorkerFactory(new List<ITaskWorker>{ worker.Object });
            Assert.IsNull(sut.GetTaskWorker(anothertaskworker));
        }

        [TestMethod]
        public void GetTaskWorker_ContainWorker_ReturnWorker()
        {
            var workerKeys = new List<string>
            {
                taskworker.WorkerKey,
                anothertaskworker.WorkerKey
            };
            var workers = GetWorkers(workerKeys);
            var sut = new TaskWorkerFactory(workers);
            Assert.IsNotNull(sut.GetTaskWorker(taskworker));
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidTaskWorkersException))]
        public void GetTaskWorker_ContainTwoWorkerWithSameKey_ThrowException()
        {
            var workerKeys = new List<string>
            {
                taskworker.WorkerKey,
                taskworker.WorkerKey,
                anothertaskworker.WorkerKey
            };
            var workers = GetWorkers(workerKeys);
            var sut = new TaskWorkerFactory(workers);
            sut.GetTaskWorker(taskworker);
        }
    }
}
