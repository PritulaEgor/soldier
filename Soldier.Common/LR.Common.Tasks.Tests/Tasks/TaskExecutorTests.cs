﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.Core.Services.Contracts;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.Enums;
using LR.Common.Tasks.Exceptions;
using LR.Common.Tasks.Services;
using LR.Common.Tasks.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Tasks.Tests.Tasks
{
    [TestClass]
    public class TaskExecutorTests
    {
        protected TaskExecutor GetTaskExecutor(
            Mock<ITaskScheduler> taskSchedulerMock = null,
            Mock<TaskWorkerFactory> taskWorkerFactoryMock = null,
            Mock<ILogService> logServiceMock = null)
        {
            taskSchedulerMock = taskSchedulerMock ?? new Mock<ITaskScheduler>();
            taskWorkerFactoryMock = taskWorkerFactoryMock ?? new Mock<TaskWorkerFactory>(new List<ITaskWorker>());
            logServiceMock = logServiceMock ?? new Mock<ILogService>();
            logServiceMock.Setup(logService => logService.LogInfo(It.IsAny<string>()));
            return new TaskExecutor(taskSchedulerMock.Object, taskWorkerFactoryMock.Object, logServiceMock.Object);
        }

        [TestMethod]
        public async Task ProcessTasks_TaskSchedulerEmptyQueue_CompleteNotCalled()
        {
            var taskSchedulerMock = new Mock<ITaskScheduler>();
            taskSchedulerMock.Setup(sched => sched.GetTasks()).Returns(new List<ScheduledTaskModel>());
            var sut = GetTaskExecutor(taskSchedulerMock);

            await sut.ProcessTasks();
            taskSchedulerMock.Verify(sched => sched.CompleteTask(It.IsAny<ScheduledTaskModel>(), It.IsAny<TaskExecutionStatus>()), Times.Never);
        }

        [TestMethod]
        public async Task ProcessTasks_TaskSchedulerNullsQueue_CompleteNotCalled()
        {
            var taskSchedulerMock = new Mock<ITaskScheduler>();
            taskSchedulerMock.Setup(sched => sched.GetTasks()).Returns(new List<ScheduledTaskModel>{ null, null, null});
            var sut = GetTaskExecutor(taskSchedulerMock);

            await sut.ProcessTasks();
            taskSchedulerMock.Verify(sched => sched.CompleteTask(It.IsAny<ScheduledTaskModel>(), It.IsAny<TaskExecutionStatus>()), Times.Never);
        }

        [TestMethod]
        public async Task ProcessTasks_TaskSchedulerFactoryThrowException_CompleteNotCalled()
        {
            var taskSchedulerMock = new Mock<ITaskScheduler>();
            var taskWorkerFactoryMock =  new Mock<TaskWorkerFactory>(null);
            taskSchedulerMock.Setup(sched => sched.GetTasks()).Returns(new List<ScheduledTaskModel> { new ScheduledTaskModel() });
            taskWorkerFactoryMock.Setup(factory => factory.GetTaskWorker(It.IsAny<ScheduledTaskModel>()))
                .Throws<NoValidTaskWorkersException>();
            var sut = GetTaskExecutor(taskSchedulerMock, taskWorkerFactoryMock);

            await sut.ProcessTasks();
            taskSchedulerMock.Verify(sched => sched.CompleteTask(It.IsAny<ScheduledTaskModel>(), It.IsAny<TaskExecutionStatus>()), Times.Never);
        }

        [TestMethod]
        public async Task ProcessTasks_TaskSchedulerReturn1InQueue_ExecuteAndCompleteCalled()
        {
            var taskSchedulerMock = new Mock<ITaskScheduler>();
            var taskWorkerMock = new Mock<ITaskWorker>();
            taskWorkerMock.SetupProperty(tw => tw.TaskModel, new ScheduledTaskModel {WorkerKey = "test"});
            var taskWorkerFactoryMock = new Mock<TaskWorkerFactory>(new List<ITaskWorker>{ taskWorkerMock.Object });

            taskSchedulerMock.Setup(sched => sched.GetTasks()).Returns(new List<ScheduledTaskModel> { new ScheduledTaskModel() });
            taskWorkerFactoryMock.Setup(factory => factory.GetTaskWorker(It.IsAny<ScheduledTaskModel>()))
                .Returns(taskWorkerMock.Object);
            var sut = GetTaskExecutor(taskSchedulerMock, taskWorkerFactoryMock);

            await sut.ProcessTasks();
            taskWorkerMock.Verify(worker => worker.Execute(It.IsAny<Action<TaskExecutionStatus>>()), Times.Once);
        }

        [TestMethod]
        public async Task ProcessTasks_TaskSchedulerReturn2InQueue_ExecuteAndCompleteCalled()
        {
            var taskSchedulerMock = new Mock<ITaskScheduler>();
            var taskWorkerMock = new Mock<ITaskWorker>();
            taskWorkerMock.SetupProperty(tw => tw.TaskModel, new ScheduledTaskModel { WorkerKey = "test" });
            var taskWorkerFactoryMock = new Mock<TaskWorkerFactory>(new List<ITaskWorker> { taskWorkerMock.Object });

            taskSchedulerMock.Setup(sched => sched.GetTasks()).Returns(new List<ScheduledTaskModel> { new ScheduledTaskModel(), new ScheduledTaskModel() });
            taskWorkerFactoryMock.Setup(factory => factory.GetTaskWorker(It.IsAny<ScheduledTaskModel>()))
                .Returns(taskWorkerMock.Object);
            var sut = GetTaskExecutor(taskSchedulerMock, taskWorkerFactoryMock);

            await sut.ProcessTasks();
            taskWorkerMock.Verify(worker => worker.Execute(It.IsAny<Action<TaskExecutionStatus>>()), Times.Exactly(2));
        }
    }
}
