﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Tasks.Attributes;
using LR.Common.Tasks.Enums;
using LR.Common.Tasks.Services;
using LR.Common.Tasks.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Tasks.Tests.Tasks
{
    [TestClass]
    public class TaskSchedulerTests
    {
        protected TaskScheduler<ScheduledTask> CreateTaskSchedulter(
            Mock<IGenericRepository<ScheduledTask>> tasksRepositoryMock = null,
            Mock<IUnitOfWorkFactory> uowFactoryMock = null)
        {
            CommonMapperCreator.Create();
            tasksRepositoryMock = tasksRepositoryMock ?? new Mock<IGenericRepository<ScheduledTask>>();
            tasksRepositoryMock.Setup(repo => repo.Mapper).Returns(Mapper.Instance);
            uowFactoryMock = uowFactoryMock ?? UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var sut = new TaskScheduler<ScheduledTask>(tasksRepositoryMock.Object, uowFactoryMock.Object);
            return sut;
        }

        [TestMethod]
        public void CompleteTask_CompleteTask_BaseTask()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            tasksRepositoryMock.Setup(repo => repo.GetById(It.IsAny<Guid>())).Returns(new ScheduledTask());

            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);
            sut.CompleteTask(new ScheduledTaskModel(), TaskExecutionStatus.Success);
            tasksRepositoryMock.Verify(repo => repo.GetById(It.IsAny<Guid>()), Times.Once);
            tasksRepositoryMock.Verify(repo => repo.Update(It.IsAny<ScheduledTask>()), Times.Once);
            uowFactoryMock.Verify(uowFactory => uowFactory.Create(), Times.Once);
        }

        [TestMethod]
        public void CompleteTask_CompleteTask_ScheduledTask()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            tasksRepositoryMock.Setup(repo => repo.GetById(It.IsAny<Guid>())).Returns(new ScheduledTask());

            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);
            sut.CompleteTask(new ScheduledTaskModel(), TaskExecutionStatus.Success);
            tasksRepositoryMock.Verify(repo => repo.GetById(It.IsAny<Guid>()), Times.Once);
            tasksRepositoryMock.Verify(repo => repo.Update(It.IsAny<ScheduledTask>()), Times.Once);
            uowFactoryMock.Verify(uowFactory => uowFactory.Create(), Times.Once);
        }

        [TestMethod]
        public void CompleteTask_CompleteTask_CronTask()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            tasksRepositoryMock.Setup(repo => repo.GetById(It.IsAny<Guid>())).Returns(new CronTask { CronExpression = "0 */5 * * * *" });

            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);
            sut.CompleteTask(new CronTaskModel { CronExpression = "0 */5 * * * *" }, TaskExecutionStatus.Success);
            tasksRepositoryMock.Verify(repo => repo.GetById(It.IsAny<Guid>()), Times.Once);
            tasksRepositoryMock.Verify(repo => repo.Update(It.IsAny<ScheduledTask>()), Times.Once);
            uowFactoryMock.Verify(uowFactory => uowFactory.Create(), Times.Once);
        }

        [TestMethod]
        public void CompleteTask_CompleteTaskClassNotHaveAttribute_CalledDefaultAttribute()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            tasksRepositoryMock.Setup(repo => repo.GetById(It.IsAny<Guid>())).Returns(new CronTask());

            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);

            var unknownTask = new Mock<ScheduledTaskModel>();
            sut.CompleteTask(unknownTask.Object, TaskExecutionStatus.Success);

            tasksRepositoryMock.Verify(repo => repo.GetById(It.IsAny<Guid>()), Times.Once);
            tasksRepositoryMock.Verify(repo => repo.Update(It.IsAny<ScheduledTask>()), Times.Once);
            uowFactoryMock.Verify(uowFactory => uowFactory.Create(), Times.Once);
        }

        [TestMethod]
        public void CompleteTask_CompleteTaskWrongResolverType_ExecutedEmpty()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            tasksRepositoryMock.Setup(repo => repo.GetById(It.IsAny<Guid>())).Returns(new CronTask());

            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);

            var unknownTask = new Mock<ScheduledTaskModel>();
            var attribute = new OnCompleteActionTypeAttribute(typeof(object));
            TypeDescriptor.AddAttributes(unknownTask.Object, attribute);

            sut.CompleteTask(unknownTask.Object, TaskExecutionStatus.Success);

            tasksRepositoryMock.Verify(repo => repo.GetById(It.IsAny<Guid>()), Times.Once);
            tasksRepositoryMock.Verify(repo => repo.Update(It.IsAny<ScheduledTask>()), Times.Once);
            uowFactoryMock.Verify(uowFactory => uowFactory.Create(), Times.Once);
        }

        [TestMethod]
        public void GetTasks_GetTasks()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);

            tasksRepositoryMock.Setup(repo =>
                repo.GetBy<ScheduledTask>(It.IsAny<Expression<Func<ScheduledTask, bool>>>(),
                    It.IsAny<Func<IQueryable<ScheduledTask>, IQueryable<ScheduledTask>>>())).Returns(
                new List<ScheduledTask>
                {
                    new CronTask(),
                    new ScheduledTask()
                }.AsQueryable());
            var tasks = sut.GetTasks().ToList();
            Assert.AreEqual(2, tasks.Count);
            Assert.AreEqual(1, tasks.Count(t => t.GetType() == typeof(CronTaskModel) ));
            Assert.AreEqual(1, tasks.Count(t => t.GetType() == typeof(ScheduledTaskModel)));
        }

        [TestMethod]
        public void GetTasks_EmptyCollection()
        {
            var uowFactoryMock = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            var tasksRepositoryMock = new Mock<IGenericRepository<ScheduledTask>>();
            var sut = CreateTaskSchedulter(uowFactoryMock: uowFactoryMock, tasksRepositoryMock: tasksRepositoryMock);

            tasksRepositoryMock.Setup(repo =>
                repo.GetBy<ScheduledTask>(It.IsAny<Expression<Func<ScheduledTask, bool>>>(),
                    It.IsAny<Func<IQueryable<ScheduledTask>, IQueryable<ScheduledTask>>>())).Returns(
                new List<ScheduledTask>().AsQueryable());
            var tasks = sut.GetTasks().ToList();
            Assert.AreEqual(0, tasks.Count);
        }
    }
}
