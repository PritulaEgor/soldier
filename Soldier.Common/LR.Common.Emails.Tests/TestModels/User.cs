﻿using System.Collections.Generic;
using LR.Common.Core.DAL;
using LR.Common.Notifications.DAL.Contracts;
using LR.Common.UserSettings.DAL.Contracts;

namespace LR.Common.Emails.Tests.TestModels
{
    public class User : BaseAppUser, IUserNotifications<UserNotification>, IUserSettings<UserSetting>
    {
        public List<UserNotification> UserNotifications { get; set; }

        public virtual List<UserSetting> Settings { get; set; }
    }
}
