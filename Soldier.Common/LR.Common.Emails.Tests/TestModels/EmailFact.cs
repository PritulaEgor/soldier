﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.Emails.DAL;

namespace LR.Common.Emails.Tests.TestModels
{
    public class EmailFact : BaseEmailFact
    {
        [Required]
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
