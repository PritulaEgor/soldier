﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Emails.Attributes;
using LR.Common.Emails.DAL;
using LR.Common.Emails.Models;
using LR.Common.Emails.Strategies.Contracts;
using LR.Common.Emails.Templates;
using LR.Common.Emails.Tests.TestModels;
using LR.Common.Notifications.Emails.Strategies;
using LR.Common.Notifications.Services.Contracts;
using LR.Common.UserSettings.Constants;
using LR.Common.UserSettings.Services.Contracts;
using LR.Common.UserSettings.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Emails.Tests.Services
{
    [TestClass]
    public class EmailFactStrategyTests
    {
        protected IEmailSendStrategy GetStrategy(Mock<IUserNotificationsService<User, UserNotification>> userNotificationServiceMock = null,
            Mock<IGenericRepository<BaseEmailFact>> emailFactRepositoryMock = null, Mock<IUserSettingService> userSettingsServiceMock = null)
        {
            userNotificationServiceMock = userNotificationServiceMock ?? new Mock<IUserNotificationsService<User, UserNotification>>();
            emailFactRepositoryMock = emailFactRepositoryMock ?? new Mock<IGenericRepository<BaseEmailFact>>();
            userSettingsServiceMock = userSettingsServiceMock ?? new Mock<IUserSettingService>();
            var uowFactory = UnitOfWorkFactoryMocks.CreateGetUnitOfWorkFactory();
            return new EmailFactSaveStrategy<User, UserNotification>(userNotificationServiceMock.Object, uowFactory.Object, emailFactRepositoryMock.Object, userSettingsServiceMock.Object);
        }

        [TestMethod]
        public void PassNotSummaryEmailDeliver_NotThrowException()
        {
            var emailFactRepositoryMock = new Mock<IGenericRepository<BaseEmailFact>>();
            var sut = GetStrategy(emailFactRepositoryMock: emailFactRepositoryMock);
            sut.ProcessEmail(new List<string>(), new Mock<BaseSummaryEmail>().Object);
            emailFactRepositoryMock.Verify<BaseEmailFact>(rep => rep.Create(It.IsAny<EmailFact>()), Times.Never);
        }

        [TestMethod]
        public void PassSummaryEmailDeliver_UserNotificationReturnNoUser_NoFactsCreates()
        {
            var userNotificationServiceMock = new Mock<IUserNotificationsService<User, UserNotification>>();
            var emailFactRepositoryMock = new Mock<IGenericRepository<BaseEmailFact>>();
            userNotificationServiceMock.Setup(userService =>
                userService.GetUsersForGivenNotification(It.IsAny<IEnumerable<string>>(), It.IsAny<string>()))
                .Returns(new List<User>());
            var sut = GetStrategy(userNotificationServiceMock, emailFactRepositoryMock);
            sut.ProcessEmail(new List<string>(), new Mock<BaseSummaryEmail>().Object);
            emailFactRepositoryMock.Verify(rep => rep.Create(It.IsAny<BaseEmailFact>()), Times.Never);
        }

        [TestMethod]
        public void PassSummaryEmailDeliver_UserNotificationReturnUser_FactsCreated()
        {
            var userNotificationServiceMock = new Mock<IUserNotificationsService<User, UserNotification>>();
            var emailFactRepositoryMock = new Mock<IGenericRepository<BaseEmailFact>>();
            var userSettingsServiceMock = new Mock<IUserSettingService>();
            userSettingsServiceMock.Setup(uss => uss.Get(It.IsAny<Guid>(), It.IsAny<string>())).Returns(
                new UserSettingModel {SettingKey = BaseUserSettingsKeys.NotificationPeriod, Value = "0 */5 * * * *"});
            userNotificationServiceMock.Setup(userService =>
                    userService.GetUsersForGivenNotification(It.IsAny<IEnumerable<string>>(), It.IsAny<string>()))
                .Returns(new List<User>
                {
                    new User()
                });
            var sut = GetStrategy(userNotificationServiceMock, emailFactRepositoryMock, userSettingsServiceMock);
            var summaryEmail = new Mock<BaseSummaryEmail>();
            summaryEmail.SetupProperty(e => e.Model, new Mock<BaseEmailModel>().Object);
            var fact = new Mock<EmailFact>();
            var attribute = new EmailFactConvertAttribute(fact.Object.GetType());
            TypeDescriptor.AddAttributes(summaryEmail.Object, attribute);
            sut.ProcessEmail(new List<string>(), summaryEmail.Object);
            emailFactRepositoryMock.Verify<BaseEmailFact>(rep => rep.Create(It.IsAny<EmailFact>()), Times.Once);
        }

        [TestMethod]
        public void PassSummaryEmailDeliver_UserNotificationReturnUser_FactsNoCreated()
        {
            var userNotificationServiceMock = new Mock<IUserNotificationsService<User, UserNotification>>();
            var emailFactRepositoryMock = new Mock<IGenericRepository<BaseEmailFact>>();
            var userSettingsServiceMock = new Mock<IUserSettingService>();
            userSettingsServiceMock.Setup(uss => uss.Get(It.IsAny<Guid>(), It.IsAny<string>())).Returns(
                new UserSettingModel { SettingKey = BaseUserSettingsKeys.NotificationPeriod, Value = "0 */5 * * * *" });
            userNotificationServiceMock.Setup(userService =>
                    userService.GetUsersForGivenNotification(It.IsAny<IEnumerable<string>>(), It.IsAny<string>()))
                .Returns(new List<User>
                {
                    new User
                    {
                        Settings = new List<UserSetting>
                        {
                            new UserSetting {Key = BaseUserSettingsKeys.NotificationPeriod, Value = "0 */5 * * * *"}
                        }
                    }
                });
            var sut = GetStrategy(userNotificationServiceMock, emailFactRepositoryMock, userSettingsServiceMock);
            var summaryEmail = new Mock<BaseSummaryEmail>();
            summaryEmail.SetupProperty(e => e.Model, new Mock<BaseEmailModel>().Object);
            sut.ProcessEmail(new List<string>(), summaryEmail.Object);
            emailFactRepositoryMock.Verify<BaseEmailFact>(rep => rep.Create(It.IsAny<EmailFact>()), Times.Never);
        }
    }
}
