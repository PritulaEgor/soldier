﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LR.Common.Emails.Attributes;
using LR.Common.Emails.Services;
using LR.Common.Emails.Services.Contracts;
using LR.Common.Emails.Strategies.Contracts;
using LR.Common.Emails.Templates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Emails.Tests.Services
{
    [TestClass]
    public class PostmanServiceTests
    {
        protected IPostmanService GetService(
            IEnumerable<Mock<IEmailSendStrategy>> emailSendStrategies = null)
        {
            emailSendStrategies = emailSendStrategies ?? new List<Mock<IEmailSendStrategy>>();
            return new PostmanService(emailSendStrategies.Select(m => m.Object));
        }

        [TestMethod]
        public void IfNoStrategies_NotThrowException()
        {
            var sut = GetService();
            sut.Send(new List<string>(), new Mock<BaseEmail>().Object);
        }

        [TestMethod]
        public void HaveStrategyRightStrategy_ExecuteIt()
        {
            var strategy = new Mock<IEmailSendStrategy>();
            var sut = GetService(new List<Mock<IEmailSendStrategy>> { strategy });
            var emailMock = new Mock<BaseEmail>();
            var attribute = new EmailProcessedByStrategyAttribute(typeof(BaseEmail));
            TypeDescriptor.AddAttributes(strategy.Object, attribute);

            sut.Send(new List<string>(), emailMock.Object);
            strategy.Verify(s => s.ProcessEmail(It.IsAny<IEnumerable<string>>(), It.IsAny<BaseEmail>()), Times.Once);
        }
    }
}
