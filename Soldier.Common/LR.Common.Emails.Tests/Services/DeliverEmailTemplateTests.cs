﻿using System.Collections.Generic;
using System.Net.Mail;
using LR.Common.Core.Services.Contracts;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Emails.Models.Configs;
using LR.Common.Emails.Services;
using LR.Common.Emails.Strategies;
using LR.Common.Emails.Strategies.Contracts;
using LR.Common.Emails.Templates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RazorLight;

namespace LR.Common.Emails.Tests.Services
{
    [TestClass]
    public class DeliverEmailTemplateTests
    {
        protected IEmailSendStrategy GetStrategy(
            Mock<EmailTemplateProcessor> emailTemplateProcessorMock,
            Mock<IMailSenderService> emailSendServiceMock = null)
        {
            emailSendServiceMock = emailSendServiceMock ?? new Mock<IMailSenderService>();
            return new DeliverEmailStrategy(emailTemplateProcessorMock.Object, emailSendServiceMock.Object);
        }

        [TestMethod]
        public void PassImmediateDeliver_GenerateAndSendEmail()
        {
            var sendServiceMock = new Mock<IMailSenderService>();
            var configProperties = new ConfigProperties { AppName = "CO2 Verifier", Header = "LR Services" };
            var templateProcessorMock =
                new Mock<EmailTemplateProcessor>(ConfigurationMocks.GetConfigurationForJwtService().Object,
                    new Mock<IRazorLightEngine>().Object, configProperties);
            var sut = GetStrategy(templateProcessorMock, sendServiceMock);
            sut.ProcessEmail(new List<string>(), new Mock<ImmidiateDeliverEmailTemplate>().Object);
            templateProcessorMock.Verify(s => s.Generate(It.IsAny<BaseEmailTemplate>()), Times.Once);
            sendServiceMock.Verify(s => s.SendEmail(It.IsAny<IEnumerable<string>>(), It.IsAny<MailMessage>()),
                Times.Once);
        }
    }
}
