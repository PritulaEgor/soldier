﻿using System;
using System.Linq;
using LR.Common.BaseIdentity.Constants;
using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.BaseIdentity.Services
{
    /// <summary>
    /// Service provide ability to get necessary info about current user
    /// </summary>
    public class CurrentUserService<TUser, TUserCompany> : ICurrentUserService<TUser>
        where TUserCompany : BaseUserCompany
        where TUser : BaseAppUser, IUserAppRole, IUserCompanies<TUserCompany>
    {
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly UserContext userContext;

        public CurrentUserService(
            IGenericRepository<TUser> userRepository,
            UserContext userContext
            )
        {
            this.userRepository = userRepository;
            this.userContext = userContext;
        }

        public virtual TUser GetCurrentUser()
        {
            var actualUser = GetActualUser();
            if (IsUserSuperAdmin(actualUser) && userContext.IsUserAdopted())
            {
                return userRepository.GetBy(u => u.Id.Equals(userContext.GetAdoptedUserId()), CurrentUserIncludes())
                    .FirstOrDefault();
            }
            return actualUser;
        }

        public virtual TUser GetActualUser()
        {
            var userId = userContext.GetActualUserId();
            return userId.HasValue
                ? userRepository.GetBy(u => u.Id.Equals(userId), CurrentUserIncludes()).FirstOrDefault()
                : null;
        }

        public virtual string GetUserIPAddress()
        {
            return userContext.GetUserIPAddress();
        }

        private bool IsUserSuperAdmin(TUser user)
        {
            return user != null && user.UserRole == BaseUserRoles.SuperUser;
        }

        private Func<IQueryable<TUser>, IQueryable<TUser>> CurrentUserIncludes()
        {
            return q => q.Include(u => u.UserCompanies).ThenInclude(uc => uc.Company);
        }
    }
}
