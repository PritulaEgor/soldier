﻿using System.Collections.Generic;

namespace LR.Common.BaseIdentity.Services.Contracts
{
    public interface IRoleContainer
    {
        bool CanUserAssignToRole(string currentUserRole, string modelTargetRole);

        bool IsInternalRoleAssignementValid(string email, string targetRole);

        bool IsInternalUser(string email);

        bool IsInternalUserRole(string userRole);

        bool CanUserSeeRole(string currentUserRole, string modelTargetRole);

        List<string> GetUserVisibleRoles(string currentUserRole);

        List<string> GetPossibleAssignRoles(string userRole);

        List<string> GetInternalRoles();
    }
}
