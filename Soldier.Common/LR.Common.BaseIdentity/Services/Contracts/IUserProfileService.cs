﻿using LR.Common.BaseIdentity.ViewModels;

namespace LR.Common.BaseIdentity.Services.Contracts
{
    public interface IUserProfileService
    {
        void Update(UpdateUserProfileModel model);
    }
}
