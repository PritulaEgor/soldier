﻿using System;
using System.Threading.Tasks;

namespace LR.Common.BaseIdentity.Services.Contracts
{
    public interface IAccountAccessibilityService
    {
        /// <summary>
        /// Blocks the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task BlockUser(Guid id);


        /// <summary>
        /// Lock user automatically the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isTempLock"></param>
        /// <returns></returns>
        Task LockUser(Guid id, bool isTempLock = false);

        /// <summary>
        /// Unlocks the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task UnlockUser(Guid id);

        /// <summary>
        /// Verifies the email.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        Task<string> VerifyEmail(Guid userId, string code);
    }
}
