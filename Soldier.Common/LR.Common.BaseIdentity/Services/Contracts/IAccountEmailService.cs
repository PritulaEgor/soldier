﻿using System;
using System.Threading.Tasks;

namespace LR.Common.BaseIdentity.Services.Contracts
{
    public interface IAccountEmailService
    {
        /// <summary>
        /// Sends the verification email.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task SendVerificationEmail(Guid id);

        /// <summary>
        /// Sends the verification email.
        /// </summary>
        /// <param name="email">Email.</param>
        /// <returns></returns>
        Task SendVerificationEmail(string email);

        /// <summary>
        /// Sends the email confirm.
        /// </summary>
        /// <param name="userEmail">The user email.</param>
        /// <returns></returns>
        Task SendEmailConfirm(string userEmail);

        /// <summary>
        /// Sends the email confirm.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        Task SendGeneralEnquiry(string message);
    }
}
