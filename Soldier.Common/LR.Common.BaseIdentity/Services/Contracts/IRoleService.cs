﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.DAL;

namespace LR.Common.BaseIdentity.Services.Contracts
{
    /// <summary>
    /// Role service interface, provide possibility to manage user roles
    /// </summary>
    public interface IRoleService<in TUser>
        where TUser : BaseAppUser
    {
        Task AssignToRole(string currentUserEmail, ChangeRoleModel model);

        Task<List<string>> GetPossibleAssignRole(Guid userId, string targetUserEmail);

        Task<List<string>> GetRoles(Guid userId);

        Task<List<string>> GetAvailableToViewRoles(TUser user);

        Task<string> GetUserRole(string userEmail);

        void ValidateRoleAssignment(TUser currentUser, ChangeRoleModel model);

        bool IsInternalUser(string email);
    }
}
