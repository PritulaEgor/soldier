﻿using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;

namespace LR.Common.BaseIdentity.Services
{
    public class UserProfileService<TUser> : IUserProfileService
        where TUser : BaseAppUser
    {
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly ICurrentUserService<TUser> currentUserService;
        protected readonly IUnitOfWorkFactory uowFactory;

        public UserProfileService(IGenericRepository<TUser> userRepository,
            ICurrentUserService<TUser> currentUserService,
            IUnitOfWorkFactory uowFactory)
        {
            this.userRepository = userRepository;
            this.currentUserService = currentUserService;
            this.uowFactory = uowFactory;
        }

        public void Update(UpdateUserProfileModel model)
        {
            var user = currentUserService.GetCurrentUser();
            user.DisplayName = model.DisplayName;
            using (uowFactory.Create())
            {
                userRepository.Update(user);
            }
        }
    }
}
