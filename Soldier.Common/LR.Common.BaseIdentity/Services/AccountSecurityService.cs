﻿using System;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.Emails.Builders;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Services.Contracts;
using LR.Common.Emails.Services.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.BaseIdentity.Services
{
    public class AccountSecurityService<TUser> : IAccountSecurityService
        where TUser : BaseAppUser
    {
        protected readonly UserManager<TUser> userManager;
        protected readonly IPostmanService postmanService;
        protected readonly ICurrentUserService<TUser> currentUserService;

        public AccountSecurityService(UserManager<TUser> userManager,
            IPostmanService postmanService,
            ICurrentUserService<TUser> currentUserService)
        {
            this.userManager = userManager;
            this.postmanService = postmanService;
            this.currentUserService = currentUserService;
        }

        public virtual async Task SendResetPasswordEmail(ResetPasswordModel model)
        {
            var user = await userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                throw new UserDoesNotExistException(model.Email);
            }
            user.IsPasswordCompromised = true;
            await userManager.UpdateAsync(user);
            var code = await userManager.GeneratePasswordResetTokenAsync(user);
            await postmanService.Send(new[] {user.Email}, new ResetPasswordEmailTemplateBuilder(user.Id, code));
        }

        public virtual async Task ChangePassword(ChangePasswordModel model)
        {
            var user = await userManager.FindByIdAsync(model.UserId.ToString());
            await userManager.ResetPasswordAsync(user, model.Code, model.Password);
            user = await userManager.FindByIdAsync(model.UserId.ToString());
            user.IsPasswordCompromised = false;
            await userManager.UpdateAsync(user);
        }

        public virtual async Task UpdatePassword(UpdatePasswordModel model)
        {
            var user = currentUserService.GetCurrentUser();
            var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.Password);
            if (!result.Succeeded)
            {
                throw new PasswordNotChangedException();
            }
        }

        public virtual async Task MarkPasswordAsCompromised(Guid userId)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            user.IsPasswordCompromised = true;
            await userManager.UpdateAsync(user);
            await SendResetPasswordEmail(new ResetPasswordModel {Email = user.Email});
        }
    }
}
