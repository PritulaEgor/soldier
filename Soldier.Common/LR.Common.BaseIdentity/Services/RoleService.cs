﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.DAL;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.BaseIdentity.Services
{
    /// <summary>
    /// Service which helps assign roles, and get roles.
    /// </summary>
    /// <seealso>
    ///     <cref>LR.MRV.Core.Services.Contracts.IRoleService</cref>
    /// </seealso>
    public class RoleService<TUser> : IRoleService<TUser>
        where TUser : BaseAppUser, IUserAppRole
    {
        protected readonly UserManager<TUser> userManager;
        protected readonly IRoleContainer roleContainer;

        public RoleService(UserManager<TUser> userManager, IRoleContainer roleContainer)
        {
            this.userManager = userManager;
            this.roleContainer = roleContainer;
        }

        /// <summary>
        /// Assigns to role.
        /// </summary>
        /// <param name="currentUserEmail">The current user email.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public async Task AssignToRole(string currentUserEmail, ChangeRoleModel model)
        {
            try
            {
                var currentUser = await userManager.FindByEmailAsync(currentUserEmail);
                await GetUserRole(currentUser);
                ValidateRoleAssignment(currentUser, model);
                await SetToRole(model);
            }
            catch (UserCantAssignThisRoleException)
            {
                throw;
            }
            catch (CannotChangeRoleToYourSelfeException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new UserRoleAssignException("Assign role exception", e);
            }
        }

        /// <summary>
        /// Gets the possible assign role.
        /// </summary>
        /// <param name="userId">User email.</param>
        /// <param name="targetUserEmail">The target user email.</param>
        /// <returns></returns>
        /// <exception cref="UserDoesNotExistException"></exception>
        public async Task<List<string>> GetPossibleAssignRole(Guid userId, string targetUserEmail)
        {
            var roles = await GetRoles(userId);
            return roleContainer.IsInternalUser(targetUserEmail)
                ? roles.Intersect(roleContainer.GetInternalRoles()).ToList()
                : roles.Except(roleContainer.GetInternalRoles()).ToList();
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <param name="userId">user Id.</param>
        /// <returns></returns>
        /// <exception cref="UserDoesNotExistException"></exception>
        public async Task<List<string>> GetRoles(Guid userId)
        {
            var currentUser = await userManager.FindByIdAsync(userId.ToString());
            if (currentUser == null)
            {
                throw new UserDoesNotExistException(userId);
            }
            var currentUserRole = await GetUserRole(currentUser);
            return roleContainer.GetPossibleAssignRoles(currentUserRole);

        }

        public async Task<List<string>> GetAvailableToViewRoles(TUser user)
        {
            if (user == null)
            {
                throw new UserDoesNotExistException();
            }
            var currentUserRole = await GetUserRole(user);
            return roleContainer.GetUserVisibleRoles(currentUserRole);
        }

        /// <summary>
        /// Gets the user role.
        /// </summary>
        /// <param name="userEmail">User email.</param>
        /// <returns></returns>
        public async Task<string> GetUserRole(string userEmail)
        {
            var user = await userManager.FindByEmailAsync(userEmail);
            return await GetUserRole(user);
        }

        private async Task<string> GetUserRole(TUser currentUser)
        {
            var roles = await userManager.GetRolesAsync(currentUser);
            return roles.FirstOrDefault();
        }

        private async Task SetToRole(ChangeRoleModel model)
        {
            var user = await userManager.FindByEmailAsync(model.Email);
            var userRole = await GetUserRole(user);
            await userManager.RemoveFromRoleAsync(user, userRole);
            await userManager.AddToRoleAsync(user, model.TargetRole);
            await userManager.UpdateAsync(user);
        }

        public void ValidateRoleAssignment(TUser currentUser, ChangeRoleModel model)
        {
            if (currentUser.Email == model.Email)
            {
                throw new CannotChangeRoleToYourSelfeException();
            }
            if (!roleContainer.CanUserAssignToRole(currentUser.UserRole, model.TargetRole) ||
                !roleContainer.IsInternalRoleAssignementValid(model.Email, model.TargetRole))
            {
                throw new UserCantAssignThisRoleException(model.TargetRole);
            }
        }

        public bool IsInternalUser(string email)
        {
            return roleContainer.IsInternalUser(email);
        }
    }
}
