﻿using System;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.Emails.Builders;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Core.Services.Contracts;
using LR.Common.Emails.Builders;
using LR.Common.Emails.Services.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.BaseIdentity.Services
{
    public class AccountEmailService<TUser> : IAccountEmailService
        where TUser : BaseAppUser
    {
        protected readonly UserManager<TUser> userManager;
        protected readonly IPostmanService postmanService;
        protected readonly ICurrentUserService<TUser> currentUserService;

        public AccountEmailService(
            UserManager<TUser> userManager,
            IPostmanService postmanService,
            ICurrentUserService<TUser> currentUserService)
        {
            this.userManager = userManager;
            this.postmanService = postmanService;
            this.currentUserService = currentUserService;
        }

        /// <summary>
        /// Send verification email, if email wasn't confirmed.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <returns></returns>
        public async Task SendVerificationEmail(Guid id)
        {
            var user = await userManager.FindByIdAsync(id.ToString());
            if (!user.EmailConfirmed)
            {
                await SendEmailConfirm(user);
            }
        }

        /// <summary>
        /// Send verification email, if email wasn't confirmed.
        /// </summary>
        /// <param name="email">ExtendedUser email to verify.</param>
        /// <returns></returns>
        public async Task SendVerificationEmail(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return;
            }
            if (user.EmailConfirmed)
            {
                throw new EmailAlreadyConfirmedException(email);
            }

            await SendEmailConfirm(user);
        }

        /// <summary>
        /// Sends the email confirm.
        /// </summary>
        /// <param name="userEmail">The user email.</param>
        /// <returns></returns>
        public async Task SendEmailConfirm(string userEmail)
        {
            var user = await userManager.FindByEmailAsync(userEmail);
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            await postmanService.Send(new[] { user.Email }, new ConfirmationEmailTemplateBuilder(user.Id, code));
        }

        /// <summary>
        /// Sends the email confirm.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public async Task SendGeneralEnquiry(string message)
        {
            var currentUser = currentUserService.GetActualUser();
            await postmanService.Send(new[] {postmanService.SupportEmail},
                new GeneralEnquiryReceivedEmailTemplateBuilder(message, currentUser.Email));
        }

        /// <summary>
        /// Sends the email confirm.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected async Task SendEmailConfirm(TUser user)
        {
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            await postmanService.Send(new[] { user.Email }, new ConfirmationEmailTemplateBuilder(user.Id, code));
        }
    }
}
