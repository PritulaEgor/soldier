﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.BaseIdentity.Emails.Builders;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Emails.Services.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.BaseIdentity.Services
{
    public class AccountAccessibilityService<TUser> : IAccountAccessibilityService
        where TUser : BaseAppUser
    {
        protected readonly UserManager<TUser> userManager;
        protected readonly IPostmanService postmanService;
        protected readonly IUrlRedirectionService urlRedirectionService;


        public AccountAccessibilityService(
            UserManager<TUser> userManager,
            IPostmanService postmanService,
            IUrlRedirectionService urlRedirectionService)
        {
            this.userManager = userManager;
            this.postmanService = postmanService;
            this.urlRedirectionService = urlRedirectionService;
        }

        /// <summary>
        /// Verifies the email.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public async Task<string> VerifyEmail(Guid userId, string code)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            var result = await userManager.ConfirmEmailAsync(user, code);
            if (!result.Succeeded)
            {
                return urlRedirectionService.RedirectToVerificationExpired();
            }
            if (string.IsNullOrEmpty(user.PasswordHash))
            {
                var resetPasswordCode = await userManager.GeneratePasswordResetTokenAsync(user);
                return urlRedirectionService.RedirectToSetupPassword(userId, resetPasswordCode);
            }

            return urlRedirectionService.RedirectToLoginPage();
        }

        /// <summary>
        /// Disables the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task BlockUser(Guid id)
        {
            var user = await userManager.FindByIdAsync(id.ToString());
            await userManager.SetLockoutEnabledAsync(user, true);
            await userManager.SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);
            await postmanService.Send(new List<string> {user.Email},
               new AccountLockedByAdministratorEmailTemplateBuilder());
        }

        public async Task LockUser(Guid id, bool isTempLock = false)
        {
            var user = await userManager.FindByIdAsync(id.ToString());
            await userManager.SetLockoutEnabledAsync(user, true);
            var lockoutEnd = isTempLock ? DateTimeOffset.Now.AddMinutes(15) : DateTimeOffset.MaxValue;
            await userManager.SetLockoutEndDateAsync(user, lockoutEnd);
            await userManager.ResetAccessFailedCountAsync(user);
            await postmanService.Send(new List<string> {user.Email},
                new AccountLockedAutomaticallyEmailTemplateBuilder(lockoutEnd, isTempLock));
        }

        /// <summary>
        /// Enables the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task UnlockUser(Guid id)
        {
            var user = await userManager.FindByIdAsync(id.ToString());
            await userManager.SetLockoutEnabledAsync(user, false);
            await userManager.ResetAccessFailedCountAsync(user);
            await userManager.SetLockoutEndDateAsync(user, null);
            user = await userManager.FindByIdAsync(id.ToString());
            user.DateLastLogin = null;
            await userManager.UpdateAsync(user);
            await postmanService.Send(new List<string> { user.Email }, new AccountEnabledEmailTemplateTemplateBuilder());
        }
    }
}
