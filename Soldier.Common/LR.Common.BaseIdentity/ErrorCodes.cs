﻿namespace LR.Common.BaseIdentity
{
    public static class ErrorCodes
    {
        public static readonly int UserDoesNotExist = 1003;
        public static readonly int PasswordWasNotChanged = 1012;
        public static readonly int EmailAlreadyConfirmed = 1017;
        public static readonly int RoleWasNotAssigned = 2000;
        public static readonly int CannotChangeYourselfRole = 2001;
    }
}
