﻿using System;
using LR.Common.BaseIdentity.Constants;
using LR.Common.BaseIdentity.Context;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.Constants;
using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.BaseIdentity
{
    public class IdentityModule<TUser, TUserRole, TRole> : IModule
        where TUser : BaseAppUser, IUserAppRole
        where TRole : IdentityRole<Guid>
        where TUserRole : IdentityUserRole<Guid>
    {
        protected readonly IConfiguration configuration;

        public IdentityModule(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Load(IServiceCollection services)
        {
            services.AddIdentity<TUser, TRole>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = true;
                    config.User.RequireUniqueEmail = true;
                    config.Lockout.AllowedForNewUsers = false;
                    config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromDays(
                        configuration.GetValue<double>(
                            $"{ApplicationOptionsConfigurationKeys.AppSettingPrefix}{ConfigurationKeys.LockoutDays}"));
                    config.Password.RequireDigit = true;
                    config.Password.RequiredLength = 8;
                    config.Password.RequireNonAlphanumeric = true;
                    config.Password.RequireLowercase = false;
                    config.Password.RequireUppercase = false;
                    config.Password.RequiredUniqueChars = 2;
                })
                .AddEntityFrameworkStores<AppDbContext<TUser, TUserRole, TRole>>()
                .AddDefaultTokenProviders();
        }
    }
}
