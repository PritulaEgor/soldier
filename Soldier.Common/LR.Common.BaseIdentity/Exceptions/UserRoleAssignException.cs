﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.BaseIdentity.Exceptions
{
    [Serializable]
    public class UserRoleAssignException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.RoleWasNotAssigned;

        public override string ErrorData => string.Empty;

        public UserRoleAssignException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
