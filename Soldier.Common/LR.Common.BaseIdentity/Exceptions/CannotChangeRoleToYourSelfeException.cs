﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.BaseIdentity.Exceptions
{
    [Serializable]
    public class CannotChangeRoleToYourSelfeException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.CannotChangeYourselfRole;

        public override string ErrorData => string.Empty;
    }
}
