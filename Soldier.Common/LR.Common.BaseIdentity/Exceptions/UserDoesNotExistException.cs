﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.BaseIdentity.Exceptions
{
    [Serializable]
    public class UserDoesNotExistException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.UserDoesNotExist;

        public override string ErrorData =>
            string.IsNullOrEmpty(Email) ? "User not found" : $"User not found email: {Email}";

        public string Email { get; set; }
        public Guid UserId { get; set; }

        public UserDoesNotExistException()
        {
        }

        public UserDoesNotExistException(string email)
        {
            Email = email;
        }

        public UserDoesNotExistException(Guid userId)
        {
            UserId = userId;
        }
    }
}
