﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.BaseIdentity.Exceptions
{
    [Serializable]
    public class EmailAlreadyConfirmedException : MrvApplicationException
    {
        public string Email { get; set; }

        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.EmailAlreadyConfirmed;

        public override string ErrorData => Email ?? string.Empty;

        public EmailAlreadyConfirmedException(string email)
        {
            Email = email;
        }
    }
}
