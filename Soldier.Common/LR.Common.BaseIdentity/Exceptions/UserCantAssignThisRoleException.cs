﻿using System;

namespace LR.Common.BaseIdentity.Exceptions
{
    [Serializable]
    public class UserCantAssignThisRoleException : Exception
    {
        public string Role { get; set; }

        public UserCantAssignThisRoleException(string role)
        {
            Role = role;
        }
    }
}
