﻿namespace LR.Common.BaseIdentity.Constants
{
    public static class BaseUserRoles
    {
        public const string SuperUser = "SuperUser";

        public const string Unassigned = "Unassigned";
    }
}
