﻿using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.BaseIdentity.Services;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Core.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.BaseIdentity
{
    public class UserManagementModule<TUser, TUserCompany> : IModule
        where TUserCompany : BaseUserCompany
        where TUser : BaseAppUser, IUserAppRole, IUserCompanies<TUserCompany>
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<ICurrentUserService<TUser>, CurrentUserService<TUser, TUserCompany>>();
            services.AddTransient<IRoleService<TUser>, RoleService<TUser>>();
            services.AddTransient<IUserProfileService, UserProfileService<TUser>>();
            services.AddTransient<IAccountSecurityService, AccountSecurityService<TUser>>();
            services.AddTransient<IAccountEmailService, AccountEmailService<TUser>>();
            services.AddTransient<IAccountAccessibilityService, AccountAccessibilityService<TUser>>();
            services.AddSingleton<UserContext>();
        }
    }
}
