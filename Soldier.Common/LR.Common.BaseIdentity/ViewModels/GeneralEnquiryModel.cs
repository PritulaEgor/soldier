﻿using System.ComponentModel.DataAnnotations;

namespace LR.Common.BaseIdentity.ViewModels
{
    public class GeneralEnquiryModel
    {
        [Required]
        public string Message { get; set; }
    }
}
