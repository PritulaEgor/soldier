﻿using System.ComponentModel.DataAnnotations;

namespace LR.Common.BaseIdentity.ViewModels
{
    public class ResetPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
