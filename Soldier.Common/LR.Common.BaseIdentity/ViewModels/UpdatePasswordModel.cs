﻿using System.ComponentModel.DataAnnotations;

namespace LR.Common.BaseIdentity.ViewModels
{
    public class UpdatePasswordModel : BaseChangePasswordModel
    {
        [Required]
        [StringLength(32, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 8)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).*")]
        public string OldPassword { get; set; }
    }
}
