﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LR.Common.BaseIdentity.ViewModels
{
    public class ChangePasswordModel : BaseChangePasswordModel
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public string Code { get; set; }
    }
}
