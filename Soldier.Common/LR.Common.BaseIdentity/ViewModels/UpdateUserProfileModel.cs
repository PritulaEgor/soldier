﻿using System.ComponentModel.DataAnnotations;

namespace LR.Common.BaseIdentity.ViewModels
{
    public class UpdateUserProfileModel
    {
        [Required]
        public string DisplayName { get; set; }
    }
}
