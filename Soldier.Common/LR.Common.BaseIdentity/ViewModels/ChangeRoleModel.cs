﻿using LR.Common.Core.ViewModels;

namespace LR.Common.BaseIdentity.ViewModels
{
    public class ChangeRoleModel : BaseViewModel
    {
        public string Email { get; set; }

        public string TargetRole { get; set; }
    }
}
