﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.Core.DAL;
using LR.Common.Core.DAL.Contracts;

namespace LR.Common.BaseIdentity.DAL
{
    public abstract class BaseUserCompany : ILinkEntity
    {
        public virtual BaseAppUser User { get; set; }
        public virtual BaseCompany Company { get; set; }

        public Guid UserId { get; set; }

        public Guid CompanyId { get; set; }

        [NotMapped]
        public Guid LeftId
        {
            get => UserId;
            set => UserId = value;
        }

        [NotMapped]
        public Guid RightId
        {
            get => CompanyId;
            set => CompanyId = value;
        }
    }
}
