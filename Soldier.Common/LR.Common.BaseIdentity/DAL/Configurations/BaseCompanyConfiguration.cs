﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.BaseIdentity.DAL.Configurations
{
    public class BaseCompanyConfiguration<TCompany> : IEntityTypeConfiguration<TCompany>
        where TCompany : BaseCompany
    {
        public void Configure(EntityTypeBuilder<TCompany> builder)
        {
            builder.HasIndex(c => c.Name).IsUnique();
        }
    }
}
