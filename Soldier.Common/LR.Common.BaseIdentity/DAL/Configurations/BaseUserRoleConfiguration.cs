﻿using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.BaseIdentity.DAL.Configurations
{
    public class BaseUserRoleConfiguration<TUser> : IEntityTypeConfiguration<TUser>
        where TUser : BaseAppUser, IUserAppRole
    {
        public virtual void Configure(EntityTypeBuilder<TUser> builder)
        {
// Need to be created function, to read user role
//            migrationBuilder.Sql(@"
//CREATE FUNCTION [dbo].[GetUserRole]
//(
//    @userId uniqueidentifier
//)
//RETURNS nvarchar(256)
//AS
//BEGIN
//    DECLARE @role nvarchar(256)
//    SELECT TOP 1 @role = roles.Name FROM AspNetUsers as users
//        INNER JOIN AspNetUserRoles as ur ON users.Id = ur.UserId AND users.Id = @userId
//        INNER JOIN AspNetRoles as roles ON roles.Id = Ur.RoleId
//    RETURN @role
//END
//");

            builder
                .Property(p => p.UserRole)
                .HasComputedColumnSql("[dbo].[GetUserRole]([Id])");
        }
    }
}
