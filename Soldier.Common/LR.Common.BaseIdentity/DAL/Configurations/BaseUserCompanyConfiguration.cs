﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.BaseIdentity.DAL.Configurations
{
    public class BaseUserCompanyConfiguration<TUserCompany> : IEntityTypeConfiguration<TUserCompany>
        where TUserCompany : BaseUserCompany
    {
        public virtual void Configure(EntityTypeBuilder<TUserCompany> builder)
        {
            builder
                .HasKey(uc => new { uc.UserId, uc.CompanyId });
        }
    }
}
