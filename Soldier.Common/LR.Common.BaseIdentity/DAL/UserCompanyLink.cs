﻿using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.DAL;

namespace LR.Common.BaseIdentity.DAL
{
    public abstract class UserCompanyLink<TUser, TCompany> : BaseUserCompany, IUserCompany
        where TUser : BaseAppUser
        where TCompany : BaseCompany
    {
        public new TUser User { get; set; }

        public new TCompany Company { get; set; }

        public BaseAppUser GetUser()
        {
            return User;
        }

        public BaseCompany GetCompany()
        {
            return Company;
        }
    }
}
