﻿using LR.Common.Core.DAL;

namespace LR.Common.BaseIdentity.DAL.Contracts
{
    public interface IUserCompany
    {
        BaseAppUser GetUser();

        BaseCompany GetCompany();
    }
}
