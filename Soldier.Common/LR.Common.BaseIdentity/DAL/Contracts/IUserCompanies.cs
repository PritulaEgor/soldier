﻿using System.Collections.Generic;

namespace LR.Common.BaseIdentity.DAL.Contracts
{
    public interface IUserCompanies<TUserCompany>
        where TUserCompany : BaseUserCompany
    {
        List<TUserCompany> UserCompanies { get; set; }
    }
}
