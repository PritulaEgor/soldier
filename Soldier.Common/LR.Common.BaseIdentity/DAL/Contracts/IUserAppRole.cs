﻿namespace LR.Common.BaseIdentity.DAL.Contracts
{
    public interface IUserAppRole
    {
        string UserRole { get; set; }
    }
}
