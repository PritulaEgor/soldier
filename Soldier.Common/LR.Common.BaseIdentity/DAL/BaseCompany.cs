﻿using System.ComponentModel.DataAnnotations;
using LR.Common.Core.DAL;

namespace LR.Common.BaseIdentity.DAL
{
    public abstract class BaseCompany : Entity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(300)]
        public string Address { get; set; }

        [Required]
        [MaxLength(100)]
        public string Phone { get; set; }

        [Required]
        public bool IsInternal { get; set; }
    }
}
