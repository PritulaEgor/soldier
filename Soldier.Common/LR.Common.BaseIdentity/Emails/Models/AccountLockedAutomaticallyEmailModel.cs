﻿using System;
using LR.Common.BaseIdentity.Emails.Models.Enums;
using LR.Common.Emails.Models;

namespace LR.Common.BaseIdentity.Emails.Models
{
    public class AccountLockedAutomaticallyEmailModel : BaseEmailModel
    {
        public LockType LockType { get; set; }

        public DateTimeOffset LockoutEndDate { get; set; }
    }
}
