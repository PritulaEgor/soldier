﻿using System;
using LR.Common.Emails.Models;

namespace LR.Common.BaseIdentity.Emails.Models
{
    public class ConfirmEmailEmailModel : BaseEmailModel
    {
        public string Code { get; set; }

        public Guid UserId { get; set; }
    }
}
