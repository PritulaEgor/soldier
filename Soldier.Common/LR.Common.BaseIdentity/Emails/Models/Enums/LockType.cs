﻿namespace LR.Common.BaseIdentity.Emails.Models.Enums
{
    public enum LockType
    {
        WrongPassword = 0,
        Inactivity
    }
}
