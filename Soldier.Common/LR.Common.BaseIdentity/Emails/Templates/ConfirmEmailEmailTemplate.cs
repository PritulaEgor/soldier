﻿using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Templates
{
    public class ConfirmEmailEmailTemplate : ImmidiateDeliverEmailTemplate
    {
        public override string TemplateName => "ConfirmEmailEmailTemplate";

        public override string Subject => $"Set password for {Model.Properties.Header} - {Model.Properties.AppName}";

        public override string GetPlainView()
        {
            if (!(Model is ConfirmEmailEmailModel model))
            {
                return string.Empty;
            }

            return
                "Please enable your account by using this link:" + 
                $" {model.Properties.AppUrl}/api/Account/VerifyEmail?code={model.Code}&userId={model.UserId}";
        }
    }
}
