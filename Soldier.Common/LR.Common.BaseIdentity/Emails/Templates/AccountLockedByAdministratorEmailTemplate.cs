﻿using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Templates
{
    public class AccountLockedByAdministratorEmailTemplate : ImmidiateDeliverEmailTemplate
    {
        public override string TemplateName => "AccountLockedByAdministratorEmailTemplate";

        public override string Subject => $"Your account for {Model.Properties.Header} - {Model.Properties.AppName} has been locked by administrators";

        public override string GetPlainView()
        {
            if (!(Model is AccountLockedByAdministratorEmailModel model))
            {
                return string.Empty;
            }

            return
                "Your account has been locked by the administrators. " +
                "Please contact your client manager or email co2verifier.support@lr.org to unlock your account.";
        }
    }
}
