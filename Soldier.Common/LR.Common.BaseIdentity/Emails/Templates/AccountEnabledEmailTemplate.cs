﻿using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Templates
{
    public class AccountEnabledEmailTemplate : ImmidiateDeliverEmailTemplate
    {
        public override string TemplateName => "AccountEnabledEmailTemplate";

        public override string Subject => $"Your account for {Model.Properties.Header} - {Model.Properties.AppName} has been enabled";

        public override string GetPlainView()
        {
            if (!(Model is AccountEnabledEmailModel model))
            {
                return string.Empty;
            }

            return "Your LR Services Account has been enabled.";
        }
    }
}
