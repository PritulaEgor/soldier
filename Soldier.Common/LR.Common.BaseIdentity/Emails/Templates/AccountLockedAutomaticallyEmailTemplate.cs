﻿using System.Text;
using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.BaseIdentity.Emails.Models.Enums;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Templates
{
    public class AccountLockedAutomaticallyEmailTemplate : ImmidiateDeliverEmailTemplate
    {
        public override string TemplateName => "AccountLockedAutomaticallyEmailTemplate";

        public override string Subject =>
            $"Your account for {Model.Properties.Header} - {Model.Properties.AppName} has been locked";

        public override string GetPlainView()
        {
            if (!(Model is AccountLockedAutomaticallyEmailModel model))
            {
                return string.Empty;
            }
            var message = new StringBuilder();
            switch (model.LockType)
            {
                case LockType.WrongPassword:
                    message.AppendLine($"There have been a number of attempts made to access your account with the wrong password and your LR Services account has been locked for 15 minutes. Your account will be unlocked at {model.LockoutEndDate.ToUniversalTime()} (UTC).");
                    break;
                case LockType.Inactivity:
                    message.AppendLine("Your account has been locked following 30 days of inactivity.");
                    break;
                default: return string.Empty;
            }
            message.AppendLine();
            message.AppendLine(
                "Please contact your client manager or email co2verifier.support@lr.org to unlock your account.");
            return message.ToString();
        }
    }
}
