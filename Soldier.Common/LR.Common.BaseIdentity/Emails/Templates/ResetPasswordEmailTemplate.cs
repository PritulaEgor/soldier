﻿using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Templates
{
    public class ResetPasswordEmailTemplate : ImmidiateDeliverEmailTemplate
    {
        public override string TemplateName => "ResetPasswordEmailTemplate";

        public override string Subject => $"Reset password request for {Model.Properties.Header} - {Model.Properties.AppName}";

        public override string GetPlainView()
        {
            if (!(Model is ResetPasswordEmailModel model))
            {
                return string.Empty;
            }

            return
                $"Reset account password by using this link: {model.Properties.AppUrl}/changePassword?code={model.Code}&userId={model.UserId}";
        }
    }
}
