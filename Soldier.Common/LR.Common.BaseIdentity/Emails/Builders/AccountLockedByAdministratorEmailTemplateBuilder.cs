﻿using System;
using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.BaseIdentity.Emails.Templates;
using LR.Common.Emails.Services.Contracts;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Builders
{
    public class AccountLockedByAdministratorEmailTemplateBuilder : IEmailTemplateBuilder
    {
        public BaseEmailTemplate GetEmail()
        {
            return new AccountLockedByAdministratorEmailTemplate
            {
                Model = new AccountLockedByAdministratorEmailModel
                {
                    EventDate = DateTime.UtcNow
                }
            };
        }
    }
}
