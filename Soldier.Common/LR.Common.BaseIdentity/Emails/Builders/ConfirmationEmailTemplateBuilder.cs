﻿using System;
using System.Web;
using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.BaseIdentity.Emails.Templates;
using LR.Common.Emails.Services.Contracts;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Builders
{
    public class ConfirmationEmailTemplateBuilder : IEmailTemplateBuilder
    {
        protected readonly Guid userId;
        protected readonly string code;

        public ConfirmationEmailTemplateBuilder(Guid userId, string code)
        {
            this.userId = userId;
            this.code = code;
        }

        public BaseEmailTemplate GetEmail()
        {
            return new ConfirmEmailEmailTemplate
            {
                Model = new ConfirmEmailEmailModel
                {
                    EventDate = DateTime.UtcNow,
                    Code = HttpUtility.UrlEncode(code),
                    UserId = userId
                }
            };
        }
    }
}
