﻿using System;
using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.BaseIdentity.Emails.Models.Enums;
using LR.Common.BaseIdentity.Emails.Templates;
using LR.Common.Emails.Services.Contracts;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Builders
{
    public class AccountLockedAutomaticallyEmailTemplateBuilder : IEmailTemplateBuilder
    {
        protected readonly DateTimeOffset lockoutEnd;
        protected readonly bool lockTemporarily;

        public AccountLockedAutomaticallyEmailTemplateBuilder(DateTimeOffset lockoutEnd, bool lockTemporarily)
        {
            this.lockoutEnd = lockoutEnd;
            this.lockTemporarily = lockTemporarily;
        }

        public BaseEmailTemplate GetEmail()
        {
            return new AccountLockedAutomaticallyEmailTemplate
            {
                Model = new AccountLockedAutomaticallyEmailModel
                {
                    EventDate = DateTime.UtcNow,
                    LockoutEndDate = lockoutEnd,
                    LockType = lockTemporarily ? LockType.WrongPassword : LockType.Inactivity
                }
            };
        }
    }
}
