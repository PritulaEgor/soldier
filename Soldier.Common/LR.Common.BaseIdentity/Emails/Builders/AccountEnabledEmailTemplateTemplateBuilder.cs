﻿using System;
using LR.Common.BaseIdentity.Emails.Models;
using LR.Common.BaseIdentity.Emails.Templates;
using LR.Common.Emails.Services.Contracts;
using LR.Common.Emails.Templates;

namespace LR.Common.BaseIdentity.Emails.Builders
{
    public class AccountEnabledEmailTemplateTemplateBuilder : IEmailTemplateBuilder
    {
        public BaseEmailTemplate GetEmail()
        {
            return new AccountEnabledEmailTemplate
            {
                Model = new AccountEnabledEmailModel
                {
                    EventDate = DateTime.UtcNow
                }
            };
        }
    }
}
