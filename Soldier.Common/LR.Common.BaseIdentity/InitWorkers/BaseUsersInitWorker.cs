﻿using System.Threading.Tasks;
using LR.Common.Core.DAL;
using LR.Common.Core.InitWorkers.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.BaseIdentity.InitWorkers
{
    public abstract class BaseUsersInitWorker<TUser> : IInitWorker
        where TUser : BaseAppUser
    {
        protected readonly UserManager<TUser> userManager;

        protected BaseUsersInitWorker(UserManager<TUser> userManager)
        {
            this.userManager = userManager;
        }

        public virtual int Order => 9;

        public abstract void Init();

        protected async Task CreateUser(TUser user, string role, string password)
        {
            await userManager.CreateAsync(user, password);
            user = await userManager.FindByEmailAsync(user.Email);
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            await userManager.ConfirmEmailAsync(user, code);
            await userManager.AddToRoleAsync(user, role);
        }
    }
}
