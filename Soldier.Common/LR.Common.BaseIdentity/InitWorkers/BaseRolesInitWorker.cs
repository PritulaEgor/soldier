﻿using System;
using System.Threading.Tasks;
using LR.Common.Core.InitWorkers.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.BaseIdentity.InitWorkers
{
    public abstract class BaseRolesInitWorker<TRole> : IInitWorker where TRole : IdentityRole<Guid>, new()
    {
        protected readonly RoleManager<TRole> roleManager;

        protected BaseRolesInitWorker(RoleManager<TRole> roleManager)
        {
            this.roleManager = roleManager;
        }

        public int Order => 5;

        public abstract void Init();

        /// <summary>
        /// Creates the role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns></returns>
        protected async Task CreateRole(string roleName)
        {
            var isUserRoleExist = await roleManager.FindByNameAsync(roleName);
            if (isUserRoleExist == null)
            {
                await roleManager.CreateAsync(new TRole
                {
                    Name = roleName,
                    Id = Guid.NewGuid()
                });
            }
        }

        /// <summary>
        /// Removes the role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns></returns>
        protected async Task RemoveRole(string roleName)
        {
            var userRole = await roleManager.FindByNameAsync(roleName);
            if (userRole != null)
            {
                await roleManager.DeleteAsync(userRole);
            }
        }
    }
}
