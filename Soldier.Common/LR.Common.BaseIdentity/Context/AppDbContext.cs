﻿using System;
using System.Collections.Generic;
using LR.Common.Core.DAL;
using LR.Common.Core.DAL.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.BaseIdentity.Context
{
    public abstract class AppDbContext<TUser, TUserRole, TRole> : IdentityDbContext<TUser, TRole, Guid,
            IdentityUserClaim<Guid>, TUserRole, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>,
            IdentityUserToken<Guid>>,
        IDbContext
        where TUser : BaseAppUser
        where TRole : IdentityRole<Guid>
        where TUserRole : IdentityUserRole<Guid>
    {
        protected AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public IEnumerable<EntityEntry> GetEntries()
        {
            return ChangeTracker.Entries();
        }
    }
}
