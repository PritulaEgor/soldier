﻿namespace LR.Common.Auth.Constants
{
    public static class GrantTypeKeys
    {
        public const string Password = "password";

        public const string RefreshToken = "refreshToken";
    }
}
