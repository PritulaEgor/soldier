﻿using System.ComponentModel.DataAnnotations;
using LR.Common.Core;
using LR.Common.Core.ViewModels;

namespace LR.Common.Auth.ViewModels
{
    public class CompanyModel : BaseViewModel
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(300)]
        public string Address { get; set; }

        [Required]
        [MaxLength(100)]
        public string Phone { get; set; }

        [Required]
        public bool IsInternal { get; set; }
    }
}
