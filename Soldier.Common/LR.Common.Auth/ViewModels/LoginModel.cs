﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LR.Common.Auth.ViewModels
{
    public class LoginModel
    {
        [EmailAddress(ErrorMessage = "E-mail must be in X@X.X format")]
        public string Email { get; set; }

        [StringLength(32, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 8)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).*")]
        public string Password { get; set; }

        [Required]
        public string GrantType { get; set; }

        public Guid RefreshToken { get; set; }
    }
}
