﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LR.Common.Auth.ViewModels
{
    public class AcceptAgreementModel
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public string Code { get; set; }
    }
}
