﻿using System.ComponentModel.DataAnnotations;

namespace LR.Common.Auth.ViewModels
{
    public class RegisterUserModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "E-mail must be in X@X.X format")]
        public string Email { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 8)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).*")]
        public virtual string Password { get; set; }
    }
}
