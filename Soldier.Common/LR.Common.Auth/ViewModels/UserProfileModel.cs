﻿using System;

namespace LR.Common.Auth.ViewModels
{
    public class UserProfileModel : CreateUserModel
    {
        public string Username { get; set; }

        public bool IsActive { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? LastLoginFailedDate { get; set; }
    }
}
