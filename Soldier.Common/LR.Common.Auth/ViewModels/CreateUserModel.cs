﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LR.Common.Core.ViewModels;

namespace LR.Common.Auth.ViewModels
{
    public class CreateUserModel : BaseViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "E-mail must be in X@X.X format")]
        public string Email { get; set; }

        [Required]
        public string DisplayName { get; set; }

        public List<CompanyModel> Companies { get; set; }

        public string UserRole { get; set; }

        public Guid CreatorId { get; set; }
    }
}
