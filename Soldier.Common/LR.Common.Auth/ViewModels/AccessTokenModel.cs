﻿using System;

namespace LR.Common.Auth.ViewModels
{
    public class AccessTokenModel
    {
        public string AccessToken { get; set; }

        public string DisplayName { get; set; }

        public string Email { get; set; }

        public DateTime Expires { get; set; }

        public Guid RefreshToken { get; set; }

        public string Role { get; set; }

        public Guid UserId { get; set; }

        public Guid[] CompaniesId { get; set; }
    }
}
