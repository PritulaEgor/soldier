﻿using System;
using AutoMapper;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.DAL;

namespace LR.Common.Auth.Mappings
{
    public abstract class BaseUserModelProfile<TUser> : Profile
        where TUser : BaseAppUser
    {
        protected BaseUserModelProfile()
        {
            CreateMap<RegisterUserModel, TUser>()
                .ForMember(user => user.Email, c => c.MapFrom(registerModel => registerModel.Email))
                .ForMember(user => user.UserName, c => c.MapFrom(registerModel => registerModel.Email))
                .ForMember(user => user.DisplayName, c => c.MapFrom(registerModel => registerModel.DisplayName))
                .ForMember(user => user.DateCreated, c => c.MapFrom(registerModel => DateTime.UtcNow));

            CreateMap<UserProfileModel, TUser>()
                .ForMember(user => user.Id, c => c.MapFrom(userProfile => userProfile.Id))
                .ForMember(user => user.Email, c => c.MapFrom(userProfile => userProfile.Email))
                .ForMember(user => user.DateCreated, c => c.MapFrom(userProfile => userProfile.Created))
                .ForMember(user => user.DisplayName, c => c.MapFrom(userProfile => userProfile.DisplayName))
                .ForMember(user => user.DateLastLogin, c => c.MapFrom(user => user.LastLoginDate))
                .ForMember(user => user.LockoutEnabled, c => c.MapFrom(userProfile => !userProfile.IsActive))
                .ForMember(user => user.EmailConfirmed, c => c.MapFrom(userProfile => userProfile.IsEmailConfirmed))
                .ForMember(user => user.LastFailedLoginDate,
                    c => c.MapFrom(userProfile => userProfile.LastLoginFailedDate));
        }
    }
}
