﻿using AutoMapper;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.DAL;

namespace LR.Common.Auth.Mappings
{
    public abstract class BaseCompanyModelProfile<TCompany> : Profile
        where TCompany : BaseCompany
    {
        protected BaseCompanyModelProfile()
        {
            CreateMap<TCompany, CompanyModel>()
                .ForMember(cm => cm.Id, config => config.MapFrom(c => c.Id))
                .ForMember(cm => cm.Name, config => config.MapFrom(c => c.Name))
                .ForMember(cm => cm.Address, config => config.MapFrom(c => c.Address))
                .ForMember(cm => cm.Phone, config => config.MapFrom(c => c.Phone))
                .ForMember(cm => cm.IsInternal, config => config.MapFrom(c => c.IsInternal))
                .ReverseMap();
        }
    }
}
