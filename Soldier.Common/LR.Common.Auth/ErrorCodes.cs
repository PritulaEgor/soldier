﻿namespace LR.Common.Auth
{
    public static class ErrorCodes
    {
        public static readonly int DuplicateUser = 1002;
        public static readonly int WrongPassword = 1004;
        public static readonly int RegistrationFailed = 1005;
        public static readonly int EmailNotConfirmed = 1006;
        public static readonly int RefreshTokenNotExist = 1007;
        public static readonly int RefreshTokenExpired = 1008;
        public static readonly int UserIsLocked = 1009;
        public static readonly int PasswordCompromised = 1011;
        public static readonly int SimoulteneousLogin = 1014;
        public static readonly int UserTemporarilyLock = 1021;
        public static readonly int UserCannotBeDeleted = 1022;
        public static readonly int GrantTypeNotSupported = 1023;
        public static readonly int TokenTypeNotSupported = 1024;
        public static readonly int UserLockedDueToInactivity = 1025;
    }
}
