﻿using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.Auth.Exceptions
{
    public class GrantTypeNotSupportedException : MrvApplicationException
    {
        public string GrantType { get; set; }

        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;
        public override int ErrorCode => ErrorCodes.GrantTypeNotSupported;
        public override string ErrorData => GrantType ?? string.Empty;

        public GrantTypeNotSupportedException(string grantType)
        {
            this.GrantType = grantType;
        }
    }
}
