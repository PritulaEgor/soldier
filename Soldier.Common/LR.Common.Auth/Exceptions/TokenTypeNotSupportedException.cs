﻿using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.Auth.Exceptions
{
    public class TokenTypeNotSupportedException : MrvApplicationException
    {
        public string TokenType { get; set; }

        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;
        public override int ErrorCode => ErrorCodes.TokenTypeNotSupported;
        public override string ErrorData => TokenType ?? string.Empty;

        public TokenTypeNotSupportedException(string tokenType)
        {
            this.TokenType = tokenType;
        }
    }
}
