﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.Auth.Exceptions
{
    [Serializable]
    public class UserAlreadyExistException : MrvApplicationException
    {
        public string Email { get; set; }

        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.DuplicateUser;

        public override string ErrorData => Email ?? string.Empty;

        public UserAlreadyExistException(string email)
        {
            Email = email;
        }
    }
}
