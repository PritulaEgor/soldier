﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.Auth.Exceptions
{
    [Serializable]
    public class UserWasNotRegisteredException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.RegistrationFailed;

        public override string ErrorData => string.Empty;
    }
}
