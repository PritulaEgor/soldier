﻿using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.Auth.Exceptions
{
    public class UserLockedDueToInactivityException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;
        public override int ErrorCode => ErrorCodes.UserLockedDueToInactivity;
        public override string ErrorData => string.Empty;
    }
}
