﻿using System;
using System.Net;
using LR.Common.Core.Exceptions;

namespace LR.Common.Auth.Exceptions
{
    public class PasswordCompromisedException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.PasswordCompromised;

        public override string ErrorData => String.Empty;
    }
}
