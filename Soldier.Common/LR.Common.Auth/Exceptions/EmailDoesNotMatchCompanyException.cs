﻿using System;

namespace LR.Common.Auth.Exceptions
{
    [Serializable]
    public class EmailDoesNotMatchCompanyException : Exception
    {
    }
}
