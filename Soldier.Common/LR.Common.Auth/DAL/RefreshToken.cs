﻿using System;
using LR.Common.Core.DAL;

namespace LR.Common.Auth.DAL
{
    public class RefreshToken : Entity
    {
        public Guid UserId { get; set; }

        public string UserEmail { get; set; }

        public DateTime ExpiresUtc { get; set; }

        public DateTime LastTimeRefreshTokenRequest { get; set; }

        public string IPAddress { get; set; }
    }
}
