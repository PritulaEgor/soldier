﻿using System;
using System.Threading.Tasks;
using LR.Common.Auth.ViewModels;

namespace LR.Common.Auth.Services.Contracts
{
    public interface IAccountManagementService
    {
        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="createUserModel">The create user model.</param>
        /// <returns></returns>
        Task<UserProfileModel> CreateUser(CreateUserModel createUserModel);

        /// <summary>
        /// Updates the specified user profile model.
        /// </summary>
        /// <param name="userProfileModel">The user profile model.</param>
        /// <returns></returns>
        Task<bool> Update(UserProfileModel userProfileModel);

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <returns></returns>
        UserProfileModel GetCurrentUser();

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task DeleteUser(Guid id);
    }
}
