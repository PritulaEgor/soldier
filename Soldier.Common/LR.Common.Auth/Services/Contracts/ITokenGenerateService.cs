﻿using System.Collections.Generic;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.DAL;

namespace LR.Common.Auth.Services.Contracts
{
    public interface ITokenGenerateService<in TUser> where TUser : BaseAppUser
    {
        string TokenType { get; }

        AccessTokenModel GenerateToken(TUser user, IList<string> roles);
    }
}