﻿using System.Threading.Tasks;
using LR.Common.Auth.ViewModels;

namespace LR.Common.Auth.Services.Contracts
{
    /// <summary>
    /// Interface for authorization
    /// </summary>
    public interface IAuthorizator
    {
        /// <summary>
        /// Authorizes the specified login model.
        /// </summary>
        /// <param name="loginModel">Login model.</param>
        /// <returns>Access token</returns>
        Task<AccessTokenModel> Authorize(LoginModel loginModel);
    }
}
