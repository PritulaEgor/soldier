﻿using System;
using System.Collections.Generic;
using System.Linq;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services.Contracts;
using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.DAL;

namespace LR.Common.Auth.Services
{
    public class TokenGenerateServicesFactory<TUser>
        where TUser : BaseAppUser
    {
        protected readonly IEnumerable<ITokenGenerateService<TUser>> tokenGenerateServices;

        public TokenGenerateServicesFactory(IEnumerable<ITokenGenerateService<TUser>> tokenGenerateServices)
        {
            this.tokenGenerateServices = tokenGenerateServices;
        }

        public virtual ITokenGenerateService<TUser> GetTokenGenerateService(string tokenType)
        {
            try
            {
                return tokenGenerateServices.Single(service => service.TokenType == tokenType);
            }
            catch (InvalidOperationException)
            {
                throw new TokenTypeNotSupportedException(tokenType);
            }
        }
    }
}
