﻿using System.Threading.Tasks;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;

namespace LR.Common.Auth.Services
{
    public class Authorizator : IAuthorizator
    {
        protected readonly AuthorizeStrategiesFactory strategiesFactory;

        public Authorizator(AuthorizeStrategiesFactory strategiesFactory)
        {
            this.strategiesFactory = strategiesFactory;
        }

        public async Task<AccessTokenModel> Authorize(LoginModel loginModel)
        {
            var strategy = strategiesFactory.GetStrategy(loginModel.GrantType);
            return await strategy.Authorize(loginModel);
        }
    }
}
