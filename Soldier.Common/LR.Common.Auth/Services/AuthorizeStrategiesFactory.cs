﻿using System;
using System.Collections.Generic;
using System.Linq;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services.Contracts;

namespace LR.Common.Auth.Services
{
    public class AuthorizeStrategiesFactory
    {
        protected readonly IEnumerable<IAuthorizeStrategy> authorizationStrategies;

        public AuthorizeStrategiesFactory(IEnumerable<IAuthorizeStrategy> authorizationStrategies)
        {
            this.authorizationStrategies = authorizationStrategies;
        }

        public virtual IAuthorizeStrategy GetStrategy(string grantType)
        {
            try
            {
                return authorizationStrategies.Single(strategy => strategy.GrantType == grantType);
            }
            catch (InvalidOperationException)
            {
                throw new GrantTypeNotSupportedException(grantType);
            }
        }
    }
}
