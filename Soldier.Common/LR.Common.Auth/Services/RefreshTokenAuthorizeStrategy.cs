﻿using System;
using System.Threading.Tasks;
using LR.Common.Auth.Constants;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Managers;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.DAL;
using Microsoft.AspNetCore.Identity;

namespace LR.Common.Auth.Services
{
    public class RefreshTokenAuthorizeStrategy<TUser> : IAuthorizeStrategy
        where TUser : BaseAppUser
    {
        protected readonly TokenGenerateServicesFactory<TUser> tokenGenerateServicesFactory;
        protected readonly UserManager<TUser> userManager;
        protected readonly RefreshTokenManager<TUser> refreshTokenManager;

        public RefreshTokenAuthorizeStrategy(TokenGenerateServicesFactory<TUser> tokenGenerateServicesFactory,
            UserManager<TUser> userManager,
            RefreshTokenManager<TUser> refreshTokenManager)
        {
            this.tokenGenerateServicesFactory = tokenGenerateServicesFactory;
            this.userManager = userManager;
            this.refreshTokenManager = refreshTokenManager;
        }

        public string GrantType => GrantTypeKeys.RefreshToken;

        public string TokenType => TokenTypeKeys.Common;

        public async Task<AccessTokenModel> Authorize(LoginModel loginModel)
        {
            var refreshToken = refreshTokenManager.GetRefreshToken(loginModel.RefreshToken);
            if (refreshToken != null)
            {
                if (refreshToken.ExpiresUtc < DateTime.UtcNow)
                {
                    throw new RefreshTokenExpiredException();
                }

                var appUser = await userManager.FindByEmailAsync(refreshToken.UserEmail);
                var roles = await userManager.GetRolesAsync(appUser);
                return tokenGenerateServicesFactory.GetTokenGenerateService(TokenType).GenerateToken(appUser, roles);
            }

            throw new RefreshTokenNotFoundException();
        }
    }
}