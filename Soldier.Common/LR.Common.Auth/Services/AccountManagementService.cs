﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity;
using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.BaseIdentity.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Auth.Services
{
    public abstract  class AccountManagementService<TUser, TUserCompany, TCompany> : IAccountManagementService
        where TUserCompany : BaseUserCompany, new()
        where TUser : BaseAppUser, IUserAppRole, IUserCompanies<TUserCompany>
        where TCompany : BaseCompany
    {
        protected readonly ICurrentUserService<TUser> currentUserService;
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly IUnitOfWorkFactory uowFactory;
        protected readonly UserManager<TUser> userManager;
        protected readonly ILinkRepository<TUserCompany> linkRepository;
        protected readonly IRoleService<TUser> roleService;
        protected readonly IGenericRepository<TCompany> companyRepository;

        protected AccountManagementService(ICurrentUserService<TUser> currentUserService,
            IGenericRepository<TUser> userRepository, IUnitOfWorkFactory uowFactory,
            UserManager<TUser> userManager, ILinkRepository<TUserCompany> linkRepository,
            IRoleService<TUser> roleService, IGenericRepository<TCompany> companyRepository)
        {
            this.currentUserService = currentUserService;
            this.userRepository = userRepository;
            this.uowFactory = uowFactory;
            this.userManager = userManager;
            this.linkRepository = linkRepository;
            this.roleService = roleService;
            this.companyRepository = companyRepository;
        }

        public async Task<UserProfileModel> CreateUser(CreateUserModel createUserModel)
        {
            var user = userRepository.Mapper.Map<TUser>(createUserModel);
            var userCreator = userRepository.GetById(createUserModel.CreatorId, q => q.Include(u => u.UserCompanies));
            SetupPropertiesToUser(user, createUserModel);
            ValidateUser(user, createUserModel);
            var companyIds = GetAssignCompanyIds(createUserModel.Companies.Select(c => c.Id).ToList(), userCreator);
            var userModel = await RegisterUser(user, createUserModel);
            AssignCompaniesToUser(user, createUserModel, companyIds);
            return userModel;
        }

        public async Task<bool> Update(UserProfileModel userProfileModel)
        {
            var userToUpdate = userRepository.GetById(userProfileModel.Id,q => q.Include(u => u.UserCompanies));
            var isEmailUpdated = WasUserEmailUpdated(userToUpdate, userProfileModel);
            if (IsUserWasUpdated(userToUpdate, userProfileModel))
            {
                ValidateUser(userToUpdate, userProfileModel);
            }
            await UpdateUserProperties(userToUpdate, userProfileModel);
            await userManager.UpdateAsync(userToUpdate);
            return isEmailUpdated;
        }

        public UserProfileModel GetCurrentUser()
        {
            return userRepository.Mapper.Map<UserProfileModel>(currentUserService.GetCurrentUser());
        }

        public Task DeleteUser(Guid id)
        {
            if (!UserCanBeDeleted(id)) throw new UserCannotBeDeletedException();
            try
            {
                uowFactory.Execute(() => userRepository.Remove(u => u.Id == id));
            }
            catch (DbUpdateException)
            {
                throw new UserCannotBeDeletedException();
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Checking that user can be deleted.
        /// </summary>
        /// <param name="userToDeleteId">The user to delete identifier.</param>
        /// <returns>True, if user can be deleted</returns>
        protected virtual bool UserCanBeDeleted(Guid userToDeleteId)
        {
            return true;
        }

        protected virtual List<Guid> GetAssignCompanyIds(List<Guid> companyIds, TUser userCreator)
        {
            if (companyIds == null || companyIds.Count == 0)
            {
                return userCreator?.UserCompanies.Select(uc => uc.CompanyId).ToList();
            }
            return companyIds;
        }

        protected virtual void SetupPropertiesToUser(TUser user, CreateUserModel createUserModel)
        {
            user.CreatedById = createUserModel.CreatorId;
        }

        protected virtual void ValidateUser(TUser user, CreateUserModel createUserModel)
        {
            createUserModel.Companies = createUserModel.Companies ?? new List<CompanyModel>();
            var companyIds = createUserModel.Companies.Select(c => c.Id).ToList();
            if (roleService.IsInternalUser(user.Email) != companyRepository.Any(c => companyIds.Contains(c.Id) && c.IsInternal))
            {
                throw new EmailDoesNotMatchCompanyException();
            }
            roleService.ValidateRoleAssignment(currentUserService.GetActualUser(), new ChangeRoleModel
            {
                Email = user.Email,
                TargetRole = createUserModel.UserRole
            });
        }

        protected virtual void AssignCompaniesToUser(TUser user, CreateUserModel createUserModel, List<Guid> companyIds)
        {
            ProcessUserCompanies(user, companyIds);
        }

        protected async Task<UserProfileModel> RegisterUser(TUser user, CreateUserModel createUserModel)
        {
            if (await userManager.FindByEmailAsync(user.Email) != null)
            {
                throw new UserAlreadyExistException(user.Email);
            }
            SetupPropertiesToUser(user, createUserModel);
            var result = await userManager.CreateAsync(user);
            if (result.Succeeded)
            {
                var createdUser = await userManager.FindByEmailAsync(user.Email);
                await userManager.AddToRoleAsync(createdUser, createUserModel.UserRole);
                return userRepository.Mapper.Map<UserProfileModel>(createdUser);
            }
            throw new UserWasNotRegisteredException();
        }

        private void ProcessUserCompanies(TUser user, List<Guid> companiesIds)
        {
            using (uowFactory.Create())
            {
                var linkedEntities =
                    linkRepository.ProcessRelations(user.Id, companiesIds);
                user.UserCompanies = linkedEntities;
            }
        }

        protected virtual bool IsUserWasUpdated(TUser user, UserProfileModel userProfileModel)
        {
            return WasUserEmailUpdated(user, userProfileModel)
                || WasUserCompaniesUpdated(user, userProfileModel)
                || WasUserRoleUpdated(user, userProfileModel);
        }

        protected virtual async Task UpdateUserProperties(TUser userToUpdate, UserProfileModel userProfileModel)
        {
            userToUpdate.DisplayName = userProfileModel.DisplayName;
            if (WasUserCompaniesUpdated(userToUpdate, userProfileModel))
            {
                var companiesIds = userProfileModel.Companies.Select(c => c.Id).ToList();
                ProcessUserCompanies(userToUpdate, companiesIds);
            }
            if (WasUserEmailUpdated(userToUpdate, userProfileModel))
            {
                userToUpdate.Email = userProfileModel.Email;
                userToUpdate.UserName = userProfileModel.Email;
                userToUpdate.EmailConfirmed = false;
            }
            if (WasUserRoleUpdated(userToUpdate, userProfileModel))
            {
                var currentUser = currentUserService.GetActualUser();
                await roleService.AssignToRole(currentUser.Email, new ChangeRoleModel
                {
                    Email = userToUpdate.Email,
                    TargetRole = userProfileModel.UserRole
                });
            }
        }

        private bool WasUserEmailUpdated(TUser user, UserProfileModel userProfileModel)
        {
            return user.Email != userProfileModel.Email;
        }

        private bool WasUserCompaniesUpdated(TUser user, UserProfileModel userProfileModel)
        {
            return user.UserCompanies.Count != userProfileModel.Companies.Count ||
                   !user.UserCompanies.Select(uc => uc.CompanyId)
                       .All(userProfileModel.Companies.Select(c => c.Id).Contains);
        }

        private bool WasUserRoleUpdated(TUser user, UserProfileModel userProfileModel)
        {
            return user.UserRole != userProfileModel.UserRole;
        }
    }
}
