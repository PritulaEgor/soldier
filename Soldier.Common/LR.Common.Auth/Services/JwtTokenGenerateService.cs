﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using LR.Common.Auth.Constants;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Managers;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.Core.Configurations;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace LR.Common.Auth.Services
{
    public class JwtTokenGenerateService<TUser, TUserCompany> : ITokenGenerateService<TUser>
        where TUserCompany : BaseUserCompany
        where TUser : BaseAppUser, IUserCompanies<TUserCompany>
    {
        protected readonly IOptions<ApplicationOptions> configuration;
        protected readonly IGenericRepository<TUser> usersRepository;
        protected readonly RefreshTokenManager<TUser> refreshTokenManager;

        public JwtTokenGenerateService(
            IOptions<ApplicationOptions> configuration,
            IGenericRepository<TUser> usersRepository,
            RefreshTokenManager<TUser> refreshTokenManager)
        {
            this.configuration = configuration;
            this.usersRepository = usersRepository;
            this.refreshTokenManager = refreshTokenManager;
        }

        public string TokenType => TokenTypeKeys.Common;

        public AccessTokenModel GenerateToken(TUser user, IList<string> roles)
        {
            //var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.Value.IssuerSigningKey));
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("1234657890123456789012345"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            //var expires = DateTime.UtcNow.AddMinutes(configuration.Value.ExpiresMinutes);
            var expires = DateTime.UtcNow.AddMinutes(15);
            var claims = GetClaims(user, roles, expires);
            var token = GetSecurityToken(claims, expires, creds);
            var userCompanies = usersRepository.GetById(user.Id, q => q.Include(u => u.UserCompanies)).UserCompanies
                .Select(uc => uc.CompanyId).ToArray();
            var refreshToken = refreshTokenManager.CreateRefreshToken(user);
            return BuildAccessToken(user, token, roles, refreshToken, userCompanies);
        }

        private AccessTokenModel BuildAccessToken(TUser user, JwtSecurityToken token,
            IList<string> roles, RefreshToken refreshToken, Guid[] userCompanies)
        {
            return new AccessTokenModel
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                DisplayName = user.DisplayName,
                Email = user.Email,
                Expires = token.ValidTo,
                Role = roles.FirstOrDefault(),
                RefreshToken = refreshToken.Id,
                UserId = user.Id,
                CompaniesId = userCompanies
            };
        }

        private JwtSecurityToken GetSecurityToken(List<Claim> claims, DateTime expires, SigningCredentials creds)
        {
            //return new JwtSecurityToken(
            //    configuration.Value.JwtIssuer,
            //    configuration.Value.JwtIssuer,
            //    claims,
            //    expires: expires,
            //    signingCredentials: creds
            //);
            return new JwtSecurityToken(
                "LR_ABC",
                "LR_ABC",
                claims,
                expires: expires,
                signingCredentials: creds
            );
        }

        private List<Claim> GetClaims(TUser user, IList<string> roles, DateTime expires)
        {
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.DisplayName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roles.FirstOrDefault()),
                new Claim(ClaimTypes.Expired, expires.ToLongDateString())
            };
        }
    }
}
