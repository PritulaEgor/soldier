﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Auth.Constants;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.Constants;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.Configurations;
using LR.Common.Core.DAL;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace LR.Common.Auth.Services
{
    public class PasswordAuthorizeStrategy<TUser> : IAuthorizeStrategy
        where TUser : BaseAppUser
    {
        protected readonly TokenGenerateServicesFactory<TUser> tokenGenerateServicesFactory;
        protected readonly SignInManager<TUser> signInManager;
        protected readonly UserManager<TUser> userManager;
        protected readonly IOptions<ApplicationOptions> configuration;
        protected readonly IAccountAccessibilityService accountAccessibilityService;

        public PasswordAuthorizeStrategy(
            TokenGenerateServicesFactory<TUser> tokenGenerateServicesFactory,
            SignInManager<TUser> signInManager,
            UserManager<TUser> userManager,
            IOptions<ApplicationOptions> configuration,
            IAccountAccessibilityService accountAccessibilityService)
        {
            this.tokenGenerateServicesFactory = tokenGenerateServicesFactory;
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.configuration = configuration;
            this.accountAccessibilityService = accountAccessibilityService;
        }

        public virtual string GrantType => GrantTypeKeys.Password;

        public virtual string TokenType => TokenTypeKeys.Common;

        public virtual async Task<AccessTokenModel> Authorize(LoginModel loginModel)
        {
            var appUser = await userManager.FindByEmailAsync(loginModel.Email);
            await ValidateUser(appUser, loginModel.Email);

            var result = await signInManager.PasswordSignInAsync(loginModel.Email, loginModel.Password, false, false);

            return await ProcessLoginResult(result, appUser);
        }

        protected virtual async Task ValidateUser(TUser appUser, string email)
        {
            if (appUser == null)
            {
                throw new UserDoesNotExistException(email);
            }
            if (appUser.IsPasswordCompromised)
            {
                throw new PasswordCompromisedException();
            }
            //if (await IsUserSuperAdmin(appUser)
            //    && appUser.DateLastLogin != null
            //    && GetUserInactiveDays(appUser) > configuration.Value.MaxInactivityDays)
            //{
            //    await accountAccessibilityService.LockUser(appUser.Id);
            //    throw new UserLockedDueToInactivityException();
            //}
            if (await IsUserSuperAdmin(appUser)
                && appUser.DateLastLogin != null
                && GetUserInactiveDays(appUser) > 30)
            {
                await accountAccessibilityService.LockUser(appUser.Id);
                throw new UserLockedDueToInactivityException();
            }
        }

        private async Task<bool> IsUserSuperAdmin(TUser appUser)
        {
            var userRoles = await userManager.GetRolesAsync(appUser);
            return userRoles.FirstOrDefault() != BaseUserRoles.SuperUser;
        }

        private static double GetUserInactiveDays(TUser appUser)
        {
            return ((TimeSpan) (DateTime.UtcNow - appUser.DateLastLogin)).Duration().TotalDays;
        }

        protected virtual async Task<AccessTokenModel> ProcessLoginResult(SignInResult result, TUser appUser)
        {
            await UpdateLoginDate(result, appUser);
            if (result.Succeeded)
            {
                return await ProcessSucceededLoginResult(appUser);
            }
            if (result.IsNotAllowed)
            {
                throw new EmailNotConfirmedException();
            }
            if (result.IsLockedOut)
            {
                throw new UserLockedException();
            }
            //if (appUser.AccessFailedCount >= configuration.Value.MaxAccessFails)
            //{
            //    await accountAccessibilityService.LockUser(appUser.Id, true);
            //    throw new UserTemporarilyLockedException();
            //}
            if (appUser.AccessFailedCount >= 3)
            {
                await accountAccessibilityService.LockUser(appUser.Id, true);
                throw new UserTemporarilyLockedException();
            }
            await userManager.AccessFailedAsync(appUser);
            throw new UserAuthenticationException();
        }

        protected virtual async Task<AccessTokenModel> ProcessSucceededLoginResult(TUser appUser)
        {
            await userManager.ResetAccessFailedCountAsync(appUser);
            var roles = await userManager.GetRolesAsync(appUser);
            return tokenGenerateServicesFactory.GetTokenGenerateService(TokenType).GenerateToken(appUser, roles);
        }

        private async Task UpdateLoginDate(SignInResult result, TUser appUser)
        {
            if (result.Succeeded)
            {
                appUser.DateLastLogin = DateTime.UtcNow;
            }
            else
            {
                appUser.LastFailedLoginDate = DateTime.UtcNow;
            }
            await userManager.UpdateAsync(appUser);
        }
    }
}
