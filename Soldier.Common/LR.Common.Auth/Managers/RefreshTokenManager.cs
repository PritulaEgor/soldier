﻿using System;
using System.Linq;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Exceptions;
using LR.Common.Core.Configurations;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using Microsoft.Extensions.Options;

namespace LR.Common.Auth.Managers
{
    /// <summary>
    /// Refresh token manager provide possibility to get, create, remove refresh tokens.
    /// </summary>
    public class RefreshTokenManager<TUser>
        where TUser : BaseAppUser
    {
        protected readonly IGenericRepository<RefreshToken> refreshTokenRepository;
        protected readonly IOptions<ApplicationOptions> configuration;
        protected readonly IUnitOfWorkFactory uofFactory;
        protected readonly ICurrentUserService<TUser> currentUserService;

        /// <summary>
        /// Initializes a new instance of the <see>
        ///         <cref>RefreshTokenManager</cref>
        ///     </see>
        ///     class.
        /// </summary>
        /// <param name="refreshTokenRepository">The refresh token repository.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="uofFactory">The uof factory.</param>
        /// <param name="currentUserService">The HTTP context wrapper.</param>
        public RefreshTokenManager(IGenericRepository<RefreshToken> refreshTokenRepository,
            IOptions<ApplicationOptions> configuration, IUnitOfWorkFactory uofFactory,
            ICurrentUserService<TUser> currentUserService)
        {
            this.refreshTokenRepository = refreshTokenRepository;
            this.configuration = configuration;
            this.uofFactory = uofFactory;
            this.currentUserService = currentUserService;
        }

        /// <summary>
        /// Creates the refresh token.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public virtual RefreshToken CreateRefreshToken(TUser user)
        {
            var token = ReceiveRefreshToken(user);
            if (token != null)
            {
                UpdateExpirationDate(token);
                return token;
            }
            var refreshToken = BuildRefreshToken(user);
            using (uofFactory.Create())
            {
                return refreshTokenRepository.Create(refreshToken);
            }
        }

        /// <summary>
        /// Gets the refresh token.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public virtual RefreshToken ReceiveRefreshToken(TUser user)
        {
            var refreshToken = GetRefreshToken(user);
            if (refreshToken == null)
            {
                return null;
            }

            if (refreshToken.IPAddress != currentUserService.GetUserIPAddress())
            {
                if (IsRefreshTokenReceivedLongTimeAgo(refreshToken))
                {
                    RemoveRefreshToken(refreshToken.Id);
                    return CreateRefreshToken(user);
                }
                throw new SimoulteneousLoginException();
            }

            UpdateLastTimeRefreshTokenRequest(refreshToken);
            return refreshToken;
        }

        private bool IsRefreshTokenReceivedLongTimeAgo(RefreshToken refreshToken)
        {
            //return refreshToken.LastTimeRefreshTokenRequest.AddMinutes(configuration.Value.ExpiresMinutes) <
            //       DateTime.UtcNow;
            return refreshToken.LastTimeRefreshTokenRequest.AddMinutes(15) <
                   DateTime.UtcNow;
        }

        /// <summary>
        /// Gets the refresh token
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public virtual RefreshToken GetRefreshToken(TUser user)
        {
            if (user?.Email == null)
            {
                return null;
            }
            var refreshToken = refreshTokenRepository.GetBy(rt => rt.UserEmail.Equals(user.Email))
                .FirstOrDefault();
            return refreshToken;
        }

        /// <summary>
        /// Gets the refresh token.
        /// </summary>
        /// <param name="refreshTokenId">The refresh token identifier.</param>
        /// <returns></returns>
        public virtual RefreshToken GetRefreshToken(Guid refreshTokenId)
        {
            var refreshToken = refreshTokenRepository.GetById(refreshTokenId);
            return refreshToken;
        }

        /// <summary>
        /// Removes the refresh token.
        /// </summary>
        /// <param name="refreshTokenId">The refresh token identifier.</param>
        public virtual void RemoveRefreshToken(Guid refreshTokenId)
        {
            using (uofFactory.Create())
            {
                refreshTokenRepository.Remove(rt => rt.Id == refreshTokenId);
            }
        }

        private RefreshToken BuildRefreshToken(TUser user)
        {
            return new RefreshToken
            {
                UserEmail = user.Email,
                UserId = user.Id,
                //ExpiresUtc =
                //    DateTime.UtcNow.AddDays(configuration.Value.RefreshTokenExpiresDays),
                ExpiresUtc =
                    DateTime.UtcNow.AddDays(1),
                LastTimeRefreshTokenRequest = DateTime.UtcNow,
                IPAddress = currentUserService.GetUserIPAddress()
            };
        }

        private void UpdateExpirationDate(RefreshToken token)
        {
            //token.ExpiresUtc =
            //    DateTime.UtcNow.AddDays(configuration.Value.RefreshTokenExpiresDays);
            token.ExpiresUtc =
                DateTime.UtcNow.AddDays(1);
            using (uofFactory.Create())
            {
                refreshTokenRepository.Update(token);
            }
        }

        private void UpdateLastTimeRefreshTokenRequest(RefreshToken token)
        {
            if (token != null)
            {
                token.LastTimeRefreshTokenRequest = DateTime.UtcNow;
                using (uofFactory.Create())
                {
                    refreshTokenRepository.Update(token);
                }
            }
        }
    }
}