﻿using LR.Common.Auth.Managers;
using LR.Common.Auth.Services;
using LR.Common.Auth.Services.Contracts;
using LR.Common.BaseIdentity.DAL;
using LR.Common.BaseIdentity.DAL.Contracts;
using LR.Common.BaseIdentity.Services;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Auth
{
    public class AuthModule<TUser, TUserCompany> : IModule
        where TUserCompany : BaseUserCompany
        where TUser : BaseAppUser, IUserAppRole, IUserCompanies<TUserCompany>
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<RefreshTokenManager<TUser>>();
            services.AddTransient<AuthorizeStrategiesFactory>();
            services.AddTransient<TokenGenerateServicesFactory<TUser>>();
            services.AddTransient<IAuthorizator, Authorizator>();
            services.AddTransient<IAuthorizeStrategy, PasswordAuthorizeStrategy<TUser>>();
            services.AddTransient<IAuthorizeStrategy, RefreshTokenAuthorizeStrategy<TUser>>();
            services.AddTransient<ITokenGenerateService<TUser>, JwtTokenGenerateService<TUser, TUserCompany>>();
        }
    }
}
