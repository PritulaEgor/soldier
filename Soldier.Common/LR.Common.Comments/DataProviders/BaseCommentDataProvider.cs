﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LR.Common.Comments.DataProviders.Contracts;
using LR.Common.Comments.DAL;
using LR.Common.Comments.ViewModels;
using LR.Common.Core.DataProviders;
using LR.Common.Core.DataProviders.Enums;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;

namespace LR.Common.Comments.DataProviders
{
    public class CommentDataProvider<TComment, TCommentModel> : BaseDataProvider<TComment, TCommentModel>, ICommentDataProvider<TComment, TCommentModel>
        where TComment : CoreComment
        where TCommentModel : BaseCommentModel
    {
        public CommentDataProvider(IGenericRepository<TComment> repository, IUnitOfWorkFactory uowFactory) : base(repository, uowFactory)
        {
        }

        public List<TCommentModel> GetAll(Expression<Func<TComment, bool>> filterExpression, Func<IQueryable<TComment>, IQueryable<TComment>> includes)
        {
            var comments =
                repository.GetBy(filterExpression, includes);
            return repository.Mapper.Map<List<TCommentModel>>(comments.OrderByDescending(c => c.Created));
        }

        public override List<TCommentModel> GetByFilter(string value)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<TComment, bool>> GetFilterExpression(string filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Expression<Func<TComment, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy)
        {
            throw new NotImplementedException();
        }

        protected override Func<IQueryable<TComment>, IQueryable<TComment>> GetSortExpression(Dictionary<string, OrderByOption> orderBy)
        {
            throw new NotImplementedException();
        }
    }
}
