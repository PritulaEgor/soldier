﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LR.Common.Comments.DAL;
using LR.Common.Comments.ViewModels;
using LR.Common.Core.DataProviders.Contracts;

namespace LR.Common.Comments.DataProviders.Contracts
{
    public interface ICommentDataProvider<TComment, TCommentModel> : IDataProvider<TComment, TCommentModel>
        where TComment: CoreComment
        where TCommentModel: BaseCommentModel
    {
        List<TCommentModel> GetAll(Expression<Func<TComment, bool>> commentFilter,
            Func<IQueryable<TComment>, IQueryable<TComment>> includes);
    }
}
