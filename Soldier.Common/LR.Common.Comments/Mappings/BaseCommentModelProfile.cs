﻿using AutoMapper;
using LR.Common.Comments.DAL;
using LR.Common.Comments.ViewModels;
using LR.Common.Core.DAL;

namespace LR.Common.Comments.Mappings
{
    public abstract class BaseCommentModelProfile<TUser> : Profile
        where TUser: BaseAppUser
    {
        protected BaseCommentModelProfile()
        {
            CreateMap<CoreComment, BaseCommentModel>()
                .ForMember(cm => cm.Id, config => config.MapFrom(c => c.Id))
                .ForMember(cm => cm.Text, config => config.MapFrom(c => c.Text))
                .ForMember(cm => cm.Created, config => config.MapFrom(c => c.Created))
                .ForMember(cm => cm.AuthorId, config => config.MapFrom(c => c.AuthorId))
                .ReverseMap();
            CreateMap<BaseComment<TUser>, BaseCommentModel>()
                .ForMember(cm => cm.Author, config => config.MapFrom(c => c.Author))
                .IncludeBase<CoreComment, BaseCommentModel>()
                .ReverseMap();
        }
    }
}
