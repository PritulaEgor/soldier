﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.Core.DAL;

namespace LR.Common.Comments.DAL
{
    public abstract class BaseComment<TUser> : CoreComment
        where TUser: BaseAppUser
    {
        [Required]
        [ForeignKey("AuthorId")]
        public new TUser Author { get; set; }
    }

    public abstract class CoreComment : Entity
    {
        [Required]
        public Guid AuthorId { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        [StringLength(1000)]
        public string Text { get; set; }

        public BaseAppUser Author { get; set; }

        [Required]
        public Guid DocumentId { get; set; }
    }
}
