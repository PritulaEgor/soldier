﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Comments.DAL.Configuration
{
    public class BaseCommentConfiguration<TComment> : IEntityTypeConfiguration<TComment>
        where TComment : CoreComment
    {
        public void Configure(EntityTypeBuilder<TComment> builder)
        {

        }
    }
}
