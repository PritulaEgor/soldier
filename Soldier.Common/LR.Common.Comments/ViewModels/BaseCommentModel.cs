﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.ViewModels;

namespace LR.Common.Comments.ViewModels
{
    public class BaseCommentModel : BaseViewModel
    {
        public Guid AuthorId { get; set; }

        public UserProfileModel Author { get; set; }

        public DateTime Created { get; set; }

        [Required]
        public string Text { get; set; }

        public Guid DocumentId { get; set; }
    }
}
