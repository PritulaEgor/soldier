﻿using LR.Common.Comments.DataProviders;
using LR.Common.Comments.DataProviders.Contracts;
using LR.Common.Comments.DAL;
using LR.Common.Comments.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Comments.Modules
{
    public class CommentsModule<TComment, TUser, TCommentModel> : IModule
        where TComment : CoreComment, new()
        where TUser : BaseAppUser
        where TCommentModel: BaseCommentModel, new()
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient(typeof(ICommentDataProvider<,>), typeof(CommentDataProvider<,>));
        }
    }
}
