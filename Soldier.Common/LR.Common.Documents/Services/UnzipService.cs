﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services
{
    public class UnzipService : IUnzipService
    {
        public List<TUploadModel> Unzip<TUploadModel>(TUploadModel zipArchiveUploadModel)
            where TUploadModel : BaseDocumentUploadModel, new()
        {
            var zipArchive = new ZipArchive(zipArchiveUploadModel.Stream, ZipArchiveMode.Read);
            return zipArchive.Entries.Where(e => e.Length > 0).Select(e =>
            {
                var stream = new MemoryStream();
                e.Open().CopyTo(stream);
                return new TUploadModel
                {
                    Filename = Path.GetFileName(e.FullName),
                    Stream = stream,
                    DocumentGroupId = zipArchiveUploadModel.DocumentGroupId,
                    ContentType = MimeTypes.GetMimeType(Path.GetFileName(e.FullName))
                };
            }).ToList();
        }
    }
}
