﻿using System;
using System.Linq;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Documents.DAL;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Documents.Services
{
    public abstract class BaseDocumentSaveStrategy<TDocument, TUser> : IDocumentSaveStrategy
            where TUser : BaseAppUser
            where TDocument : BaseDocument
    {
        protected readonly ICurrentUserService<TUser> currentUserService;
        protected readonly IGenericRepository<TDocument> documentRepository;
        protected readonly IUnitOfWorkFactory uowFactory;

        protected BaseDocumentSaveStrategy(ICurrentUserService<TUser> currentUserService,
            IGenericRepository<TDocument> documentRepository, IUnitOfWorkFactory uowFactory)
        {
            this.currentUserService = currentUserService;
            this.documentRepository = documentRepository;
            this.uowFactory = uowFactory;
        }

        public virtual BaseDocument Save(BaseDocumentUploadModel uploadModel)
        {
            var document = BuildDocument(uploadModel);
            using (uowFactory.Create())
            {
                return documentRepository.Create(document);
            }
        }

        protected abstract TDocument BuildDocument(BaseDocumentUploadModel model);

        protected virtual TDocument SetBaseDocumentProperties(TDocument document, BaseDocumentUploadModel uploadModel)
        {
            document.Filename = uploadModel.Filename;
            document.AuthorId = currentUserService.GetActualUser().Id;
            document.Uploaded = DateTime.UtcNow;
            document.ContentType = uploadModel.ContentType;
            document.DocumentGroupId = uploadModel.DocumentGroupId;
            document.RevisionNumber =
                GetDocumentRevision(document, uploadModel.DocumentGroupId);
            return document;
        }

        protected virtual int GetDocumentRevision(BaseDocument document, Guid documentGroupId)
        {
            var docsWithSameName = documentRepository.GetBy(d => d.GetType() == document.GetType() &&
                                                                 d.DocumentGroupId == documentGroupId &&
                                                                 d.Filename.Equals(document.Filename,
                                                                     StringComparison.OrdinalIgnoreCase),
                q => q.IgnoreQueryFilters());
            return docsWithSameName.Any() ? docsWithSameName.Max(d => d.RevisionNumber) + 1 : 1;
        }
    }
}
