﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LR.Common.Core.Extensions;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Documents.Attributes;
using LR.Common.Documents.DAL;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Documents.Services
{

    /// <summary>
    /// Service for controlling documents (metadata and file data)
    /// </summary>
    public class DocumentService : IDocumentService
    {
        protected readonly IEnumerable<IDocumentGroupDocumentProcessStrategy> strategies;
        protected readonly IGenericRepository<CommonDocumentGroup> documentGroupRepository; // add here plz base doc group
        protected readonly IGenericRepository<BaseDocument> documentRepository;

        public DocumentService(
            IGenericRepository<CommonDocumentGroup> documentGroupRepository,
            IEnumerable<IDocumentGroupDocumentProcessStrategy> strategies, IGenericRepository<BaseDocument> documentRepository)
        {
            this.documentGroupRepository = documentGroupRepository;
            this.strategies = strategies;
            this.documentRepository = documentRepository;
        }

        /// <summary>
        /// Upload document to storage, check permissions and attach document
        /// </summary>
        /// <param name="uploadModel">document upload model containing necessary document metadata</param>
        /// <returns>
        /// DocumentModel for saved document
        /// </returns>
        public async Task<DocumentModel> SaveNewDocument(BaseDocumentUploadModel uploadModel)
        {
            var documentGroup = documentGroupRepository.GetById(uploadModel.DocumentGroupId);
            var strategy = GetProcessStrategy(documentGroup);
            return await strategy.ProcessUpload(uploadModel);
        }

        public List<DocumentModel> GetDocumentsForReport<TDocumentType>(Guid reportId)
            where TDocumentType : BaseDocument
        {
            var documentGroup =
                documentGroupRepository.GetById(reportId, q => q.Include(dg => dg.Documents).IgnoreQueryFilters());
            var documents = documentGroup.Documents.Where(d => d.GetType() == typeof(TDocumentType));
            return documents.Select(d => documentGroupRepository.Mapper.Map<DocumentModel>(d)).ToList();
        }

        public async Task RemoveDocument(Guid documentId)
        {
            var strategy = GetProcessStrategy(GetDocumentGroupByDocument(documentId));
            await strategy.ProcessRemove(documentId);
        }

        public async Task<DocumentDownloadModel> DownloadDocument(Guid documentId)
        {
            var strategy = GetProcessStrategy(GetDocumentGroupByDocument(documentId));
            return await strategy.ProcessDownload(documentId);
        }

        private IDocumentGroupDocumentProcessStrategy GetProcessStrategy(CommonDocumentGroup documentGroup)
        {
            var strategyWhichCanProcess = strategies.Where(strat =>
            {
                var type = strat.GetAttribute<StrategyDocumentGroupTypeAttribute>().DocumentGroupType;
                return type.IsInstanceOfType(documentGroup);
            }).ToList();
            if (strategyWhichCanProcess.Count == 0 || strategyWhichCanProcess.Count == 1)
            {
                return strategyWhichCanProcess.FirstOrDefault();
            }
            return strategyWhichCanProcess.FirstOrDefault(strat =>
            {
                var type = strat.GetAttribute<StrategyDocumentGroupTypeAttribute>().DocumentGroupType;
                return type == documentGroup.GetType();
            });
        }

        private CommonDocumentGroup GetDocumentGroupByDocument(Guid documentId)
        {
            var doc = documentRepository.GetById(documentId, q => q.IgnoreQueryFilters());
            return documentGroupRepository
                .GetById(
                    doc.DocumentGroupId,
                    q => q.Include(dg => dg.Documents).IgnoreQueryFilters());
        }
    }
}
