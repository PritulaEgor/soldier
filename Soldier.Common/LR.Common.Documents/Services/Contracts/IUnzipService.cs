﻿using System.Collections.Generic;
using System.IO;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services.Contracts
{
    public interface IUnzipService
    {
        List<TUploadModel> Unzip<TUploadModel>(TUploadModel zipArchiveUploadModel)
            where TUploadModel: BaseDocumentUploadModel, new();
    }
}
