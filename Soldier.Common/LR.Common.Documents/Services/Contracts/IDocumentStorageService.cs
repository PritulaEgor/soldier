﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace LR.Common.Documents.Services.Contracts
{
    public interface IDocumentStorageService
    {
        /// <summary>
        /// Uploads file stream to storage
        /// </summary>
        /// <param name="documentId">unique id to be used to identify file in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="documentStream">file data as stream</param>
        /// <returns></returns>
        Task UploadToFileStorage(Guid documentId, string fileName, Stream documentStream);

        /// <summary>
        /// Downloads file stream from storage
        /// </summary>
        /// <param name="documentId">unique id to be used as file name in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// file data as stream
        /// </returns>
        Task<Stream> DownloadFromFileStorage(Guid documentId, string fileName);

        /// <summary>
        /// Get file access link
        /// </summary>
        /// <param name="documentId">unique id to be used as file name in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// file link in storage
        /// </returns>
        string GetDownloadFileLink(Guid documentId, string fileName);

        /// <summary>
        /// Removes file from storage
        /// </summary>
        /// <param name="documentId">unique id for file in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        Task DeleteFileFromFileStorage(Guid documentId, string fileName);

        /// <summary>
        /// Copies the file from root to document id folder, and rename to name of file.
        /// </summary>
        /// <param name="sourceFileName">Name of the source file.</param>
        /// <param name="targetFolder">The target folder.</param>
        /// <param name="targetFileName">Name of the target file.</param>
        /// <returns></returns>
        Task CopyFileFromRootAndRename(string sourceFileName, string targetFolder, string targetFileName);
    }
}
