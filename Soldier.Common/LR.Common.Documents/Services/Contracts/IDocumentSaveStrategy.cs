﻿using LR.Common.Documents.DAL;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services.Contracts
{
    public interface IDocumentSaveStrategy
    {
        BaseDocument Save(BaseDocumentUploadModel uploadModel);
    }
}
