﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LR.Common.Documents.DAL;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services.Contracts
{
    public interface IDocumentService
    {
        /// <summary>
        /// Saves a file stream to data storage and creates a new document
        /// </summary>
        /// <param name="uploadModel">document upload model containing necessary document metadata</param>
        /// <returns>DocumentModel for saved document</returns>
        Task<DocumentModel> SaveNewDocument(BaseDocumentUploadModel uploadModel);

        List<DocumentModel> GetDocumentsForReport<TDocumentType>(Guid reportId) where TDocumentType : BaseDocument;

        /// <summary>
        /// Downloads a file from data storage.
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        /// <returns>
        /// DocumentDownloadModel for saved document
        /// </returns>
        Task<DocumentDownloadModel> DownloadDocument(Guid documentId);

        /// <summary>
        /// Removes the document.
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        /// <returns></returns>
        Task RemoveDocument(Guid documentId);
    }
}
