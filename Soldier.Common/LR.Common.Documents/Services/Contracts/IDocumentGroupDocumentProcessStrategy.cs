﻿using System;
using System.Threading.Tasks;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services.Contracts
{
    public interface IDocumentGroupDocumentProcessStrategy
    {
        Task<DocumentDownloadModel> ProcessDownload(Guid documentId);

        Task ProcessRemove(Guid documentId);

        Task<DocumentModel> ProcessUpload(BaseDocumentUploadModel uploadModel);
    }
}
