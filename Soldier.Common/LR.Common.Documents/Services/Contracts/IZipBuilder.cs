﻿using System.IO;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services.Contracts
{
    public interface IZipBuilder
    {
        IZipBuilder AddFolder(string path);

        IZipBuilder AddDocument(DocumentDownloadModel document);

        IZipBuilder AddDocument(DocumentDownloadModel document, string root);

        Stream Build();
    }
}
