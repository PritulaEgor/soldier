﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Core.Exceptions;
using LR.Common.Core.Extensions;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Documents.Attributes;
using LR.Common.Documents.DAL;
using LR.Common.Documents.Exceptions;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Documents.Services
{
    public class DocumentUploadManager
    {
        protected readonly IEnumerable<IDocumentSaveStrategy> saveStrategies;
        protected readonly IDocumentStorageService storageService;
        protected readonly IGenericRepository<BaseDocument> documentRepository;
        protected readonly IUnitOfWorkFactory uowFactory;

        public DocumentUploadManager(
            IEnumerable<IDocumentSaveStrategy> saveStrategies,
            IDocumentStorageService storageService,
            IGenericRepository<BaseDocument> documentRepository,
            IUnitOfWorkFactory uowFactory)
        {
            this.saveStrategies = saveStrategies;
            this.storageService = storageService;
            this.documentRepository = documentRepository;
            this.uowFactory = uowFactory;
        }

        public virtual async Task<BaseDocument> UploadAndAttach(BaseDocumentUploadModel uploadModel)
        {
            var strategy = GetSaveStrategy(uploadModel);
            BaseDocument savedDocument = null;
            try
            {
                savedDocument = strategy.Save(uploadModel);
                await storageService.UploadToFileStorage(savedDocument.Id, savedDocument.Filename, uploadModel.Stream);
                return savedDocument;
            }
            catch (MrvApplicationException)
            {
                if (savedDocument != null)
                {
                    return savedDocument;
                }
                throw;
            }
            catch (Exception ex)
            {
                if (savedDocument != null)
                {
                    RemoveDocument(savedDocument.Id);
                }
                throw new DocumentUploadFailureException(ex.Message, ex);
            }
        }

        private IDocumentSaveStrategy GetSaveStrategy(BaseDocumentUploadModel uploadDocumentModel)
        {
            var saveStrategyAttribute = uploadDocumentModel.GetAttribute<SaveStrategyAttribute>();
            return saveStrategies.FirstOrDefault(strategy => strategy.GetType() == saveStrategyAttribute.StrategyType);
        }

        public virtual async Task Remove(BaseDocument document)
        {
            RemoveDocument(document.Id);
            await storageService.DeleteFileFromFileStorage(document.Id, document.Filename);
        }

        private void RemoveDocument(Guid savedDocumentId)
        {
            var docToRemove = documentRepository.GetById(savedDocumentId, q => q.IgnoreQueryFilters());
            if (docToRemove != null)
            {
                uowFactory.Execute(() => documentRepository.Remove(docToRemove));
            }
        }

        public virtual async Task<DocumentDownloadModel> Download(BaseDocument document)
        {
            var documentStream = await storageService.DownloadFromFileStorage(document.Id, document.Filename);
            if (documentStream.Length == 0)
            {
                throw new DocumentZeroLengthException();
            }
            return new DocumentDownloadModel(document, documentStream);
        }

        public virtual Task<DocumentDownloadModel> GetDownloadDocumentLink(BaseDocument document)
        {
            var linkToDownload = storageService.GetDownloadFileLink(document.Id, document.Filename);
            return Task.FromResult(new DocumentDownloadModel(document, linkToDownload));
        }
    }
}
