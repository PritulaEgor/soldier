﻿using System;
using System.IO;
using System.Threading.Tasks;
using LR.Common.Documents.Configurations;
using LR.Common.Documents.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage.File;

namespace LR.Common.Documents.Services
{
    public class DocumentStorageService : IDocumentStorageService
    {
        protected readonly AzureConfig azureConfig;
        protected readonly CloudFileClient cloudFileClient;
        protected readonly CloudFileShare cloudFileShare;
        protected readonly CloudFileDirectory documentsFileDirectory;

        public DocumentStorageService(IOptions<AzureConfig> azureConfigOptions, CloudFileClient cloudFileClient)
        {
            azureConfig = azureConfigOptions.Value;
            this.cloudFileClient = cloudFileClient;
            cloudFileShare = cloudFileClient.GetShareReference(azureConfig.ShareReference);
            documentsFileDirectory = cloudFileShare.GetRootDirectoryReference().GetDirectoryReference(azureConfig.DocumentDirectory);
        }

        /// <summary>
        /// Generate file access link
        /// </summary>
        /// <param name="documentId">unique id to be used as file name in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// file link in storage
        /// </returns>
        public string GetDownloadFileLink(Guid documentId, string fileName)
        {
            var fileDirectory = documentsFileDirectory.GetDirectoryReference(documentId.ToString());
            var file = fileDirectory.GetFileReference(fileName);
            var url = file.GetSharedAccessSignature(new SharedAccessFilePolicy
            {
                Permissions = SharedAccessFilePermissions.Read,
                SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddMinutes(azureConfig.AccessInMinutesTime),
                SharedAccessStartTime = DateTimeOffset.UtcNow
            });
            return new Uri(file.Uri, url).ToString();
        }

        /// <summary>
        /// Removes file from storage
        /// </summary>
        /// <param name="documentId">unique id for file in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public async Task DeleteFileFromFileStorage(Guid documentId, string fileName)
        {
            var fileDirectory = documentsFileDirectory.GetDirectoryReference(documentId.ToString());
            var file = await GetFileReference(documentId, fileName);
            await file.DeleteIfExistsAsync();
            await fileDirectory.DeleteIfExistsAsync();
        }

        public async Task CopyFileFromRootAndRename(string sourceFileName, string targetFolder, string targetFileName)
        {
            var sourceFile = documentsFileDirectory.GetFileReference(sourceFileName);
            if (await sourceFile.ExistsAsync())
            {
                var tmpFile = documentsFileDirectory.GetFileReference($"tmp{sourceFileName}");
                await tmpFile.StartCopyAsync(sourceFile);
                await sourceFile.DeleteAsync();
                var targetFile = await GetFileReference(Guid.Parse(targetFolder), targetFileName);
                await targetFile.StartCopyAsync(tmpFile);
                await tmpFile.DeleteAsync();
            }
        }

        /// <summary>
        /// Downloads file stream from storage
        /// </summary>
        /// <param name="documentId">unique id to be used as file name in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// file data as stream
        /// </returns>
        public async Task<Stream> DownloadFromFileStorage(Guid documentId, string fileName)
        {
            MemoryStream documentStream = new MemoryStream();
            var file = await GetFileReference(documentId, fileName);
            await file.DownloadToStreamAsync(documentStream);
            documentStream.Position = 0;
            return documentStream;
        }

        /// <summary>
        /// Uploads file stream to storage
        /// </summary>
        /// <param name="documentId">unique id to be used to identify file in storage</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="documentStream">file data as stream</param>
        /// <returns></returns>
        public async Task UploadToFileStorage(Guid documentId, string fileName, Stream documentStream)
        {
            var file = await GetFileReference(documentId, fileName);
            documentStream.Position = 0;
            await file.UploadFromStreamAsync(documentStream);
        }

        private async Task<CloudFile> GetFileReference(Guid documentId, string fileName)
        {
            var fileDirectory = documentsFileDirectory.GetDirectoryReference(documentId.ToString());
            await fileDirectory.CreateIfNotExistsAsync();
            return fileDirectory.GetFileReference($"{fileName}");
        }
    }
}
