﻿using System.IO;
using System.IO.Compression;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Services
{
    public class ZipBuilder : IZipBuilder
    {
        protected Stream zipStream;
        protected ZipArchive zipArchive;

        public ZipBuilder()
        {
            zipStream = new MemoryStream();
            zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Create, true);
        }

        public IZipBuilder AddFolder(string path)
        {
            zipArchive.CreateEntry(path);
            return this;
        }

        public IZipBuilder AddDocument(DocumentDownloadModel document)
        {
            return AddDocument(document, string.Empty);
        }

        public IZipBuilder AddDocument(DocumentDownloadModel document, string root)
        {
            var entry = zipArchive.CreateEntry($"{root}{Path.GetFileNameWithoutExtension(document.Filename)}" +
                                               $"_v{document.RevisionNumber}{Path.GetExtension(document.Filename)}");
            using (var entryStream = entry.Open())
            {
                document.Stream.CopyTo(entryStream);
            }
            return this;
        }

        public Stream Build()
        {
            zipArchive.Dispose();
            zipStream.Position = 0;
            return zipStream;
        }
    }
}
