﻿using System.Net;
using LR.Common.Core;
using LR.Common.Core.Exceptions;

namespace LR.Common.Documents.Exceptions
{
    public class DocumentFileNameTooLongException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.FileNameTooLong;

        public override string ErrorData => string.Empty;
    }
}
