﻿using System;
using System.Net;
using LR.Common.Core;
using LR.Common.Core.Exceptions;

namespace LR.Common.Documents.Exceptions
{
    [Serializable]
    public class DocumentZeroLengthException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.FileZeroLength;

        public override string ErrorData => string.Empty;
    }
}
