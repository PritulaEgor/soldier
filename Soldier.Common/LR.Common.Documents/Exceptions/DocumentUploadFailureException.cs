﻿using System;
using System.Net;
using LR.Common.Core;
using LR.Common.Core.Exceptions;

namespace LR.Common.Documents.Exceptions
{
    [Serializable]
    public class DocumentUploadFailureException : MrvApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.FileDocumentNotSaved;

        public override string ErrorData => string.Empty;

        public DocumentUploadFailureException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
