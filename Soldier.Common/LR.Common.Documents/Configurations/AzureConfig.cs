﻿namespace LR.Common.Documents.Configurations
{
    public class AzureConfig
    {
        public string FileShareUrl { get; set; }

        public string DocumentDirectory { get; set; }

        public string ShareReference { get; set; }

        public int AccessInMinutesTime { get; set; }
    }
}
