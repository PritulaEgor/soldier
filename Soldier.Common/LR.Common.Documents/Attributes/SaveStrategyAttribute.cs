﻿using System;

namespace LR.Common.Documents.Attributes
{
    public class SaveStrategyAttribute : Attribute
    {
        public Type StrategyType { get; set; }

        public SaveStrategyAttribute(Type strategyType)
        {
            StrategyType = strategyType;
        }
    }
}
