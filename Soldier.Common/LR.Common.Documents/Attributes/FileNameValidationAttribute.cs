﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.Documents.Exceptions;

namespace LR.Common.Documents.Attributes
{
    [AttributeUsage(AttributeTargets.Property |
                    AttributeTargets.Field)]
    public class FileNameValidationAttribute : ValidationAttribute
    {
        protected readonly int maxFileNameLength;

        public FileNameValidationAttribute(int maxFileNameLength)
        {
            this.maxFileNameLength = maxFileNameLength;
        }

        public override bool IsValid(object value)
        {
            if (value.ToString().Length > maxFileNameLength)
            {
                throw new DocumentFileNameTooLongException();
            }
            return true;
        }
    }
}
