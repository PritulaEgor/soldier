﻿using System;

namespace LR.Common.Documents.Attributes
{
    public class StrategyDocumentGroupTypeAttribute : Attribute
    {
        public Type DocumentGroupType { get; set; }

        public StrategyDocumentGroupTypeAttribute(Type documentGroupType)
        {
            DocumentGroupType = documentGroupType;
        }
    }
}
