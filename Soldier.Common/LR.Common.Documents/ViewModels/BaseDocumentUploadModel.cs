﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using LR.Common.Core.ViewModels;
using LR.Common.Documents.Attributes;

namespace LR.Common.Documents.ViewModels
{
    public abstract class BaseDocumentUploadModel : BaseViewModel
    {
        [Required]
        public Guid DocumentGroupId { get; set; }

        [Required]
        [FileNameValidation(500)]
        public string Filename { get; set; }

        public Stream Stream { get; set; }

        public string ContentType { get; set; }
    }
}
