﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.ViewModels;

namespace LR.Common.Documents.ViewModels
{
    public class DocumentModel : BaseViewModel
    {
        [Required]
        [MaxLength(500)]
        public string Filename { get; set; }

        [Required]
        public DateTime Uploaded { get; set; }

        [Required]
        public UserProfileModel Author { get; set; }

        public string ContentType { get; set; }

        public int RevisionNumber { get; set; }

        public bool? IsLocked { get; set; }

        public int CommentsCount { get; set; }

        public string LinkToDocument { get; set; }

        public Guid DocumentGroupId { get; set; }
    }
}
