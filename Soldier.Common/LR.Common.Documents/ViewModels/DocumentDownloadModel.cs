﻿using System.IO;
using LR.Common.Core.ViewModels;
using LR.Common.Documents.DAL;

namespace LR.Common.Documents.ViewModels
{
    public class DocumentDownloadModel : BaseViewModel
    {
        public DocumentDownloadModel()
        {
        }

        public DocumentDownloadModel(BaseDocument document, Stream stream)
        {
            Id = document.Id;
            Filename = document.Filename;
            ContentType = document.ContentType;
            RevisionNumber = document.RevisionNumber;
            Stream = stream;
        }

        public DocumentDownloadModel(BaseDocument document, string linkToDocument)
        {
            Id = document.Id;
            Filename = document.Filename;
            ContentType = document.ContentType;
            LinkToDocument = linkToDocument;
            RevisionNumber = document.RevisionNumber;
        }

        public string LinkToDocument { get; set; }

        public string Filename { get; set; }

        public Stream Stream { get; set; }

        public string ContentType { get; set; }

        public int RevisionNumber { get; set; }
    }
}
