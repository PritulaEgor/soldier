﻿using LR.Common.Core.IoC.Contracts;

namespace LR.Common.Documents.Modules
{
    public static class ServiceCollectionBuilderExtensions
    {
        public static IServiceCollectionBuilder WithDocumentModule(
            this IServiceCollectionBuilder serviceCollectionBuilder)
        {
            serviceCollectionBuilder.RegisterModule(new AzureConfigModule());
            serviceCollectionBuilder.RegisterModule(new DocumentManagementModule());
            return serviceCollectionBuilder;
        }
    }
}
