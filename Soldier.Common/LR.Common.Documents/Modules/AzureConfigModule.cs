﻿using LR.Common.Core.IoC.Contracts;
using LR.Common.Documents.Configurations;
using LR.Common.Documents.Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Documents.Modules
{
    public class AzureConfigModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.Configure<AzureConfig>(options =>
            {
                var configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.AddEnvironmentVariables(ConfigurationKeys.AzurePrefix);
                configurationBuilder.Build().Bind(options);
            });
        }
    }
}
