﻿using AutoMapper;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Documents.DAL;
using LR.Common.Documents.Mappings.Resolvers;
using LR.Common.Documents.Services;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Documents.Modules
{
    public class DocumentManagementModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IDocumentService, DocumentService>();
            services.AddTransient<IDocumentStorageService, DocumentStorageService>();
            services.AddTransient<DocumentUploadManager>();
            services.AddTransient<IValueResolver<BaseDocument, DocumentModel, string>, DocumentLinkResolver>();
            services.AddScoped<IZipBuilder, ZipBuilder>();
            services.AddScoped<IUnzipService, UnzipService>();
        }
    }
}
