﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using LR.Common.Core.DAL;
using LR.Common.Documents.DAL.Contracts;

namespace LR.Common.Documents.DAL
{
    public abstract class CommonDocumentGroup : Entity, IDocumentGroupDocuments
    {
        [ForeignKey("DocumentGroupId")]
        public virtual List<BaseDocument> Documents { get; set; }

        public virtual IEnumerable<BaseDocument> GetDocuments()
        {
            return Documents;
        }
    }
}
