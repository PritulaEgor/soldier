﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LR.Common.Documents.DAL.Contracts
{
    public interface IDocumentGroupDocuments
    {
        IEnumerable<BaseDocument> GetDocuments();
    }
}
