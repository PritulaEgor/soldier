﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Documents.DAL.Configurations
{
    public class CommonDocumentGroupConfiguration : IEntityTypeConfiguration<CommonDocumentGroup>
    {
        public void Configure(EntityTypeBuilder<CommonDocumentGroup> builder)
        {
            builder.HasMany(dg => dg.Documents);
        }
    }
}
