﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LR.Common.Documents.DAL
{
    public abstract class CoreDocumentGroup<TDocument> : CommonDocumentGroup
        where TDocument : BaseDocument
    {
        [ForeignKey("DocumentGroupId")]
        public new List<TDocument> Documents { get; set; }
    }
}
