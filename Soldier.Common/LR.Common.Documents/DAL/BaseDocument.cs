﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using LR.Common.Core.DAL;

namespace LR.Common.Documents.DAL
{
    public abstract class BaseDocument : Entity
    {
        [Required]
        [MaxLength(500)]
        public string Filename { get; set; }

        [Required]
        public DateTime Uploaded { get; set; }

        [Required]
        public Guid AuthorId { get; set; }

        [Required]
        public string ContentType { get; set; }

        public int RevisionNumber { get; set; }

        [ForeignKey("DocumentGroupId")]
        public Guid DocumentGroupId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int CommentCount { get; set; }
    }
}
