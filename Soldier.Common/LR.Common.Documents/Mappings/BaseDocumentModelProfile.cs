﻿using AutoMapper;
using LR.Common.Documents.DAL;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Mappings
{
    public class BaseDocumentModelProfile : Profile
    {
        public BaseDocumentModelProfile()
        {
            CreateMap<BaseDocument, DocumentModel>()
                .ForMember(documentModel => documentModel.Id, c => c.MapFrom(document => document.Id))
                .ForMember(documentModel => documentModel.Filename, c => c.MapFrom(document => document.Filename))
                .ForMember(documentModel => documentModel.RevisionNumber, c => c.MapFrom(document => document.RevisionNumber))
                .ForMember(documentModel => documentModel.CommentsCount, c => c.MapFrom(document => document.CommentCount))
                .ForMember(documentModel => documentModel.LinkToDocument, c => c.ResolveUsing<IValueResolver<BaseDocument, DocumentModel, string>>())
                .ForMember(documentModel => documentModel.DocumentGroupId, c => c.MapFrom(document => document.DocumentGroupId))
                .ReverseMap();
        }
    }
}
