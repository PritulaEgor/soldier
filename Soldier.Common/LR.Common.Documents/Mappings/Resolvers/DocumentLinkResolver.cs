﻿using System.Reflection.Metadata;
using AutoMapper;
using LR.Common.Documents.DAL;
using LR.Common.Documents.Services.Contracts;
using LR.Common.Documents.ViewModels;

namespace LR.Common.Documents.Mappings.Resolvers
{
    public class DocumentLinkResolver : IValueResolver<BaseDocument, DocumentModel, string>
    {
        protected readonly IDocumentStorageService documentStorageService;

        public DocumentLinkResolver(IDocumentStorageService documentStorageService)
        {
            this.documentStorageService = documentStorageService;
        }

        public string Resolve(BaseDocument source, DocumentModel destination, string destMember, ResolutionContext context)
        {
            return documentStorageService.GetDownloadFileLink(source.Id, source.Filename);
        }
    }
}
