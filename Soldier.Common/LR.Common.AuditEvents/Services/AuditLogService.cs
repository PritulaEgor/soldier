﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LR.Common.AuditEvents.Attributes;
using LR.Common.AuditEvents.Builders.Contracts;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Services
{
    public class AuditLogService<TUser> : IAuditLogService
        where TUser : BaseAppUser
    {
        protected readonly ICurrentUserService<TUser> currentUserService;
        protected readonly IGenericRepository<BaseAuditEvent> auditRepository;
        protected readonly IEnumerable<IAuditLogEntryBuilder> auditLogEntryBuilders;

        public AuditLogService(ICurrentUserService<TUser> currentUserService,
            IGenericRepository<BaseAuditEvent> auditRepository,
            IEnumerable<IAuditLogEntryBuilder> auditLogEntryBuilders)
        {
            this.currentUserService = currentUserService;
            this.auditRepository = auditRepository;
            this.auditLogEntryBuilders = auditLogEntryBuilders;
        }

        public void LogAuditEntitiesChangeEvents(IEnumerable<EntityEntry> entries)
        {
            var listAuditEvents = new List<BaseAuditEvent>();
            entries.Where(ShouldTrack).ToList().ForEach(entry =>
            {
                var entityAuditLogEntry = GetEntityAuditLogEntries(entry);
                if (entityAuditLogEntry != null)
                {
                    listAuditEvents.AddRange(
                        entityAuditLogEntry
                            .Select(logEntry =>
                            {
                                SetUserIdForLogEntry(logEntry);
                                return logEntry;
                            }).Where(logEntry => logEntry.UserId != Guid.Empty)
                    );
                }
            });
            listAuditEvents.ForEach(a => auditRepository.Create(a));
        }

        protected void SetUserIdForLogEntry(BaseAuditEvent logEntry)
        {
            var appUser = currentUserService.GetActualUser();
            if (appUser != null)
            {
                logEntry.UserId = appUser.Id;
            }
        }

        protected List<BaseAuditEvent> GetEntityAuditLogEntries(EntityEntry entityEntry)
        {
            return auditLogEntryBuilders.Where(builder => builder.CanBuild(entityEntry))
                .Select(b => b.Build(entityEntry)).ToList();
        }

        protected bool ShouldTrack(EntityEntry entry)
        {
            return entry.State == EntityState.Detached
                || entry.State == EntityState.Unchanged
                || entry.Entity.GetType().GetCustomAttribute<AuditLogEntityAttribute>() != null;
        }
    }
}
