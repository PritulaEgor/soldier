﻿using LR.Common.AuditEvents.DAL.Configurations;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.AuditEvents.Extensions
{
    public static class ModelBuilderAuditEventsExtensions
    {
        public static void ApplyAuditEventsConfiguration(this ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BaseEntityAuditEventConfiguration());
            builder.ApplyConfiguration(new EntityCreatedAuditEventConfiguration());
            builder.ApplyConfiguration(new EntityChangedAuditEventConfiguration());
            builder.ApplyConfiguration(new EntityDeletedAuditEventConfiguration());
            builder.ApplyConfiguration(new EntityLinkedAuditEventConfiguration());
            builder.ApplyConfiguration(new EntityUnlinkedAuditEventConfiguration());
            builder.ApplyConfiguration(new FieldValueChangedAuditEventConfiguration());
            builder.ApplyConfiguration(new LoginAuditEventConfiguration());
            builder.ApplyConfiguration(new AgreementAcceptedAuditEventConfiguration());
        }
    }
}
