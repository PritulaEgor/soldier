﻿namespace LR.Common.AuditEvents.DAL
{
    public class EntityCreatedAuditEvent : BaseEntityAuditEvent
    {
        public EntityCreatedAuditEvent()
        {
        }

        public EntityCreatedAuditEvent(BaseEntityAuditEvent baseEntityAuditEvent) : base(baseEntityAuditEvent)
        {
        }
    }
}
