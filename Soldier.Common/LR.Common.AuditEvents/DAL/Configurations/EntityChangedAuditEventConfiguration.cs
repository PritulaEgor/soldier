﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class EntityChangedAuditEventConfiguration : IEntityTypeConfiguration<EntityChangedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<EntityChangedAuditEvent> builder)
        {
            builder.HasBaseType<BaseEntityAuditEvent>();
        }
    }
}
