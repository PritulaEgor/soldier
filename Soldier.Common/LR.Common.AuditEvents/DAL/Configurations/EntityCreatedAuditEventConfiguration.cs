﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class EntityCreatedAuditEventConfiguration : IEntityTypeConfiguration<EntityCreatedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<EntityCreatedAuditEvent> builder)
        {
            builder.HasBaseType<BaseEntityAuditEvent>();
        }
    }
}
