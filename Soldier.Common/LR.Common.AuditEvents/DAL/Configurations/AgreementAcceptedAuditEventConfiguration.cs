﻿using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class AgreementAcceptedAuditEventConfiguration : IEntityTypeConfiguration<AgreementAcceptedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<AgreementAcceptedAuditEvent> builder)
        {
            builder.HasBaseType<BaseAuditEvent>();
        }
    }
}
