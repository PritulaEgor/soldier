﻿using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class BaseEntityAuditEventConfiguration : IEntityTypeConfiguration<BaseEntityAuditEvent>
    {
        public void Configure(EntityTypeBuilder<BaseEntityAuditEvent> builder)
        {
            builder.HasBaseType<BaseAuditEvent>();
            builder.Property(e => e.IsOptimized).HasDefaultValue(false);
        }
    }
}
