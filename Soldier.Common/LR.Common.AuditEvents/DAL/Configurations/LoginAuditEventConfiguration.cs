﻿using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class LoginAuditEventConfiguration : IEntityTypeConfiguration<LoginAuditEvent>
    {
        public void Configure(EntityTypeBuilder<LoginAuditEvent> builder)
        {
            builder.HasBaseType<BaseAuditEvent>();
        }
    }
}
