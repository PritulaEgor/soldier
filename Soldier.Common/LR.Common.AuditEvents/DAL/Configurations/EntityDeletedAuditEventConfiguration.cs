﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class EntityDeletedAuditEventConfiguration : IEntityTypeConfiguration<EntityDeletedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<EntityDeletedAuditEvent> builder)
        {
            builder.HasBaseType<BaseEntityAuditEvent>();
        }
    }
}
