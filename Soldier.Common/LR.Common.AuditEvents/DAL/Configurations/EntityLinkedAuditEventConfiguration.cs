﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class EntityLinkedAuditEventConfiguration : IEntityTypeConfiguration<EntityLinkedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<EntityLinkedAuditEvent> builder)
        {
            builder.HasBaseType<BaseEntityAuditEvent>();
        }
    }
}
