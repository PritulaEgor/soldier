﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class EntityUnlinkedAuditEventConfiguration : IEntityTypeConfiguration<EntityUnlinkedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<EntityUnlinkedAuditEvent> builder)
        {
            builder.HasBaseType<EntityLinkedAuditEvent>();
        }
    }
}
