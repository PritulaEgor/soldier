﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.AuditEvents.DAL.Configurations
{
    public class FieldValueChangedAuditEventConfiguration : IEntityTypeConfiguration<FieldValueChangedAuditEvent>
    {
        public void Configure(EntityTypeBuilder<FieldValueChangedAuditEvent> builder)
        {
            builder.HasBaseType<BaseFieldChangedAuditEvent>();
        }
    }
}
