﻿using System.ComponentModel.DataAnnotations;
using LR.Common.Core.DAL;

namespace LR.Common.AuditEvents.DAL
{
    public class BaseFieldChangedAuditEvent : Entity
    {
        [Required]
        public virtual string FieldName { get; set; }

        [Required]
        [MaxLength(200)]
        public string DataType { get; set; }
    }
}
