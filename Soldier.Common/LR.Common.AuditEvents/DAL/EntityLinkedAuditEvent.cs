﻿using System;
using LR.Common.Core.DAL;

namespace LR.Common.AuditEvents.DAL
{
    public class EntityLinkedAuditEvent : BaseEntityAuditEvent
    {
        public EntityLinkedAuditEvent()
        {
        }

        public EntityLinkedAuditEvent(BaseAuditEvent baseAuditEvent) : base(baseAuditEvent)
        {
        }

        public EntityLinkedAuditEvent(BaseEntityAuditEvent baseEntityAuditEvent) : base(baseEntityAuditEvent)
        {
        }

        public string LinkedEntityType { get; set; }
        public Guid LinkedId { get; set; }
    }
}
