﻿using LR.Common.Core.DAL;

namespace LR.Common.AuditEvents.DAL
{
    public class EntityUnlinkedAuditEvent : EntityLinkedAuditEvent
    {
        public EntityUnlinkedAuditEvent()
        {
        }

        public EntityUnlinkedAuditEvent(BaseAuditEvent baseEntityAuditEvent) : base(baseEntityAuditEvent)
        {
        }
    }
}
