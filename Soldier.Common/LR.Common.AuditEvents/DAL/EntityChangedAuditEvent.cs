﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LR.Common.AuditEvents.DAL
{
    public class EntityChangedAuditEvent : BaseEntityAuditEvent
    {
        public EntityChangedAuditEvent()
        {
        }

        public EntityChangedAuditEvent(BaseEntityAuditEvent baseAuditEvent) : base(baseAuditEvent)
        {
        }

        [Required]
        public virtual List<BaseFieldChangedAuditEvent> ChangedFields { get; set; }
    }
}
