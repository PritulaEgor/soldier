﻿namespace LR.Common.AuditEvents.DAL
{
    public class FieldValueChangedAuditEvent : BaseFieldChangedAuditEvent
    {
        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string OldValueSerialized { get; set; }

        public string NewValueSerialized { get; set; }
    }
}
