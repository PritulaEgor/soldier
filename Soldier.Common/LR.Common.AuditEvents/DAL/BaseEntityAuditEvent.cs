﻿using System;
using LR.Common.Core.DAL;

namespace LR.Common.AuditEvents.DAL
{
    public class BaseEntityAuditEvent : BaseAuditEvent
    {
        public BaseEntityAuditEvent()
        {
        }

        public BaseEntityAuditEvent(BaseAuditEvent baseAuditEvent) : base(baseAuditEvent)
        {
        }

        public BaseEntityAuditEvent(BaseEntityAuditEvent baseEntityAuditEvent) : base(baseEntityAuditEvent)
        {
            EntityId = baseEntityAuditEvent.EntityId;
            EntityType = baseEntityAuditEvent.EntityType;
            SerializedValue = baseEntityAuditEvent.SerializedValue;
        }

        public Guid EntityId { get; set; }

        public string EntityType { get; set; }

        public string SerializedValue { get; set; }
    }
}
