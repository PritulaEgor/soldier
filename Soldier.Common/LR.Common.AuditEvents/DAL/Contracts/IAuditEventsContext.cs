﻿using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.AuditEvents.DAL.Contracts
{
    public interface IAuditEventsContext
    {
        DbSet<BaseAuditEvent> AuditEvents { get; set; }
        DbSet<BaseFieldChangedAuditEvent> FieldValueChangedAuditEvents { get; set; }
    }
}
