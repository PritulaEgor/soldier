﻿using LR.Common.AuditEvents.Builders;
using LR.Common.AuditEvents.Builders.Contracts;
using LR.Common.AuditEvents.Services;
using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using LR.Common.Core.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.AuditEvents.Modules
{
    public class AuditEventsLogModule<TUser> : IModule
        where TUser : BaseAppUser
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IAuditLogService, AuditLogService<TUser>>();
            services.AddTransient<IAuditLogEntryBuilder, EntityChangedAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, EntityCreatedAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, EntityDeletedAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, SourceEntityLinkedAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, TargetEntityLinkedAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, SourceEntityUnlinkedAuditEventBuilder>();
            services.AddTransient<IAuditLogEntryBuilder, TargetEntityUnlinkedAuditEventBuilder>();
        }
    }
}
