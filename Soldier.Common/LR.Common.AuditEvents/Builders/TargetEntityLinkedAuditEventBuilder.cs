﻿using System.Reflection;
using LR.Common.AuditEvents.Attributes;
using LR.Common.AuditEvents.DAL;
using LR.Common.AuditEvents.Helpers;
using LR.Common.Core.DAL.Contracts;

namespace LR.Common.AuditEvents.Builders
{
    public class TargetEntityLinkedAuditEventBuilder : SourceEntityLinkedAuditEventBuilder
    {
        protected override void SetLinkValue(EntityLinkedAuditEvent linkedEntityEvent, ILinkEntity linkEntity)
        {
            var linkToEntity = linkEntity.GetType().GetCustomAttribute<LinkToEntityAttribute>();
            linkedEntityEvent.EntityId = linkEntity.RightId;
            linkedEntityEvent.EntityType = linkToEntity.RightType.Name;
            linkedEntityEvent.LinkedId = linkEntity.LeftId;
            linkedEntityEvent.LinkedEntityType = AuditHelper.GetEntityType(linkToEntity.LeftType.Name);
        }
    }
}
