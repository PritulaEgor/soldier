﻿using System;
using System.Linq;
using AutoMapper;
using LR.Common.AuditEvents.DAL;
using LR.Common.AuditEvents.Helpers;
using LR.Common.Core.DAL;
using LR.Common.Core.DAL.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Builders
{
    public class EntityChangedAuditEventBuilder : BaseEntityAuditLogEventBuilder
    {
        public EntityChangedAuditEventBuilder(IMapper mapper) : base(mapper)
        {
        }

        public override bool CanBuild(EntityEntry entityEntry)
        {
            return entityEntry.Entity is IEntity && entityEntry.State == EntityState.Modified;
        }

        public override BaseAuditEvent Build(EntityEntry entityEntry)
        {
            var baseEvent = base.Build(entityEntry) as BaseEntityAuditEvent;
            var fieldsChanged = entityEntry.Properties
                .Where(prop => prop.IsModified)
                .Select(GetFieldValueChangedAuditEvent).ToList();
            var createdEntityEvent = new EntityChangedAuditEvent(baseEvent) { ChangedFields = fieldsChanged };
            return createdEntityEvent;
        }

        private BaseFieldChangedAuditEvent GetFieldValueChangedAuditEvent(PropertyEntry prop)
        {
            var dataType = AuditHelper.GetDataType(prop);
            var fieldChangedAudit = new FieldValueChangedAuditEvent
            {
                DataType = dataType,
                FieldName = prop.Metadata.Name,
                OldValue = prop.OriginalValue == null ? "null" : prop.OriginalValue.ToString(),
                NewValue = prop.CurrentValue == null ? "null" : prop.CurrentValue.ToString()
            };
            if (dataType == typeof(Guid).Name)
            {
                var fk = prop.Metadata.GetContainingForeignKeys().ToList()[0];
                fieldChangedAudit.DataType = fk.DependentToPrincipal.ClrType.Name;
                fieldChangedAudit.FieldName = fk.DependentToPrincipal.Name;
            }

            return fieldChangedAudit;
        }
    }
}
