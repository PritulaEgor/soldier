﻿using System;
using LR.Common.AuditEvents.Builders.Contracts;
using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Builders
{
    public abstract class BaseAuditLogEntityBuilder : IAuditLogEntryBuilder
    {
        public abstract bool CanBuild(EntityEntry entityEntry);

        public virtual BaseAuditEvent Build(EntityEntry entityEntry)
        {
            return new BaseAuditEvent
            {
                EventDate = DateTime.UtcNow
            };
        }
    }
}
