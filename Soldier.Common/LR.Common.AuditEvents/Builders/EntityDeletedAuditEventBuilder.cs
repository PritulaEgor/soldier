﻿using AutoMapper;
using LR.Common.AuditEvents.DAL;
using LR.Common.Core.DAL;
using LR.Common.Core.DAL.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Builders
{
    public class EntityDeletedAuditEventBuilder : BaseEntityAuditLogEventBuilder
    {
        public EntityDeletedAuditEventBuilder(IMapper mapper) : base(mapper)
        {
        }

        public override bool CanBuild(EntityEntry entityEntry)
        {
            return entityEntry.Entity is IEntity && entityEntry.State == EntityState.Deleted;
        }

        public override BaseAuditEvent Build(EntityEntry entityEntry)
        {
            var baseEvent = base.Build(entityEntry) as BaseEntityAuditEvent;
            var deletedEntityEvent = new EntityDeletedAuditEvent(baseEvent);
            return deletedEntityEvent;
        }

    }
}
