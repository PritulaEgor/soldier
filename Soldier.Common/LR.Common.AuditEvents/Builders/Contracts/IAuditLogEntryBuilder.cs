﻿using LR.Common.Core.DAL;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Builders.Contracts
{
    public interface IAuditLogEntryBuilder
    {
        bool CanBuild(EntityEntry entityEntry);

        BaseAuditEvent Build(EntityEntry entityEntry);
    }
}
