﻿using System;
using AutoMapper;
using LR.Common.AuditEvents.DAL;
using LR.Common.AuditEvents.Helpers;
using LR.Common.Core.DAL;
using LR.Common.Core.Helpers;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Builders
{
    public abstract class BaseEntityAuditLogEventBuilder : BaseAuditLogEntityBuilder
    {
        protected readonly IMapper mapper;

        protected BaseEntityAuditLogEventBuilder(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public override BaseAuditEvent Build(EntityEntry entityEntry)
        {
            var entity = (Entity)entityEntry.Entity;
            var baseEvent = base.Build(entityEntry);
            var serializedValue = GetSerializedValue(entity);
            var baseEntityEvent = new BaseEntityAuditEvent(baseEvent)
            {
                SerializedValue = serializedValue ?? JsonHelper.SerializeObjectToJson(entity),
                EntityId = entity.Id,
                EntityType = AuditHelper.GetEntityType(entity),
                IsOptimized = serializedValue != null
            };
            return baseEntityEvent;
        }

        protected virtual string GetSerializedValue(Entity entity)
        {
            try
            {
                return mapper.Map<EntitySerializedValue>(entity).SerializedValue;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class EntitySerializedValue
    {
        public string SerializedValue { get; set; }
    }
}
