﻿using System.Reflection;
using LR.Common.AuditEvents.Attributes;
using LR.Common.AuditEvents.DAL;
using LR.Common.Core.DAL;
using LR.Common.Core.DAL.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Builders
{
    public class SourceEntityLinkedAuditEventBuilder : BaseAuditLogEntityBuilder
    {
        public override bool CanBuild(EntityEntry entityEntry)
        {
            return entityEntry.Entity is ILinkEntity && entityEntry.State == EntityState.Added;
        }

        public override BaseAuditEvent Build(EntityEntry entityEntry)
        {
            ILinkEntity linkEntity = (ILinkEntity)entityEntry.Entity;
            var baseEvent = base.Build(entityEntry);
            var linkedEntityEvent = new EntityLinkedAuditEvent(baseEvent);
            SetLinkValue(linkedEntityEvent, linkEntity);
            return linkedEntityEvent;
        }

        protected virtual void SetLinkValue(EntityLinkedAuditEvent linkedEntityEvent, ILinkEntity linkEntity)
        {
            var linkToEntity = linkEntity.GetType().GetCustomAttribute<LinkToEntityAttribute>();
            linkedEntityEvent.EntityId = linkEntity.LeftId;
            linkedEntityEvent.EntityType = linkToEntity.LeftType.Name;
            linkedEntityEvent.LinkedId = linkEntity.RightId;
            linkedEntityEvent.LinkedEntityType = linkToEntity.RightType.Name;
        }
    }
}
