﻿using System;

namespace LR.Common.AuditEvents.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LinkToEntityAttribute : Attribute
    {
        public Type LeftType { get; set; }

        public Type RightType { get; set; }

        public LinkToEntityAttribute(Type leftType, Type rightType)
        {
            LeftType = leftType;
            RightType = rightType;
        }
    }
}
