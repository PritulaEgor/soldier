﻿using LR.Common.Core.DAL.Contracts;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LR.Common.AuditEvents.Helpers
{
    public class AuditHelper
    {
        public static string GetEntityType(IEntity entityObject)
        {
            return entityObject.GetType().Name;
        }

        public static string GetEntityType(string entityType)
        {
            return entityType;
        }

        public static string GetDataType(PropertyEntry property)
        {
            return property.Metadata.ClrType.Name;
        }
    }
}
