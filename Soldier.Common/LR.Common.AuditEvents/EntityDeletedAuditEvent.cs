﻿using LR.Common.AuditEvents.DAL;

namespace LR.Common.AuditEvents
{
    public class EntityDeletedAuditEvent : BaseEntityAuditEvent
    {
        public EntityDeletedAuditEvent()
        {
        }

        public EntityDeletedAuditEvent(BaseEntityAuditEvent baseAuditEvent) : base(baseAuditEvent)
        {
        }
    }
}
