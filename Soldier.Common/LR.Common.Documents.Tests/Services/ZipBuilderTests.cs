﻿using System.IO;
using System.IO.Compression;
using System.Text;
using LR.Common.Documents.Services;
using LR.Common.Documents.Tests.DAL;
using LR.Common.Documents.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Documents.Tests.Services
{
    [TestClass]
    public class ZipBuilderTests
    {
        [TestMethod]
        public void Build_EmptyArchive()
        {
            var builder = new ZipBuilder();
            var zipStream = builder.Build();

            Assert.IsNotNull(zipStream);
        }

        [TestMethod]
        public void Build_AddedFolder()
        {
            var builder = new ZipBuilder();
            var zipStream = builder
                .AddFolder("test")
                .Build();

            Assert.IsNotNull(zipStream);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            var testEntryFolder = zipArchive.GetEntry("test");
            Assert.IsNotNull(testEntryFolder);
            Assert.IsNull(zipArchive.GetEntry("test1"));
        }

        [TestMethod]
        public void Build_AddedDocument()
        {
            var builder = new ZipBuilder();
            var zipStream = builder
                .AddDocument(new DocumentDownloadModel(new Document {Filename = "testFile.docx", RevisionNumber = 0},
                    new MemoryStream(Encoding.ASCII.GetBytes("Test"))))
                .Build();

            Assert.IsNotNull(zipStream);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            var document = zipArchive.GetEntry("testFile_v0.docx");
            Assert.IsNotNull(document);
        }
    }
}
