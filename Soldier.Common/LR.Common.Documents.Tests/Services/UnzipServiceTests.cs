﻿using System;
using System.IO;
using System.Text;
using LR.Common.Documents.Services;
using LR.Common.Documents.Tests.DAL;
using LR.Common.Documents.Tests.ViewModels;
using LR.Common.Documents.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Documents.Tests.Services
{
    [TestClass]
    public class UnzipServiceTests
    {
        [TestMethod]
        public void Build_CheckHowWork()
        {
            var builder = new ZipBuilder();
            var zipStream = builder
                .AddDocument(new DocumentDownloadModel(new Document { Filename = "testFile.docx", RevisionNumber = 0 },
                    new MemoryStream(Encoding.ASCII.GetBytes("Test"))))
                    .AddFolder("test")
                .AddDocument(new DocumentDownloadModel(new Document { Filename = "testFileInTestFolder.docx", RevisionNumber = 0 },
                    new MemoryStream(Encoding.ASCII.GetBytes("Test"))), "test/")
                .Build();

            var zipDoc = new DocumentUploadModel
            {
                DocumentGroupId = Guid.NewGuid(),
                Stream = zipStream
            };

            var sut = new UnzipService();
            var res = sut.Unzip(zipDoc);

            Assert.AreEqual(2, res.Count);
            Assert.IsTrue(res.TrueForAll(model => model.DocumentGroupId == zipDoc.DocumentGroupId));
            Assert.IsTrue(res.Exists(model => model.Filename == "testFile_v0.docx"));
            Assert.IsTrue(res.Exists(model => model.Filename == "testFileInTestFolder_v0.docx"));
            Assert.IsFalse(res.Exists(model => model.Filename == "testNotExist.FIle"));
        }
    }
}
