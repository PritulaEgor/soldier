﻿using LR.Common.Documents.Attributes;
using LR.Common.Documents.Exceptions;
using LR.Common.Documents.Tests.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Documents.Tests.Attributes
{
    [TestClass]
    public class FileNameValidationAttributeTests
    {
        [TestMethod]
        public void IsValid()
        {
            var attr = new FileNameValidationAttribute(10);
            Assert.IsTrue(attr.IsValid("test"));
        }

        [TestMethod]
        [ExpectedException(typeof(DocumentFileNameTooLongException))]
        public void NotValid()
        {
            var attr = new FileNameValidationAttribute(1);
            attr.IsValid("test");
        }

        [TestMethod]
        public void SaveStrategyAttribute_InitializedWell()
        {
            var sut = new SaveStrategyAttribute(typeof(Document));
            Assert.AreEqual(typeof(Document), sut.StrategyType);
        }

        [TestMethod]
        public void StrategyDocumentGroupTypeAttribute_InitializedWell()
        {
            var sut = new StrategyDocumentGroupTypeAttribute(typeof(Document));
            Assert.AreEqual(typeof(Document), sut.DocumentGroupType);
        }
    }
}
