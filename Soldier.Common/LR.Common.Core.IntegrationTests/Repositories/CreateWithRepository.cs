﻿using System;
using System.Linq;
using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class CreateWithRepository
    {
        public TestDbContext GetContext(string name)
        {
            CommonMapperCreator.Create();
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase(name)
                .Options;
            return new TestDbContext(options);
        }

        [TestMethod]
        public void Create_Item()
        {
            var ctx = GetContext("create_item");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Create(new TestEntity { Name = "test2" });
            ctx.SaveChanges();
            Assert.AreEqual(1, ctx.TestEntities.Count());
        }

        [TestMethod]
        public void Create_Item_WithChild()
        {
            var ctx = GetContext("create_item_with_child");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Create(new TestEntity {Name = "test3", Child = new TestChild {Name = "child 1"}});
            ctx.SaveChanges();
            Assert.AreEqual(1, ctx.TestChildren.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_Item_Null()
        {
            var ctx = GetContext("create_item_null");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Create(null);
            ctx.SaveChanges();
        }
    }
}
