﻿using System.Linq;
using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class RemoveFromRepository
    {
        public TestDbContext GetContext(string name)
        {
            CommonMapperCreator.Create();
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase($"Count_to_database_{name}")
                .Options;

            var ctx = new TestDbContext(options);
            var testEntity = new TestEntity
            {
                Name = "test1"
            };
            var testEntity2 = new TestEntity
            {
                Name = "test2"
            };
            ctx.TestEntities.Add(testEntity);
            ctx.TestEntities.Add(testEntity2);
            ctx.SaveChanges();
            return ctx;
        }

        [TestMethod]
        public void Remove_All()
        {
            var ctx = GetContext("Remove_All");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Remove(e => e != null);
            ctx.SaveChanges();
            Assert.AreEqual(0, Enumerable.ToList<TestEntity>(ctx.TestEntities).Count);
        }

        [TestMethod]
        public void Remove_Entity()
        {
            var ctx = GetContext("Remove_Entity");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var e = repo.GetBy(te => te.Name == "test1").FirstOrDefault();
            repo.Remove(e);
            ctx.SaveChanges();
            Assert.AreEqual(1, Enumerable.ToList<TestEntity>(ctx.TestEntities).Count);
        }
    }
}
