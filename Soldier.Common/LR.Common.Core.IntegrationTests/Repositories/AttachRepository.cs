﻿using System.Linq;
using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class AttachRepository
    {
        public TestDbContext TestDbContext { get; set; }

        [TestInitialize]
        public void Startup()
        {
            CommonMapperCreator.Create();
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase("Attach_to_database")
                .Options;

            TestDbContext = new TestDbContext(options);
        }

        [TestCleanup]
        public void Cleanup()
        {
            TestDbContext.Dispose();
            TestDbContext = null;
        }

        [TestMethod]
        public void Attach_Item()
        {
            var dbContextProvider = DbContextProviderMocks.GetProvider(TestDbContext).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Attach(new TestEntity { Name = "test" });
            var count = Enumerable.ToList<EntityEntry>(TestDbContext.ChangeTracker.Entries()).Count;
            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void Attach_ItemAlreadyAttached_Work()
        {
            var dbContextProvider = DbContextProviderMocks.GetProvider(TestDbContext).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var testEntity = new TestEntity { Name = "test" };
            repo.Attach(testEntity);
            repo.Attach(testEntity);
            var count = Enumerable.ToList<EntityEntry>(TestDbContext.ChangeTracker.Entries()).Count;
            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void Attach_Null_NotThrowException()
        {
            var dbContextProvider = DbContextProviderMocks.GetProvider(TestDbContext).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Attach((TestEntity) null);
        }
    }
}
