﻿using System.Linq;
using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.IntegrationTests.Creators;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class ReadFromRepositoryTest
    {
        [TestMethod]
        public void ReadFromRepository_GetAll()
        {
            var ctx = ContextInitializer.GetContext("ReadFromRepository_GetAll");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            CommonMapperCreator.Create();
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            Assert.AreEqual(4, repo.Get().Count());
            Assert.AreEqual(3, repo.Get<TestChild>().Count());
        }

        [TestMethod]
        public void ReadFromRepository_Get_Tracked()
        {
            var ctx = ContextInitializer.GetContext("ReadFromRepository_Get_Tracked");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var testEntities = repo.GetBy(te => te.Name != null).ToList();
            Assert.AreEqual(4, testEntities.Count);
            Assert.AreEqual(4, Enumerable.Count<EntityEntry>(ctx.ChangeTracker.Entries()));
        }

        [TestMethod]
        public void ReadFromRepository_Get_WithIncludes()
        {
            var ctx = ContextInitializer.GetContext("ReadFromRepository_Get_WithIncludes");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            CommonMapperCreator.Create();
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var testEntities = repo.GetBy(te => te.Child != null, q => q.Include(te => te.Child)).ToList();
            Assert.AreEqual(3, testEntities.Count);
            Assert.AreEqual(3, testEntities.Select(te => te.Child.Name).Count(s => s != null));
            Assert.AreEqual(6, Enumerable.Count<EntityEntry>(ctx.ChangeTracker.Entries()));
        }

        [TestMethod]
        public void ReadFromRepository_GetPage()
        {
            var ctx = ContextInitializer.GetContextForPaging("ReadFromRepository_GetPage");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var page = repo.GetPage(1, 5, te => te.Name != null).ToList();
            Assert.AreEqual(5, page.Count);
        }

        [TestMethod]
        public void ReadFromRepository_GetPage_SortedDesc()
        {
            var ctx = ContextInitializer.GetContextForPaging("ReadFromRepository_GetPage_Sorted");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var page = repo.GetBy(te => te.Name != null).OrderByDescending(te => te.Number).Skip(0).Take(5).ToList();
            var pageSorted = repo.GetPage(1, 5, te => te.Name != null, q => q.OrderByDescending(te => te.Number)).ToList();
            for (var i = 0; i < 5; i++)
            {
                Assert.AreEqual(page[i].Name, pageSorted[i].Name);
            }
            Assert.AreEqual(5, pageSorted.Count);
        }

        [TestMethod]
        public void ReadFromRepository_GetPage_SortedAsc()
        {
            var ctx = ContextInitializer.GetContextForPaging("ReadFromRepository_GetPage_Sorted");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var page = repo.GetBy(te => te.Name != null).OrderBy(te => te.Number).Skip(0).Take(5).ToList();
            var pageSorted = repo.GetPage(1, 5, te => te.Name != null, q => q.OrderBy(te => te.Number)).ToList();
            for (var i = 0; i < 5; i++)
            {
                Assert.AreEqual(page[i].Name, pageSorted[i].Name);
            }
            Assert.AreEqual(5, pageSorted.Count);
        }

        [TestMethod]
        public void ReadFromRepository_GetPage_SortedOnly()
        {
            var ctx = ContextInitializer.GetContextForPaging("ReadFromRepository_GetPage_SortedOnly");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var page = repo.GetBy(te => te.Name != null).OrderBy(te => te.Number).Skip(0).Take(5).ToList();
            var pageSorted = repo.GetPage(1, 5, q => q.OrderBy(te => te.Number)).ToList();
            for (var i = 0; i < 5; i++)
            {
                Assert.AreEqual(page[i].Name, pageSorted[i].Name);
            }
            Assert.AreEqual(5, pageSorted.Count);
        }

        [TestMethod]
        public void ReadFromRepository_GetPage_OutOfRange()
        {
            var ctx = ContextInitializer.GetContextForPaging("ReadFromRepository_GetPage_OutOfRange");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var page = repo.GetPage(10, 20, te => te.Name != null).OrderByDescending(te => te.Number).ToList();
            Assert.AreEqual(0, page.Count);
        }

        [TestMethod]
        public void ReadFromRepository_GetPage_Dispose()
        {
            var ctx = ContextInitializer.GetContextForPaging("ReadFromRepository_GetPage_Dispose");
            CommonMapperCreator.Create();
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            using (var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance))
            {
                var page = repo.GetPage(10, 20, te => te.Name != null).OrderByDescending(te => te.Number).ToList();
                Assert.IsNotNull(page);
            }
        }
    }
}