﻿using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class CountForRepository
    {
        public TestDbContext GetContext(string name)
        {
            CommonMapperCreator.Create();
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase($"Count_to_database_{name}")
                .Options;

            var ctx = new TestDbContext(options);
            var testEntity = new TestEntity
            {
                Name = "test1"
            };
            var testEntity2 = new TestEntity
            {
                Name = "test2"
            };
            ctx.TestEntities.Add(testEntity);
            ctx.TestEntities.Add(testEntity2);
            ctx.SaveChanges();
            return ctx;
        }

        [TestMethod]
        public void GetCount_All()
        {
            var ctx = GetContext("GetCount_All");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var res = repo.Count();
            Assert.AreEqual(2, res);
        }

        [TestMethod]
        public void GetCount_WithoutIncludes()
        {
            var ctx = GetContext("GetCount_WithoutIncludes");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var res = repo.Count(te => te.Name == "test1");
            Assert.AreEqual(1, res);
        }

        [TestMethod]
        public void GetCount_WithIncludes()
        {
            var ctx = GetContext("GetCount_WithIncludes");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Create(new TestEntity {Name = "test 3", Child = new TestChild {Name = "child 1"}});
            ctx.SaveChanges();
            var res = repo.Count(te => te.Child != null && te.Child.Name == "child 1", q => q.Include(te => te.Child));
            Assert.AreEqual(1, res);
        }
    }
}