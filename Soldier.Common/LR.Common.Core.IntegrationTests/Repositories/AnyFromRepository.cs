﻿using System;
using System.Linq;
using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class AnyFromRepository
    {
        public TestDbContext TestDbContext { get; set; }

        [TestInitialize]
        public void Startup()
        {
            CommonMapperCreator.Create();
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase("Any_to_database")
                .Options;
            
            TestDbContext = new TestDbContext(options);
            var testEntity = new TestEntity
            {
                Name = "test1"
            };
            var testEntity2 = new TestEntity
            {
                Name = "test2"
            };
            TestDbContext.TestEntities.Add(testEntity);
            TestDbContext.TestEntities.Add(testEntity2);
            TestDbContext.SaveChanges();
        }

        [TestCleanup]
        public void Cleanup()
        {
            TestDbContext.Dispose();
            TestDbContext = null;
        }

        [TestMethod]
        public void Any_ReturnTrue()
        {
            var dbContextProvider = DbContextProviderMocks.GetProvider(TestDbContext).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var res = repo.Any(te => te.Name.Contains("test1"));
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void Any_ReturnFalse()
        {
            var dbContextProvider = DbContextProviderMocks.GetProvider(TestDbContext).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var res = repo.Any(te => te.Name.Contains("newitem"));
            Assert.IsFalse(res);
        }

        [TestMethod]
        public void Any_ReturnTrue_NoTrack()
        {
            var prevCount = Enumerable.ToList<EntityEntry>(TestDbContext.ChangeTracker.Entries()).Count;
            Any_ReturnTrue();
            var count = Enumerable.ToList<EntityEntry>(TestDbContext.ChangeTracker.Entries()).Count;
            Assert.AreEqual(prevCount, count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Any_ReturnTrue_NullPredicate()
        {
            var dbContextProvider = DbContextProviderMocks.GetProvider(TestDbContext).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            repo.Any(null);
        }
    }
}
