﻿using System;
using System.Linq;
using AutoMapper;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Repositories
{
    [TestClass]
    public class UpdateInRepository
    {
        public TestDbContext GetContext(string name)
        {
            CommonMapperCreator.Create();
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase($"Count_to_database_{name}")
                .Options;

            var ctx = new TestDbContext(options);
            var testEntity = new TestEntity
            {
                Name = "test1"
            };
            var testEntity2 = new TestEntity
            {
                Name = "test2"
            };
            ctx.TestEntities.Add(testEntity);
            ctx.TestEntities.Add(testEntity2);
            ctx.SaveChanges();
            return ctx;
        }

        [TestMethod]
        public void Update()
        {
            var ctx = GetContext("Update");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);
            var e = repo.GetBy(te => te.Name == "test1").First();
            e.Name = "test111";
            repo.Update(e);
            Assert.AreEqual(1, Enumerable.Count<EntityEntry>(ctx.ChangeTracker.Entries(), ent => ent.State == EntityState.Modified));
            ctx.SaveChanges();
            e = repo.GetBy(te => te.Name == "test111").FirstOrDefault();
            Assert.IsNotNull(e);
        }

        [TestMethod]
        public void Update_NotExist_WillBeCreated()
        {
            var ctx = GetContext("Update_AfterCreate_ChangesApplied");
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var repo = new GenericRepository<TestEntity>(dbContextProvider, Mapper.Instance);

            var newNameValue = "updated name";
            var item = new TestEntity
            {
                Name = newNameValue,
                Number = 20
            };
            repo.Update(item);
            ctx.SaveChanges();

            var res = repo.GetBy(i => i.Name == newNameValue).FirstOrDefault();
            Assert.IsNotNull(res);
            Assert.IsTrue(res.Id != Guid.Empty);
        }
    }
}
