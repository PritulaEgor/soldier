﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Auth.Constants;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.Constants;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.IntegrationTests.Creators;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Core.IntegrationTests.Managers
{
    [TestClass]
    public class PasswordAuthorizeStrategyTests
    {
        protected IGenericRepository<User> GetRepository()
        {
            var userMock = new Mock<IGenericRepository<User>>();
            userMock.Setup(r => r.GetById(It.IsAny<Guid>(), It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new User { UserCompanies = new List<UserCompany>() });
            return userMock.Object;
        }

        [TestMethod]
        public void Authorize_Password_WithRightPasswordWorksGood()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_Password_WithRightPasswordWorksGood");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org" };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sut.Authorize(loginModel).Result;
            Assert.AreEqual<string>(user.Email, token.Email);
            Assert.AreEqual<string>(user.DisplayName, token.DisplayName);
            Assert.AreEqual<string>(superUserRole, token.Role);
            Assert.IsFalse(string.IsNullOrEmpty(token.AccessToken));
        }

        [TestMethod]
        public async Task Authorize_PasswordIsCompromised_ThrowException()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_PasswordIsCompromised_ThrowException");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User
            {
                Email = "super_user@lr.org",
                DisplayName = "super_user@lr.org",
                IsPasswordCompromised = true
            };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<PasswordCompromisedException>(async () =>
                await sut.Authorize(loginModel));
        }

        [TestMethod]
        public async Task Authorize_UserNotExist_ThrowException()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_UserNotExist_ThrowException");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User
            {
                Email = "super_user@lr.org",
                DisplayName = "super_user@lr.org",
                IsPasswordCompromised = true
            };
            var superUserRole = BaseUserRoles.Unassigned;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = "not@exist.user",
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<UserDoesNotExistException>(async () =>
                await sut.Authorize(loginModel));
        }

        [TestMethod]
        public async Task Authorize_LoginFailed_ThrowException()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_LoginFailed_ThrowException");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org", EmailConfirmed = true };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetFailedSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<UserAuthenticationException>(async () =>
                await sut.Authorize(loginModel));
        }

        [TestMethod]
        public async Task Authorize_LoginFailed_LastFailedLoginDateNotEmpty()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager(
                    "Authorize_LoginFailed_LastFailedLoginDateNotEmpty");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org", EmailConfirmed = true };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetFailedSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<UserAuthenticationException>(async () =>
                await sut.Authorize(loginModel));
            Assert.IsNotNull(user.LastFailedLoginDate);
        }

        [TestMethod]
        public async Task Authorize_UserIsLocked_ThrowException()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_UserIsLocked_ThrowException");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User
            {
                Email = "super_user@lr.org",
                DisplayName = "super_user@lr.org",
                EmailConfirmed = true,
                LockoutEnabled = true
            };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetFailedSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<UserLockedException>(async () =>
                await sut.Authorize(loginModel));
        }

        [TestMethod]
        public async Task Authorize_UserIsInactive_ThrowException()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_UserIsLocked_ThrowException");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User
            {
                Email = "cm@lr.org",
                DisplayName = "cm@lr.org",
                EmailConfirmed = true,
                DateLastLogin = DateTime.UtcNow.AddDays(-31),
                UserRole = "role"
            };
            var userRole  = "role";
            var users = new Dictionary<User, string>
            {
                {user, userRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetFailedSignInManager(user, userRole).Object;
            var accountAccessibilityServiceMock = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityServiceMock);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<UserLockedDueToInactivityException>(async () =>
                await sut.Authorize(loginModel));
        }


        [TestMethod]
        public async Task Authorize_EmailNotConfirmed_ThrowException()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_EmailNotConfirmed_ThrowException");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User
            {
                Email = "super_user@lr.org",
                DisplayName = "super_user@lr.org",
                EmailConfirmed = false,
                LockoutEnabled = false
            };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetFailedSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            await Assert.ThrowsExceptionAsync<EmailNotConfirmedException>(async () =>
                await sut.Authorize(loginModel));
        }

        [TestMethod]
        public void Authorize_PasswordSuccess_RefreshTokenExist()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_PasswordSuccess_RefreshTokenExist");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org" };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sut.Authorize(loginModel).Result;
            Assert.AreNotEqual<Guid>(Guid.Empty, token.RefreshToken);
        }

        [TestMethod]
        public void Authorize_PasswordSuccess_DateLastLoginUpdated()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_PasswordSuccess_DateLastLoginUpdated");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManager);
            var user = new User { Email = "super_user@lr.org", DisplayName = "super_user@lr.org" };
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var sut = new PasswordAuthorizeStrategy<User>(new TokenGenerateServicesFactory<User>(new List<ITokenGenerateService<User>> { jwtService }), signInManager, userManagerMock.Object,
                ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object, accountAccessibilityService);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sut.Authorize(loginModel).Result;
            Assert.IsNotNull(token);
            Assert.IsNotNull(user.DateLastLogin);
        }
    }
}
