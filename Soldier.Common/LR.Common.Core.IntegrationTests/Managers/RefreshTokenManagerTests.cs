﻿using System;
using System.Linq;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Exceptions;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.IntegrationTests.Creators;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Managers
{
    [TestClass]
    public class RefreshTokenManagerTests
    {
        public TestDbContext GetContext(string name)
        {
            var ctx = ContextInitializer.GetEmptyContext(name);
            ctx.Users.Add(new User
            {
                Email = "user@lr.org",
                Name = "user@lr.org",
                DisplayName = "user@lr.org",
                EmailConfirmed = true
            });
            ctx.SaveChanges();
            return ctx;
        }

        [TestMethod]
        public void CreateRefreshToken_WithIncreaseTime_RefreshTokenNotExist()
        {
            var dbName = "CreateRefreshToken_WithIncreaseTime_RefreshTokenNotExist";
            var ctx = GetContext(dbName);

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            Assert.AreEqual(0, Queryable.Count<RefreshToken>(ctx.RefreshTokens));

            sut.CreateRefreshToken(user);
            Assert.AreEqual(1, Queryable.Count<RefreshToken>(ctx.RefreshTokens));
        }

        [TestMethod]
        public void CreateRefreshToken_RefreshTokenCreated_AllPropsExist()
        {
            var dbName = "CreateRefreshToken_RefreshTokenCreated_AllPropsExist";
            var ctx = GetContext(dbName);

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            Assert.AreEqual(0, Queryable.Count<RefreshToken>(ctx.RefreshTokens));

            var result = sut.CreateRefreshToken(user);
            Assert.AreEqual(1, Queryable.Count<RefreshToken>(ctx.RefreshTokens));
            Assert.IsNotNull(result);
            Assert.AreEqual("127.0.0.1", result.IPAddress);
            Assert.IsFalse(string.IsNullOrEmpty(result.UserEmail));
            Assert.AreNotEqual(DateTime.MinValue, result.ExpiresUtc);
            Assert.AreNotEqual(DateTime.MinValue, result.LastTimeRefreshTokenRequest);
        }

        [TestMethod]
        public void GetRefreshToken_ById_RefreshTokenReturned()
        {
            var dbName = "GetRefreshToken_ById_RefreshTokenReturned";
            var ctx = GetContext(dbName);

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            sut.CreateRefreshToken(user);
            Assert.AreEqual(1, Queryable.Count<RefreshToken>(ctx.RefreshTokens));
            var rt = Queryable.First<RefreshToken>(ctx.RefreshTokens);

            var result = sut.GetRefreshToken(rt.Id);

            Assert.AreEqual(rt.Id, result.Id);
        }

        [TestMethod]
        public void GetRefreshToken_UserNotExist_ReturnNull()
        {
            var dbName = "GetRefreshToken_UserNotExist_ReturnNull";
            var ctx = GetContext(dbName);

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var user = new User {Email = "test@test.tst"};

            var rt = sut.GetRefreshToken(user);

            Assert.IsNull(rt);
        }

        [TestMethod]
        public void GetRefreshToken_UserNull_ReturnNull()
        {
            var dbName = "GetRefreshToken_UserNull_ReturnNull";
            var ctx = GetContext(dbName);

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);

            var result = sut.GetRefreshToken(null);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void CreateRefreshToken_WithIncreaseTime_RefreshTokenExistAndTimeIncreased()
        {
            var dbName = "CreateRefreshToken_WithIncreaseTime_RefreshTokenExistAndTimeIncreased";
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            sut.CreateRefreshToken(user);
            var frstToken = sut.GetRefreshToken(user);
            var expirationDate = frstToken.ExpiresUtc;
            var lastTimeRequested = frstToken.LastTimeRefreshTokenRequest;

            Assert.IsNotNull(frstToken);

            var result = sut.CreateRefreshToken(user);

            Assert.IsNotNull(result);
            Assert.AreEqual(frstToken.Id, result.Id);
            Assert.AreNotEqual(expirationDate, result.ExpiresUtc);
            Assert.IsTrue(result.ExpiresUtc > expirationDate);
            Assert.IsTrue(result.LastTimeRefreshTokenRequest > lastTimeRequested);
        }

        [TestMethod]
        public void RemoveRefreshToken_TokenExist_Success()
        {
            var dbName = "RemoveRefreshToken_TokenExist_Success";
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            sut.CreateRefreshToken(user);
            var result = sut.GetRefreshToken(user);

            Assert.IsNotNull(result);

            sut.RemoveRefreshToken(result.Id);
            result = sut.GetRefreshToken(user);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void ReceiveRefreshToken_TokenExist_Success()
        {
            var dbName = "RemoveRefreshToken_TokenExist_Success";
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            sut.CreateRefreshToken(user);
            var result = sut.ReceiveRefreshToken(user);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(SimoulteneousLoginException))]
        public void ReceiveRefreshToken_FromDifferentComputer_ThrowException()
        {
            var dbName = "ReceiveRefreshToken_FromDifferentComputer_ThrowException";
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sutC1 = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var sutC2 = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, "192.168.0.1", ctx);
            sutC1.CreateRefreshToken(user);
            var token = sutC1.ReceiveRefreshToken(user);
            Assert.IsNotNull(token);

            sutC2.ReceiveRefreshToken(user);
        }

        [TestMethod]
        public void ReceiveRefreshToken_FromDifferentComputerAfterSomeTime_ReturnNewToken()
        {
            var dbName = "ReceiveRefreshToken_FromDifferentComputerAfterSomeTime_ReturnNewToken";
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sutC1 = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var sutC2 = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, "192.168.0.1", ctx);

            var t = sutC1.CreateRefreshToken(user);

            var tmpRt = Queryable.First<RefreshToken>(ctx.RefreshTokens, rt => rt.Id == t.Id);
            tmpRt.LastTimeRefreshTokenRequest =
                tmpRt.LastTimeRefreshTokenRequest.AddMinutes(0 - (config.Object.Value.ExpiresMinutes + 1));
            ctx.RefreshTokens.Update(tmpRt);
            ctx.SaveChanges();
            
            var newToken = sutC2.ReceiveRefreshToken(user);
            Assert.AreNotEqual(tmpRt.Id, newToken.Id);
        }

        [TestMethod]
        public void ReceiveRefreshToken_FromDifferentComputerAfterSomeTime_OldTokenRemoved()
        {
            var dbName = "ReceiveRefreshToken_FromDifferentComputerAfterSomeTime_OldTokenRemoved";
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sutC1 = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            var sutC2 = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, "192.168.0.1", ctx);
            var t = sutC1.CreateRefreshToken(user);

            var tmpRt = Queryable.First<RefreshToken>(ctx.RefreshTokens, rt => rt.Id == t.Id);
            tmpRt.LastTimeRefreshTokenRequest =
                tmpRt.LastTimeRefreshTokenRequest.AddMinutes(0 - (config.Object.Value.ExpiresMinutes + 1));
            ctx.RefreshTokens.Update(tmpRt);
            ctx.SaveChanges();

            var newToken = sutC2.ReceiveRefreshToken(user);

            var res1 = sutC1.GetRefreshToken(tmpRt.Id);
            Assert.IsNull(res1);

            var res2 = sutC1.GetRefreshToken(newToken.Id);
            Assert.IsNotNull(res2);
        }


        [TestMethod]
        public void ReceiveRefreshToken_WithIncreaseTime_RefreshTokenExistAndTimeIncreased()
        {
            var dbName = "CreateRefreshToken_WithIncreaseTime_RefreshTokenExistAndTimeIncreased";
            var ctx = GetContext(dbName);
            var user = ctx.Users.FirstOrDefault(u => u.Email == "user@lr.org");

            var sut = RefreshTokenManagerInitializer.GetRefreshTokenManager(dbName, context: ctx);
            sut.CreateRefreshToken(user);
            var frstToken = sut.GetRefreshToken(user);
            var lastTimeRequested = frstToken.LastTimeRefreshTokenRequest;

            Assert.IsNotNull(frstToken);

            var result = sut.ReceiveRefreshToken(user);

            Assert.IsNotNull(result);
            Assert.AreEqual(frstToken.Id, result.Id);
            Assert.IsTrue(result.LastTimeRefreshTokenRequest > lastTimeRequested);
        }
    }
}
