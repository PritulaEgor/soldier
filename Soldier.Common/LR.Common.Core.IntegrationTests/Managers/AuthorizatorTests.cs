﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Auth.Constants;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Exceptions;
using LR.Common.Auth.Services;
using LR.Common.Auth.ViewModels;
using LR.Common.BaseIdentity.Constants;
using LR.Common.BaseIdentity.Services.Contracts;
using LR.Common.Core.IntegrationTests.Creators;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LR.Common.Core.IntegrationTests.Managers
{
    [TestClass]
    public class AuthorizatorTests
    {
        protected IGenericRepository<User> GetRepository()
        {
            var userMock = new Mock<IGenericRepository<User>>();
            userMock.Setup(r => r.GetById(It.IsAny<Guid>(), It.IsAny<Func<IQueryable<User>, IQueryable<User>>>()))
                .Returns(new User {UserCompanies = new List<UserCompany>()});
            return userMock.Object;
        }

        [TestMethod]
        public void Authorize_RefreshTokenSuccess_Login()
        {
            var rtManager =
                RefreshTokenManagerInitializer.GetRefreshTokenManager("Authorize_RefreshTokenSuccess_Login");
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object,
                GetRepository(), rtManager);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var factory = AuthorizeStrategiesFactoryCreator.GetFactory(jwtService, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManager);
            var sut = new Authorizator(factory);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sut.Authorize(loginModel).Result;

            var refreshTokenLoginModel = new LoginModel
            {
                Email = user.Email,
                RefreshToken = token.RefreshToken,
                GrantType = GrantTypeKeys.RefreshToken
            };
            var newToken = sut.Authorize(refreshTokenLoginModel).Result;
            Assert.AreEqual<string>(user.Email, newToken.Email);
            Assert.AreEqual<string>(user.DisplayName, newToken.DisplayName);
            Assert.AreEqual(superUserRole, newToken.Role);
            Assert.IsFalse(string.IsNullOrEmpty(newToken.AccessToken));
        }

        [TestMethod]
        public async Task Authorize_RefreshTokenFailed_RefreshTokenNotExist()
        {
            var testname = "Authorize_RefreshTokenFailed_RefreshTokenNotExist";
            var ctx = ContextInitializer.GetEmptyContext(testname);
            var rtManager = RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, context: ctx);
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object,
                GetRepository(), rtManager);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var factory = AuthorizeStrategiesFactoryCreator.GetFactory(jwtService, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManager);
            var sut = new Authorizator(factory);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sut.Authorize(loginModel).Result;

            Assert.AreEqual(1, Queryable.Count<RefreshToken>(ctx.RefreshTokens));
            rtManager.RemoveRefreshToken(token.RefreshToken);
            Assert.AreEqual(0, Queryable.Count<RefreshToken>(ctx.RefreshTokens));

            var refreshTokenLoginModel = new LoginModel
            {
                Email = user.Email,
                RefreshToken = token.RefreshToken,
                GrantType = GrantTypeKeys.RefreshToken
            };

            await Assert.ThrowsExceptionAsync<RefreshTokenNotFoundException>(async () =>
                await sut.Authorize(refreshTokenLoginModel));
        }

        [TestMethod]
        public async Task Authorize_NotSupportedAuthType_ThrowException()
        {
            var testname = "Authorize_NotSupportedAuthType_ThrowException";
            var ctx = ContextInitializer.GetEmptyContext(testname);
            var rtManager = RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, context: ctx);
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object,
                GetRepository(), rtManager);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var factory = AuthorizeStrategiesFactoryCreator.GetFactory(jwtService, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManager);
            var sut = new Authorizator(factory);
            var refreshTokenLoginModel = new LoginModel
            {
                Email = user.Email,
                GrantType = "unsupported"
            };

            await Assert.ThrowsExceptionAsync<GrantTypeNotSupportedException>(async () =>
                await sut.Authorize(refreshTokenLoginModel));
        }

        [TestMethod]
        public async Task Authorize_RefreshTokenExpired_ThrowException()
        {
            var testname = "Authorize_RefreshTokenExpired_ThrowException";
            var ctx = ContextInitializer.GetEmptyContext(testname);
            var rtManager = RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, context: ctx);
            var jwtService = new JwtTokenGenerateService<User, UserCompany>(ConfigurationMocks.GetConfigurationForJwtService().Object,
                GetRepository(), rtManager);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var factory = AuthorizeStrategiesFactoryCreator.GetFactory(jwtService, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManager);
            var sut = new Authorizator(factory);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sut.Authorize(loginModel).Result;

            Assert.AreEqual(1, Queryable.Count<RefreshToken>(ctx.RefreshTokens));
            var rt = Queryable.First<RefreshToken>(ctx.RefreshTokens);
            rt.ExpiresUtc = rt.ExpiresUtc.AddDays(-5);
            ctx.RefreshTokens.Update(rt);
            ctx.SaveChanges();

            var reftreshTokenLoginModel = new LoginModel
            {
                Email = user.Email,
                RefreshToken = token.RefreshToken,
                GrantType = GrantTypeKeys.RefreshToken
            };

            await Assert.ThrowsExceptionAsync<RefreshTokenExpiredException>(async () =>
                await sut.Authorize(reftreshTokenLoginModel));
        }

        [TestMethod]
        public async Task Login_FromDifferentComputersNotPossibleByRefreshToken_ThrowException()
        {
            var testname = "Login_FromDifferentComputersNotPossibleByRefreshToken_ThrowException";
            var ctx = ContextInitializer.GetEmptyContext(testname);
            var rtManagerComputer1 = RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, context: ctx);
            var rtManagerComputer2 =
                RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, "192.168.0.6", ctx);
            var jwtServiceComputer1 = new JwtTokenGenerateService<User, UserCompany>(
                ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManagerComputer1);
            var jwtServiceComputer2 = new JwtTokenGenerateService<User, UserCompany>(
                ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManagerComputer2);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var factoryComputer1 = AuthorizeStrategiesFactoryCreator.GetFactory(jwtServiceComputer1, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManagerComputer1);
            var factoryComputer2 = AuthorizeStrategiesFactoryCreator.GetFactory(jwtServiceComputer2, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManagerComputer2);
            var sutComputer1 = new Authorizator(factoryComputer1);
            var sutComputer2 = new Authorizator(factoryComputer2);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sutComputer1.Authorize(loginModel).Result;

            var refreshTokenLoginModel = new LoginModel
            {
                Email = user.Email,
                RefreshToken = token.RefreshToken,
                GrantType = GrantTypeKeys.RefreshToken
            };

            await Assert.ThrowsExceptionAsync<SimoulteneousLoginException>(async () =>
                await sutComputer2.Authorize(refreshTokenLoginModel));
        }

        [TestMethod]
        public async Task Login_FromDifferentComputersNotPossible_ThrowException()
        {
            var testname = "Login_FromDifferentComputersNotPossible_ThrowException";
            var ctx = ContextInitializer.GetEmptyContext(testname);
            var rtManagerComputer1 = RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, context: ctx);
            var rtManagerComputer2 =
                RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, "192.168.0.6", ctx);
            var jwtServiceComputer1 = new JwtTokenGenerateService<User, UserCompany>(
                ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManagerComputer1);
            var jwtServiceComputer2 = new JwtTokenGenerateService<User, UserCompany>(
                ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManagerComputer2);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;

            var factoryComputer1 = AuthorizeStrategiesFactoryCreator.GetFactory(jwtServiceComputer1, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManagerComputer1);
            var factoryComputer2 = AuthorizeStrategiesFactoryCreator.GetFactory(jwtServiceComputer2, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManagerComputer2);
            var sutComputer1 = new Authorizator(factoryComputer1);
            var sutComputer2 = new Authorizator(factoryComputer2);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sutComputer1.Authorize(loginModel).Result;
            Assert.IsNotNull(token);

            await Assert.ThrowsExceptionAsync<SimoulteneousLoginException>(async () =>
                await sutComputer2.Authorize(loginModel));
        }

        [TestMethod]
        public void Login_FromDifferentComputersByPasswordAfterSomeTime_Success()
        {
            var testname = "Login_FromDifferentComputersByPasswordAfterSomeTime_Success";
            var ctx = ContextInitializer.GetEmptyContext(testname);
            var config = ConfigurationMocks.GetConfigurationForJwtService();
            var rtManagerComputer1 = RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, context: ctx);
            var rtManagerComputer2 =
                RefreshTokenManagerInitializer.GetRefreshTokenManager(testname, "192.168.0.6", ctx);
            var jwtServiceComputer1 = new JwtTokenGenerateService<User, UserCompany>(
                ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManagerComputer1);
            var jwtServiceComputer2 = new JwtTokenGenerateService<User, UserCompany>(
                ConfigurationMocks.GetConfigurationForJwtService().Object, GetRepository(), rtManagerComputer2);
            var user = new User {Email = "super_user@lr.org", DisplayName = "super_user@lr.org"};
            var superUserRole = BaseUserRoles.SuperUser;
            var users = new Dictionary<User, string>
            {
                {user, superUserRole}
            };
            var userManagerMock = UserManagerMocks.GetUserManager(users);
            var signInManager = SignInManagerMocks.GetSuccessSignInManager(user, superUserRole).Object;
            var accountAccessibilityService = new Mock<IAccountAccessibilityService>().Object;
            var factoryComputer1 = AuthorizeStrategiesFactoryCreator.GetFactory(jwtServiceComputer1, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManagerComputer1);
            var factoryComputer2 = AuthorizeStrategiesFactoryCreator.GetFactory(jwtServiceComputer2, signInManager,
                userManagerMock.Object, ConfigurationMocks.GetConfigurationForAuthorisationStrategies().Object,
                accountAccessibilityService, rtManagerComputer2);
            var sutComputer1 = new Authorizator(factoryComputer1);
            var sutComputer2 = new Authorizator(factoryComputer2);

            var loginModel = new LoginModel
            {
                Email = user.Email,
                Password = "qwaszx@1",
                GrantType = GrantTypeKeys.Password
            };
            var token = sutComputer1.Authorize(loginModel).Result;


            var tmpRt = Queryable.First<RefreshToken>(ctx.RefreshTokens, rt => rt.Id == token.RefreshToken);
            tmpRt.LastTimeRefreshTokenRequest =
                tmpRt.LastTimeRefreshTokenRequest.AddMinutes(0 - (config.Object.Value.ExpiresMinutes + 1));
            ctx.RefreshTokens.Update(tmpRt);
            ctx.SaveChanges();


            var newToken = sutComputer2.Authorize(loginModel).Result;
            Assert.AreEqual<string>(user.Email, newToken.Email);
            Assert.AreEqual<string>(user.DisplayName, newToken.DisplayName);
            Assert.AreEqual(superUserRole, newToken.Role);
            Assert.IsFalse(string.IsNullOrEmpty(newToken.AccessToken));
            Assert.AreNotEqual(token.RefreshToken, newToken.RefreshToken);
        }
    }
}