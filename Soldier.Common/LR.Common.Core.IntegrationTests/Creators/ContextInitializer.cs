﻿using LR.Common.Core.IntegrationTests.Context;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Core.IntegrationTests.Creators
{
    public static class ContextInitializer
    {
        public static TestDbContext GetEmptyContext(string name)
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase($"DB_FOR_{name}")
                .Options;
            var ctx = new TestDbContext(options);
            return ctx;
        }

        public static TestDbContext GetContext(string name)
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase($"Read_to_database_{name}")
                .Options;
            var ctx = new TestDbContext(options);
            ctx.TestEntities.Add(new TestEntity {Name = "test 1", Child = new TestChild {Name = "child 1"}});
            ctx.TestEntities.Add(new TestEntity { Name = "test 2" });
            ctx.TestEntities.Add(new TestEntity { Name = "test 3", Child = new TestChild { Name = "child 3" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 4", Child = new TestChild { Name = "child 4" } });
            ctx.SaveChanges();
            ctx = new TestDbContext(options);
            return ctx;
        }


        public static TestDbContext GetContextForPaging(string name)
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase($"Read_to_database_With_Paging_{name}")
                .Options;
            var ctx = new TestDbContext(options);
            ctx.TestEntities.Add(new TestEntity { Name = "test 7", Number = 7, Child = new TestChild { Name = "child 7" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 8", Number = 8, Child = new TestChild { Name = "child 8" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 9", Number = 9, Child = new TestChild { Name = "child 9" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 10", Number = 10 });
            ctx.TestEntities.Add(new TestEntity { Name = "test 11", Number = 11, Child = new TestChild { Name = "child 11" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 12", Number = 12, Child = new TestChild { Name = "child 12" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 1", Number = 1, Child = new TestChild { Name = "child 1" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 2", Number = 2 });
            ctx.TestEntities.Add(new TestEntity { Name = "test 3", Number = 3, Child = new TestChild { Name = "child 3" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 4", Number = 4, Child = new TestChild { Name = "child 4" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 5", Number = 5, Child = new TestChild { Name = "child 5" } });
            ctx.TestEntities.Add(new TestEntity { Name = "test 6", Number = 6 });
            ctx.SaveChanges();
            ctx = new TestDbContext(options);
            return ctx;
        }
    }
}
