﻿using System;
using AutoMapper;
using LR.Common.Auth.DAL;
using LR.Common.Auth.Managers;
using LR.Common.BaseIdentity.Context;
using LR.Common.BaseIdentity.Services;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using LR.Common.Core.UnitOfWork;

namespace LR.Common.Core.IntegrationTests.Creators
{
    public static class RefreshTokenManagerInitializer
    {
        public static RefreshTokenManager<User> GetRefreshTokenManager(string name, string ip = "127.0.0.1",
            AppDbContext<User, UserRole, Role> context = null)
        {
            CommonMapperCreator.Create();
            var ctx = context ?? ContextInitializer.GetEmptyContext(name);
            var configuration = ConfigurationMocks.GetConfigurationForJwtService().Object;
            var dbContextProvider = DbContextProviderMocks.GetProvider(ctx).Object;
            var refreshTokenRepository = new GenericRepository<RefreshToken>(dbContextProvider, Mapper.Instance);
            var userRepository = new GenericRepository<User>(dbContextProvider, Mapper.Instance);
            var httpContextWraper =
                new CurrentUserService<User, UserCompany>(userRepository,
                    new UserContext(HttpContextAccessorMocks.GetHttpContextAccessor(Guid.NewGuid(), "user@lr.org", ip).Object));
            var uofFactory = new UnitOfWorkFactory(dbContextProvider, AuditLogServiceMocks.GetAuditLogService().Object);
            return new RefreshTokenManager<User>(refreshTokenRepository, configuration, uofFactory, httpContextWraper);
        }
    }
}