﻿using System;
using AutoMapper;
using LR.Common.BaseIdentity.Services;
using LR.Common.Core.IntegrationTests.Context;
using LR.Common.Core.IntegrationTests.Creators;
using LR.Common.Core.Repositories;
using LR.Common.Core.Tests.Creators;
using LR.Common.Core.Tests.Mocks;
using LR.Common.Core.Tests.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LR.Common.Core.IntegrationTests.Wrappers
{
    [TestClass]
    public class HttpContextWrapperTests
    {
        public TestDbContext Context { get; set; }

        [TestInitialize]
        public void InitDb()
        {
            CommonMapperCreator.Create();
            Context = ContextInitializer.GetEmptyContext("HttpContextWrapperTests");
            Context.Users.Add(new User
            {
                Email = "user@lr.org",
                Name = "user@lr.org",
                DisplayName = "user@lr.org",
                EmailConfirmed = true
            });
            Context.SaveChanges();
        }

        [TestMethod]
        public void GetCurrentUser_WithNotExistUser_UserIsNull()
        {
            var httpContextAccessor = HttpContextAccessorMocks
                .GetHttpContextAccessor(Guid.NewGuid(), "test@test.tst", "127.0.0.1").Object;
            var dbContextProvider = DbContextProviderMocks.GetProvider(Context).Object;
            var userRepository = new GenericRepository<User>(dbContextProvider, Mapper.Instance);

            var sut = new CurrentUserService<User, UserCompany>(userRepository, new UserContext(httpContextAccessor));

            var user = sut.GetCurrentUser();

            Assert.IsNull(user);
        }

        [TestMethod]
        public void GetCurrentUser_GetIPAddress()
        {
            var httpContextAccessor = HttpContextAccessorMocks.GetHttpContextAccessor().Object;
            var dbContextProvider = DbContextProviderMocks.GetProvider(Context).Object;
            var userRepository = new GenericRepository<User>(dbContextProvider, Mapper.Instance);

            var sut = new CurrentUserService<User, UserCompany>(userRepository, new UserContext(httpContextAccessor));

            var ip = sut.GetUserIPAddress();

            Assert.AreEqual<string>("127.0.0.1", ip);
        }

        //[TestMethod]
        //public void ExecuteInUserContext()
        //{
        //    var httpContextAccessor = HttpContextAccessorMocks.GetHttpContextAccessor().Object;
        //    var userContext = new UserContext(httpContextAccessor);
        //    var userId1 = Guid.Parse("11111111-1111-1111-1111-111111111111");
        //    var userId2 = Guid.Parse("22222222-2222-2222-2222-222222222222");
        //    userContext.ExecuteInUserContext(userId1);
        //    Assert.AreEqual(userId1, userContext.GetActualUserId());
        //    userContext.ExecuteInUserContext(userId2);
        //    Assert.AreEqual(userId2, userContext.GetActualUserId());
        //}
    }
}
