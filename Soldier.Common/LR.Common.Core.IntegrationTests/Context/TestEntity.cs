﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LR.Common.Core.DAL.Contracts;

namespace LR.Common.Core.IntegrationTests.Context
{
    public class TestEntity : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int Number { get; set; }

        public TestChild Child { get; set; }
    }
}
