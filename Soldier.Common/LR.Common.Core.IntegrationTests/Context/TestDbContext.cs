﻿using LR.Common.Auth.DAL;
using LR.Common.BaseIdentity.Context;
using LR.Common.Core.Tests.TestModels;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.Core.IntegrationTests.Context
{
    public class TestDbContext : AppDbContext<User, UserRole, Role>
    {
        public TestDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserCompany>()
                .HasKey(uc => new { uc.UserId, uc.CompanyId });

            builder.Entity<User>(b =>
            {
                b.HasMany(u => u.UserRoles)
                    .WithOne(ur => ur.User)
                    .HasPrincipalKey(u => u.Id)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<Role>(role =>
            {
                role.HasMany<UserRole>()
                    .WithOne(ur => ur.Role)
                    .HasPrincipalKey(r => r.Id)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();
            });
        }

        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<TestEntity> TestEntities { get; set; }
        public DbSet<TestChild> TestChildren { get; set; }
    }
}
