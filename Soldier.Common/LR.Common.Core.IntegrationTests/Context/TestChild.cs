﻿using LR.Common.Core.DAL;

namespace LR.Common.Core.IntegrationTests.Context
{
    public class TestChild : Entity
    {
        public string Name { get; set; }
    }
}
