﻿using System;

namespace LR.Common.Tasks.Exceptions
{
    public class NoValidTaskWorkersException : Exception
    {
        public string WorkerKey { get; set; }

        public NoValidTaskWorkersException()
        {
        }

        public NoValidTaskWorkersException(string workerKey)
        {
            WorkerKey = workerKey;
        }
    }
}
