﻿using System;
using LR.Common.Core.Helpers;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.DAL;
using LR.Common.Tasks.Enums;

namespace LR.Common.Tasks.CompleteActions
{
    public class OnCronTaskComplete : IOnTaskComplete
    {
        public Action<ScheduledTask, TaskExecutionStatus> GetAction()
        {
            return (task, status) =>
            {
                if (task is CronTask cronTask)
                {
                    task.ExecuteAt = CronExpressionHelper.GetNextOccurrenceDate(cronTask.CronExpression);
                    task.Status = status;
                    task.LastRunTime = DateTime.UtcNow;
                }
            };
        }
    }
}
