﻿using System;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.DAL;
using LR.Common.Tasks.Enums;

namespace LR.Common.Tasks.CompleteActions
{
    public class OnScheduledTaskComplete : IOnTaskComplete
    {
        public Action<ScheduledTask, TaskExecutionStatus> GetAction()
        {
            return (task, status) =>
            {
                task.IsCompleted = true;
                task.Status = status;
                task.LastRunTime = DateTime.UtcNow;
            };
        }
    }
}
