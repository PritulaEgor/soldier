﻿using System.Collections.Generic;
using LR.Common.Tasks.Enums;
using LR.Common.Tasks.ViewModels;

namespace LR.Common.Tasks.Contracts
{
    /// <summary>
    /// Represents interface for task scheduler, which provide task info about execution
    /// </summary>
    public interface ITaskScheduler
    {
        /// <summary>
        /// Get tasks which should be executed now.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ScheduledTaskModel> GetTasks();

        /// <summary>
        /// Removes the task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="result">The result.</param>
        void CompleteTask(ScheduledTaskModel task, TaskExecutionStatus result);
    }
}
