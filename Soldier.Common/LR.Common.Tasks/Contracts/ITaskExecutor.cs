﻿using System.Threading.Tasks;

namespace LR.Common.Tasks.Contracts
{
    /// <summary>
    /// Interface fot task executor
    /// </summary>
    public interface ITaskExecutor
    {
        /// <summary>
        /// Read from DB, tasks which should be executed and execute it
        /// </summary>
        Task ProcessTasks();
    }
}
