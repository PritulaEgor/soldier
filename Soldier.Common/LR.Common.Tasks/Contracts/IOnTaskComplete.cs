﻿using System;
using LR.Common.Tasks.DAL;
using LR.Common.Tasks.Enums;

namespace LR.Common.Tasks.Contracts
{
    /// <summary>
    /// Interface for actions, which contain action to complete task.
    /// </summary>
    public interface IOnTaskComplete
    {
        Action<ScheduledTask, TaskExecutionStatus> GetAction();
    }
}
