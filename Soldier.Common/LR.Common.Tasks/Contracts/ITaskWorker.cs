﻿using System;
using System.Threading.Tasks;
using LR.Common.Tasks.Enums;
using LR.Common.Tasks.ViewModels;

namespace LR.Common.Tasks.Contracts
{
    /// <summary>
    /// Interface for task workers, which implements some worker for tasks feature
    /// </summary>
    public interface ITaskWorker
    {
        /// <summary>
        /// Gets or sets task model.
        /// </summary>
        /// <value>
        /// Task model, which hold neceessary info.
        /// </value>
        ScheduledTaskModel TaskModel { get; set; }

        /// <summary>
        /// Execute logic, which are incapsulated in implementation
        /// </summary>
        /// <returns>Result of task execution</returns>
        Task Execute(Action<TaskExecutionStatus> onCompleteAction);
    }
}
