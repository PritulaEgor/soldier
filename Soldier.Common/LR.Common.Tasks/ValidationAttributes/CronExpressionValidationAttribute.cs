﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.Core.Helpers;

namespace LR.Common.Tasks.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property |
                    AttributeTargets.Field)]
    public class CronExpressionValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var cronExpression = value.ToString();
            return CronExpressionHelper.IsValid(cronExpression);
        }
    }
}
