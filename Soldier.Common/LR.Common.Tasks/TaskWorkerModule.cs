﻿using LR.Common.Core.IoC.Contracts;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.Tasks
{
    public class TaskWorkerModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<TaskWorkerFactory>();
            services.AddTransient<ITaskExecutor, TaskExecutor>();
            services.AddTransient<ITaskScheduler, TaskScheduler>();
        }
    }
}
