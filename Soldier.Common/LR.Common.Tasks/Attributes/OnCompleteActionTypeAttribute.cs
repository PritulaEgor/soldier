﻿using System;

namespace LR.Common.Tasks.Attributes
{
    public class OnCompleteActionTypeAttribute : Attribute
    {
        public Type CompletionActionType { get; set; }

        public OnCompleteActionTypeAttribute(Type completionActionType)
        {
            CompletionActionType = completionActionType;
        }
    }
}
