﻿using System;

namespace LR.Common.Tasks.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TaskWorkerKeyAttribute : Attribute
    {
        public string WorkerKey { get; set; }

        public TaskWorkerKeyAttribute(string workerKey)
        {
            WorkerKey = workerKey;
        }
    }
}
