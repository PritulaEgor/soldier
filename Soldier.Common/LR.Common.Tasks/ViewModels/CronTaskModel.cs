﻿using System.ComponentModel.DataAnnotations;
using LR.Common.Tasks.Attributes;
using LR.Common.Tasks.CompleteActions;

namespace LR.Common.Tasks.ViewModels
{
    [OnCompleteActionType(typeof(OnCronTaskComplete))]
    public class CronTaskModel : ScheduledTaskModel
    {
        [Required]
        [MaxLength(20)]
        public string CronExpression { get; set; }
    }
}
