﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.Core.ViewModels;
using LR.Common.Tasks.Attributes;
using LR.Common.Tasks.CompleteActions;
using LR.Common.Tasks.Enums;

namespace LR.Common.Tasks.ViewModels
{
    [OnCompleteActionType(typeof(OnScheduledTaskComplete))]
    public class ScheduledTaskModel : BaseViewModel
    {
        [Required]
        [MaxLength(200)]
        public string WorkerKey { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public TaskExecutionStatus Status { get; set; }

        [Required]
        public bool IsCompleted { get; set; }

        public Guid? AuthorId { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public DateTime LastRunTime { get; set; }

        [Required]
        public DateTime ExecuteAt { get; set; }
    }
}
