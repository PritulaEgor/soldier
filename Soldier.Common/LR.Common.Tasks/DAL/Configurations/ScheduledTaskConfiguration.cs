﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Tasks.DAL.Configurations
{
    public class ScheduledTaskConfiguration : IEntityTypeConfiguration<ScheduledTask>
    {
        public void Configure(EntityTypeBuilder<ScheduledTask> builder)
        {
            builder.Property(b => b.IsCompleted).HasDefaultValue(false);
        }
    }
}
