﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.Tasks.DAL.Configurations
{
    public class CronTaskConfiguration : IEntityTypeConfiguration<CronTask>
    {
        public void Configure(EntityTypeBuilder<CronTask> builder)
        {
            builder.HasBaseType<ScheduledTask>();
        }
    }
}
