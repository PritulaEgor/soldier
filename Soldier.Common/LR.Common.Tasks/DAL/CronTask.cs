﻿using System.ComponentModel.DataAnnotations;

namespace LR.Common.Tasks.DAL
{
    public class CronTask : ScheduledTask
    {
        [Required]
        [MaxLength(20)]
        public string CronExpression { get; set; }
    }
}
