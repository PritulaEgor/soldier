﻿namespace LR.Common.Tasks.Constants
{
    /// <summary>
    /// Const because it should be constant, we use it in attributes
    /// </summary>
    public static class TaskWorkerKeys
    {
        public const string SummaryEmailKey = "SummaryEmail";

        public const string StorageReorganizeKey = "StorageReorganize";

        public const string LogCleanerKey = "LogCleaner";
    }
}
