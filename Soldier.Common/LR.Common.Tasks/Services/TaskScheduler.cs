﻿using System;
using System.Collections.Generic;
using LR.Common.Core.Extensions;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Tasks.Attributes;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.DAL;
using LR.Common.Tasks.Enums;
using LR.Common.Tasks.ViewModels;

namespace LR.Common.Tasks.Services
{
    public class TaskScheduler : ITaskScheduler
    {
        protected readonly IGenericRepository<ScheduledTask> tasksRepository;
        protected readonly IUnitOfWorkFactory uowFactory;

        public TaskScheduler(IGenericRepository<ScheduledTask> tasksRepository, IUnitOfWorkFactory uowFactory)
        {
            this.tasksRepository = tasksRepository;
            this.uowFactory = uowFactory;
        }

        public IEnumerable<ScheduledTaskModel> GetTasks()
        {
            var shouldBeExecutedTasks =
                tasksRepository.Mapper.Map<IEnumerable<ScheduledTaskModel>>(
                    tasksRepository.GetBy<ScheduledTask>(
                        task => !task.IsCompleted && task.ExecuteAt < DateTime.UtcNow, q => q));
            return shouldBeExecutedTasks;
        }

        public void CompleteTask(ScheduledTaskModel task, TaskExecutionStatus result)
        {
            var completeAction = GetCompleteAction(task);
            var taskToComplete = tasksRepository.GetById(task.Id);
            if (taskToComplete != null)
            {
                using (uowFactory.Create())
                {
                    completeAction(taskToComplete, result);
                    tasksRepository.Update(taskToComplete);
                }
            }
        }

        protected Action<ScheduledTask, TaskExecutionStatus> GetCompleteAction(ScheduledTaskModel task)
        {
            var completeAttribute = task.GetAttribute<OnCompleteActionTypeAttribute>();
            return Activator.CreateInstance(completeAttribute.CompletionActionType) is IOnTaskComplete
                completeAction
                ? completeAction.GetAction()
                : (t, s) => { };
        }
    }
}
