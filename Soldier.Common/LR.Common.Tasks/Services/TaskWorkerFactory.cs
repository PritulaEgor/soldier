﻿using System;
using System.Collections.Generic;
using System.Linq;
using LR.Common.Core.Extensions;
using LR.Common.Tasks.Attributes;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.Exceptions;
using LR.Common.Tasks.ViewModels;

namespace LR.Common.Tasks.Services
{
    public class TaskWorkerFactory
    {
        protected readonly IEnumerable<ITaskWorker> taskWorkers;

        public TaskWorkerFactory(IEnumerable<ITaskWorker> taskWorkers)
        {
            this.taskWorkers = taskWorkers;
        }

        public virtual ITaskWorker GetTaskWorker(ScheduledTaskModel task)
        {
            try
            {
                CheckTaskWorkers();
                var taskWorker = taskWorkers.Single(worker => CheckWorkerKeyAttribute(task.WorkerKey, worker));
                taskWorker.TaskModel = task;
                return taskWorker;
            }
            catch (InvalidOperationException)
            {
                throw new NoValidTaskWorkersException(task.WorkerKey);
            }
        }

        protected void CheckTaskWorkers()
        {
            if (taskWorkers == null || !taskWorkers.Any())
            {
                throw new InvalidOperationException("There is no task workers");
            }
        }

        protected bool CheckWorkerKeyAttribute(string workerKey, ITaskWorker worker)
        {
            var workerKeyAttribute = worker.GetAttribute<TaskWorkerKeyAttribute>();
            return workerKeyAttribute?.WorkerKey.Equals(workerKey) ?? false;
        }
    }
}
