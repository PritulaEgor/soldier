﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LR.Common.Core.Services.Contracts;
using LR.Common.Tasks.Contracts;
using LR.Common.Tasks.Exceptions;
using LR.Common.Tasks.ViewModels;

namespace LR.Common.Tasks.Services
{
    public class TaskExecutor : ITaskExecutor
    {
        protected readonly ITaskScheduler taskScheduler;
        protected readonly TaskWorkerFactory taskWorkerFactory;
        protected readonly ILogService logService;

        public TaskExecutor(
            ITaskScheduler taskScheduler,
            TaskWorkerFactory taskWorkerFactory,
            ILogService logService
            )
        {
            this.taskScheduler = taskScheduler;
            this.taskWorkerFactory = taskWorkerFactory;
            this.logService = logService;
        }

        public async Task ProcessTasks()
        {
            var taskWorkers = GetTaskWorkers(taskScheduler.GetTasks());
            var tasks = taskWorkers?.Select(taskWorker =>
            {
                logService.LogInfo($"Start execute task: {taskWorker.TaskModel.WorkerKey} at {DateTime.UtcNow}");
                return taskWorker.Execute(executionResult =>
                    taskScheduler.CompleteTask(taskWorker.TaskModel, executionResult));
            });
            await Task.WhenAll(tasks);
        }

        protected IEnumerable<ITaskWorker> GetTaskWorkers(IEnumerable<ScheduledTaskModel> tasksToExecute)
        {
            return tasksToExecute?.Select(task =>
            {
                try
                {
                    return taskWorkerFactory.GetTaskWorker(task);
                }
                catch (NoValidTaskWorkersException ex)
                {
                    logService.LogError($"No task worker with key{ex.WorkerKey}", ex);
                    return null;
                }
            }).Where(taskWorker => taskWorker != null);
        }
    }
}
