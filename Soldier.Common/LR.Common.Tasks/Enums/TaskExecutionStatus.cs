﻿namespace LR.Common.Tasks.Enums
{
    public enum TaskExecutionStatus
    {
        Error = 0,
        Success
    }
}
