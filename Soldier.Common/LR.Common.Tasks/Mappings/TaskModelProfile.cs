﻿using AutoMapper;
using LR.Common.Tasks.DAL;
using LR.Common.Tasks.ViewModels;

namespace LR.Common.Tasks.Mappings
{
    public class TaskModelProfile : Profile
    {
        public TaskModelProfile()
        {
            CreateMap<ScheduledTask, ScheduledTaskModel>()
                .Include<CronTask, CronTaskModel>();
            CreateMap<CronTask, CronTaskModel>();
        }
    }
}
