﻿using LR.Common.Core.InitWorkers.Contracts;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.Tasks.DAL;

namespace LR.Common.Tasks.InitWorkers
{
    public abstract class BaseTasksInitWorker : IInitWorker
    {
        protected readonly IUnitOfWorkFactory uowFactory;
        protected readonly IGenericRepository<ScheduledTask> tasksRepository;

        protected BaseTasksInitWorker(IUnitOfWorkFactory uowFactory, IGenericRepository<ScheduledTask> tasksRepository)
        {
            this.uowFactory = uowFactory;
            this.tasksRepository = tasksRepository;
        }

        public int Order => 10;

        public abstract void Init();

        protected virtual void AddTask(ScheduledTask task)
        {
            uowFactory.Execute(() => tasksRepository.Create(task), false);
        }
    }
}
