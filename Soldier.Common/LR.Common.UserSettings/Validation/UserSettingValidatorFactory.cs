﻿using System.Collections.Generic;
using System.Linq;
using LR.Common.Core.Extensions;
using LR.Common.UserSettings.Validation.Attributes;
using LR.Common.UserSettings.Validation.Contracts;

namespace LR.Common.UserSettings.Validation
{
    /// <summary>
    /// Factory for user settings validator
    /// </summary>
    public sealed class UserSettingValidatorFactory
    {
        private readonly IEnumerable<IUserSettingValidator> validators;

        public UserSettingValidatorFactory(IEnumerable<IUserSettingValidator> validators)
        {
            this.validators = validators;
        }

        public IUserSettingValidator GetValidator(string key)
        {
            return validators.SingleOrDefault(validator =>
            {
                var validatorType = validator.GetAttribute<UserSettingsValidatorTypeAttribute>();
                return validatorType != null && validatorType.Key == key;
            });
        }
    }
}
