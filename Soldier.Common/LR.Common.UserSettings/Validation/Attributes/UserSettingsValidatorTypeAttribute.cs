﻿using System;

namespace LR.Common.UserSettings.Validation.Attributes
{
    public class UserSettingsValidatorTypeAttribute : Attribute
    {
        public string Key { get; set; }

        public UserSettingsValidatorTypeAttribute(string key)
        {
            Key = key;
        }
    }
}
