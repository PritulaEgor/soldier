﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.UserSettings.ViewModels;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.UserSettings.Validation.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class UserSettingValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var factory = validationContext.GetService<UserSettingValidatorFactory>();
            if (!(value is UserSettingModel settingModel))
            {
                return new ValidationResult("Setting is not valid");
            }
            var validator = factory.GetValidator(settingModel.SettingKey);
            if (validator == null || !validator.IsValid(settingModel.Value))
            {
                return new ValidationResult($"Setting {settingModel.SettingKey} is not valid");
            }
            return ValidationResult.Success;
        }
    }
}
