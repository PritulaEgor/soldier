﻿namespace LR.Common.UserSettings.Validation.Contracts
{
    /// <summary>
    /// Interface for user settings validator
    /// </summary>
    public interface IUserSettingValidator
    {
        bool IsValid(string value);
    }
}
