﻿using LR.Common.Core.DAL;
using LR.Common.Core.IoC.Contracts;
using LR.Common.UserSettings.DAL;
using LR.Common.UserSettings.DAL.Contracts;
using LR.Common.UserSettings.Services;
using LR.Common.UserSettings.Services.Contracts;
using LR.Common.UserSettings.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace LR.Common.UserSettings
{
    public class UserSettingsModule<TUser, TSetting> : IModule
        where TUser : BaseAppUser, IUserSettings<TSetting>
        where TSetting : BaseUserSetting, new()
    {
        public void Load(IServiceCollection services)
        {
            services.AddSingleton<UserSettingValidatorFactory>();
            services.AddTransient<IUserSettingService, UserSettingService<TUser, TSetting>>();
        }
    }
}
