﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.UserSettings.Validation.Attributes;

namespace LR.Common.UserSettings.ViewModels
{
    [UserSettingValidation]
    public class UserSettingModel
    {
        [Required]
        public string SettingKey { get; set; }

        public string Value { get; set; }

        [Required]
        public Guid UserId { get; set; }
    }
}
