﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LR.Common.UserSettings.DAL.Configuration
{
    public class BaseUserSettingConfiguration<TUserSetting> : IEntityTypeConfiguration<TUserSetting>
        where TUserSetting : BaseUserSetting
    {
        public void Configure(EntityTypeBuilder<TUserSetting> builder)
        {
            builder.HasIndex(us => new { us.UserId, us.Key });
        }
    }
}
