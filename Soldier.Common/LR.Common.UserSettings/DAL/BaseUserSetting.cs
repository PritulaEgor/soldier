﻿using System;
using System.ComponentModel.DataAnnotations;
using LR.Common.Core.DAL;

namespace LR.Common.UserSettings.DAL
{
    public abstract class BaseUserSetting : Entity
    {
        public Guid UserId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Key { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Value { get; set; }
    }
}
