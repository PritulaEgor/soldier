﻿using System.Collections.Generic;

namespace LR.Common.UserSettings.DAL.Contracts
{
    public interface IUserSettings<TSetting> where TSetting : BaseUserSetting
    {
        List<TSetting> Settings { get; set; }
    }
}
