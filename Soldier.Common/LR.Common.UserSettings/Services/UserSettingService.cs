﻿using System;
using System.Linq;
using LR.Common.BaseIdentity.Exceptions;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using LR.Common.UserSettings.DAL;
using LR.Common.UserSettings.DAL.Contracts;
using LR.Common.UserSettings.Services.Contracts;
using LR.Common.UserSettings.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LR.Common.UserSettings.Services
{
    /// <summary>
    /// Implements service which, set user userSetting to user profile
    /// </summary>
    /// <seealso cref="IUserSettingService" />
    public class UserSettingService<TUser, TSetting> : IUserSettingService
        where TUser : BaseAppUser, IUserSettings<TSetting>
        where TSetting: BaseUserSetting, new()
    {
        protected readonly IGenericRepository<TUser> userRepository;
        protected readonly IUnitOfWorkFactory uowFactory;

        public UserSettingService(IGenericRepository<TUser> userRepository, IUnitOfWorkFactory uowFactory)
        {
            this.userRepository = userRepository;
            this.uowFactory = uowFactory;
        }

        public void Set(UserSettingModel userSetting)
        {
            var user = GetUserWithSettings(userSetting.UserId);
            SetSettingForUser(user, userSetting.SettingKey, userSetting.Value);
            using (uowFactory.Create())
            {
                userRepository.Update(user);
            }
        }

        public UserSettingModel Get(Guid userId, string settingKey)
        {
            var user = GetUserWithSettings(userId);
            var setting = GetUserSetting(user, settingKey);
            return new UserSettingModel {UserId = userId, SettingKey = settingKey, Value = setting?.Value};
        }

        private TUser GetUserWithSettings(Guid userId)
        {
            var user = userRepository.GetById(userId, q => q.Include(u => u.Settings));
            if (user == null)
            {
                throw new UserDoesNotExistException(userId);
            }
            return user;
        }

        private void SetSettingForUser(TUser user, string settingKey, string value)
        {
            var setting = GetUserSetting(user, settingKey);
            if (setting != null)
            {
                setting.Value = value;
            }
            else
            {
                user.Settings.Add(new TSetting { UserId = user.Id, Key = settingKey, Value = value });
            }
        }

        private TSetting GetUserSetting(TUser user, string settingSettingKey)
        {
            return user.Settings.FirstOrDefault(s => s.Key == settingSettingKey);
        }
    }
}
