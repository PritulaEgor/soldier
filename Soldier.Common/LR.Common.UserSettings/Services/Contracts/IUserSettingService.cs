﻿using System;
using LR.Common.UserSettings.ViewModels;

namespace LR.Common.UserSettings.Services.Contracts
{
    /// <summary>
    /// Interface for service, which provide possibility to manage user settings
    /// </summary>
    public interface IUserSettingService
    {
        void Set(UserSettingModel userSetting);

        UserSettingModel Get(Guid userId, string settingKey);
    }
}
