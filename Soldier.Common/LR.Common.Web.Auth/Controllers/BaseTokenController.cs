﻿using System;
using System.Threading.Tasks;
using LR.Common.Auth.Managers;
using LR.Common.Auth.Services.Contracts;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LR.Common.Web.Auth.Controllers
{
    public abstract class BaseTokenController<TUser> : Controller
        where TUser : BaseAppUser
    {
        protected readonly IAuthorizator authorizator;
        protected readonly RefreshTokenManager<TUser> refreshTokenManager;

        protected BaseTokenController(IAuthorizator authorizator, RefreshTokenManager<TUser> refreshTokenManager)
        {
            this.authorizator = authorizator;
            this.refreshTokenManager = refreshTokenManager;
        }

        /// <summary>
        /// Posts the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("Token")]
        public virtual async Task<object> Post([FromBody] LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            return await authorizator.Authorize(model);
        }

        /// <summary>
        /// Posts the specified model.
        /// </summary>
        /// <param name="refreshTokenId">The refresh token identifier.</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpDelete]
        [Route("Token")]
        public virtual object Delete(Guid refreshTokenId)
        {
            refreshTokenManager.RemoveRefreshToken(refreshTokenId);
            return Ok();
        }
    }
}
