﻿using System;
using LR.Common.AuditEvents.DAL;
using LR.Common.Auth.Constants;
using LR.Common.Auth.ViewModels;
using LR.Common.Core.DAL;
using LR.Common.Core.Repositories.Contracts;
using LR.Common.Core.UnitOfWork.Contracts;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LR.Common.Web.Auth.Filters
{
    public abstract class BaseLoginFilterActionAttribute : ActionFilterAttribute
    {
        protected readonly IUnitOfWorkFactory uowFactory;
        protected readonly IGenericRepository<BaseAuditEvent> auditLogRepository;

        protected LoginModel LoginModel { get; set; }

        protected BaseLoginFilterActionAttribute(IUnitOfWorkFactory uowFactory,
            IGenericRepository<BaseAuditEvent> auditLogRepository)
        {
            this.uowFactory = uowFactory;
            this.auditLogRepository = auditLogRepository;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            LoginModel = context.ActionArguments["model"] as LoginModel;
            base.OnActionExecuting(context);
        }

        public override void OnResultExecuted(ResultExecutedContext context)
        {
            if (!string.IsNullOrWhiteSpace(LoginModel?.Email) && LoginModel.GrantType == GrantTypeKeys.Password)
            {
                var user = GetUser(LoginModel);
                if (user != null)
                {
                    var loginActionEvent = new LoginAuditEvent
                    {
                        UserId = user.Id,
                        EventDate = DateTime.UtcNow
                    };
                    using (uowFactory.Create())
                    {
                        auditLogRepository.Create(loginActionEvent);
                    }
                }
            }
            base.OnResultExecuted(context);
        }

        protected abstract BaseAppUser GetUser(LoginModel model);
    }
}
