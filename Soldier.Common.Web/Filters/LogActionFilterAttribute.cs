﻿using Microsoft.AspNetCore.Mvc.Filters;
using Soldier.Common.Core.Services.Interfaces;

namespace Soldier.Common.Web.Filters
{
    public class LogActionFilterAttribute : ActionFilterAttribute
    {
        protected readonly ILogService logService;

        public LogActionFilterAttribute(ILogService logService)
        {
            this.logService = logService;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            logService.LogRequest(context.ActionDescriptor.DisplayName, context.ActionArguments);
            base.OnActionExecuting(context);
        }
    }
}