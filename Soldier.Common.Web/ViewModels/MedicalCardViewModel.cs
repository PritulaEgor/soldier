﻿using Soldier.Common.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.ViewModels
{
    public class MedicalCardViewModel : BaseViewModel
    {
        public int ValidityGroup { get; set; }

        public virtual List<IssueViewModel> Issues { get; set; }

        public virtual List<MedicalExaminationViewModel> MedicalExaminations { get; set; }
    }
}
