﻿using Soldier.Common.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.ViewModels
{
    public class EnlistmentViewModel : BaseViewModel
    {
        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }
}
