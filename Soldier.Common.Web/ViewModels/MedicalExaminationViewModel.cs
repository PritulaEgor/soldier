﻿using Soldier.Common.Core.ViewModels;
using System;

namespace Soldier.Common.Web.ViewModels
{
    public class MedicalExaminationViewModel : BaseViewModel
    {
        public string MedicalCardID { get; set; }

        public string ExaminationResult { get; set; }

        public DateTime ExaminationDate { get; set; }
    }
}
