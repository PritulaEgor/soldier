﻿using Soldier.Common.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.ViewModels
{
    public class SubpoenaViewModel : BaseViewModel
    {
        public Guid RecipientID { get; set; }

        public int Status { get; set; }

        public string Sended { get; set; }

        public int DaysActive { get; set; }

        public string SoldierDateOfArriving { get; set; }

        public Guid SignaturerID { get; set; }

        public string SignaturerName { get; set; }

        public Guid EnlistmentID { get; set; }

        public EnlistmentViewModel Enlistment { get; set; }
    }
}
