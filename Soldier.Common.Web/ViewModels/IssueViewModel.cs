﻿using Soldier.Common.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.ViewModels
{
    public class IssueViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateOfDiagnosis { get; set; }

        public string DoctorData { get; set; }

        public string MedicalCardID { get; set; }
    }
}
