﻿using Soldier.Common.Core.DAL.Soldier;
using System;
using System.Collections.Generic;

namespace Soldier.Common.Web.ViewModels
{
    public class CompleteCivilianInfoViewModel
    {
        public Guid Id { get; set; }

        public UserTypes UserType { get; set; }

        public string TemporaryPassword { get; set; }

        public string PassportNumber { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public string CurrentAddress { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public UserRoles Role { get; set; }

        public string PlaceOfResidence { get; set; }

        public Guid CreatorId { get; set; }

        public string PassportIdentificationNumber { get; set; }

        public MilitaryIdentificatorViewModel MilitaryID { get; set; }

        public MedicalCardViewModel MedicalCardID { get; set; }

        public List<SubpoenaViewModel> Subpoenas { get; set; }
    }
}
