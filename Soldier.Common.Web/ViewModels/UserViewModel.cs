﻿using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.ViewModels;
using System;

namespace Soldier.Common.Web.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        public UserTypes UserType { get; set; }

        public string TemporaryPassword { get; set; }

        #region BaseAppUser Fields

        public string PassportNumber { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public Guid IdentityId { get; set; }

        public string CurrentAddress { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public UserRoles Role { get; set; }

        public string PlaceOfResidence { get; set; }

        public Guid CreatorId { get; set; }
        #endregion

        #region Civilian fields
        public string PassportIdentificationNumber { get; set; }

        public Guid MilitaryID { get; set; }

        public Guid MedicalCardID { get; set; }
        #endregion

        #region Employee
        public string Rank { get; set; }

        public string ServiceStartDate { get; set; }

        public string WorkStartDate { get; set; }

        //public Guid ReportsToID { get; set; }
        #endregion
    }
}
