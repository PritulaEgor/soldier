﻿using Soldier.Common.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.ViewModels
{
    public class MilitaryIdentificatorViewModel : BaseViewModel
    {
        public bool IsMilitaryServicePassed { get; set; }

        public string MilitaryID { get; set; }

        public string MilitarySpeciality { get; set; }

        public DateTime ServiceStartDate { get; set; }

        public DateTime ServiceEndDate { get; set; }
    }
}
