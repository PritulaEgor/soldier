﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.DTO
{
    public class UpdateValidityGroupDTO
    {
        public Guid CivilianId { get; set; }

        public int ValidityGroup { get; set; }
    }
}
