﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.DTO
{
    public class UpdateSubpoenaStatusDTO
    {
        public Guid UserId { get; set; }

        public int NewStatus { get; set; }

        public Guid EnlistmentId { get; set; }
    }
}
