﻿using Soldier.Common.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.DTO
{
    public class DistributionDTO
    {
        public List<UserViewModel> Conscripts { get; set; }

        public UserViewModel Signaturer { get; set; }
        
        public EnlistmentViewModel Enlistment { get; set; }
    }
}
