﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Web.DTO
{
    public class EnlistmentStatisticsDTO
    {
        public int DeviantsPercentage { get; set; }

        public int ConscriptsCount { get; set; }

        public int ArrivingAwaiting { get; set; }

        public int Arrived { get; set; }

        public int NotValid { get; set; }
    }
}
