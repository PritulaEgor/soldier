﻿using Soldier.Common.BaseIdentity.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Soldier.Common.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Soldier.Common.BaseIdentity.ViewModels;

namespace Soldier.Common.Web.Controllers
{
    public class BaseUserProfileController : ControllerBase
    {
        protected readonly IUserProfileService _userProfileService;

        public BaseUserProfileController(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        [HttpPost]
        public virtual IActionResult UpdateUser([FromBody] UpdateUserProfileModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelNotValidException();
            }

            _userProfileService.Update(model);
            return Ok();
        }
    }
}
