﻿using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Soldier.Common.Web
{
    public class BaseWebModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            //services.AddTransient<LogActionFilterAttribute>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
    }
}
