﻿using Newtonsoft.Json;

namespace Soldier.Common.Core.Helpers
{
    public static class JsonHelper
    {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            TypeNameHandling = TypeNameHandling.All
        };

        public static string SerializeObjectToJson(object obj)
        {
            return SerializeObjectToJson(obj, Settings);
        }

        public static TType DeserializeObjectToJson<TType>(string json)
        {
            return DeserializeObjectToJson<TType>(json, Settings);
        }

        public static string SerializeObjectToJson(object obj, JsonSerializerSettings settings)
        {
            return JsonConvert.SerializeObject(obj, settings);
        }

        public static TType DeserializeObjectToJson<TType>(string json, JsonSerializerSettings settings)
        {
            return JsonConvert.DeserializeObject<TType>(json, settings);
        }
    }
}
