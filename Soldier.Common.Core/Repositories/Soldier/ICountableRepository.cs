﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Common.Core.Repositories.Soldier
{
    public interface ICountableRepository<TItem>
    {
        int Count();

        int Count(Expression<Func<TItem, bool>> predicate,
            Func<IQueryable<TItem>, IQueryable<TItem>> includes = null);
    }
}
