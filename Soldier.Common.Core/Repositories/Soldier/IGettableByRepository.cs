﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Soldier.Common.Core.Repositories.Soldier
{
    public interface IGettableByRepository<TItem>
    {
        IQueryable<TItem> GetBy(Expression<Func<TItem, bool>> predicate);
    }
}
