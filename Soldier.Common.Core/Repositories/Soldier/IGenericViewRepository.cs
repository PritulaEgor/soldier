﻿namespace Soldier.Common.Core.Repositories.Soldier
{
    public interface IGenericViewRepository<TView> : IPageableRepository<TView>, ICountableRepository<TView>, IGettableByRepository<TView>
    {
    }
}
