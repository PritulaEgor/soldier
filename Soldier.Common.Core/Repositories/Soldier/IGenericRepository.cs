﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Soldier.Common.Core.DAL.Soldier;

namespace Soldier.Common.Core.Repositories.Soldier
{
    public interface IGenericRepository<TEntity> : IPageableRepository<TEntity>, ICountableRepository<TEntity>, IGettableByRepository<TEntity>
        where TEntity : class, IEntity
    {
        IMapper Mapper { get; }

        TEntity Create(TEntity item);

        bool Any(Expression<Func<TEntity, bool>> predicate);

        bool Any(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes);

        TEntity GetById(Guid id);

        TEntity GetById(Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes);

        IQueryable<TEntity> GetBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes);

        IQueryable<TEntity> Get();

        IQueryable<TQueryEntity> Get<TQueryEntity>() where TQueryEntity : class, IEntity;

        IQueryable<TQueryEntity> GetBy<TQueryEntity>(Expression<Func<TQueryEntity, bool>> predicate,
            Func<IQueryable<TQueryEntity>, IQueryable<TQueryEntity>> includes) where TQueryEntity : class, IEntity;

        void Remove(TEntity item);

        void Remove(Expression<Func<TEntity, bool>> predicate);

        void Update(TEntity item);

        void Attach<TAttachEntity>(params TAttachEntity[] entities) where TAttachEntity : class, IEntity;
    }
}
