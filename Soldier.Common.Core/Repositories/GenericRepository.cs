﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Repositories.Soldier;
using Microsoft.EntityFrameworkCore;

namespace Soldier.Common.Core.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>, IDisposable where TEntity : class, IEntity
    {
        protected readonly IDbContext dbContext;
        protected readonly IMapper mapper;

        public GenericRepository(IDbContextProvider dbContextProvider, IMapper mapper)
        {
            dbContext = dbContextProvider.GetContext();
            this.mapper = mapper;
        }

        protected DbSet<TEntity> GetDbSet()
        {
            return GetDbSet<TEntity>();
        }

        protected DbSet<TQueryEntity> GetDbSet<TQueryEntity>() where TQueryEntity : class, IEntity
        {
            return dbContext.Set<TQueryEntity>();
        }

        public virtual IMapper Mapper => mapper;

        public TEntity Create(TEntity item)
        {
            return GetDbSet().Add(item).Entity;
        }

        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return GetDbSet().Any(predicate);
        }

        public bool Any(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes)
        {
            return includes(GetDbSet()).Any(predicate);
        }

        public TEntity GetById(Guid id)
        {
            return GetBy(e => e.Id == id).FirstOrDefault();
        }

        public TEntity GetById(Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes)
        {
            return GetBy(e => e.Id == id, includes).FirstOrDefault();
        }

        public IQueryable<TEntity> Get()
        {
            return Get<TEntity>();
        }

        public IQueryable<TQueryEntity> Get<TQueryEntity>() where TQueryEntity : class, IEntity
        {
            return GetDbSet<TQueryEntity>();
        }

        public IQueryable<TEntity> GetBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes)
        {
            return includes(GetDbSet()).Where(predicate).AsQueryable();
        }

        public IQueryable<TEntity> GetBy(Expression<Func<TEntity, bool>> predicate)
        {
            return GetDbSet().Where(predicate).AsQueryable();
        }

        public IQueryable<TQueryEntity> GetBy<TQueryEntity>(Expression<Func<TQueryEntity, bool>> predicate,
            Func<IQueryable<TQueryEntity>, IQueryable<TQueryEntity>> includes) where TQueryEntity : class, IEntity
        {
            var query = includes(GetDbSet<TQueryEntity>());
            return query.Where(predicate).AsQueryable();
        }

        public IQueryable<TEntity> GetPage(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> sortExpression)
        {
            var query = GetDbSet().AsQueryable();
            query = sortExpression == null ? query : sortExpression(query);
            query = includes == null
                ? query
                : includes(query);
            query = predicate == null
                ? query
                : query.Where(predicate).AsQueryable();
            return query.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<TEntity> GetPage(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> sortExpression)
        {
            return GetPage(pageNumber, pageSize, predicate, null, sortExpression);
        }

        public IQueryable<TEntity> GetPage(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> predicate)
        {
            return GetPage(pageNumber, pageSize, predicate, null, null);
        }

        public IQueryable<TEntity> GetPage(int pageNumber, int pageSize,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> sortExpression)
        {
            return GetPage(pageNumber, pageSize, null, null, sortExpression);
        }

        public void Remove(TEntity item)
        {
            GetDbSet().Remove(item);
        }

        public void Remove(Expression<Func<TEntity, bool>> predicate)
        {
            GetDbSet().RemoveRange(GetDbSet().Where(predicate));
        }

        public void Update(TEntity item)
        {
            Attach(item);
        }

        public int Count()
        {
            return GetDbSet().Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            return includes?.Invoke(GetDbSet()).Count(predicate) ?? GetDbSet().Count(predicate);
        }

        public void Attach<TAttachEntity>(params TAttachEntity[] entities) where TAttachEntity : class, IEntity
        {
            entities = entities.Where(e => e != null).ToArray();
            var entitiesToAttach = entities.Where(entity => GetDbSet<TAttachEntity>().Local.All(e => e.Id != entity.Id))
                .ToList();
            GetDbSet<TAttachEntity>().AttachRange(entitiesToAttach);
        }

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
