﻿using Soldier.Common.Core.DAL.Soldier;
using System;
using System.ComponentModel.DataAnnotations;

namespace Soldier.Common.Core.DAL
{
    public class Identity : Entity
    {
        [Required]
        public string PassportNumber { get; set; }

        [Required]
        public string PasswordSalt { get; set; }

        [Required]
        public UserRoles Role { get; set; }

        public string RefreshToken { get; set; }

        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}
