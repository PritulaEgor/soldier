﻿using System;
using Soldier.Common.Core.DAL.Soldier;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.Common.Core.DAL
{
    public abstract class BaseAppUser : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string PassportNumber { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Patronymic { get; set; }

        [Required]
        public Guid IdentityId { get; set; }

        [ForeignKey("IdentityId")]
        public virtual Identity Identity { get; set; }

        public string CurrentAddress { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public UserRoles Role { get; set; }

        [Required]
        public string PlaceOfResidence { get; set; }

        [Required]
        public Guid CreatorId { get; set; }
    }
}
