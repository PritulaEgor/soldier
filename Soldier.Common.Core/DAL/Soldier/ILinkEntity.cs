﻿using System;

namespace Soldier.Common.Core.DAL.Soldier
{
    public interface ILinkEntity
    {
        Guid LeftId { get; set; }

        Guid RightId { get; set; }
    }
}
