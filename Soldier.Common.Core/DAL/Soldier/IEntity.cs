﻿using System;

namespace Soldier.Common.Core.DAL.Soldier
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
