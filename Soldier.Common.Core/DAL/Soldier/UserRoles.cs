﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.DAL.Soldier
{
    // таблицы с должностями и правами доступа
    public enum UserRoles
    {
        PotentialConscript = 0, //Призывник
        Volunteer = 1,          //Доброволец
        Conscript = 2,          //Срочник
        Commissar = 3,          //Коммисар
        MedicalStaff = 4,
        AccountantStaff = 5,
        ITStaff = 6,
        ITAdmin = 7,
        CommissariatStaff = 8,
        Guest = 9
    }
}
