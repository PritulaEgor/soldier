﻿namespace Soldier.Common.Core.DAL.Soldier
{
    public interface IDbContextProvider
    {
        IDbContext GetContext();
    }
}
