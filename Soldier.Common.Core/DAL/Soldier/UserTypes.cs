﻿namespace Soldier.Common.Core.DAL.Soldier
{
    public enum UserTypes
    {
        Employee = 0,
        Civilian = 1,
        Guest = 2
    }
}
