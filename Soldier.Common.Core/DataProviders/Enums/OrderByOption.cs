﻿namespace Soldier.Common.Core.DataProviders.Enums
{
    public enum OrderByOption
    {
        Desc,
        Asc
    }
}
