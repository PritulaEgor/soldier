﻿using System.Collections.Generic;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.DataProviders.Enums;

namespace Soldier.Common.Core.DataProviders.Models
{
    public class PageInfo
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public Dictionary<string, OrderByOption> OrderBy { get; set; }

        public string FilterBy { get; set; }

        public UserTypes UserType { get; set; }
    }
}
