﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Soldier.Common.Core.DataProviders.Soldier;
using Soldier.Common.Core.DataProviders.Enums;
using Soldier.Common.Core.DataProviders.Models;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Repositories.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Core.ViewModels;

namespace Soldier.Common.Core.DataProviders
{
    /// <summary>
    /// Base data provider for contract entities, provide possibility to do CRUD operations. Also check for unique of some properties
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    /// <seealso cref="IDataProvider{TEntity,TModel}" />
    public abstract class BaseDataProvider<TEntity, TModel> : IDataProvider<TEntity, TModel> where TEntity : class, IEntity
        where TModel : BaseViewModel
    {
        protected readonly IGenericRepository<TEntity> repository;
        protected readonly IUnitOfWorkFactory uowFactory;

        protected BaseDataProvider(IGenericRepository<TEntity> repository, IUnitOfWorkFactory uowFactory)
        {
            this.repository = repository;
            this.uowFactory = uowFactory;
        }

        /// <summary>
        /// Gets the specified page with entities.
        /// </summary>
        /// <param name="pageInfo">Page information.</param>
        /// <returns></returns>
        public virtual Page<TModel> Get(PageInfo pageInfo)
        {
            var filter = GetFilterExpression(pageInfo.FilterBy);
            var items = repository.GetPage(pageInfo.PageNumber, 
                pageInfo.PageSize, 
                filter,
                null, 
                GetSortExpression(pageInfo.OrderBy));
            var page = new Page<TModel>
            {
                Number = pageInfo.PageNumber,
                Size = pageInfo.PageSize,
                TotalCount = repository.Count(filter),
                Items = items.Select(e => repository.Mapper.Map<TModel>(e)).ToList()
            };
            return page;
        }

        /// <summary>
        /// Creates the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public virtual TModel Create(TModel model)
        {
            ValidateModel(model);
            using (uowFactory.Create())
            {
                var entity = Map(model);
                var createdEntity = repository.Create(entity);
                return repository.Mapper.Map<TModel>(createdEntity);
            }
        }

        /// <summary>
        /// Maps the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        protected virtual TEntity Map(TModel model)
        {
            return repository.Mapper.Map<TEntity>(model);
        }

        /// <summary>
        /// Updates the entity properties.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        protected virtual TEntity UpdateEntityProperties(TModel model)
        {
            var entity = repository.GetById(model.Id);
            return repository.Mapper.Map(model, entity);
        }

        /// <summary>
        /// Validates the model.
        /// </summary>
        /// <param name="model">The model.</param>
        protected virtual void ValidateModel(TModel model)
        {
        }

        /// <summary>
        /// Deletes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        public virtual void Delete(TModel model)
        {
            using (uowFactory.Create())
            {
                var entity = repository.Mapper.Map<TEntity>(model);
                repository.Remove(e => e.Id == entity.Id);
            }
        }

        /// <summary>
        /// Updates the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        public virtual void Update(TModel model)
        {
            ValidateModel(model);
            using (uowFactory.Create())
            {
                var entity = UpdateEntityProperties(model);
                repository.Update(entity);
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public virtual TModel GetById(Guid id)
        {
            var entity = repository.GetBy(e => e.Id == id).FirstOrDefault();
            return repository.Mapper.Map<TModel>(entity);
        }

        /// <summary>
        /// Gets the by filter.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public abstract List<TModel> GetByFilter(string value);

        /// <summary>
        /// Generate filter expression
        /// </summary>
        /// <param name="filterBy">The term to filter on</param>
        /// <returns></returns>
        protected abstract Expression<Func<TEntity, bool>> GetFilterExpression(string filterBy);

        /// <summary>
        /// Generate filter expression using compound filter value
        /// </summary>
        /// <param name="filterBy">The term to filter on</param>
        /// <returns></returns>
        protected abstract Expression<Func<TEntity, bool>> GetCompoundFilterExpression(Dictionary<string, string> filterBy);

        /// <summary>
        /// Generate sort expression
        /// </summary>
        /// <param name="orderBy">The order by.</param>
        /// <returns></returns>
        protected abstract Func<IQueryable<TEntity>, IQueryable<TEntity>> GetSortExpression(
            Dictionary<string, OrderByOption> orderBy);

        protected KeyValuePair<string, OrderByOption>? GetSortOption(Dictionary<string, OrderByOption> orderBy)
        {
            return orderBy?.ToList()[0];
        }

        protected Func<IQueryable<TEntity>, IQueryable<TEntity>> SortExpression(Expression<Func<TEntity, object>> comparer, OrderByOption orderBy)
        {
            if (orderBy == OrderByOption.Asc)
            {
                return q => q.OrderBy(comparer);
            }
            return q => q.OrderByDescending(comparer);
        }
    }
}
