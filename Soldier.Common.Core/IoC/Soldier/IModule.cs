﻿using Microsoft.Extensions.DependencyInjection;

namespace Soldier.Common.Core.IoC.Soldier
{
    public interface IModule
    {
        void Load(IServiceCollection services);
    }
}
