﻿namespace Soldier.Common.Core.Configurations
{
    public class SmtpConfig
    {
        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string NoReply { get; set; }

        public bool EnableSsl { get; set; }
    }
}
