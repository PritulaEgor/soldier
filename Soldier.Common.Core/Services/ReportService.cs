﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using Soldier.Common.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Services
{
    public class ReportService<Tmodel> : IReportService<Tmodel>
    {
        public byte[] GetReport(
            List<Tmodel> data,
            string fileName,
            List<string> fieldsNames)
        {
            if (data is null || data.Count == 0)
            {
                return null;
            }

            var type = typeof(Tmodel);

            var fields1 = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            var fields = fields1.ToList();

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var excelPackage = new ExcelPackage())
            {
                var workSheet = CheckAndCreateWorkSheet(
                        excelPackage,
                        fileName,
                        fields,
                        fieldsNames);

                (int X, int Y) pos = (2, 1);

                foreach (var unit in data)
                {
                    foreach (var fieldInfo in fields)
                    {
                        var value = fieldInfo.GetValue(unit);

                        workSheet.Cells[pos.X, pos.Y++].Value = value;
                    }

                    pos.X++;
                    pos.Y = 1;
                }

                #region Autofit
                AutoFit(workSheet);
                #endregion

                return excelPackage.GetAsByteArray();
            }
        }

        private ExcelWorksheet CheckAndCreateWorkSheet(
       ExcelPackage excelPackage,
       string sheetName,
       List<FieldInfo> fields,
       List<string> fieldsNames)
        {
            ExcelWorksheet workSheet = excelPackage
                .Workbook
                .Worksheets
                .FirstOrDefault(x => x.Name.Equals(sheetName));

            if (workSheet == null)
            {
                workSheet = excelPackage.Workbook.Worksheets.Add(sheetName);
            }
            else
            {
                if (workSheet.Dimension != null)
                {
                    workSheet.Cells[
                        workSheet.Dimension.Start.Row,
                        workSheet.Dimension.Start.Column,
                        workSheet.Dimension.End.Row,
                        workSheet.Dimension.End.Column]
                        .Clear();
                }
            }

            CreateHead(
                workSheet,
                fields,
                fieldsNames);

            return workSheet;
        }

        private void CreateHead(
            ExcelWorksheet worksheet,
            List<FieldInfo> fields,
            List<string> fieldsNames = null,
            int startPosition = 1)
        {
            var currentPosition = startPosition;

            foreach (var field in fields)
            {
                worksheet.Cells[1, currentPosition++].Value = field.Name;
            }

            if(!(fieldsNames is null) && fieldsNames.Count > 0)
            {
                var currentNamePosition = startPosition;

                foreach (var name in fieldsNames)
                {
                    worksheet.Cells[1, currentNamePosition++].Value = name;
                }
            }

            for (int i = startPosition; i <= currentPosition; i++)
            {
                using (var rng = worksheet.Cells[1, i])
                {
                    rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    rng.Style.Font.Bold = true;

                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                    rng.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                }
            }
        }

        private void AutoFit(ExcelWorksheet worksheet)
        {
            worksheet.Cells[
                worksheet.Dimension.Start.Row,
                worksheet.Dimension.Start.Column,
                worksheet.Dimension.End.Row,
                worksheet.Dimension.End.Column]
                .AutoFitColumns();
        }
    }
}
