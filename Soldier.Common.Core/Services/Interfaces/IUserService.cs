﻿using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Services.Interfaces
{
    public interface IUserService
    {
        Task<Identity> GetUserIdentityByCredentials(string passportNumber, string password);

        Task<Identity> GetUserIdentityByPassport(string passportNumber);

        void Update(Identity identity);

        Identity Create(Identity identity);

        Task<UserTypes> GetUserType(string passportNumber);

        Task<bool> ChangePasswordAsync(string oldPass, string newPass);
    }
}
