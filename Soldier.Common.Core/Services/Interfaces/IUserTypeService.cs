﻿using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Services.Interfaces
{
    public interface IUserTypeService
    {
        public void CreateRelation(UserTypes userType, string passportNumber);
    }
}
