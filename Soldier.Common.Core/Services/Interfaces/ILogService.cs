﻿using System;
using System.Collections.Generic;

namespace Soldier.Common.Core.Services.Interfaces
{
    public interface ILogService
    {
        Guid CorrelationId { get; }

        void LogRequest(string displayName, IDictionary<string, object> actionArguments);

        void LogInfo(string message);

        void LogError(Exception e);

        void LogError(string message, Exception e);
    }
}
