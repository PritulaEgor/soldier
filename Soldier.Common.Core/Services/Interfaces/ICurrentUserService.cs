﻿using Microsoft.AspNetCore.Http;
using Soldier.Common.Core.DAL;

namespace Soldier.Common.Core.Services.Interfaces
{
    public interface ICurrentUserService<out TUser> where TUser : BaseAppUser
    {
        TUser GetCurrentUser(HttpRequest request);

        TUser GetActualUser(string passportNumber);

        string GetUserIPAddress();
    }
}
