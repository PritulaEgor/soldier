﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Services.Interfaces
{
    public interface IReportService<Tmodel>
    {
        byte[] GetReport(
            List<Tmodel> model,
            string fileName,
            List<string> fieldsNames);
    }
}
