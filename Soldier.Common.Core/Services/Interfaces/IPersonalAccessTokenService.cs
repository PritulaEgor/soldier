﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Services.Interfaces
{
    public interface IPersonalAccessTokenService
    {
        Task<ClaimsPrincipal> ValidateToken(string userEmail, string personalAccessToken);

        string CreateToken();

        Task RevokeToken(Guid userId);
    }
}
