﻿using System;

namespace Soldier.Common.Core.UnitOfWork.Contracts
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(bool needToTrack = true);

        void Execute(Action action, bool needToTrack = true);
    }
}
