﻿using System;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.UnitOfWork.Contracts;

namespace Soldier.Common.Core.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        protected readonly IDbContextProvider _contextProvider;
        //protected readonly IAuditLogService _auditLogService;

        public UnitOfWorkFactory(IDbContextProvider provider/*, IAuditLogService auditLogService*/)
        {
            _contextProvider = provider;
            //_auditLogService = auditLogService;
        }

        /// <summary>
        /// Creates a new instance of <c>IUnitOfWork</c>.
        /// </summary>
        /// <param name="needToTrack">if set to <c>true</c> we track changes by audit log service.</param>
        /// <returns>
        /// Returns a new instance of <c>IUnitOfWork</c>.
        /// </returns>
        public virtual IUnitOfWork Create(bool needToTrack = true)
        {
            //if (needToTrack)
            //{
            //    return new UnitOfWork(_contextProvider.GetContext(), _auditLogService);
            //}
            return new UnitOfWork(_contextProvider.GetContext());
        }

        /// <summary>
        /// Execute passed action, wrapped in unit of work
        /// </summary>
        /// <param name="action">Action which should be executed.</param>
        /// <param name="needToTrack">if set to <c>true</c> we track changes by audit log service.</param>
        public virtual void Execute(Action action, bool needToTrack = true)
        {
            using (Create(needToTrack))
            {
                action();
            }
        }
    }
}
