﻿using System;
using System.Linq;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.Common.Core.Services.Interfaces;
using Soldier.Common.Core.UnitOfWork.Contracts;

namespace Soldier.Common.Core.UnitOfWork
{
    /// <summary>
    /// Unit of work, wrap context. Create scope, for DB operations
    /// </summary>
    /// <seealso cref="IUnitOfWork" />
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly IDbContext context;
        //protected readonly IAuditLogService auditLogService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork" /> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        public UnitOfWork(IDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork" /> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="auditLogService">The audit log service.</param>
        //public UnitOfWork(IDbContext context, IAuditLogService auditLogService) : this(context)
        //{
        //    this.auditLogService = auditLogService;
        //}

        /// <summary>
        /// Commits all made changes.
        /// </summary>
        public void Commit()
        {
            context.SaveChanges();
        }

        /// <summary>
        /// The Dispose method.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Handles cleaning up the object instance.
        /// </summary>
        /// <param name="disposing">Indicates whether or not the class is disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            Commit();
        }
    }
}
