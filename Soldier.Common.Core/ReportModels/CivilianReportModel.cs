﻿using Soldier.Common.Core.DAL.Soldier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.ReportModels
{
    public class CivilianReportModel
    {
        public string PassportNumber { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public string CurrentAddress { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public UserRoles Role { get; set; }

        public string PlaceOfResidence { get; set; }

        public string PassportIdentificationNumber { get; set; }

        public Guid MilitaryID { get; set; }
    }
}
