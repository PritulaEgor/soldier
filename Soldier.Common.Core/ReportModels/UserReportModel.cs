﻿using Soldier.Common.Core.DAL.Soldier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.ReportModels
{
    public class UserReportModel
    {
        #region BaseAppUser Fields

        public string PassportNumber { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public string CurrentAddress { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public UserRoles Role { get; set; }

        public string PlaceOfResidence { get; set; }

        #endregion

        #region Civilian fields
        public string PassportIdentificationNumber { get; set; }

        public Guid MilitaryID { get; set; }
        #endregion

        #region Employee
        public string Rank { get; set; }

        public string ServiceStartDate { get; set; }

        public string WorkStartDate { get; set; }
        #endregion
    }
}
