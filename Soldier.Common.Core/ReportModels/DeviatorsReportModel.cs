﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.ReportModels
{
    public class DeviatorsReportModel
    {
        public string PassportNumber { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public string CurrentAddress { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string PlaceOfResidence { get; set; }

        public DateTime Sended { get; set; }

        public int DaysActive { get; set; }

        public DateTime SoldierDateOfArriving { get; set; }

        public string SignaturerName { get; set; }

        public DateTime EnlistmentStartDate { get; set; }

        public DateTime EnlistmentEndDate { get; set; }
    }
}
