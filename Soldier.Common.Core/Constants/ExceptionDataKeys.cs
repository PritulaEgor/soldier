﻿namespace Soldier.Common.Core.Constants
{
    public static class ExceptionDataKeys
    {
        public static readonly string ErrorCode = "ErrorCode";

        public static readonly string ErrorData = "ErrorData";

        public static readonly string StatusCode = "StatusCode";
    }
}
