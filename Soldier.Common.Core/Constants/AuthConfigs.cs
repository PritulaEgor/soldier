﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Soldier.Common.Core.Constants
{
    public class AuthConfigs
    {
        public const string ISSUER = "https://localhost:5001";
        public const string AUDIENCE = "https://localhost:5001";
        const string KEY = "aj45klfdx_fe-d;fs;aaar345fv";
        public const int LIFETIME = 10;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
