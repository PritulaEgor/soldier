﻿using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Core.Services;
using Soldier.Common.Core.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.UnitOfWork.Contracts;
using Soldier.Common.Core.UnitOfWork;

namespace Soldier.Common.Core.Modules
{
    public class CoreServicesModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddTransient<IMailSenderService, SmtpSenderService>();
            services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddTransient<ILogService, LogService>();
        }
    }
}
