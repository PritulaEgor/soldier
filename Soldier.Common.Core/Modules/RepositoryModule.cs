﻿using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Core.Repositories;
using Soldier.Common.Core.Repositories.Soldier;
using Microsoft.Extensions.DependencyInjection;

namespace Soldier.Common.Core.Modules
{
    public class RepositoryModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IGenericViewRepository<>), typeof(GenericViewRepository<>));
        }
    }
}
