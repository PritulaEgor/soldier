﻿using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.IoC.Soldier;
using Soldier.Common.Core.Services;
using Soldier.Common.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.Common.Core.Modules
{
    public class ReportModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddScoped(typeof(IReportService<>), typeof(ReportService<>));
        }
    }
}
