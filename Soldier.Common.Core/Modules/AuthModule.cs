﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Soldier.Common.Core.IoC.Soldier;
using Microsoft.IdentityModel.Tokens;
using Soldier.Common.Core.Constants;
using Microsoft.AspNetCore.Authorization;
using System;

namespace Soldier.Common.Core.Modules
{
    public class AuthModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = AuthConfigs.ISSUER,
                            ValidateAudience = true,
                            ValidAudience = AuthConfigs.AUDIENCE,
                            ValidateLifetime = true,
                            IssuerSigningKey = AuthConfigs.GetSymmetricSecurityKey(),
                            ValidateIssuerSigningKey = true,
                            LifetimeValidator = CustomLifetimeValidator
                        };
                    });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                .RequireAuthenticatedUser()
                .Build();
            });
        }

        public static bool CustomLifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if(expires!= null)
            {
                return DateTime.UtcNow < expires;
            }

            return false;
        }
    }
}
