﻿using Soldier.Common.Core.IoC.Soldier;
using Microsoft.Extensions.Configuration;

namespace Soldier.Common.Core.Modules
{
    public static class ServiceCollectionBuilderExtensions
    {
        public static IServiceCollectionBuilder WithCoreModules(
            this IServiceCollectionBuilder serviceCollectionBuilder,
            IConfiguration configuration)
        {
            serviceCollectionBuilder
                .RegisterModule(new CoreServicesModule())
                .RegisterModule(new RepositoryModule());
            return serviceCollectionBuilder;
        }
    }
}
