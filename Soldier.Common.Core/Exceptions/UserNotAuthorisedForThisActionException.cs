﻿using System;
using System.Net;

namespace Soldier.Common.Core.Exceptions
{
    [Serializable]
    public class UserNotAuthorizedForThisActionException : SoldierApplicationException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        public override int ErrorCode => ErrorCodes.UserNotAuthorized;

        public override string ErrorData => string.Empty;
    }
}
