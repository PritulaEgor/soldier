﻿using System;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Soldier.Common.Core.Constants;

namespace Soldier.Common.Core.Exceptions
{
    [Serializable]
    public abstract class SoldierApplicationException : Exception
    {
        public abstract HttpStatusCode StatusCode { get; }

        public abstract int ErrorCode { get; }

        public abstract string ErrorData { get; }

        protected SoldierApplicationException()
        {
        }

        protected SoldierApplicationException(string message) : base(message)
        {
        }

        protected SoldierApplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SoldierApplicationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(ExceptionDataKeys.ErrorCode, ErrorCode);
            info.AddValue(ExceptionDataKeys.StatusCode, StatusCode);
            info.AddValue(ExceptionDataKeys.ErrorData, ErrorData ?? string.Empty);
            base.GetObjectData(info, context);
        }
    }
}
