﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.DAL.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.DAL.DAL.Configurations
{
    public class UserTypeConfig : IEntityTypeConfiguration<UserType>
    {
        public void Configure(EntityTypeBuilder<UserType> builder)
        {
            builder.HasData(new UserType
            {
                Id = Guid.NewGuid(),
                PassportNumber = "admin",
                Type = UserTypes.Employee
            });

            builder.HasData(new UserType
            {
                Id = Guid.NewGuid(),
                PassportNumber = "med",
                Type = UserTypes.Employee
            });

            builder.HasData(new UserType
            {
                Id = Guid.NewGuid(),
                PassportNumber = "staff",
                Type = UserTypes.Employee
            });
        }
    }
}
