﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;
using System;

namespace Soldier.DAL.DAL.Configurations
{
    public class IdentityConfig : IEntityTypeConfiguration<Identity>
    {
        private readonly Guid _identityId;
        private readonly Guid _medicalStaffIdentityId;
        private readonly Guid _staffIdentityId;

        public IdentityConfig(Guid identityId,
            Guid medicalStaffIdentityId,
            Guid staffIdentityId)
        {
            _identityId = identityId;
            _medicalStaffIdentityId = medicalStaffIdentityId;
            _staffIdentityId = staffIdentityId;
        }

        public void Configure(EntityTypeBuilder<Identity> builder)
        {
            builder.HasData(new Identity { Id = _identityId,  PassportNumber = "admin", PasswordSalt = "123123", Role = UserRoles.ITAdmin  });
            builder.HasData(new Identity { Id = _medicalStaffIdentityId, PassportNumber = "med", PasswordSalt = "123123", Role = UserRoles.MedicalStaff });
            builder.HasData(new Identity { Id = _staffIdentityId, PassportNumber = "staff", PasswordSalt = "123123", Role = UserRoles.AccountantStaff });
        }
    }
}
