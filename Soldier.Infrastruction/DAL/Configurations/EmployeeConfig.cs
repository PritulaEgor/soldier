﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.DAL.DAL.Models;
using System;

namespace Soldier.DAL.DAL.Configurations
{
    public class EmployeeConfig : IEntityTypeConfiguration<Employee>
    {
        private readonly Guid _identityId;
        private readonly Guid _medicalStaffIdentityId;
        private readonly Guid _staffIdentityId;

        public EmployeeConfig(Guid identityId,
            Guid medicalStaffIdentityId,
            Guid staffIdentityId)
        {
            _identityId = identityId;
            _medicalStaffIdentityId = medicalStaffIdentityId;
            _staffIdentityId = staffIdentityId;
        }

        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasData(new Employee
            {
                Id = Guid.NewGuid(),
                PassportNumber = "admin",
                Role = UserRoles.ITAdmin,
                CreatorId = Guid.NewGuid(),
                CurrentAddress = "Adress",
                Email = "admin@gmail.com",
                IdentityId = _identityId,
                Name = "admin",
                Patronymic = "patronymic",
                PhoneNumber = "Number",
                PlaceOfResidence = "Place",
                Rank = "rank",
                ServiceStartDate = DateTime.Now,
                Surname = "surn",
                WorkStartDate = DateTime.Now
            });

            builder.HasData(new Employee
            {
                Id = Guid.NewGuid(),
                PassportNumber = "med",
                Role = UserRoles.MedicalStaff,
                CreatorId = Guid.NewGuid(),
                CurrentAddress = "Adress",
                Email = "med@gmail.com",
                IdentityId = _medicalStaffIdentityId,
                Name = "med",
                Patronymic = "patronymic",
                PhoneNumber = "Number",
                PlaceOfResidence = "Place",
                Rank = "rank",
                ServiceStartDate = DateTime.Now,
                Surname = "surn",
                WorkStartDate = DateTime.Now
            });

            builder.HasData(new Employee
            {
                Id = Guid.NewGuid(),
                PassportNumber = "staff",
                Role = UserRoles.CommissariatStaff,
                CreatorId = Guid.NewGuid(),
                CurrentAddress = "Adress",
                Email = "staff@gmail.com",
                IdentityId = _staffIdentityId,
                Name = "staff",
                Patronymic = "patronymic",
                PhoneNumber = "Number",
                PlaceOfResidence = "Place",
                Rank = "rank",
                ServiceStartDate = DateTime.Now,
                Surname = "surn",
                WorkStartDate = DateTime.Now
            });
        }
    }
}
