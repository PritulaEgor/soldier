﻿using Soldier.Common.Core.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.DAL.DAL.Models
{
    public class Issue : Entity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime DateOfDiagnosis { get; set; }

        [Required]
        public string DoctorData { get; set; }

        [Required]
        public Guid MedicalCardID { get; set; }

        [ForeignKey("MedicalCardID")]
        public virtual MedicalCard MedicalCard {get;set;}
    }
}
