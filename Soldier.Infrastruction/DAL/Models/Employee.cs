﻿using Soldier.Common.Core.DAL;
using System;
using System.ComponentModel.DataAnnotations;

namespace Soldier.DAL.DAL.Models
{
    public class Employee : BaseAppUser
    {
        public string Rank { get; set; }

        public DateTime ServiceStartDate { get; set; }

        [Required]
        public DateTime WorkStartDate { get; set; }

        //[Required]
        //public Guid ReportsToID { get; set; }
    }
}