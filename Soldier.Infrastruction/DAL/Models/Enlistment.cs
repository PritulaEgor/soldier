﻿using Soldier.Common.Core.DAL;
using System;
using System.ComponentModel.DataAnnotations;

namespace Soldier.DAL.DAL.Models
{
    public class Enlistment : Entity
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}