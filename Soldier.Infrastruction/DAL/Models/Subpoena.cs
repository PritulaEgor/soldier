﻿using Soldier.Common.Core.DAL;
using Soldier.DAL.DAL.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.DAL.DAL.Models
{
    //Повестка
    public class Subpoena : Entity
    {
        [Required]
        public Guid RecipientID { get; set; }

        [ForeignKey("RecipientID")]
        public virtual Civilian Recipient { get; set; }

        [Required]
        public SubpoenaStates Status { get; set; }

        [Required]
        public DateTime Sended { get; set; }

        [Required]
        public int DaysActive { get; set; }

        //[Required]
        public DateTime SoldierDateOfArriving { get; set; }

        [Required]
        public Guid SignaturerID { get; set; }

        [ForeignKey("SignaturerID")]
        public virtual Employee Signaturer { get; set; }

        [Required]
        public Guid EnlistmentId { get; set; }

        [ForeignKey("EnlistmentId")]
        public virtual Enlistment Enlistment { get; set; }
    }
}
