﻿using Soldier.Common.Core.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.DAL.DAL.Models
{
    public class PersonalAccessToken : Entity
    {
        [Required]
        [ForeignKey("UserId")]
        public virtual BaseAppUser User { get; set; }

        public Guid UserId { get; set; }
        
        [Required]
        public string Token { get; set; }
    }
}
