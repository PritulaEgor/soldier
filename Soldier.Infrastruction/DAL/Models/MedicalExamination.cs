﻿using Soldier.Common.Core.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.DAL.DAL.Models
{
    public class MedicalExamination : Entity
    {
        public Guid MedicalCardID { get; set; }

        [Required]
        [ForeignKey("MedicalCardID")]
        public virtual MedicalCard MedicalCard { get; set; }

        [Required]
        public string ExaminationResult { get; set; }

        [Required]
        public DateTime ExaminationDate { get; set; }
    }
}
