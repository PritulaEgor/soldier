﻿using Soldier.Common.Core.DAL;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.DAL.DAL.Models
{
    public class Postponement : Entity
    {
        [Required]
        public bool IsActive { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Purpose { get; set; }

        public string Descripion { get; set; }

        public Guid SoldierID { get; set; }

        [ForeignKey("SoldierID")]
        public Civilian Soldier { get; set; }
    }
}
