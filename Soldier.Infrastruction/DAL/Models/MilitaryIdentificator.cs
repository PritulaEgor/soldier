﻿using Soldier.Common.Core.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.DAL.DAL.Models
{
    public class MilitaryIdentificator : Entity
    {
        public bool IsMilitaryServicePassed { get; set; }

        public string MilitaryID { get; set; }

        public string MilitarySpeciality { get; set; }

        public DateTime ServiceStartDate { get; set; }

        public DateTime ServiceEndDate { get; set; }
    }
}
