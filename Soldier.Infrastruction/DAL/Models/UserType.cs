﻿using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;

namespace Soldier.DAL.DAL.Models
{
    public class UserType : Entity
    {
        public string PassportNumber { get; set; }

        public UserTypes Type { get; set; }
    }
}
