﻿using Soldier.Common.Core.DAL;
using Soldier.DAL.DAL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.DAL.DAL.Models
{
    public class MedicalCard : Entity
    {
        [Required]
        public ValidityGroups ValidityGroup { get; set; }

        public virtual List<Issue> Issues { get; set; }

        public virtual List<MedicalExamination> MedicalExaminations { get; set; }
    }
}
