﻿using Soldier.Common.Core.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soldier.DAL.DAL.Models
{
    public class Civilian : BaseAppUser
    {
        [Required]
        public string PassportIdentificationNumber { get; set; }

        public Guid MilitaryID { get; set; }

        [ForeignKey("MilitaryID")]
        public virtual MilitaryIdentificator MilitaryIdentificator { get; set; }

        public virtual List<Postponement> Postponements { get; set; }

        [Required]
        public Guid MedicalCardId { get; set; }

        [ForeignKey("MedicalCardId")]
        public virtual MedicalCard MedicalCard { get; set; }
    }
}
