﻿using Soldier.Common.Core.DAL.Soldier;

namespace Soldier.DAL.DAL
{
    public class SoldierContextProvider : IDbContextProvider
    {
        protected readonly SoldierContext _dbContext;

        public SoldierContextProvider(SoldierContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IDbContext GetContext()
        {
            return _dbContext;
        }
    }
}
