﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.DAL.DAL.Enums
{
    public enum ValidityGroups
    {
        // Fill with data
        A = 0,
        B = 1,
        V = 2,
        G = 3,
        D = 4
    }
}
