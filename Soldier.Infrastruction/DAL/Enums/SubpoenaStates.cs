﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soldier.DAL.DAL.Enums
{
    public enum SubpoenaStates
    {
        Sended = 0, 
        Overdue = 1,
        Received = 2,
        Cancelled = 3,
        Realized = 4
    }
}
