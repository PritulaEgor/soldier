﻿using Microsoft.EntityFrameworkCore;
using Soldier.Common.Core.DAL;
using Soldier.Common.Core.DAL.Soldier;
using Soldier.DAL.DAL.Models;
using Soldier.DAL.DAL.Configurations;
using System;

namespace Soldier.DAL.DAL
{
    public class SoldierContext : DbContext, IDbContext
    {
        public SoldierContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        // configs applying section
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(typeof(Subpoena))
                .HasOne(typeof(Employee), "Signaturer")
                .WithMany()
                .HasForeignKey("SignaturerID")
                .OnDelete(DeleteBehavior.Restrict); // no ON DELETE

            var applyingGuid = Guid.NewGuid();
            var medicalStaffGuid = Guid.NewGuid();
            var comissariatStaffGuid = Guid.NewGuid();

            builder.ApplyConfiguration(new IdentityConfig(
                applyingGuid,
                medicalStaffGuid,
                comissariatStaffGuid)
                ).ApplyConfiguration(new EmployeeConfig(
                    applyingGuid,
                    medicalStaffGuid,
                    comissariatStaffGuid)
                ).ApplyConfiguration(new UserTypeConfig());

        }

        public DbSet<Civilian> Civilians { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Enlistment> Enlistments { get; set; }

        public DbSet<Issue> Issues { get; set; }

        public DbSet<MedicalCard> MedicalCards { get; set; }

        public DbSet<Postponement> Postponements { get; set; }

        public DbSet<Subpoena> Subpoenas { get; set; }

        public DbSet<Identity> Identities { get; set; }

        public DbSet<UserType> UserTypes { get; set; }
    }
}
